﻿#if UNITY_EDITOR
using System.Collections.Generic;

namespace AntonTools {
    public class ClassRequirement : GenerationRequirement {
        public readonly string name;
        public ClassRequirement(string name) {
            this.name = name;
            visibility = new Visibility<ClassRequirement>(this);
            virtualization = new Virtualization<ClassRequirement>(this);
            partialization = new Partialization<ClassRequirement>(this);
            staticity = new Staticity<ClassRequirement>(this);
        }
        public readonly Visibility<ClassRequirement> visibility;
        public readonly Virtualization<ClassRequirement> virtualization;
        public readonly Partialization<ClassRequirement> partialization;
        public readonly Staticity<ClassRequirement> staticity;

        public MethodRequirement RequireMethod(string returnType, string name, params (string paramType, string paramName)[] parameters)
            => Add(new MethodRequirement(returnType, name, parameters));
        public PropertyRequirement RequireProperty(string returnType, string name)
            => Add(new PropertyRequirement(returnType, name));
        public FieldRequirement RequireField(string returnType, string name)
            => Add(new FieldRequirement(returnType, name));
        public CustomCodeRequirement RequireCustomCode(string code)
            => Add(new CustomCodeRequirement(code));
        public CustomCodeRequirement RequireLine()
            => RequireCustomCode("");
        public ClassRequirement RequireUsing(string @namespace) {
            parentFile.RequireUsing(@namespace);
            return this;
        }

        List<string> inheritedFrom = new List<string>();
        public ClassRequirement RequireInheritance(string inheritedFrom) {
            if (!this.inheritedFrom.Contains(inheritedFrom))
                this.inheritedFrom.Add(inheritedFrom);
            return this;
        }

        string definitionLine => $"{visibility}{staticity}{virtualization}{partialization}class {name}" +
            $"{(inheritedFrom.Count > 0 ? " : " : "")}{inheritedFrom.PrintCollection(", ", "")}";
        protected override bool completeSelf => parentFile.existingText.Contains(definitionLine);
        public override void Generate(int indent) {
            sb.AppendLine($"{GetIndent(indent)}{definitionLine} {{");
            base.Generate(indent + 1);
            sb.AppendLine($"{GetIndent(indent)}}}");
        }
    }
}
#endif