﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;

namespace AntonTools {
    public class EnumRequirement : GenerationRequirement {
        string name;
        public EnumRequirement(string name) => this.name = name;

        HashSet<string> values = new HashSet<string>();
        public EnumRequirement RequireValue(string value) {
            values.Add(value);
            return this;
        }
        protected override bool completeSelf {
            get {
                var enumType = ReflectionUtils.GetTypeByName(name);
                if (enumType == null)
                    return false;
                var enumValues = Enum.GetValues(enumType).ConvertAll(v => v.ToString());
                if (values.Count != enumValues.Count)
                    return false;
                foreach (var valueString in values) {
                    if (!enumValues.Contains(valueString))
                        return false;
                }
                return true;
            }
        }
        public override void Generate(int indent) {
            sb.AppendLine($"{GetIndent(indent)}public enum {name} {{");
            values.ForEach(v => sb.AppendLine($"{GetIndent(indent + 1)}{v},"));
            sb.AppendLine($"{GetIndent(indent)}}}");
            base.Generate(indent);
        }
    }
}
#endif