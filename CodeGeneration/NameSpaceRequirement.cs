﻿#if UNITY_EDITOR
namespace AntonTools {
    public class NameSpaceRequirement : GenerationRequirement {
        public string name { get; private set; }
        public NameSpaceRequirement(string name) => this.name = name;
        public ClassRequirement RequireClass(string name) => Add(new ClassRequirement(name));
        public EnumRequirement RequireEnum(string name) => Add(new EnumRequirement(name));
        public CustomCodeRequirement RequireCustomCode(string code) => Add(new CustomCodeRequirement(code));

        protected override bool completeSelf => true;
        public override void Generate(int indent) {
            if (name.IsNothing())
                base.Generate(indent);
            else {
                sb.AppendLine($"{GetIndent(indent)}namespace {name} {{");
                base.Generate(indent + 1);
                sb.AppendLine($"{GetIndent(indent)}}}");
            }
        }
    }
}
#endif