﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;

namespace AntonTools {
    public class CustomCodeRequirement : GenerationRequirement {
        List<string> codeLines;
        public CustomCodeRequirement(string code = null) {
            codeLines = new List<string>();
            AddLines(code);
        }
        protected override bool completeSelf => codeLines.All(codeLine => parentFile.existingText.Contains(codeLine));
        public override void Generate(int indent) {
            foreach (var codeLine in codeLines)
                sb.AppendLine($"{GetIndent(indent)}{codeLine}");
            base.Generate(indent);
        }
        public void AddLines(string code) {
            if (code == null) return;
            var codeLines = code.ToLf().Split('\n');
            foreach (var line in codeLines)
                this.codeLines.Add(line);
        }
        public CustomCodeRequirement RequireUsing(string @namespace) {
            parentFile.RequireUsing(@namespace);
            return this;
        }
    }
}
#endif