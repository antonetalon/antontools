﻿#if UNITY_EDITOR

namespace AntonTools
{
    public class Hiding<TOwner> where TOwner : GenerationRequirement {
        TOwner owner;
        public Hiding(TOwner owner) => this.owner = owner;
        public bool isNew { get; private set; }
        public TOwner RequireNew() {
            isNew = true;
            return owner;
        }
        public override string ToString() => isNew ? "new " : "";
    }
}
#endif