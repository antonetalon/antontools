﻿#if UNITY_EDITOR
namespace AntonTools {
    public class FieldRequirement : GenerationRequirement {
        string returnType, name;
        public FieldRequirement(string returnType, string name) {
            this.returnType = returnType;
            this.name = name;
            visibility = new Visibility<FieldRequirement>(this);
            virtualization = new Virtualization<FieldRequirement>(this);
            staticity = new Staticity<FieldRequirement>(this);
            readonlyness = new ReadOnlyness<FieldRequirement>(this);
            hiding = new Hiding<FieldRequirement>(this);
            attributes = new Attributes<FieldRequirement>(this);
        }

        public readonly Visibility<FieldRequirement> visibility;
        public readonly Virtualization<FieldRequirement> virtualization;
        public readonly Staticity<FieldRequirement> staticity;
        public readonly ReadOnlyness<FieldRequirement> readonlyness;
        public readonly Hiding<FieldRequirement> hiding;
        public readonly Attributes<FieldRequirement> attributes;

        string initExpression;
        bool initExpressionOnSameLine;
        bool hasInitExpression => !initExpression.IsNothing();
        string initExpressionSameLine => (hasInitExpression && initExpressionOnSameLine) ? $" = {initExpression}" : "";
        public FieldRequirement RequireInlineInit(string initExpression, bool sameLine = true) {
            this.initExpression = initExpression;
            initExpressionOnSameLine = sameLine;
            return this;
        }
        public FieldRequirement RequireInlineInit(bool sameLine = true)
            => RequireInlineInit($"new {returnType}()", sameLine);

        string definitionLine => $"{attributes}{visibility}{staticity}{readonlyness}{hiding}{virtualization}{returnType} {name}{initExpressionSameLine};";
        protected override bool completeSelf => parentFile.existingText.Contains(definitionLine);
        public override void Generate(int indent) {
            sb.AppendLine($"{GetIndent(indent)}{definitionLine}");
            if (hasInitExpression && !initExpressionOnSameLine)
                sb.AppendLine($"{GetIndent(indent + 1)}= {initExpression};");
            base.Generate(indent);
        }
    }
}
#endif