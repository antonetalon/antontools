﻿#if UNITY_EDITOR
using System;

namespace AntonTools {
    public class MethodRequirement : GenerationRequirement {
        string returnType, name;
        (string paramType, string paramName)[] parameters;
        public MethodRequirement(string returnType, string name, params (string paramType, string paramName)[] parameters) {
            this.returnType = returnType;
            this.name = name;
            this.parameters = parameters;
            visibility = new Visibility<MethodRequirement>(this);
            virtualization = new Virtualization<MethodRequirement>(this);
            staticity = new Staticity<MethodRequirement>(this);
            hiding = new Hiding<MethodRequirement>(this);
        }

        public readonly Visibility<MethodRequirement> visibility;
        public readonly Virtualization<MethodRequirement> virtualization;
        public readonly Staticity<MethodRequirement> staticity;
        public readonly Hiding<MethodRequirement> hiding;

        string expressionBody;
        bool bodyIsExpression => !expressionBody.IsNothing();
        public MethodRequirement RequireExpressionBody(string expressionBody) {
            this.expressionBody = expressionBody;
            return this;
        }
        public MethodRequirement RequireBody(string body) {
            var bodyRequirement = RequireBody();
            bodyRequirement.AddLines(body);
            return this;
        }
        public CustomCodeRequirement RequireBody() 
            => Add(new CustomCodeRequirement());

        bool isAbstract => virtualization.virtualization == VirtualizationType.Abstract;
        string definitionLine => $"{visibility}{staticity}{hiding}{virtualization}{returnType} {name}({parameters.ConvertAll(p => $"{p.paramType} {p.paramName}").PrintCollection(", ", "")}){(isAbstract ? ";" : "")}";
        protected override bool completeSelf => parentFile.existingText.Contains(definitionLine);
        public override void Generate(int indent) {
            var openBracesSameLine = (!isAbstract && !bodyIsExpression) ? " {" : "";
            sb.AppendLine($"{GetIndent(indent)}{definitionLine}{openBracesSameLine}");
            if (!isAbstract) {
                // Create method body.
                if (bodyIsExpression)
                    sb.AppendLine($"{GetIndent(indent + 1)}=> {expressionBody};");
                else {
                    //sb.AppendLine($"{GetIndent(indent)}{{");
                    base.Generate(indent + 1);
                    sb.AppendLine($"{GetIndent(indent)}}}");
                }
            }
        }
    }
}
#endif