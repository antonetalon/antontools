﻿#if UNITY_EDITOR
using System.Collections.Generic;

namespace AntonTools
{
    public class Attributes<TOwner> where TOwner : GenerationRequirement
    {
        TOwner owner;
        public Attributes(TOwner owner) => this.owner = owner;
        public List<string> properties { get; private set; } = new List<string>();
        public TOwner RequireAttribute(string property) {
            properties.AddIfNotExists(property);
            return owner;
        }
        public override string ToString() => properties.Count == 0 ? "" : $"[{properties.PrintCollection()}] ";
    }
}
#endif