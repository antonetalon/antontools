﻿#if UNITY_EDITOR
namespace AntonTools {
    public class ReadOnlyness<TOwner> where TOwner : GenerationRequirement {
        TOwner owner;
        public ReadOnlyness(TOwner owner) => this.owner = owner;
        public bool isReadonly { get; private set; }
        public TOwner RequireReadonly() {
            isReadonly = true;
            return owner;
        }
        public override string ToString() => isReadonly ? "readonly " : "";
    }
}
#endif