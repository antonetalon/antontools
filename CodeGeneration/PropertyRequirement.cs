﻿#if UNITY_EDITOR
using UnityEngine;

namespace AntonTools {
    public class PropertyRequirement : GenerationRequirement {
        string returnType, name;
        public PropertyRequirement(string returnType, string name) {
            this.returnType = returnType;
            this.name = name;
            visibility = new Visibility<PropertyRequirement>(this);
            virtualization = new Virtualization<PropertyRequirement>(this);
            staticity = new Staticity<PropertyRequirement>(this);
            hiding = new Hiding<PropertyRequirement>(this);
            attributes = new Attributes<PropertyRequirement>(this);
        }

        public readonly Visibility<PropertyRequirement> visibility;
        public readonly Virtualization<PropertyRequirement> virtualization;
        public readonly Staticity<PropertyRequirement> staticity;
        public readonly Hiding<PropertyRequirement> hiding;
        public readonly Attributes<PropertyRequirement> attributes;

        string expressionBody;
        bool expressionBodySameLine;
        bool hasExpressionBody => !expressionBody.IsNothing();
        public PropertyRequirement RequireExpressionBody(string expressionBody, bool sameLine = true) {
            this.expressionBody = expressionBody;
            expressionBodySameLine = sameLine;
            Debug.Assert(!isAbstract);
            return this;
        }
        bool requiresGet; // { get; }
        public PropertyRequirement RequireGet() {
            Debug.Assert(isAbstract);
            requiresGet = true;
            return this;
        }
        bool isAbstract => virtualization.virtualization == VirtualizationType.Abstract;
        string definitionLine => $"{attributes}{visibility}{staticity}{hiding}{virtualization}{returnType} {name}" +
            $"{(isAbstract ? (requiresGet ? " { get; }" : ";") : ((hasExpressionBody && expressionBodySameLine) ? $" => {expressionBody};" : ""))}";
        protected override bool completeSelf => parentFile.existingText.Contains(definitionLine)
            && (!hasExpressionBody || parentFile.existingText.Contains(expressionBody));
        public override void Generate(int indent) {
            sb.AppendLine($"{GetIndent(indent)}{definitionLine}");
            if (!isAbstract) {
                // Create property body.
                if (hasExpressionBody) {
                    if (!expressionBodySameLine)
                        sb.AppendLine($"{GetIndent(indent + 1)}=> {expressionBody};");
                } else {
                    sb.AppendLine($"{GetIndent(indent)}{{");
                    sb.AppendLine($"{GetIndent(indent)}}}");
                }
            }
            base.Generate(indent);
        }
    }
}
#endif