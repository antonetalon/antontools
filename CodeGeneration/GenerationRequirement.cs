﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AntonTools {
    public abstract class GenerationRequirement {
        public GenerationRequirement parent { get; private set; }
        protected List<GenerationRequirement> children = new List<GenerationRequirement>();
        public bool complete => completeSelf && children.All(c => c.complete);
        protected abstract bool completeSelf { get; }
        public T Add<T>(T item) where T : GenerationRequirement {
            item.parent = this;
            children.Add(item);
            return item;
        }
        public FileRequirement parentFile => (this is FileRequirement f) ? f : parent?.parentFile;
        public string currNamespace => (this is NameSpaceRequirement n) ? n.name : parent?.currNamespace;
        public StringBuilder sb => parentFile.fileStringBuilder;
        public const string indent1 = "    ";
        public static string indent2 => GetIndent(2);
        public static string indent3 => GetIndent(3);
        static List<string> indents = new List<string>(); 
        public static string GetIndent(int count) {
            while (indents.Count < count)
                indents.Add(indents.LastOrDefault() + indent1);
            return indents.GetElementSafe(count - 1);
        } 
        public virtual void Generate(int indent) => children.ForEach(c => c.Generate(indent));
    }
}
#endif