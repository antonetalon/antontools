﻿#if UNITY_EDITOR
using UnityEditor;

namespace AntonTools {
    public class CodeGenerator : GenerationRequirement {
        public FolderRequirement RequireFolder(string name) => Add(new FolderRequirement(name));
        protected override bool completeSelf => true;
        public void Generate() => Generate(0);
        public override void Generate(int indent) {
            base.Generate(indent);
            AssetDatabase.Refresh();
        }
    }
}
#endif