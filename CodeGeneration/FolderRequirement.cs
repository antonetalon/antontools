﻿#if UNITY_EDITOR
using System.IO;

namespace AntonTools {
    public class FolderRequirement : GenerationRequirement {
        public string name { get; private set; }
        public FolderRequirement(string name) => this.name = name;
        public FileRequirement RequireFile(string name) => Add(new FileRequirement(name));

        public string folder {
            get {
                var name = this.name.Replace("\\", "/");
                if (name.StartsWith($"{AntonToolsManager.AssetsFolder}/"))
                    return name; // Full path.
                string parentFolder;
                if (parent is FolderRequirement f)
                    parentFolder = f.folder;
                else
                    parentFolder = AntonToolsManager.AssetsFolder;
                return $"{parentFolder}/{name}";
            }
        }
        protected override bool completeSelf => Directory.Exists(folder);
        public override void Generate(int indent) {
            Directory.CreateDirectory(folder);
            base.Generate(indent);
        }
    }
}
#endif