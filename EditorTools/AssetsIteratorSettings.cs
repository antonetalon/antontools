﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public class AssetsIteratorSettings : SettingsScriptable<AssetsIteratorSettings> {
        protected override bool inRepository => false;
        public float saveProgressEach = 0.01f;
        public List<ProgressItem> progressItems = new List<ProgressItem>();
        [Serializable] public class ProgressItem {
            public float progress;
            public GameObject marker;
        }
        public List<AssetIterationItem> searchItemResults = new List<AssetIterationItem>();
        public float searchDuration = -1;
    }
    [Serializable] public struct AssetIterationItem {
        public string path, fullName;
        public GameObject go;
    }
}
