﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.Compilation;
using UnityEngine;

namespace AntonTools {
    public class CompilationCallback : SettingsScriptable<CompilationCallback> {
        protected override bool inResources => false;
        protected override bool inRepository => false;

        [HideInInspector] List<string> callbacks;
        public static void CallStaticMethodAfterRecompilation(Action doAfterRecompile) {
            if (instance == null || instance.callbacks == null)
                instance.callbacks = new List<string>();
            instance.callbacks.Add(doAfterRecompile.Method.EncodeToString());
            ForceRecompilation();
        }
        public static void ForceRecompilation() {
            var cMonoScript = MonoImporter.GetAllRuntimeMonoScripts()[0];
            MonoImporter.SetExecutionOrder(cMonoScript, MonoImporter.GetExecutionOrder(cMonoScript));
        }

        [DidReloadScripts]
        private static void OnScriptsReloaded() {
            if (instance == null || instance.callbacks == null)
                return;
            var callbacksCopy = instance.callbacks.ConvertAll(item => item);
            instance.callbacks.Clear();
            foreach (var methodInfoString in callbacksCopy) {
                var methodInfo = SerializationUtils.DecodeFromString<MethodInfo>(methodInfoString);
                methodInfo.Invoke(null, null);
            }
        }

        #region Log compilation time
        static new CompilationCallback instance => SettingsInEditor<CompilationCallback>.instance;
        [InitializeOnLoadMethod]
        static void InitOnLoad() {
            CompilationPipeline.compilationStarted += OnCompilationStarted;
            CompilationPipeline.compilationFinished += OnCompilationFinished;
            EditorApplication.update += EditorUpdate;
        }
        [SerializeField] bool logCompilationDuration;
        [SerializeField, HideInInspector] float startCompilingTime;
        private static void OnCompilationStarted(object obj)
            => instance.startCompilingTime = Time.realtimeSinceStartup;
        private static void OnCompilationFinished(object obj) {
            EditorUpdate();
            var time = Time.realtimeSinceStartup - instance.startCompilingTime;
            if (instance.logCompilationDuration)
                Debug.Log($"Compiled for {time} seconds");
            instance.isUpdatingAfterRecompile = true;
        }
        [SerializeField, HideInInspector] float prevEditorUpdateTime;
        bool isUpdatingAfterRecompile;
        private static void EditorUpdate() {
            var currTime = Time.realtimeSinceStartup;
            if (instance.isUpdatingAfterRecompile) {
                instance.isUpdatingAfterRecompile = false;
                var delta = currTime - instance.prevEditorUpdateTime;
                if (instance.logCompilationDuration)
                    Debug.Log($"Updating prefabs after recompile for {delta} seconds");
            }
            instance.prevEditorUpdateTime = currTime;
        }
        #endregion
    }
}
#endif