﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace AntonTools {
    public static class FindMonoBehaviourUsages {
        static MonoScript script;
        static List<AssetIterationItem> refs => AssetsIteratorSettings.instance.searchItemResults;
        public static void OnEditorGUI() {
            var _ = false;
            GUILayout.BeginHorizontal();
            EditorGUIUtils.ScriptField("script to search", ref script, ref _, 200);
            if (GUILayout.Button("Find"))
                Find();
            GUILayout.EndHorizontal();
            if (refs.CountSafe() == 0) return;
            if (GUILayout.Button("clear"))
                refs.Clear();
            for (int i = 0; i < refs.Count; i++) {
                var r = refs[i];
                GUILayout.BeginHorizontal();
                if (EditorGUIUtils.XButton()) {
                    refs.RemoveAt(i);
                    i--;
                }
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(r.path);
                EditorGUIUtils.PushDisabling();
                EditorGUIUtils.PrefabField(r.fullName, ref prefab, ref _, labelWidth: 300);
                GUILayout.EndHorizontal();
                if (PrefabUtils.openedPrefabPath == r.path) {
                    GUILayout.BeginHorizontal();
                    EditorGUIUtils.Indent();
                    var goPath = r.fullName.Replace(r.path, "");
                    var go = PrefabUtils.openedPrefabInstanceRoot.transform.GetChildByFullPath(goPath)?.gameObject;
                    EditorGUIUtils.GameObjectField("GameObject", ref go, ref _, labelWidth: 100);
                    GUILayout.EndHorizontal();
                }
                EditorGUIUtils.PopEnabling();
            }
        }

        private static void Find() => Utils.WithProgressBar($"searching script {script.name}", "searching...", _ => {
            refs.Clear();
            Utils.IterateAllGameObjectsInProject(i => {
                Utils.UpdateProgressbar($"{Utils.progress.ToShownPercents()}, {i.path}");
                var instance = i.go.GetComponent(script.GetClass());
                if (instance == null) return;
                refs.Add(new AssetIterationItem { path = i.path, fullName = i.fullName, go = i.go });
            });
            AssetsIteratorSettings.instance.SetChanged();
        });
    }
}
#endif