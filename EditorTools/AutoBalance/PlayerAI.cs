﻿using System.Linq;

namespace AntonTools {
    // Any player AI.
    public abstract class PlayerAI {
        protected GameRoot root => GameRoot.instance;
        public abstract void Update();
    }
}