﻿#if UNITY_EDITOR
namespace AntonTools {
    public class AutoBalanceModule_HowTo : HowToModule {
        public override string forWhat => "automated balancing";
        protected override void OnHowToGUI() {
            EditorGUIUtils.RichMultilineLabel("<i>work in progress...</i>");
        }
        protected override string docsURL => "";
    }
}
#endif