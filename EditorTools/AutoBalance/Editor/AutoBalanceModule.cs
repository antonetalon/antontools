﻿#if UNITY_EDITOR
using System;

namespace AntonTools {
    public class AutoBalanceModule : ModuleManager {
        public override Type parentModule => typeof(EditorToolsModule);
        public override HowToModule HowTo() => new AutoBalanceModule_HowTo();

        protected override void OnCompiledEnable() {
            base.OnCompiledEnable();
            SettingsInEditor<SimulationSettings>.EnsureExists();
        }
        protected override void OnCompiledGUI() {
            base.OnCompiledGUI();

        }
    }
}
#endif