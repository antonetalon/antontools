﻿using UnityEngine;

namespace AntonTools {
    public class ListItemNameAttribute : PropertyAttribute {
        public string elementFieldName;
        public ListItemNameAttribute(string elementFieldName) {
            this.elementFieldName = elementFieldName;
        }
    }
}