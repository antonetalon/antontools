﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public class GitSettings : SettingsScriptable<GitSettings> {
        protected override bool inRepository => false;
        public string sourceTreePath, commitMessage;
        public List<AssetStatus> assets = new List<AssetStatus>();
        public List<string> alwaysDiscarded = new List<string>();

        public float prevUpdateTime;
        public float updateInterval = 10;
        public float remainingToUpdate => prevUpdateTime + updateInterval - Time.realtimeSinceStartup;
        public bool timeToUpdate => remainingToUpdate <= 0;
        public void OnUpdated() {
            prevUpdateTime = Time.realtimeSinceStartup;
            this.SetChanged();
        }
    }
}
#endif