﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace AntonTools {
    public class GitWindow : EditorWindow {
        static GitSettings settings => SettingsInEditor<GitSettings>.instance;
        public static GitWindow instance { get; private set; }
        const string Git = "Git";
        [MenuItem(AntonToolsManager.MainPluginName + "/" + Git)]
        static void Init() {
            instance = (GitWindow)GetWindow(typeof(GitWindow), false, Git);
            instance.Show();
        }
        static bool isExecuting => cmd.isExecuting;
        void OnLaunchSourceTreeGUI() {
            // Launch sourcetree in case of emergency.
            if (EditorGUIUtils.SubtleButton("sourcetree")) {
                if (!settings.sourceTreePath.IsNothing())
                    cmd.Executing(settings.sourceTreePath, "", workingDirectory: gitPathRelative).WrapErrors();
                else {
                    Selection.activeObject = settings;
                    EditorGUIUtility.PingObject(Selection.activeObject);
                }
            }
        }
        bool changesExist => settings.assets.Count != 0;
        bool canPull => !changesExist;
        private void OnGUI() {
            EditorGUIUtils.PushEnabling(!isExecuting);
            GUILayout.BeginHorizontal();
            if (EditorGUIUtils.SubtleButton($"cmd"))
                cmd.OpenCMD(gitPathAbsolute);
            OnLaunchSourceTreeGUI();
            if (EditorGUIUtils.SubtleButton($"update changes"))
                GitStatusAndImplicitDiscard().WrapErrors();
            if (settings.timeToUpdate && !cmd.isExecuting)
                GitStatusAndImplicitDiscard().WrapErrors();
            GUILayout.EndHorizontal();
            // Back from submodule.
            if (gitPathRelative.IsSomething()) {
                if (EditorGUIUtils.Button("< back to main repo"))
                    SetSubmoduleShown(false);
            }
            // Pull.
            GUILayout.BeginHorizontal();
            if (EditorGUIUtils.Button("receive changes")) {
                if (canPull)
                    Pull().WrapErrors();
                else
                    EditorGUIUtils.ShowInfoWindow("commit or discard all to receive changes");
            }
            GUILayout.EndHorizontal();

            // See changes.
            
            var changed = false;
            for (int i = 0; i < settings.assets.Count; i++) {
                var asset = settings.assets[i];
                GUILayout.BeginHorizontal();
                EditorGUIUtils.TextureNoBorders(EditorGUIUtils.GetFileStatusPic(asset.shownStatus));
                EditorGUIUtils.PushDisabling();
                var assetObj = asset.asset;
                if (asset.shownStatus != FileStatus.Deleted) {
                    EditorGUIUtils.TypedField("", ref assetObj, ref changed);
                } else {
                    // Deleted.
                    EditorGUIUtils.TypedField("", ref assetObj, ref changed, width: 200);
                    var name = Path.GetFileName(asset.path);
                    EditorGUIUtils.TextField("", ref name, ref changed);
                }
                EditorGUIUtils.PopEnabling();
                if (!asset.isAntonTools) {
                    if (EditorGUIUtils.Button("discard", 100))
                        Discard(asset, false);
                    if (EditorGUIUtils.Button("discard always", 100))
                        Discard(asset, true);
                } else {
                    // antonToolsFolder
                    if (EditorGUIUtils.Button("submodule", 150))
                        SetSubmoduleShown(true);
                    if (EditorGUIUtils.Button("cmd", 50))
                        cmd.OpenCMD(AntonToolsManager.MainPluginFolder);
                }
                GUILayout.EndHorizontal();
            }

            // Commit & push.
            if (settings.assets.Count > 0)
                EditorGUIUtils.TextArea("", ref settings.commitMessage, ref changed);
            if (EditorGUIUtils.Button("save changes"))
                CommitAndPushByUser();

            EditorGUIUtils.PopEnabling();

            if (changed)
                settings.SetChanged();
        }

        private async void CommitAndPushByUser() {
            if (changesExist && settings.commitMessage.IsNothing()) {
                EditorGUIUtils.ShowInfoWindow("please write commit message");
                return;
            }
            if (settings.assets.Count > 0) {
                foreach (var asset in settings.assets)
                    await StageAsset(asset);
                await cmd.Executing($"git commit -m '{settings.commitMessage}'", gitPathRelative);
                settings.commitMessage = null;
                settings.SetChanged();
                GUI.FocusControl(null);
                await GitStatus();
            }
            await Pull();
            await GitPush();
        }
        private async Task GitPush() {
            await cmd.Executing($"git push", gitPathRelative);
        }
        private async Task StageAsset(AssetStatus asset) {
            await StageFile(asset.path, asset.assetStaged);
            if (asset.metaExists)
                await StageFile(asset.metaPath, asset.metaStaged);
        }

        private async Task StageFile(string filePath, bool fileStaged) {
            if (fileStaged)
                return;
            await cmd.Executing($"git add '{filePath}'", gitPathRelative);
        }

        private void Discard(AssetStatus asset, bool always) {
            EditorGUIUtils.DoIfConfirmed((always ? "Discard changes ALWAYS?" : "Discard changes?") + "\n" + asset.path,
                () => DiscardAssetByUser(asset, always));
        }
        async void DiscardAssetByUser(AssetStatus asset, bool always) {
            await DiscardAsset(asset);
            if (always) {
                settings.alwaysDiscarded.Add(asset.path);
                settings.SetChanged();
            }
            AssetDatabase.Refresh();
            await GitStatusAndImplicitDiscard();
        }
        async Task DiscardAsset(AssetStatus asset) {
            if (asset.isAntonTools)
                return;
            await DiscardFile(asset.path, asset.assetFileStatus, asset.assetStaged);
            if (asset.metaExists)
                await DiscardFile($"{asset.path}.meta", asset.metaFileStatus, asset.metaStaged);
        }
        async Task DiscardFile(string filePath, FileStatus status, bool staged) {
            if (staged)
                await cmd.Executing($"git restore --staged '{filePath}'", gitPathRelative);
            if (status == FileStatus.Modified || status == FileStatus.Deleted) {
                await cmd.Executing($"git restore '{filePath}'", gitPathRelative);
            } else if (status == FileStatus.Added) {
                await cmd.Executing($"rm '{filePath}'", gitPathRelative);
            }
        }

        private async Task Pull() {
            await GitStatusAndImplicitDiscard();
            if (!canPull)
                return;
            await cmd.Executing("git pull", gitPathRelative);
            AssetDatabase.Refresh();
        }

        private async Task GitStatusAndImplicitDiscard() {
            await GitStatus();
            var anyDiscarded = false;
            foreach (var  asset in settings.assets) {
                if (!settings.alwaysDiscarded.Contains(asset.path))
                    continue;
                await DiscardAsset(asset);
                anyDiscarded = true;
            }
            if (anyDiscarded) {
                AssetDatabase.Refresh();
                await GitStatus();
            }
            settings.OnUpdated();
        }

        static string antonToolsFolder => AntonToolsManager.MainPluginFolder;
        static bool showingAntonTools;
        void SetSubmoduleShown(bool shown) {
            showingAntonTools = shown;
            GitStatus().WrapErrors();
        }
        static string gitPathRelative => showingAntonTools ? antonToolsFolder : null;
        static string gitPathAbsolute => gitPathRelative.IsNothing() ? Utils.GetProjectFolder() : Path.Combine(Utils.GetProjectFolder(), gitPathRelative);
        private async Task GitStatus() {
            var statusStr = (await cmd.Executing("git status", gitPathRelative)).ToLf();
            settings.assets.Clear();
            string GetTextUnderTitle(string title) {
                var stagedInd = statusStr.IndexOf(title);
                if (stagedInd == -1) return string.Empty;
                var startInd = stagedInd + title.Length;
                var endInd = statusStr.IndexOf("\n\n", startInd);
                if (endInd == -1)
                    endInd = statusStr.Length - 1;
                var stagedStr = statusStr.Substring(startInd, endInd - startInd + 1);
                return stagedStr;
            }
            void ParseStatusChapter(string status, bool staged) {
                var lines = status.Split('\n').ConvertAll(l => l.Trim());
                lines.RemoveAll(l => l.IsNothing() || (l.StartsWith("(") && l.EndsWith(")")));
                lines.ForEach(l => ParseStatusFileLine(l, staged));
            }
            void ParseStatusFileLine(string str, bool staged) {
                (string prefix, FileStatus status) added, deleted, modified, curr, renamed;
                added = ("new file:", FileStatus.Added);
                deleted = ("deleted:", FileStatus.Deleted);
                modified = ("modified:", FileStatus.Modified);
                renamed = ("renamed:", FileStatus.Renamed);
                if (str.Contains(added.prefix))
                    curr = added;
                else if (str.Contains(modified.prefix))
                    curr = modified;
                else if (str.Contains(deleted.prefix))
                    curr = deleted;
                else if (str.Contains(renamed.prefix))
                    curr = renamed;
                else
                    curr = (string.Empty, FileStatus.Added);
                if (!curr.prefix.IsNothing())
                    str = str.Replace(curr.prefix, string.Empty);
                str = str.Trim();

                if (curr.status == FileStatus.Renamed) {
                    var pathes = str.Split(new string[] { " -> " }, System.StringSplitOptions.None);
                    var pathAdded = pathes[1];
                    var pathDeleted = pathes[0];
                    ParseStatusFileLine($"{added.prefix} {pathAdded}", staged);
                    ParseStatusFileLine($"{deleted.prefix} {pathDeleted}", staged);
                    return;
                }
                
                var filePath = str;
                var antonToolsPrefix = antonToolsFolder + " ";
                if (filePath.Contains(antonToolsPrefix))
                    filePath = AntonToolsManager.MainPluginFolder;
                const string meta = ".meta";
                var isMeta = filePath.EndsWith(meta);
                var assetPath = isMeta ? filePath.Replace(meta, string.Empty) : filePath;
                //if (gitPathRelative.IsSomething())
                //    assetPath = Path.Combine(gitPathRelative, assetPath);
                var asset = settings.assets.Find(a => a.path == assetPath);
                if (asset == null) {
                    asset = new AssetStatus();
                    settings.assets.Add(asset);
                    asset.path = assetPath;
                }

                if (isMeta) {
                    asset.metaFileStatus = curr.status;
                    asset.metaStaged = staged;
                } else {
                    asset.assetFileStatus = curr.status;
                    asset.assetStaged = staged;
                }

                var assetPathToShow = asset.shownStatus != FileStatus.Deleted ? asset.path : Path.GetDirectoryName(asset.path);
                if (gitPathRelative.IsSomething())
                    assetPathToShow = Path.Combine(gitPathRelative, assetPathToShow);
                asset.asset = AssetDatabase.LoadMainAssetAtPath(assetPathToShow);
            }
            const string StagedTitle = "Changes to be committed:";
            const string NotStagedTitle = "Changes not staged for commit:";
            const string UntrackedTitle = "Untracked files:";
            ParseStatusChapter(GetTextUnderTitle(StagedTitle), true);
            ParseStatusChapter(GetTextUnderTitle(NotStagedTitle), false);
            ParseStatusChapter(GetTextUnderTitle(UntrackedTitle), false);
            settings.SetChanged();
        }
    }
}
#endif