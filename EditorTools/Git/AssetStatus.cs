﻿#if UNITY_EDITOR
using System;

namespace AntonTools {
    public enum FileStatus { None, NoChanges, Added, Deleted, Modified, Renamed }
    [Serializable]
    public class AssetStatus {
        public UnityEngine.Object asset;
        public string path;
        public string metaPath => $"{path}.meta";
        public FileStatus assetFileStatus, metaFileStatus;
        public bool assetStaged, metaStaged;
        public FileStatus shownStatus {
            get {
                if (metaFileStatus == FileStatus.None)
                    return assetFileStatus;
                if (assetFileStatus == FileStatus.None)
                    return metaFileStatus;
                return assetFileStatus == metaFileStatus ? assetFileStatus : FileStatus.Modified;
            }
        }
        public bool isAntonTools => path == AntonToolsManager.MainPluginFolder;
        public bool metaExists => !isAntonTools;
    }
}
#endif