﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public class BuildModeSettings : SettingsScriptable<BuildModeSettings> {
        protected override string SubFolder => "BuildMode";

        [SerializeField] BuildModeType _mode;
        public static BuildModeType mode {
            get => instance != null ? instance._mode : BuildModeType.Develop;
            set {
                if (mode == value) return;
                if (instance == null) return;
                instance._mode = value;
                BuildInfo.instance.defaultMode = value;
#if UNITY_EDITOR
                BuildInfo.instance.SetChanged();
#endif
                onChanged?.Invoke(mode);
            }
        }
        public static Action<BuildModeType> onChanged;
        public static bool develop => mode == BuildModeType.Develop;
        public static bool test => mode == BuildModeType.Test;
        public static bool release => mode == BuildModeType.Release;

        public bool autoReleaseModeOnReleaseBranch = true;

        public bool IOSEnabled = false;
        public bool AndroidEnabled = false;
        public bool MacOSEnabled = true;
        public bool WindowsEnabled = true;
        public bool WebGLEnabled = true;
        public bool PlatformEnabled(TargetPlatform platform) {
            switch (platform) {
                default:
                case TargetPlatform.Android: return AndroidEnabled;
                case TargetPlatform.IOS: return IOSEnabled;
                case TargetPlatform.MacOS: return MacOSEnabled;
                case TargetPlatform.Windows: return WindowsEnabled;
                case TargetPlatform.WebGL: return WebGLEnabled;
            }
        }
        public bool mobileEnabled => AndroidEnabled || IOSEnabled;
        public bool IOSSetNoEncryptionUsed;
        public string releaseErrors;

        public List<ProjectSettingItemConfig> configs = new List<ProjectSettingItemConfig>();
    }
}
namespace AntonTools {
    public static class BuildMode {
        public static bool develop => BuildModeSettings.develop;
        public static bool test => BuildModeSettings.test;
        public static bool release => BuildModeSettings.release;
        public static BuildModeType type => BuildModeSettings.mode;
    }
}