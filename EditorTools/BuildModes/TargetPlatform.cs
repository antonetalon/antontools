﻿using UnityEngine;

namespace AntonTools {
    public enum TargetPlatform { IOS, Android, Windows, MacOS, WebGL }
    public static class TargetPlatformUtils {
        public static TargetPlatform current {
            get {
#if UNITY_EDITOR
                if (Application.isEditor)
                    return UnityEditor.EditorUserBuildSettings.activeBuildTarget.ToTargetPlatform();
#endif
                return Application.platform.ToTargetPlatform();
            }
        }
#if UNITY_EDITOR
        public static UnityEditor.BuildTargetGroup currentBuildTargetGroup
            => UnityEditor.EditorUserBuildSettings.activeBuildTarget.ToBuildTargetGroup();

        public static UnityEditor.BuildTargetGroup ToBuildTargetGroup(this UnityEditor.BuildTarget buildTarget) {
            switch (buildTarget) {
                default:
                case UnityEditor.BuildTarget.StandaloneOSX:
                case UnityEditor.BuildTarget.StandaloneWindows:
                case UnityEditor.BuildTarget.StandaloneWindows64: return UnityEditor.BuildTargetGroup.Standalone;
                case UnityEditor.BuildTarget.Android: return UnityEditor.BuildTargetGroup.Android;
                case UnityEditor.BuildTarget.iOS: return UnityEditor.BuildTargetGroup.iOS;
            }
        }

        public static TargetPlatform ToTargetPlatform(this UnityEditor.BuildTarget buildTarget) {
            switch (buildTarget) {
                default:
                case UnityEditor.BuildTarget.StandaloneWindows:
                case UnityEditor.BuildTarget.StandaloneWindows64: return TargetPlatform.Windows;
                case UnityEditor.BuildTarget.StandaloneOSX: return TargetPlatform.MacOS;
                case UnityEditor.BuildTarget.Android: return TargetPlatform.Android;
                case UnityEditor.BuildTarget.iOS: return TargetPlatform.IOS;
                case UnityEditor.BuildTarget.WebGL: return TargetPlatform.WebGL;
            }
        }
        public static UnityEditor.BuildTarget ToBuildTarget(this TargetPlatform target) {
            switch (target) {
                default:
                case TargetPlatform.IOS: return UnityEditor.BuildTarget.iOS;
                case TargetPlatform.Android: return UnityEditor.BuildTarget.Android;
                case TargetPlatform.Windows: return UnityEditor.BuildTarget.StandaloneWindows64;
                case TargetPlatform.MacOS: return UnityEditor.BuildTarget.StandaloneOSX;
                case TargetPlatform.WebGL: return UnityEditor.BuildTarget.WebGL;
            }
        }
        //public static TargetPlatform ToTargetPlatform(this UnityEditor.BuildTargetGroup group) {
        //    switch (group) {
        //        default:
        //        case UnityEditor.BuildTargetGroup.Android: return TargetPlatform.Android;
        //        case UnityEditor.BuildTargetGroup.iOS: return TargetPlatform.IOS;
        //        case UnityEditor.BuildTargetGroup.Standalone: return TargetPlatform.Standalone;
        //    }
        //}

        public static bool IsInstalledInEditor(this TargetPlatform platform) {
            var target = platform.ToBuildTarget();
            var moduleManager = System.Type.GetType("UnityEditor.Modules.ModuleManager,UnityEditor.dll");
            var isPlatformSupportLoaded = moduleManager.GetMethod("IsPlatformSupportLoaded", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            var getTargetStringFromBuildTarget = moduleManager.GetMethod("GetTargetStringFromBuildTarget", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            return (bool)isPlatformSupportLoaded.Invoke(null, new object[] { (string)getTargetStringFromBuildTarget.Invoke(null, new object[] { target }) });
        }
#endif
        public static TargetPlatform ToTargetPlatform(this RuntimePlatform type) {
            switch (type) {
                default:
                case RuntimePlatform.Android: return TargetPlatform.Android;
                case RuntimePlatform.IPhonePlayer: return TargetPlatform.IOS;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer: return TargetPlatform.Windows;
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer: return TargetPlatform.MacOS;
                case RuntimePlatform.WebGLPlayer: return TargetPlatform.WebGL;
            }
        }
        public static RuntimePlatform ToRuntimePlatform(this TargetPlatform type) {
            switch (type) {
                default:
                case TargetPlatform.IOS: return RuntimePlatform.IPhonePlayer;
                case TargetPlatform.Android: return RuntimePlatform.Android;
                case TargetPlatform.Windows: return RuntimePlatform.WindowsPlayer;
                case TargetPlatform.MacOS: return RuntimePlatform.OSXPlayer;
                case TargetPlatform.WebGL: return RuntimePlatform.WebGLPlayer;
            }
        }
    }
}