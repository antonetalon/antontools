﻿using UnityEngine;

namespace AntonTools {
    public class EnableOnDevelopMode : MonoBehaviour {
        [SerializeField] GameObject developEnabledParent;
        private void Awake() {
            if (developEnabledParent == null) 
                developEnabledParent = gameObject;
            BuildModeSettings.onChanged += UpdateView;
            UpdateView(BuildModeSettings.mode);
        } 
        private void OnDestroy() => BuildModeSettings.onChanged -= UpdateView;
        private void UpdateView(BuildModeType mode)
            => developEnabledParent.SetActiveSafe(mode == BuildModeType.Develop);
    }
}