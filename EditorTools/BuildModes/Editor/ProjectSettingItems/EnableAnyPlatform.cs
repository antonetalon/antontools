﻿#if UNITY_EDITOR
using System.Text;

namespace AntonTools {
    public class EnableAnyPlatform : ProjectSettingItem {
        public override string name => "ENABLE_ANY_PLATFORM";
        public override string description => "You have to enable at least one platform";
        public override void GetReleaseCheckError(StringBuilder sb) {
            if (!settings.AndroidEnabled && !settings.IOSEnabled && !settings.WindowsEnabled && !settings.MacOSEnabled)
                sb.AppendLine($"What platform are you building for?\n{description}");
        }
    }
}
#endif