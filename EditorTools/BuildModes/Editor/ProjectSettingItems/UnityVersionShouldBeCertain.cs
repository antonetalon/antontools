﻿#if UNITY_EDITOR
using System.Text;
using UnityEngine;

namespace AntonTools {
    public class UnityVersionShouldBeCertain : ProjectSettingItem {
        const string ProperUnityVersion = "2019.4.24f1";
        public override string name => "UNITY_VERSION";
        public override string description
            => $"AntonTools requires unity version {ProperUnityVersion}";
        public override void GetReleaseCheckError(StringBuilder sb) {
            if (Application.unityVersion != ProperUnityVersion)
                sb.AppendLine(description);
        }
    }
}
#endif