﻿#if UNITY_EDITOR
using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace AntonTools {
    public class BuildModesModule : ModuleManager {
        public override Type parentModule => typeof(EditorToolsModule);
        public override HowToModule HowTo() => new BuildModesModule_HowTo();

        public static BuildModeSettings settings => SettingsInEditor<BuildModeSettings>.instance;
        private static void SetModePrivate(BuildModeType mode) {
            BuildModeSettings.mode = mode;
            EditorUtility.SetDirty(settings);
        }

        protected override void OnCompiledEnable() => SettingsInEditor<BuildModeSettings>.EnsureExists();

        public static void GeneralBuildInfoGUI() {
            var changed = false;
            EditorGUIUtils.RichMultilineLabel(BuildInfo.instance.ToString());
            OnModifyModeGUI(ref changed);
            OnModifyBuildVersionAndNumberGUI(ref changed);
            if (changed)
                Save();
        }

        protected override void OnCompiledGUI() {
            base.OnCompiledGUI();
            var changed = false;

            GeneralBuildInfoGUI();

            // Other settings and actions.
            GUILayout.Space(20);
            if (GUILayout.Button("do release checks")) {
                DoAllReleaseChecks();
                changed = true;
            }
            if (!string.IsNullOrEmpty(settings.releaseErrors)) {
                EditorGUIUtils.WithLabelTextColor(Color.red, () => EditorGUIUtils.RichMultilineLabel(settings.releaseErrors));
                if (GUILayout.Button("clear errors")) {
                    settings.releaseErrors = null;
                    changed = true;
                }
            }
            EditorGUIUtils.Toggle($"auto switch to release on release branch", ref settings.autoReleaseModeOnReleaseBranch, ref changed, labelWidth: 250);
            EditorGUIUtils.Toggle($"{TargetPlatform.IOS} setup required", ref settings.IOSEnabled, ref changed);
            EditorGUIUtils.Toggle($"{TargetPlatform.Android} setup required", ref settings.AndroidEnabled, ref changed);
            EditorGUIUtils.Toggle($"{TargetPlatform.Windows} setup required", ref settings.WindowsEnabled, ref changed);
            EditorGUIUtils.Toggle($"{TargetPlatform.MacOS} setup required", ref settings.MacOSEnabled, ref changed);
            EditorGUIUtils.Toggle($"{TargetPlatform.WebGL} setup required", ref settings.WebGLEnabled, ref changed);

            if (settings.IOSEnabled)
                EditorGUIUtils.Toggle($"IOS not using encrypiton", ref settings.IOSSetNoEncryptionUsed, ref changed);

            ProjectSettingItemManager.OnGUI(ref changed);
            //if (GUILayout.Button("setup multidex"))
            //    MultiDexSetupManager.Setup();

            if (changed)
                Save();
        }
        static void Save() {
            settings.SetChanged();
            BuildInfoManagerEditor.InitBuildInfoSettings();
        }
        static void OnModifyBuildVersionAndNumberGUI(ref bool changed) {
            var version = PlayerSettings.bundleVersion;
            if (EditorGUIUtils.TextField("version", ref version, ref changed)) {
                PlayerSettings.bundleVersion = version;
                BuildInfo.instance.version = version;
            }

            if (BuildModeSettings.instance.mobileEnabled) {
                var buildNumber = BuildInfoManagerEditor.mobileBuildNumber;
                if (EditorGUIUtils.IntField("build number", ref buildNumber, ref changed))
                    BuildInfoManagerEditor.mobileBuildNumber = buildNumber;
            }
        }
        public override void ShortcutOnGUI() {
            base.ShortcutOnGUI();
            var changed = false;
            OnModifyModeGUI(ref changed);
            OnModifyBuildVersionAndNumberGUI(ref changed);
            if (changed)
                Save();
        }
        static void OnModifyModeGUI(ref bool changed) {
            var mode = BuildModeSettings.mode;
            if (EditorGUIUtils.Toolbar("", ref mode, ref changed))
                SetMode(mode);
        }

        static void SetIOSCapabilities() {
            if (!BuildModeSettings.instance.IOSEnabled)
                return;
            // Add capabilities only on release.
            // (Adding iap capability prevents building with wildcard)
            var path = "ProjectSettings/ProjectSettings.asset";
            var text = File.ReadAllText(path).WithLineEndings("\n");
            var automaticallyAddCapabilities = BuildModeSettings.mode == BuildModeType.Release;
            text = text.ReplaceLineWith("iOSAutomaticallyDetectAndAddCapabilities",
                $"  iOSAutomaticallyDetectAndAddCapabilities: {(automaticallyAddCapabilities ? 1 : 0)}");
            File.WriteAllText(path, text);
        }

        public static void SetMode(BuildModeType mode) {
            SetModePrivate(mode);
            if (Application.isPlaying)
                return;
            SetIOSCapabilities();
            // Release checks from modules.
            if (BuildModeSettings.mode == BuildModeType.Release)
                DoAllReleaseChecks();
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static void DoAllReleaseChecks() {
            var sb = new StringBuilder();

            ProjectSettingItemManager.GetAllReleaseCheckErrors(sb);
            var notCompletedChanges = ChangesEditorUI.DoReleaseChecks();
            if (!string.IsNullOrEmpty(notCompletedChanges))
                sb.AppendLine(notCompletedChanges);

            settings.releaseErrors = sb.ToString();
            settings.SetChanged();

            return;
        }
    }
}
#endif