﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AntonTools {
    public class PlayModeFullscreenManager : MonoBehaviourSingleton<PlayModeFullscreenManager> {

#if UNITY_EDITOR

        private static List<EditorWindow> dummyViews;
        private static EditorWindow fullscreenGameView;

        protected override void Awake() {
            base.Awake();
            // Hide this game object
            //gameObject.hideFlags = HideFlags.HideInHierarchy;
            Utils.DontDestroyOnLoad(gameObject, false);
        }

        [SerializeField] KeyCode keyModifier = KeyCode.LeftControl;
        [SerializeField] KeyCode key = KeyCode.Space;
        private void Update() {
            if (Input.GetKeyUp(key) && (keyModifier== KeyCode.None || Input.GetKey(keyModifier))) {
                if (isInFullScreen)
                    ExitFullScreen();
                else
                    EnterFullScreen(false);
            }
        }

        private static bool _isInFullScreen;
        public static bool isInFullScreen {
            get => _isInFullScreen;
            set {
                if (_isInFullScreen == value) return;
                _isInFullScreen = value;
                onChanged?.Invoke(_isInFullScreen);
            }
        }
        public static event Action<bool> onChanged;
        public static void EnterFullScreen(bool showToolbar, int sizeIndex = -1) {
            if (isInFullScreen) return;

            // Create dummy views
            dummyViews = new List<EditorWindow>();

            var dockAreaType = Type.GetType("UnityEditor.DockArea,UnityEditor");
            var dockAreas = Resources.FindObjectsOfTypeAll(dockAreaType);

            var addTabMethod = dockAreaType.GetMethod("AddTab", new Type[] { typeof(EditorWindow), typeof(bool) });
            foreach (UnityEngine.Object dockArea in dockAreas) {
                var dummyView = ScriptableObject.CreateInstance<EditorWindow>();
                dummyView.titleContent = new GUIContent("Dummy");
                dummyViews.Add(dummyView);

                addTabMethod.Invoke(dockArea, new object[] { dummyView, true });
            }
            // Create fullscreen game view
            var gameViewType = Type.GetType("UnityEditor.GameView,UnityEditor");
            fullscreenGameView = (EditorWindow)ScriptableObject.CreateInstance(gameViewType);

            var showToolbarProperty = gameViewType.GetProperty("showToolbar", BindingFlags.NonPublic | BindingFlags.Instance);
            showToolbarProperty.SetValue(fullscreenGameView, showToolbar);

            fullscreenGameView.ShowPopup();
            fullscreenGameView.position = new Rect(new Vector2(0, 0), new Vector2(Screen.currentResolution.width, Screen.currentResolution.height));
            fullscreenGameView.Focus();

            var selectedSizeIndexProp = gameViewType.GetProperty("selectedSizeIndex",
                  BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (sizeIndex == -1) {
                var existingGameViews = Resources.FindObjectsOfTypeAll(gameViewType);
                foreach (var gameView in existingGameViews) {
                    if (gameView == fullscreenGameView) continue;
                    sizeIndex = (int)selectedSizeIndexProp.GetValue(gameView);
                }
            }
            selectedSizeIndexProp.SetValue(fullscreenGameView, sizeIndex, null);

            isInFullScreen = true;
        }

        public static void ExitFullScreen() {
            if (!isInFullScreen) return;

            // Destroy fullscreen game view
            fullscreenGameView.Close();
            fullscreenGameView = null;

            // Destroy dummy views
            foreach (EditorWindow dummyView in dummyViews)
                dummyView.Close();
            dummyViews.Clear();
            dummyViews = null;

            isInFullScreen = false;
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            ExitFullScreen();
        }
#endif
    }
}
