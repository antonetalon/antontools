﻿namespace AntonTools {
    public class TargetFrameRateDebugView : ModuleDebugPanel {
        public override string tab => CommonTab;
        public override string module => "EDITOR_TOOLS";
        public override void OnDebugPanelAwake() => PerformanceBudgetManager.EnsureExists();
    }
}