﻿using UnityEngine;

namespace AntonTools {
    [ExecuteAlways]
    public class VFXView : MonoBehaviour {
        public float duration = -1;
        private void OnEnable() {
            if (Application.isPlaying)
                return;
            duration = 0;
#if !NO_COM_UNITY_MODULES_PARTICLESYSTEM
            GetComponentsInChildren<ParticleSystem>(false).ForEach(p => duration = Mathf.Max(p.main.duration, duration));
#endif
        }
        VFXPool pool;
        public void Show(Vector3 pos, VFXPool pool = null) {
            this.pool = pool;
            transform.position = pos;
            gameObject.SetActive(true);
            remaining = duration;
        }
        float remaining;
        private void Update() {
            if (!Application.isPlaying)
                return;
            remaining -= UnityEngine.Time.deltaTime;
            if (remaining > 0)
                return;
            gameObject.SetActive(false);
            pool?.Return(this);
        }
    }
}