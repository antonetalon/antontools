﻿#if UNITY_EDITOR
namespace AntonTools {
    public class OtherModule : RootModule {
        public override HowToModule HowTo() => new OtherModule_HowTo();
    }
}
#endif