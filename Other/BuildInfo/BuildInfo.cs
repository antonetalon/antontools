﻿using System;
using UnityEngine;

namespace AntonTools {
    public class BuildInfo : SettingsScriptable<BuildInfo> {
        protected override bool inRepository => false;
        public string branch;
        public string commitHash;
        public string version; // 1.2.21
        public string buildVersion; // 0
        public BuildModeType defaultMode; // release.
        public string builtFrom;
        public string cloudBuildTargetName;
        public bool isCloud;
        public string cloudBuildNumber;
        public bool readyToRelease;
        public string dateString;

        public string platform => Application.isEditor ? "Editor" : Application.platform.ToString();

        public string commitHashShort => commitHash != null ? commitHash.WithMaxLength(10) : string.Empty;
        string dataVersion
            //=> DataVersion.versionInd.ToString();
            => "0";
        const string RTR = "RTR";
        const string NFR = "NFR";
        string readyToReleaseString => readyToRelease ? RTR : NFR;
        public static string CreateDateStringNow() => $"{DateTime.Now.Day.ToString("00")}.{DateTime.Now.Month.ToString("00")}.{DateTime.Now.Year.ToString().Substring(2)}";
        public override string ToString()
            => $"{version}({buildVersion})-{dataVersion}-" +
            $"{defaultMode}-{readyToReleaseString}-{platform}-{commitHashShort}-{builtFrom}-{dateString}";
        public static BuildInfo FromString(string str) {
            try {
                var info = new BuildInfo();
                var items = str.Split('-');

                var versionStr = items[0];
                var openBraceInd = versionStr.IndexOf('(');
                var closeBraceInd = versionStr.IndexOf(')');
                var buildIndStr = versionStr.Substring(openBraceInd + 1, closeBraceInd - 1 - openBraceInd);
                info.buildVersion = buildIndStr;

                versionStr = versionStr.Remove(openBraceInd);
                info.version = versionStr;

                // var dataVersionStr = items[1];

                var modeStr = items[2];
                info.defaultMode = Utils.EnumParse<BuildModeType>(modeStr);

                var readyToReleaseStr = items[3];
                info.readyToRelease = readyToReleaseStr == RTR;

                //var buildPlatformStr = items[4];

                var commitHashShort = items[5];
                info.commitHash = commitHashShort;

                info.builtFrom = items[6];

                info.dateString = items[7];

                return info;
            } catch {
                return null;
            }
        }
        public BuildInfo() { }
    }
}