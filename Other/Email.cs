﻿using System;
using System.ComponentModel;
using System.Net.Mail;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace AntonTools {
    public static class Email {
#if !NO_COM_UNITY_MODULES_UNITYWEBREQUEST
        public static void SendFromDefaultApplication(string receiverEmail, string title, string body)
            => Application.OpenURL($"mailto:{receiverEmail}?subject={EscapeURL(title)}&body={EscapeURL(body)}");
        static string EscapeURL(string URL) => UnityWebRequest.EscapeURL(URL).Replace("+", "%20");
#endif

        public static async Task<string> SendFromSMTP(
            string receiverEmail, string senderEmail, string title, string body,
            string smtpUsername, string smtpPass, string smtpHost, int smtpPort, params string[] attachmentPathes) {

            // Written this method from here:
            // https://forum.unity.com/threads/how-to-send-mail-from-unity.580639/

            // Notes:
            // Not tested so far, but:
            // Works if you allowed secure less apps here:
            // https://support.google.com/accounts/answer/6010255?hl=en
            // Also set stripping to low.

            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient(smtpHost);
            mail.From = new MailAddress(senderEmail);
            mail.To.Add(receiverEmail);
            mail.Subject = title;
            mail.Body = body;

            //System.Net.Mail.Attachment attachment;
            foreach (var attachmentPath in attachmentPathes) {
                if (attachmentPath.IsNothing()) continue;
                var attachment = new System.Net.Mail.Attachment(attachmentPath);//"c:/textfile.txt");
                mail.Attachments.Add(attachment);
            }

            smtp.Port = smtpPort;
            smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPass);
            smtp.EnableSsl = true;

            string error = string.Empty;
            smtp.SendCompleted += (object sender, AsyncCompletedEventArgs e) => {
                if (e.Error != null)
                    error = e.Error.Message;//.GetBaseException().ToString();
                else if (e.Cancelled)
                    error = "sending cancelled";
            };
            try {
                await smtp.SendMailAsync(mail);
            } catch (Exception e) {
                error = e.Message;//.GetBaseException().ToString();
            }
            if (!string.IsNullOrEmpty(error)) {
                Debug.LogError(error);
                error = "Error - can't send a message";
            }

            return error;
        }
        public static async Task<string> SendFromGoogleSMTPServer(string receiverEmail, string title, string body,
            string senderGMail, string senderGMailPass)
            => await SendFromSMTP(receiverEmail, senderGMailPass, title, body, senderGMail, senderGMailPass, "smtp.gmail.com", 587);
    }
}