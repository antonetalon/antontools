﻿using UnityEngine;

namespace AntonTools {
    public class DontDestroyOnLoad : MonoBehaviour {
        private void Awake() => Utils.DontDestroyOnLoad(gameObject);
    }
    public static partial class Utils {
        public static void DontDestroyOnLoad(GameObject go, bool logWhenNotRoot = true) {
            if (go.transform.root.gameObject == go)
                Object.DontDestroyOnLoad(go);
            else if (logWhenNotRoot)
                Debug.Log($"DontDestroyOnLoad should not be called for non-root object, path = {go.GetFullPath()}");
        }
    }
}