﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace AntonTools {
    public static class EmailToDevs {
        //public static string EmailReceiver = "someDevEmailToSendTo@gmail.com";
        static string GetTitleFromBody(string body) {
            const int MaxTitleLength = 30;
            string title;
            if (body.Length < MaxTitleLength)
                title = body;
            else {
                title = body.Substring(0, MaxTitleLength);
                int endLineInd = title.IndexOf('\n');
                if (endLineInd != -1)
                    title = title.Substring(0, endLineInd);
            }
            return title;
        }
        //const string SMTPSenderGmail = "someDevEmailtoSendFrom@gmail.com";
        //const string SMTPSenderGmailPass = "password"; // Application password.
        //public static async Task<string> Send(string body)
        //    => await Send(EmailReceiver, GetTitleFromBody(body));
        //public static async Task<string> Send(string title, string body) {
            //title = GetEmailTitle(title);
            //return await Email.SendFromGoogleSMTPServer(EmailReceiver, title, body, SMTPSenderGmail, SMTPSenderGmailPass);
        //}
        public static string GetEmailTitle(string title)
            => $"{title} from {Application.productName} {SystemInfo.deviceModel} {DateTime.Now.ToShortTimeString()} build {BuildInfoManager.buildInfo}";
    }
}