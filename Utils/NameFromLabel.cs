﻿using TMPro;
using UnityEngine;

namespace AntonTools {
    [ExecuteAlways]
    public class NameFromLabel : MonoBehaviour {
        private void OnEnable() {
            if (!Application.isPlaying) {
                var label = GetComponentInChildren<TextMeshProUGUI>();
                var name = (label?.text.IsNullOrEmpty() ?? true) ? "NO_LABEL_NAME_FOUND" : $"{label.text.ToCamelCase()}";
                transform.name = name;
                var tr = transform;
                DestroyImmediate(this);
#if UNITY_EDITOR
                UnityEditor.EditorUtility.SetDirty(tr);
#endif
            }
        }
    }
}