﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.Profiling;
using System.Linq;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.Experimental.SceneManagement;
#endif

namespace AntonTools {
    public static partial class Utils {
#if UNITY_EDITOR
        public static bool IsPrefabOpened() => PrefabUtils.prefabOpened;
        public static bool CanBeModified(this Transform transform) => PrefabChangesAllowed(transform.gameObject);
        public static bool CanBeModified(this GameObject gameObject) => PrefabChangesAllowed(gameObject);
        public static bool PrefabChangesAllowed(GameObject go) {
            //var opening = PrefabUtils.IsPrefabOpening;
            //var opened = PrefabUtils.openedPrefab;
            //var outer = UnityEditor.PrefabUtility.GetOutermostPrefabInstanceRoot(go);
            var prefabIsOpening = PrefabUtils.IsPrefabOpening;
            if (prefabIsOpening)
                return false;
            var parentPrefab = PrefabUtility.GetOutermostPrefabInstanceRoot(go);
            var notUnderPrefab = parentPrefab == null;
            if (notUnderPrefab)
                return true;
            var isParentPrefabOpened = parentPrefab == PrefabUtils.openedPrefabInstanceRoot;
            return isParentPrefabOpened;
        }
#else
        public static bool PrefabChangesAllowed(GameObject go) => true;
        public static bool IsPrefabOpened() => false;
#endif
        public static void UpdatePrefabsList<TView, TModel>(List<TView> views, IReadOnlyList<TModel> data,
            TView prefab, Action<TModel, TView> updateView, bool findExistingItems = true, GameObject noItemsParent = null) where TView : Component
            => UpdatePrefabsList(views, data, prefab, null, updateView, findExistingItems, noItemsParent);
        public static void UpdatePrefabsList<TView, TModel>(List<TView> views, IReadOnlyList<TModel> data,
            TView prefab, Transform parent, Action<TModel, TView> updateView, bool findExistingItems = true, GameObject noItemsParent = null) where TView : Component
            => UpdatePrefabsList(views, data.CountSafe(), prefab, parent, () => {
                for (int i = 0; i < data.CountSafe(); i++)
                    updateView(data[i], views[i]);
            }, findExistingItems, noItemsParent);
        public static void UpdatePrefabsList<TView>(List<TView> views, int count,
            TView prefab, Transform parent, Action updateViews, bool findExistingItems = true, GameObject noItemsParent = null, 
            Action<TView> onViewAdded = null) where TView : Component {
            if (prefab == null)
                return;
            if (parent == null)
                parent = prefab.transform.parent;
            if (findExistingItems && views.Count == 0)//prefab.transform.parent != null && parent == prefab.transform.parent && !views.Contains(prefab))
                parent.GetComponentsInChildren<TView>(true).ForEach(ch => AddView(ch));
            void AddView(TView view) {
                views.AddIfNotExists(view);
                onViewAdded?.Invoke(view);
            }
            if (prefab.transform.parent == parent && !views.Contains(prefab))
                AddView(prefab);
            for (int i = views.Count; i < count; i++)
                AddView(Object.Instantiate(prefab, parent));
            for (int i = 0; i < count; i++)
                views[i].gameObject.SetActive(true);
            updateViews?.Invoke();
            for (int i = Mathf.Max(0, count); i < views.Count; i++)
                views[i].gameObject.SetActive(false);
            noItemsParent.SetActiveSafe(count == 0);
        }
        static List<long> toRemove = new List<long>();
        public static void UpdatePrefabsDictionary<TView, TModel>(
            Dictionary<long, TView> views, IReadOnlyDictionary<long, TModel> data,
            Func<TModel, TView> getPrefab, Transform parent, Action<TModel, TView> updateView) where TView : Component {
            UpdatePrefabsDictionary(views, data, model =>
                MonoBehaviour.Instantiate(getPrefab(model), parent),
                view => MonoBehaviour.Destroy(view.gameObject),
                updateView);
        }
        public static void UpdatePrefabsDictionary<TView, TModel>(
            Dictionary<long, TView> views, IReadOnlyDictionary<long, TModel> data,
            Func<TModel, TView> createView, Action<TView> destroyView, Action<TModel, TView> updateView) where TView : Component {
            toRemove.Clear();
            foreach (var (key, view) in views) {
                if (!data.TryGetValue(key, out var model) || view == null) {
                    destroyView?.Invoke(view);
                    toRemove.Add(key);
                } else
                    updateView(model, view);
            }
            if (toRemove != null) {
                foreach (var key in toRemove)
                    views.Remove(key);
            }
            foreach (var (key, model) in data) {
                if (views.ContainsKey(key))
                    continue;
                var view = createView(model);
                views.Add(key, view);
                updateView(model, view);
            }
        }

        public static void DestroyEditOrPlayMode(this GameObject go) {
            if (Application.isPlaying)
                MonoBehaviour.Destroy(go);
            else
                MonoBehaviour.DestroyImmediate(go);
        }
    }

#if UNITY_EDITOR
    public static class PrefabUtils {
        public static bool IsPrefab(GameObject gameObject) => AssetDatabase.GetAssetPath(gameObject).IsSomething();
        public static string GetPrefabPath(GameObject instanceRoot)
            => IsPrefabInstanceRoot(instanceRoot) ? PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(instanceRoot) : null;
        public static GameObject GetPrefab(GameObject instanceRoot)
            => IsPrefabInstanceRoot(instanceRoot) ? (openedPrefabInstanceRoot == instanceRoot ? openedPrefabRoot : 
                AssetDatabase.LoadAssetAtPath<GameObject>(PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(instanceRoot))) : null;
        public static bool IsPrefabInstanceRoot(this GameObject gameObject) => PrefabUtility.GetNearestPrefabInstanceRoot(gameObject) == gameObject || openedPrefabInstanceRoot == gameObject;
        static AntonToolsLocalSettings settings => SettingsInEditor<AntonToolsLocalSettings>.instance;
        public static string currPrefabAssetPath => PrefabStageUtility.GetCurrentPrefabStage()?.assetPath ?? null;            
        public static GameObject openedPrefabInstanceRoot => PrefabStageUtility.GetCurrentPrefabStage()?.prefabContentsRoot;
        static GameObject _openedPrefabRoot;
        static string _currPrefabAssetPath;
        public static GameObject openedPrefabRoot {
            get {
                if (_currPrefabAssetPath == currPrefabAssetPath)
                    return _openedPrefabRoot;
                _currPrefabAssetPath = currPrefabAssetPath;
                _openedPrefabRoot = AssetDatabase.LoadAssetAtPath<GameObject>(currPrefabAssetPath);
                return _openedPrefabRoot;
            }
        }
        public static string openedPrefabPath => settings?.openedPrefabPath;
        public static bool prefabOpened => !openedPrefabPath.IsNullOrEmpty();
        public static Action onPrefabOpened;
        public static void InitOnLoad() {
            void SetOpenedPrefab(string assetPath) {
                if (settings == null) return;
                settings.openedPrefabPath = assetPath;
                settings.SetChanged();
                onPrefabOpened?.Invoke();
            }
            PrefabStage.prefabStageOpened += stage => SetOpenedPrefab(stage?.assetPath);
            PrefabStage.prefabStageClosing += stage => {
                if (PrefabStageUtility.GetCurrentPrefabStage() == stage)
                    SetOpenedPrefab(null);
            };
            SetOpenedPrefab(currPrefabAssetPath);
        }
        public static bool IsPrefabOpening => !openedPrefabPath.EqualsNoNull(currPrefabAssetPath);
        public static void Open(GameObject prefab) {
            if (prefab != null)
                AssetDatabase.OpenAsset(prefab);
            else {
                var scene = EditorSceneManager.GetActiveScene();
                if (scene != null)
                    EditorSceneManager.OpenScene(scene.path, OpenSceneMode.Single);
            }
        }
    }
#else
    public static class PrefabUtils
    {
        public static bool IsPrefabOpening => false;
        public static GameObject openedPrefab => null;
    }
#endif
}