﻿using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public static partial class Utils {
        public static Vector3 Center(this IEnumerable<Vector3> pts) {
            var c = Vector3.zero;
            var count = 0;
            foreach (var pt in pts) {
                c += pt;
                count++;
            }
            return c / count;
        }
        public static Vector3 Mirror(Vector3 pt, Vector3 mirrorPos, Vector3 mirrorN)
            => (Utils.ProjectPtOnPlane(pt, mirrorPos, mirrorN) - pt) * 2 + pt;
        public static Quaternion Inverse(this Quaternion q) => Quaternion.Inverse(q);
        public static Quaternion ToLocal(this Quaternion rotationInWorld, Quaternion toWorldRotation)
            => rotationInWorld * toWorldRotation.Inverse();
        //  inworld = inlocal * toworld
        //  inworld * toworld.inverse = inlocal
        public static Quaternion ToWorld(this Quaternion rotationInLocal, Quaternion toWorldRotation)
            => rotationInLocal * toWorldRotation;
        public static Vector3 CalcNormal(Vector3 v1, Vector3 v2, Vector3 v3)
            => Vector3.Cross(v2 - v1, v3 - v1);
        public static bool TriangleSphereIntersection(Vector3 c, float r, Vector3 v1, Vector3 v2, Vector3 v3)
            => CalcDistToTriangleSqr(c, v1, v2, v3) < r * r;
        public static float CalcDistToTriangleSqr(Vector3 c, Vector3 v1, Vector3 v2, Vector3 v3) {
            var n = CalcNormal(v1, v2, v3);
            var c1 = ProjectPtOnPlane(c, v1, n);
            var (t1, t2, t3) = GetPtOnPlaneInBarycentric(c1, v1, v2, v3);
            float DistToCSqr(Vector3 v) => (v - c).sqrMagnitude;
            // Dist is to vertices.
            if (t2 <= 0 && t3 <= 0) return DistToCSqr(v1);
            if (t1 <= 0 && t3 <= 0) return DistToCSqr(v2);
            if (t1 <= 0 && t2 <= 0) return DistToCSqr(v3);
            // Dist is to edges.
            if (t1 <= 0) return CalcDistSqrToSegment(c, v2, v3);
            if (t2 <= 0) return CalcDistSqrToSegment(c, v1, v3);
            if (t3 <= 0) return CalcDistSqrToSegment(c, v1, v2);
            // Dist is to plane.
            return DistToCSqr(c1);
        }
        public static float CalcTriangleAreaSqr(float edge1, float edge2, float edge3) {
            var s = (edge1 + edge2 + edge3) * 0.5f;
            var areaSqr = s * (s - edge1) * (s - edge2) * (s - edge3);
            return areaSqr;
        }
        // Line is infinite.
        public static float CalcDistSqrToLine(Vector3 pt, Vector3 linePt1, Vector3 linePt2) {
            var lineDir = linePt2 - linePt1;
            var t = GetProjectionBarycentric(pt, linePt1, linePt2);
            var x = linePt1 + lineDir * t;
            var distSqr = (x - pt).sqrMagnitude;
            return distSqr;
        }
        public static float CalcDistSqrToSegment(Vector3 pt, Vector3 segmPt1, Vector3 segmPt2) {
            var segmDir = segmPt2 - segmPt1;
            var t = GetProjectionBarycentric(pt, segmPt1, segmPt2);
            t = Mathf.Clamp01(t);
            var x = segmPt1 + segmDir * t;
            var distSqr = (x - pt).sqrMagnitude;
            return distSqr;
        }
        public static Vector3 ProjectPtOnLine(Vector3 pt, Vector3 linePt1, Vector3 linePt2) {
            var t2 = GetProjectionBarycentric(pt, linePt1, linePt2);
            var t1 = 1 - t2;
            return linePt1 * t1 + linePt2 * t2;
        }
        public static float GetProjectionBarycentric(Vector3 pt, Vector3 linePt1, Vector3 linePt2) {
            // x - pt projection on line.
            var lineDir = linePt2 - linePt1;
            // x=linePt1+lineDir*t
            // (pt-x) is perpendicular to lineDir
            // Vector3.Dot(pt-x, lineDir) = 0
            // Vector3.Dot(pt-linePt1-lineDir*t, lineDir) = 0
            // Vector3.Dot(pt-linePt1, lineDir) - Vector3.Dot(lineDir, lineDir)*t = 0
            // t = Vector3.Dot(pt-linePt1, lineDir)/Vector3.Dot(lineDir, lineDir)
            var t = Vector3.Dot(pt - linePt1, lineDir) / Vector3.Dot(lineDir, lineDir);
            return t;
        }
        public static float DistToRectSqr(Vector2 pt, Vector2 rectCenter, Vector2 rectSize) {
            var closestPt = GetClosestPtFromPtToRect(pt, rectCenter, rectSize);
            var distSqr = (closestPt - pt).sqrMagnitude;
            return distSqr;
        }
        public static Vector2 GetClosestPtFromPtToRect(Vector2 pt, Vector2 rectCenter, Vector2 rectSize) {
            // d=pt-c
            // pt = c+d*t
            // pt.x = c.x+d.x*t
            // |d.x|*t < s.x
            // t<s.x/|d.x|
            // t<s.y/|d.y|
            var d = pt - rectCenter;
            var t = 1f;
            var minByX = rectSize.x * 0.5f / Mathf.Abs(d.x);
            if (minByX.IsSane())
                t = Mathf.Min(t, minByX);
            var minByY = rectSize.y * 0.5f / Mathf.Abs(d.y);
            if (minByY.IsSane())
                t = Mathf.Min(t, minByY);
            var closestPt = rectCenter + d * t;
            return closestPt;
        }
        public static Vector3 SetLength(this Vector3 v, float length) => v.normalized * length;
        /// <summary>
        /// All lengthes arbitrary.
        /// </summary>
        public static Vector3 Project(Vector3 vector, Vector3 projectOn)
            => projectOn * Vector3.Dot(vector, projectOn) / projectOn.sqrMagnitude;
        /// <summary>
        /// planeNormal length arbitrary.
        /// </summary>
        public static Vector3 ProjectPtOnPlane(Vector3 pos, Vector3 planePos, Vector3 planeNormal)
            => pos - Project(pos - planePos, planeNormal);
        public static float DistToPlaneOriented(Vector3 pos, Vector3 planePos, Vector3 planeNormal)
            => Vector3.Dot(pos - planePos, planeNormal);
        public static float DistToPlane(Vector3 pos, Vector3 planePos, Vector3 planeNormal)
            => DistToPlaneOriented(pos, planePos, planeNormal).Abs();

        public static (float T1, float T2) GetLinePtOnPlaneBarycentric(Vector3 V1, Vector3 V2, Vector3 v1, Vector3 v2, Vector3 v3) {
            // dot(V1*T1+V2*T2 - v1, n) = 0 - Barycentric equality.
            var n = CalcNormal(v1, v2, v3); // Triangle normal.
            var T1 = Vector3.Dot(v1 - V2, n) / Vector3.Dot(V1 - V2, n);  //(n.z * (V2 - v1).y - n.y * (V2 - v1).z) / (n.y * (V1 - V2).z - n.z * (V1 - V2).y);
            var T2 = 1 - T1;
            //var c = V1 * T1 + V2 * T2; // Intersection.
            return (T1, T2);
        }
        /// <summary>
        /// c should already be on plane.
        /// </summary>
        public static (float t1, float t2, float t3) GetPtOnPlaneInBarycentric(Vector3 c, Vector3 v1, Vector3 v2, Vector3 v3) {
            c -= v3;
            v1 -= v3;
            v2 -= v3;
            var div1 = v2.x * v1.y - v2.y * v1.x;
            var div2 = v2.z * v1.y - v2.y * v1.z;
            var div3 = v2.x * v1.z - v2.z * v1.x;
            var div1Abs = Mathf.Abs(div1);
            var div2Abs = Mathf.Abs(div2);
            var div3Abs = Mathf.Abs(div3);
            float t2;
            if (div1Abs > div2Abs && div1Abs > div3Abs)
                t2 = (c.x * v1.y - c.y * v1.x) / div1;
            else if (div2Abs > div1Abs && div2Abs > div3Abs)
                t2 = (c.z * v1.y - c.y * v1.z) / div2;
            else
                t2 = (c.x * v1.z - c.z * v1.x) / div3;

            var xAbs = Mathf.Abs(v1.x);
            var yAbs = Mathf.Abs(v1.y);
            var zAbs = Mathf.Abs(v1.z);
            float t1;
            if (xAbs > yAbs && xAbs > zAbs)
                t1 = (c.x - t2 * v2.x) / v1.x;
            else if (yAbs > xAbs && yAbs > zAbs)
                t1 = (c.y - t2 * v2.y) / v1.y;
            else
                t1 = (c.z - t2 * v2.z) / v1.z;

            var t3 = 1 - t1 - t2;
            //c += v3;
            //v1 += v3;
            //v2 += v3;
            //var c1 = v1 * t1 + v2 * t2 + v3 * t3;
            //if (t1 > 0 && t2 > 0 && t3 > 0)
            //    Utils.DrawCross(c1, 0.2f, Color.blue, 10);
            return (t1, t2, t3);
        }
        public static (float t1, float t2, float t3, float T1, float T2) 
            GetPlaneLineIntersectionBarycentric(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 V1, Vector3 V2) {

            //Debug.DrawLine(v1, v2, Color.green, 10);
            //Debug.DrawLine(v1, v3, Color.green, 10);
            //Debug.DrawLine(v2, v3, Color.green, 10);
            var (T1, T2) = GetLinePtOnPlaneBarycentric(V1, V2, v1, v2, v3);
            var c = V1 * T1 + V2 * T2;
            var (t1, t2, t3) = GetPtOnPlaneInBarycentric(c, v1, v2, v3);
            return (t1, t2, t3, T1, T2);
        }
        public static bool TriangleSegmentIntersects(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 V1, Vector3 V2) {
            var (t1, t2, t3, T1, T2) = GetPlaneLineIntersectionBarycentric(v1, v2, v3, V1, V2);
            return t1 > 0 && t2 > 0 && t3 > 0 && T1 > 0 && T2 > 0;
        }
        public static (float t1, float t2) LinePlaneIntersectionBarycentric(Vector3 line1, Vector3 line2, Vector3 planePos, Vector3 planeN) {
            // dot(line1*t1+line2*(1-t1) - planePos, planeN) = 0.
            // dot(t1*(line1-line2)+line2 - planePos, planeN) = 0.
            // t1*dot(line1-line2, planeN) = dot(planePos-line2, planeN)
            // t1 = dot(planePos-line2, planeN)/dot(line1-line2, planeN) 
            var t1 = (planePos - line2, planeN).Dot() / (line1 - line2, planeN).Dot();
            return (t1, 1f - t1);
        }
        public static (Vector3 lineBetweenPt1, Vector3 lineBetweenPt2)
            GetLineBetweenLines(Vector3 line1Pt1, Vector3 line1Pt2, Vector3 line2Pt1, Vector3 line2Pt2) {
            Vector3 line1ClosestPt, line2ClosestPt;
            var line1Dir = line1Pt1 - line1Pt2;
            var line2Dir = line2Pt1 - line2Pt2;
            var linesPerpDir = Vector3.Cross(line1Dir.normalized, line2Dir.normalized);
            if (linesPerpDir.sqrMagnitude < 0.00001f) {
                // Parallel lines.
                line1ClosestPt = ProjectPtOnLine(line2Pt1, line1Pt1, line1Pt2);
                line2ClosestPt = ProjectPtOnLine(line1ClosestPt, line2Pt1, line2Pt2);
            } else {
                var plane1N = Vector3.Cross(linesPerpDir, line1Dir);
                var (t1, t2) = LinePlaneIntersectionBarycentric(line2Pt1, line2Pt2, line1Pt1, plane1N);
                line2ClosestPt = line2Pt1 * t1 + line2Pt2 * t2;
                line1ClosestPt = ProjectPtOnLine(line2ClosestPt, line1Pt1, line1Pt2);
            }
            return (line1ClosestPt, line2ClosestPt);
        }
    }
}