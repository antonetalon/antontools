﻿using UnityEngine;
using UnityEngine.UI;

namespace AntonTools
{
    public class EnableOnPlatform : MonoBehaviour
    {
        [SerializeField] Button interactableButton;
        [SerializeField] GameObject enabledGO;
        [SerializeField] bool webGL = true;
        [SerializeField] bool PC = true;
        [SerializeField] bool MAC = true;
        [SerializeField] bool ANDROID = true;
        [SerializeField] bool IOS = true;
        [SerializeField] bool overrideForEditor = false;
        [SerializeField] bool editor = true;
        new bool enabled => overrideForEditor ? editor : TargetPlatformUtils.current switch {
            TargetPlatform.IOS => IOS,
            TargetPlatform.Android => ANDROID,
            TargetPlatform.Windows => PC,
            TargetPlatform.MacOS => MAC,
            TargetPlatform.WebGL => webGL,
            _ => throw new System.NotImplementedException(),
        };
        private void Awake() {
            var enabled = this.enabled;
            if (interactableButton != null)
                interactableButton.interactable = enabled;
            if (enabledGO != null)
                enabledGO.SetActive(enabled);
        }
    }
}