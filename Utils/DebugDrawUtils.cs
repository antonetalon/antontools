﻿using UnityEngine;

namespace AntonTools {
    public static partial class Utils {
        public static void Draw(this Bounds bounds, Color col, float duration = 0) {
            Vector3 Pos(float x, float y, float z)
                => bounds.center + new Vector3(bounds.size.x * x, bounds.size.y * y, bounds.size.z * z) * 0.5f;
            Debug.DrawLine(Pos(1, 1, 1), Pos(-1, 1, 1), col, duration);
            Debug.DrawLine(Pos(1, 1, 1), Pos(1, -1, 1), col, duration);
            Debug.DrawLine(Pos(1, -1, 1), Pos(-1, -1, 1), col, duration);
            Debug.DrawLine(Pos(-1, 1, 1), Pos(-1, -1, 1), col, duration);
            Debug.DrawLine(Pos(1, 1, -1), Pos(-1, 1, -1), col, duration);
            Debug.DrawLine(Pos(1, 1, -1), Pos(1, -1, -1), col, duration);
            Debug.DrawLine(Pos(1, -1, -1), Pos(-1, -1, -1), col, duration);
            Debug.DrawLine(Pos(-1, 1, -1), Pos(-1, -1, -1), col, duration);
            Debug.DrawLine(Pos(1, 1, 1), Pos(1, 1, -1), col, duration);
            Debug.DrawLine(Pos(-1, 1, 1), Pos(-1, 1, -1), col, duration);
            Debug.DrawLine(Pos(1, -1, 1), Pos(1, -1, -1), col, duration);
            Debug.DrawLine(Pos(-1, -1, 1), Pos(-1, -1, -1), col, duration);
        }
        public static void DrawRect(Vector3 pos, Vector3 edge1, Vector3 edge2, Color col, float duration = 0) {
            edge1 *= 0.5f;
            edge2 *= 0.5f;
            Debug.DrawLine(pos + edge1 + edge2, pos + edge1 - edge2, col, duration);
            Debug.DrawLine(pos - edge1 + edge2, pos - edge1 - edge2, col, duration);
            Debug.DrawLine(pos + edge1 + edge2, pos - edge1 + edge2, col, duration);
            Debug.DrawLine(pos + edge1 - edge2, pos - edge1 - edge2, col, duration);
        }
        public static void DrawTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color col, float duration = 0) {
            Debug.DrawLine(v1, v2, col, duration);
            Debug.DrawLine(v1, v3, col, duration);
            Debug.DrawLine(v3, v2, col, duration);
        }
        public static void DrawRect(Vector3 pos, Vector2 size, Vector3 normal, Vector3 up, Color col, float duration = 0) {
            var toSide = Vector3.Cross(normal, up).normalized;
            var toUp = Vector3.Cross(toSide, normal).normalized;
            toSide *= size.x;
            toUp *= size.y;
            DrawRect(pos, toSide, toUp, col, duration);
        }
        public static void DrawCross(Vector3 pos, float size, Color col, float duration = 0) {
            Debug.DrawLine(pos + new Vector3(size, 0, 0), pos - new Vector3(size, 0, 0), col, duration);
            Debug.DrawLine(pos + new Vector3(0, size, 0), pos - new Vector3(0, size, 0), col, duration);
            Debug.DrawLine(pos + new Vector3(0, 0, size), pos - new Vector3(0, 0, size), col, duration);
        }
        public static void DrawCircle(Vector3 pos, Vector3 normal, float radius, Color col, float sectionLength = 1f, int minSections = 15, float duraiton = 0) {
            var toSide = Utils.GetPerpendicular(normal).normalized * radius;
            var ptsCount = Mathf.Max(minSections, Mathf.CeilToInt(radius * Mathf.PI * 2f / sectionLength));
            var rotate = Quaternion.AngleAxis(360f / ptsCount, normal);
            var prevPt = toSide;
            for (int i = 0; i < ptsCount; i++) {
                var nextPt = rotate * prevPt;
                Debug.DrawLine(prevPt + pos, nextPt + pos, col, duraiton);
                prevPt = nextPt;
            }
        }
    }
}