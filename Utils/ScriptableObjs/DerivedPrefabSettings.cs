using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AntonTools
{
    // Automatically creates list with prefabs, derived from T.
    public abstract class DerivedPrefabSettings<T, TSelf> : ScriptableSingleton<TSelf>
        where T : MonoBehaviour
        where TSelf : DerivedPrefabSettings<T, TSelf>
    {
        [SerializeField] List<T> _items = new List<T>();
        public static T1 Get<T1>(Func<T1, bool> findWhat = null) where T1 : T {
            if (instance == null) {
                Debug.LogError($"DerivedPrefabSettings<{typeof(T).NameInCode(false)}> instance does not exist, please create it");
                return null;
            }
#if UNITY_EDITOR
            var item = GetWithoutUpdate(findWhat);
            if (item != null)
                return item;
            UpdateItems();
#endif
            return GetWithoutUpdate(findWhat);
        }
        private static T1 GetWithoutUpdate<T1>(Func<T1, bool> findWhat = null) where T1 : T {
            foreach (var item in instance._items) {
                if (item == null)
                    return null;
                if (!(item is T1 item1))
                    continue;
                if (findWhat != null && !findWhat(item1))
                    continue;
                return item1;
            }
            return null;
        }
#if UNITY_EDITOR
        static void UpdateItems() {
            Utils.WithProgressBar($"finding {typeof(T).Name} prefabs", "", _ => {
                instance._items.Clear();
                Utils.IterateAllPrefabsInProject((path, prefab) => {
                    if (prefab == null)
                        return;
                    foreach (var c in prefab.GetComponents<Component>()) {
                        if (c == null)
                            continue;
                        if (c.GetType().HasParent(typeof(T), allowSame: true)) {
                            instance._items.Add(c as T);
                            break;
                        }
                    }
                });
                instance.SetChanged();
            });
        }
#endif
    }

#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(DerivedPrefabSettings<,>), true)]
    public class DerivedPrefabSettingsEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            if (EditorGUIUtils.Button("update"))
                ReflectionUtils.CallMethod(target, "UpdateItems");
        }
    }
#endif
}