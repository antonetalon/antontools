﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
#endif

namespace AntonTools {
    public class ScriptableSingletonSettings : ScriptableObject {
        static string pathInEditor => $"{AntonToolsManager.GeneratedFolder}/Resources/ScriptableSingletonSettings.asset";
        static string pathInResources => $"ScriptableSingletonSettings";
        public List<ScriptableObject> items = new List<ScriptableObject>();
        static ScriptableSingletonSettings _instance;
        static ScriptableSingletonSettings Load() => Resources.Load<ScriptableSingletonSettings>(pathInResources);
        public static ScriptableSingletonSettings instance {
            get {
                if (_instance == null) {
                    _instance = Load();
#if UNITY_EDITOR
                    if (_instance == null)
                        _instance = ScriptableObjectUtils.CreateInstance<ScriptableSingletonSettings>(pathInEditor);
#endif
                }
                return _instance;
            }
        }
    }
}
