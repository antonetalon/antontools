﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AntonTools {
    public abstract class ScriptableSingleton<T> : ScriptableObject where T : ScriptableSingleton<T> {
        static T _instance;
        public static T instance {
            get {
                if (_instance == null) {
                    var name = typeof(T).Name;
                    _instance = ScriptableSingletonSettings.instance.items.Find(i => i?.name == name) as T;
#if UNITY_EDITOR
                    if (_instance == null) {
                        var script = AssetByTypeAndName.Find<MonoScript>(name, TypeFilterName.MonoScript, false);
                        var scriptPath = AssetDatabase.GetAssetPath(script);
                        var assetPath = scriptPath.Replace(".cs", ".asset");
                        _instance = ScriptableObjectUtils.CreateInstance<T>(assetPath);
                        ScriptableSingletonSettings.instance.items.RemoveAll(i => i==null || i.name == name);
                        ScriptableSingletonSettings.instance.items.Add(_instance);
                        EditorUtility.SetDirty(ScriptableSingletonSettings.instance);
                    }
#endif
                }
                return _instance;
            }
        }
    }
}