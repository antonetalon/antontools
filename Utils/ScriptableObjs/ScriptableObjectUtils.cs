﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AntonTools {
    public static class ScriptableObjectUtils {
#if UNITY_EDITOR
        public static T CreateInstance<T>(string path) where T : ScriptableObject {
            var inst = ScriptableObject.CreateInstance<T>();
            Utils.EnsureFolderExists(path);
            AssetDatabase.CreateAsset(inst, path);
            return inst;
        }
#endif
    }
}
