﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AntonTools {
    [Serializable]
    public abstract class SerializableDictionaryBase {
        public abstract int Count { get; }
        public abstract Type keyType { get; }
        public abstract Type valueType { get; }
        public abstract IList untypedKeys { get; }
        public abstract IList untypedValues { get; }
        public abstract void UpdateDict();
    }
    [Serializable]
    public abstract class SerializableDictionary<TKey, TValue> : SerializableDictionaryBase, ISerializationCallbackReceiver {
        public override int Count => dict.Keys.Count;
        public override Type keyType => typeof(TKey);
        public override Type valueType => typeof(TValue);
        public override IList untypedKeys => keys;
        public override IList untypedValues => values;
        [SerializeField] List<TKey> keys = new List<TKey>();
        [SerializeField] List<TValue> values = new List<TValue>();
        [NonSerialized] Dictionary<TKey, TValue> dict = new Dictionary<TKey, TValue>();
        public TValue this[TKey key] {
            get => dict[key];
            set => dict[key] = value;
        }
        void ISerializationCallbackReceiver.OnAfterDeserialize() => UpdateDict();
        public override void UpdateDict() {
            dict.Clear();
            for (int i = 0; i < keys.Count; i++) {
                if (!dict.ContainsKey(keys[i]))
                    dict.Add(keys[i], values[i]);
            }
        }
        void ISerializationCallbackReceiver.OnBeforeSerialize() {
            keys.Clear();
            values.Clear();
            foreach (var (key, val) in dict) {
                keys.Add(key);
                values.Add(val);
            }
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SerializableDictionaryBase), true)]
    public class SerializableDictionaryEditor : PropertyDrawer {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => 0;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var dict = property.GetTargetValue<SerializableDictionaryBase>();
            var tkey = dict.keyType;
            var tvalue = dict.valueType;
            var changed = false;
            var duplicateInd1 = -1;

            GUILayout.Label(property.name);
            EditorGUI.indentLevel++;
            for (int i = 0; i < dict.Count; i++) {
                GUILayout.BeginHorizontal();
                EditorGUIUtils.Indent();
                var key = dict.untypedKeys[i];
                if (EditorGUIUtils.GenericTypeField(null, tkey, ref key, ref changed))
                    dict.untypedKeys[i] = key;
                if (EditorGUIUtils.XButton()) {
                    changed = true;
                    dict.untypedKeys.RemoveAt(i);
                    dict.untypedValues.RemoveAt(i);
                    i--;
                }
                GUILayout.EndHorizontal();
                var value = dict.untypedValues[i];
                if (EditorGUIUtils.GenericTypeField(null, tvalue, ref value, ref changed))
                    dict.untypedValues[i] = value;

                // Duplicates.
                if (duplicateInd1 == -1) {
                    for (int j = 0; j < i; j++) {
                        if (dict.untypedKeys[j] == key) {
                            duplicateInd1 = i;
                            break;
                        }
                    }
                }
            }
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+")) {
                dict.untypedKeys.Add(Activator.CreateInstance(tkey));
                dict.untypedValues.Add(null);
                changed = true;
            }
            GUILayout.EndHorizontal();
            if (duplicateInd1 != -1)
                EditorGUIUtils.Error($"duplicate key {dict.untypedKeys[duplicateInd1]}");
            EditorGUI.indentLevel--;
            if (changed) {
                dict.UpdateDict();
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }
    }
#endif
}