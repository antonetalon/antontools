﻿using System;
using System.Collections.Generic;

namespace AntonTools {
    public static class Singletons {
        static Dictionary<Type, object> singletons = new Dictionary<Type, object>();
        public static T Get<T>() where T : class, new() => Get<T>(typeof(T));
        public static TParent Get<TParent>(Type type) where TParent : class {
            if (type == null) return null;
            if (singletons.TryGetValue(type, out var instance))
                return (TParent)instance;
            instance = Activator.CreateInstance(type);
            singletons.Add(type, instance);
            return (TParent)instance;
        }
        public static object Get(Type type) => Get<object>(type);
    }
}