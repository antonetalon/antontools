﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public static class SerializableParentUtils {
        public static void SaveToSerializable<T>(this List<T> list) where T : SerializableParent
            => list.ForEach(item => item.SaveToSerializable());
        public static object ParseType(string strValue, Type type) {
            object value = strValue;
            if (type.IsEnum)
                Utils.TryParseEnum(strValue, type, out value);
            else if (type == typeof(bool)) {
                bool.TryParse(strValue, out var valueBool);
                value = valueBool;
            } else if (type == typeof(int)) {
                int.TryParse(strValue, out var intVal);
                value = intVal;
            }
            return value;
        }
#if UNITY_EDITOR
        public static bool OnFieldGUI(string name, Type type, ref string fieldValue, ref bool changed) {
            if (type == typeof(bool)) {
                bool.TryParse(fieldValue, out var boolValue);
                if (EditorGUIUtils.Toggle(name, ref boolValue, ref changed)) {
                    fieldValue = boolValue.ToString();
                    return true;
                }
            } else if (type == typeof(int)) {
                int.TryParse(fieldValue, out var intValue);
                if (EditorGUIUtils.IntField(name, ref intValue, ref changed)) {
                    fieldValue = intValue.ToString();
                    return true;
                }
            } else if (type == typeof(float)) {
                float.TryParse(fieldValue, out var floatValue);
                if (EditorGUIUtils.FloatField(name, ref floatValue, ref changed)) {
                    fieldValue = floatValue.ToString();
                    return true;
                }
            } else if (type.IsEnum) {
                if (!Utils.TryParseEnum(fieldValue, type, out var enumValue))
                    enumValue = Activator.CreateInstance(type);
                if (EditorGUIUtils.Popup(name, type, ref enumValue, ref changed)) {
                    fieldValue = enumValue.ToString();
                    return true;
                }
            } else
                GUILayout.Label($"editing {type.Name} not implemented for field {name}");
            return false;
        }
#endif
    }
}
