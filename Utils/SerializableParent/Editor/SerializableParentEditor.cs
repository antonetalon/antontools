﻿#if UNITY_EDITOR
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;

namespace AntonTools {
    [CustomPropertyDrawer(typeof(SerializableParent<>), true)]
    public class SerializableParentEditor : PropertyDrawer {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => 0;
        const int indentValue = 10;
        static void GUILayoutIndent() => GUILayout.Space(EditorGUI.indentLevel * indentValue);
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var changed = false;
            EditorGUI.indentLevel++;
            //base.OnGUI(position, property, label);
            GUILayout.BeginHorizontal();
            GUILayoutIndent();
            //GUILayout.Label(property.propertyPath);
            //var battleParameters = property.serializedObject.targetObject as BattleParameters;
            var value = property.GetTargetValue<SerializableParent>();
            var (typeInd, types) = EditType(value, ref changed);
            GUILayout.EndHorizontal();

            EditParams(value, typeInd, types, ref changed);
            
            if (changed)
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            EditorGUI.indentLevel--;
        }

        public static (int typeInd, List<Type> types) EditType(SerializableParent value, ref bool changed) {
            var parentType = value.GetType();
            foreach (var (genericType, genericArgType) in parentType.GetGenericArgumentsRecursive()) {
                if (genericType.GetGenericTypeDefinition() == typeof(SerializableParent<>))
                    parentType = genericArgType;
            }
            var types = ReflectionUtils.GetAllDerivedTypes(parentType);
            var typeNames = types.ConvertAll(t => t.Name).ToArray();
            var typeInd = types.FindIndex(t => t.FullName == value.typeFullName);
            if (EditorGUIUtils.Popup($"type", ref typeInd, typeNames, ref changed)) {
                value.typeFullName = types[typeInd].FullName;
                value.parameters.Clear();
            }
            return (typeInd, types);
        }

        public static void EditParams(SerializableParent value, int typeInd, List<Type> types, ref bool changed) {
            if (typeInd != -1) {
                EditorGUI.indentLevel++;
                var type = types[typeInd];
                foreach (var field in SerializableParent.GetParamFields(type)) {
                    GUILayout.BeginHorizontal();
                    GUILayoutIndent();
                    var fieldValue = value.GetParam(field.Name);
                    if (SerializableParentUtils.OnFieldGUI(field.Name, field.FieldType, ref fieldValue, ref changed))
                        value.SetParam(field.Name, fieldValue.ToString());
                    GUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel--;
            }
        }
    }
}
#endif