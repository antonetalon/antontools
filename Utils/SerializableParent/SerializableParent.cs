﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace AntonTools {
    public abstract class SerializableParent {
        public string typeFullName;
        public List<SerializableParam> parameters = new List<SerializableParam>();

        public void SetParam(string name, string value) {
            var ind = parameters.FindIndex(p => p.name == name);
            if (ind == -1)
                parameters.Add(new SerializableParam { name = name, value = value });
            else
                parameters[ind].value = value;
        }

        public string GetParam(string name) => parameters.Find(p => p.name == name)?.value ?? null;

        #region Utils
        public static IEnumerable<FieldInfo> GetParamFields(Type type)
            => type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .Filter(field => field.DeclaringType == type);
        public void SaveToSerializable() {
            var type = GetType();
            foreach (var field in GetParamFields(type)) {
                var fieldValue = ReflectionUtils.GetField(this, field.Name);
                SetParam(field.Name, fieldValue?.ToString());
            }
        }
        #endregion
    }
    public abstract class SerializableParent<T> : SerializableParent where T : SerializableParent<T> {
        [NonSerialized] T _value;
        public T value {
            get {
                var type = ReflectionUtils.GetTypeByName(typeFullName, false, true);
                if (_value?.GetType() != type)
                    _value = (T)Activator.CreateInstance(type);
                parameters.ForEach(p => {
                    var fieldInfo = type.GetField(p.name);
                    if (fieldInfo == null)
                        throw new Exception();
                    var pType = fieldInfo.FieldType;
                    var value = SerializableParentUtils.ParseType(p.value, pType);
                    ReflectionUtils.SetField(_value, p.name, value);
                });
                return _value;
            }
        }
    }
    [Serializable] public class SerializableParam { public string name; public string value; }
}
