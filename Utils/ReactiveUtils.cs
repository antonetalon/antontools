﻿#if ZERGRUSH
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using ZergRush;
using ZergRush.ReactiveCore;

namespace AntonTools
{
    public static partial class Utils
    {
        public static IDisposable ListenUpdates(this TMP_InputField.OnChangeEvent onValueChanged, UnityAction<string> action) {
            onValueChanged.AddListener(action);
            return new AnonymousDisposable(() => onValueChanged.RemoveListener(action));
        }
        public static IDisposable ShowReactiveCollection<TModel, TView>(this ICell<IReadOnlyList<TModel>> models,
            List<TView> views, TView prefab, Action<TModel, TView> updateView, GameObject noItemsParent = null, int maxCount = -1,
            Action<TView> initView = null)
            where TView : MonoBehaviour {
            if (initView != null)
                views.ForEach(v => initView(v));
            return models.Bind(models => {
                var count = models?.Count ?? 0;
                if (maxCount != -1)
                    count = (count, maxCount).Min();
                Utils.UpdatePrefabsList(views, count, prefab, null, () => {
                    for (int i = 0; i < count; i++)
                        updateView(models[i], views[i]);
                }, noItemsParent: noItemsParent, onViewAdded: initView);
            });
        }
        public static IDisposable ShowReactiveCollection<TModel, TView>(this IReactiveCollection<TModel> models,
            List<TView> views, TView prefab, Action<TModel, TView> updateView, Transform viewsParent = null,
            bool findExistingItems = true, GameObject noEventsParent = null, Action<TView> initView = null) where TView : Component {

            var childIndexShift = prefab.transform.GetSiblingIndex();
            if (viewsParent == null)
                viewsParent = prefab.transform.parent;

            if (findExistingItems && views.Count == 0)
                viewsParent.GetComponentsInChildren<TView>(true).Foreach(ch => {
                    views.AddIfNotExists(ch);
                    initView?.Invoke(ch);
                    ch.gameObject.SetActive(false);
                });

            void AddElement(TModel newItem, int position) {
                var firstNotUsedView = views?.Find(v => !v.gameObject.activeSelf) ?? null;
                TView newView;
                if (firstNotUsedView != null) {
                    newView = firstNotUsedView;
                    views.RemoveLast();
                } else {
                    newView = MonoBehaviour.Instantiate(prefab, viewsParent);
                    initView?.Invoke(newView);
                }
                newView.transform.SetSiblingIndex(position + childIndexShift);
                newView.gameObject.SetActive(true);
                updateView(newItem, newView);
                views.Insert(position, newView);
            }
            var connections = new Connections(models.BindCollection(change => {
                switch (change.type) {
                    case ReactiveCollectionEventType.Insert:
                        AddElement(change.newItem, change.position);
                        break;
                    case ReactiveCollectionEventType.Remove:
                        var oldView = views[change.position];
                        oldView.gameObject.SetActive(false);
                        views.RemoveAt(change.position);
                        views.Add(oldView);
                        break;
                    case ReactiveCollectionEventType.Set:
                        var updatedView = views[change.position];
                        updateView(change.newItem, updatedView);
                        break;
                    case ReactiveCollectionEventType.Reset:
                        views.ForEach(v => v.gameObject.SetActive(false));
                        change.newData.ForEachWithInd((item, ind) => AddElement(item, ind));
                        break;
                }
            }));

            if (noEventsParent != null)
                connections += models.CountCell().Map(count => count == 0).Bind(isEmpty => noEventsParent.SetActive(isEmpty));

            return connections;
        }
        public static ICell<T> ToStaticCell<T>(this T value) => new StaticCell<T>(value);
        public static ICell<T2> MapNN<T, T2>(this ICell<T> cell, Func<T, T2> map, T2 def = default) where T : class
            => cell.MapWithDefaultIfNull(map, def);
        public static IReactiveCollection<T2> FlatMapCollectionNN<T, T2>(this ICell<T> cell, Func<T, IReactiveCollection<T2>> map)
            => cell.FlatMapCollectionWithDefaultOnNull(map);
        public static ICell<T2> FlatMapNN<T, T2>(this ICell<T> cell, Func<T, ICell<T2>> map) where T : class
            => cell.FlatMapWithDefaultOnNull(map);
        public static IDisposable BindNotNull<T>(this ICell<T> cell, Action<T> action) where T : class
            => cell.Bind(value => { if (value != null) action(value); });
        public static void SetMaxCount<T>(this ReactiveCollection<T> list, ICell<int> countCell, Func<T> getNew)
            => countCell.Bind(count => list.SetMaxCount(count, getNew));
        public static void SetMaxCount<T>(this ReactiveCollection<T> list, int count, Func<T> getNew) {
            if (list.Count == count)
                return;
            while (count > list.Count)
                list.Add(getNew());
        }
        public static void SetCount<T>(this ReactiveCollection<T> list, ICell<int> countCell, Func<T> getNew)
            => countCell.Bind(count => list.SetCount(count, getNew));
        public static void SetCount<T>(this ReactiveCollection<T> list, int count, Func<T> getNew) {
            if (list.Count == count)
                return;
            while (count < list.Count)
                list.RemoveAt(list.Count - 1);
            while (count > list.Count)
                list.Add(getNew());
        }
        public static IReactiveCollection<TRes> Merge<T1, T2, TRes>(this IReactiveCollection<T1> list1, IReactiveCollection<T2> list2)
            where T1 : TRes
            where T2 : TRes
            => list1.AsReadonlyCell().Merge(list2.AsReadonlyCell()).Map(i => {
                var resList = new List<TRes>();
                list1.ForEach(l => resList.Add(l));
                list2.ForEach(l => resList.Add(l));
                return resList;
            }).ToReactiveCollection();
        public static ICell<IReadOnlyList<T>> AsReadonlyCell<T>(this IReactiveCollection<T> collection)
            => collection.AsCell().Map(c => c.ConvertAll(i => i));
        public static bool Contains<T>(this IReadOnlyList<T> list, T item) {
            foreach (var i in list) {
                if (i.DefaultEquals(item))
                    return true;
            }
            return false;
        }
        public static bool Contains<T>(this IReactiveCollection<T> list, T item) {
            foreach (var i in list) {
                if (i.DefaultEquals(item))
                    return true;
            }
            return false;
        }
        public static bool Any<T>(this IReactiveCollection<T> list, Func<T, bool> check) {
            foreach (var i in list) {
                if (check(i))
                    return true;
            }
            return false;
        }
    }
}
#endif