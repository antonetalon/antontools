﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AntonTools {
    public static partial class Utils {
        public static string GetProjectFolder() => Path.GetDirectoryName(Application.dataPath);
        public static string[] GetFiles(string folder, string extension)
            => Directory.GetFiles(folder, $"*.{extension}", SearchOption.AllDirectories);
        public static void DeleteFolder(string folderPath) {
            if (!Directory.Exists(folderPath))
                return;
            var subDirs = Directory.GetDirectories(folderPath);
            foreach (var path in subDirs)
                DeleteFolder(path);
            var filePathes = Directory.GetFiles(folderPath);
            foreach (var path in filePathes)
                File.Delete(path);
            Directory.Delete(folderPath);
        }
        public static IEnumerable<string> EnumerateFiles(string dir) {
            if (Directory.Exists(dir))
                foreach (var f in Directory.EnumerateFiles(dir))
                    yield return f;
            else
                yield break;
        }
    }
}