﻿using MiniJSON;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AntonTools {
    public interface IJsonSerializable {
#if UNITY_2020_1_OR_NEWER
        long id => -1;
#else
        long id { get; }
#endif
        void Write(Dictionary<string, object> serialized, Dictionary<long, IJsonSerializable> existing);
        void Read(Dictionary<string, object> serialized, Dictionary<long, IJsonSerializable> existing);
    }
    public static partial class SerializationUtils {
        public static object ReadAllJson(string path) {
            if (!File.Exists(path))
                return null;
            var json = File.ReadAllText(path);
            var serialized = MiniJSON.Json.Deserialize(json);
            return serialized;
        }
        public static void WriteAllJson(string path, object serialized) {
            var json = Json.Serialize(serialized, pretty: true);
            Utils.EnsureFolderExists(path);
            File.WriteAllText(path, json, Encoding.UTF8);
        }
        public const string typeFullName = "TypeFullName";
        public const string idName = "id";
        public static void SaveToJsonFile(this IJsonSerializable obj, string filePath) {
            var json = obj.SaveToJsonString();
            Utils.EnsureFolderExists(Path.GetDirectoryName(filePath));
            File.WriteAllText(filePath, json);
        }
        public static string SaveToJsonString(this IJsonSerializable obj) {
            var serialized = new Dictionary<string, object>();
            var existing = new Dictionary<long, IJsonSerializable>();
            SaveToJson(obj, serialized, existing);
            var json = Json.Serialize(serialized, pretty: true);
            return json;
        }
        /// <summary>
        /// Obj cant be null, but can be already serialized.
        /// </summary>
        public static void SaveToJson(this IJsonSerializable obj, Dictionary<string, object> serialized, Dictionary<long, IJsonSerializable> existing) {
            if (obj.id != -1) {
                serialized.Add(idName, obj.id);
                if (existing != null) {
                    if (existing.ContainsKey(obj.id))
                        return;
                    existing.Add(obj.id, obj);
                }
            }
            serialized.Add(typeFullName, obj.GetType().NameInCode());
            obj.Write(serialized, existing);
        }
        /// <summary>
        /// Obj can be null, can be already serialized.
        /// </summary>
        public static Dictionary<string, object> SaveToJson(this IJsonSerializable obj, Dictionary<long, IJsonSerializable> existing) {
            if (obj == null) return null;
            var serialized = new Dictionary<string, object>();
            obj.SaveToJson(serialized, existing);
            return serialized;
        }
        public static IJsonSerializable LoadFromJsonFile(string filePath) {
            var json = File.ReadAllText(filePath);
            return LoadFromJsonString(json);
        }
        public static IJsonSerializable LoadFromJsonString(string json) {
            if (json.IsNothing())
                return null;
            var serialized = Json.Deserialize(json) as Dictionary<string, object>;
            var existing = new Dictionary<long, IJsonSerializable>();
            var instance = LoadFromJson(serialized, existing);
            return instance;
        }
        public static void LoadFromJsonString<T>(string json, T instance) where T : IJsonSerializable {
            var existing = new Dictionary<long, IJsonSerializable>();
            LoadFromJsonString(json, instance, existing);
        }
        public static void LoadFromJsonString<T>(string json, T instance, Dictionary<long, IJsonSerializable> existing) where T : IJsonSerializable {
            if (json.IsNothing())
                return;
            var serialized = Json.Deserialize(json) as Dictionary<string, object>;
            instance.Read(serialized, existing);
        }
        /// <summary>
        /// Reads id and instance type from json, creates class. 
        /// Does not read properties.
        /// Serialized can be null, can be already deserialized.
        /// </summary>
        public static (IJsonSerializable instance, bool needsReadingProperties)
            CreateInstanceFromJson(Dictionary<string, object> serialized, Dictionary<long, IJsonSerializable> existing) {

            if (serialized == null) return (null, false);
            long id = -1;
            if (serialized.TryGetValue(idName, out var idObj))
                id = Convert.ToInt64(idObj);
            if (id != -1) {
                if (existing != null && existing.TryGetValue(id, out var existingInstance))
                    return (existingInstance, false);
            }
            var typeName = serialized[typeFullName] as string;
            var type = ReflectionUtils.GetTypeByName(typeName, fullName: true);
            var instance = Activator.CreateInstance(type) as IJsonSerializable;
            if (id != -1) {
                var prop = ReflectionUtils.GetPropertyInfoMostParent(type, idName);
                if (prop != null)
                    prop.SetValue(instance, id);
                existing.Add(id, instance);
            }
            return (instance, true);
        }
        /// <summary>
        /// serialized can be null, can be already deserialized.
        /// </summary>
        public static IJsonSerializable LoadFromJson(Dictionary<string, object> serialized, Dictionary<long, IJsonSerializable> existing) {
            if (existing == null)
                existing = new Dictionary<long, IJsonSerializable>();
            var (instance, needsReadingProperties) = CreateInstanceFromJson(serialized, existing);
            if (needsReadingProperties)
                instance.Read(serialized, existing);
            return instance;
        }
        public static IList<object> SaveCollectionToJson<T>(this IEnumerable<T> items, Action<(T item, List<object> itemsSerialized)> saveItem) {
            var serialized = new List<object>();
            if (items != null) {
                foreach (var item in items)
                    saveItem((item, serialized));
            }
            return serialized;
        }
        public static List<T> LoadCollectionFromJson<T>(IList<object> serialized, Func<object, T> loadItem) {
            var items = new List<T>();
            foreach (object itemSerialized in serialized) {
                var item = loadItem(itemSerialized);
                items.Add(item);
            }
            return items;
        }
        public static IList<object> SaveCollectionToJson<T>(IEnumerable<T> items, Dictionary<long, IJsonSerializable> existing) where T : IJsonSerializable
            => SaveCollectionToJson(items, i => {
                var itemSerialized = new Dictionary<string, object>();
                SaveToJson(i.item, itemSerialized, existing);
                i.itemsSerialized.Add(itemSerialized);
            });
        public static List<T> LoadCollectionFromJson<T>(IList<object> serialized, Dictionary<long, IJsonSerializable> existing) where T : IJsonSerializable
            => LoadCollectionFromJson(serialized, itemSerialized => (T)LoadFromJson(itemSerialized as Dictionary<string, object>, existing));
        public static List<T> LoadCollectionFromJson<T>(Dictionary<string, object> parentDict, string key, Dictionary<long, IJsonSerializable> existing)
             where T : IJsonSerializable {
            var listSerialized = parentDict != null ? (parentDict.GetValueOrDefault(key) as IList<object>) : null;
            var list = listSerialized != null ? LoadCollectionFromJson<T>(listSerialized, existing) : new List<T>();
            return list;
        }
    }
}