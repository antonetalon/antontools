﻿using UnityEngine;

namespace AntonTools {
    public static partial class Utils {
        public static Matrix4x4 RotateAround(Vector3 axis, Vector3 center, float degrees)
            => Matrix4x4.Translate(center) * Matrix4x4.Rotate(Quaternion.AngleAxis(degrees, axis)) * Matrix4x4.Translate(-center);
        public static Vector3 GetLocalX(this Matrix4x4 matrix)
            => new Vector3(matrix.m00, matrix.m10, matrix.m20);
        public static Vector3 GetLocalY(this Matrix4x4 matrix)
            => new Vector3(matrix.m01, matrix.m11, matrix.m21);
        public static Vector3 GetLocalZ(this Matrix4x4 matrix)
            => new Vector3(matrix.m02, matrix.m12, matrix.m22);

        public static Quaternion ExtractRotation(this Matrix4x4 matrix) {
            var forward = matrix.GetLocalZ();
            var upwards = matrix.GetLocalY();
            return Quaternion.LookRotation(forward, upwards);
        }

        public static Vector3 ExtractPosition(this Matrix4x4 matrix) {
            Vector3 position;
            position.x = matrix.m03;
            position.y = matrix.m13;
            position.z = matrix.m23;
            return position;
        }

        public static Vector3 ExtractScale(this Matrix4x4 matrix) {
            var x = matrix.GetLocalX();
            var y = matrix.GetLocalY();
            var z = matrix.GetLocalZ();
            var xL = x.magnitude;
            var yL = y.magnitude;
            var zL = z.magnitude;
            var zx = Vector3.Cross(z, x);
            var dot = Vector3.Dot(zx, y);
            return new Vector3(xL * dot.Sign(), yL, zL);
        }
    }
}