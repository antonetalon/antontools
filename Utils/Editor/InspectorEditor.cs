﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace AntonTools {
    public abstract class InspectorEditor<T> : Editor where T : Object { // Object should be either Component or ScriptableObject.
        protected T tgt => (T)target;
        List<T> _tgts;
        protected List<T> tgts => _tgts ?? (_tgts = targets.ConvertAll(t => (T)t));
        public override void OnInspectorGUI() {
            var changed = false;
            OnInspectorGUI(ref changed);
            if (changed)
                targets.Foreach(t => t.SetChanged());
        }
        protected abstract void OnInspectorGUI(ref bool changed);
    }
}
#endif