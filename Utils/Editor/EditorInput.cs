﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public static class EditorInput {
        static HashSet<KeyCode> keysDown = new HashSet<KeyCode>();
        public static void Update() {
            if (Event.current.type == EventType.KeyDown)
                keysDown.Add(Event.current.keyCode);
            if (Event.current.type == EventType.KeyUp)
                keysDown.Remove(Event.current.keyCode);
        }
        public static bool GetKey(KeyCode key) {
            Update();
            return keysDown.Contains(key);
        }
        public static bool GetKeyUp(KeyCode key)  {
            Update();
            return Event.current.keyCode == key && Event.current.type == EventType.KeyUp;
        }
        public static bool GetKeyDown(KeyCode key) {
            Update();
            return Event.current.keyCode == key && Event.current.type == EventType.KeyDown;
        }
    }
}
#endif