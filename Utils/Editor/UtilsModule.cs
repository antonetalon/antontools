﻿#if UNITY_EDITOR
namespace AntonTools {
    public class UtilsModule : RootModule {
        public override HowToModule HowTo() => new UtilsModule_HowTo();

        protected override void OnCompiledGUI() {
            base.OnCompiledGUI();
            FrameRateModuleUI.Show();
        }
    }
}
#endif