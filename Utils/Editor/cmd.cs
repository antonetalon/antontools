﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace AntonTools {
    public static class cmd {
        public static bool isExecuting { get; private set; }
        static readonly ProcessLauncher process = new ProcessLauncher();
        public static bool logging {
            get => process.logging;
            set => process.logging = value;
        }
        public static async Task<string> Executing(string command, string workingDirectory = null) {
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                return await Executing("powershell.exe", $"{command}", workingDirectory: workingDirectory);
            else
                return await Executing("/bin/bash", " -c \"" + command + " \"", workingDirectory: workingDirectory);
        }
        public static async Task<string> Executing(string program, string args, 
            WhatToDoIfInProcess doToPrevProcess = WhatToDoIfInProcess.WaitPrevious, string workingDirectory = null,
            Action<string> onOutputLineReceived = null, Action<string> onErrorLineReceived = null) {
            isExecuting = true;
            var prefix = $"{program} ";
            if (args.StartsWith(prefix))
                args = args.Remove(0, prefix.Length);
            var (success, output, error) = await process.Execute(program, args, doToPrevProcess: doToPrevProcess,
                workingDirectory: workingDirectory, onOutputLineReceived:onOutputLineReceived, onErrorLineReceived:onErrorLineReceived);
            isExecuting = false;
            //Debug.Log($"end, success = {success}\noutput={output}\nerror={error}");
            return output;
        }
        public static void OpenCMD(string path) {
            if (path.IsSomething())
                Executing($"wt -d {path}").WrapErrors();
            else
                Executing($"wt").WrapErrors();
        }
    }
}