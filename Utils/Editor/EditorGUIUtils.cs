﻿#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace AntonTools
{
    public static class EditorGUIUtils {

        public static void ExtendableEnum(string title,
            ref string value, ref List<string> options, ref bool changed, int controlId, float labelWidth = -1) {

            CheckUIControlFocusedStart();
            var valueExists = options.Contains(value);
            if (!valueExists)
                SetBackgroundColor(Color.red);
            if (TextField(title, ref value, ref changed, labelWidth: labelWidth))
                ExtendablePopupOptions.instance.Repaint();
            if (!valueExists)
                RestoreBackgroundColor();
            var focused = CheckUIControlFocusedEnd();
            if (Event.current.type == EventType.Repaint) {
                var closeCurrWindow = !focused && ExtendablePopupOptions.shown && (controlId == ExtendablePopupOptions.controlId);
                var openCurrWindow = focused && !ExtendablePopupOptions.shown;
                var changeWindowToCurr = focused && ExtendablePopupOptions.shown && (controlId != ExtendablePopupOptions.controlId);
                if (changeWindowToCurr) {
                    closeCurrWindow = true;
                    openCurrWindow = true;
                }
                if (closeCurrWindow)
                    ExtendablePopupOptions.CloseWindow();
                if (openCurrWindow) {
                    ExtendablePopupOptions.ShowWindow(controlId);
                    GUI.FocusControl(nextControlId);
                }
            }
            if (ExtendablePopupOptions.shown && controlId == ExtendablePopupOptions.controlId)
                ExtendablePopupOptions.OnInspectorGUI(ref value, options, ref changed);
        }

        public static bool CheckUIControlFocused(Action drawControl) {
            CheckUIControlFocusedStart();
            drawControl?.Invoke();
            return CheckUIControlFocusedEnd();
        }
        static string nextControlId;
        public static void CheckUIControlFocusedStart() => GUI.SetNextControlName(nextControlId = Utils.RandomLong().ToString());
        public static bool CheckUIControlFocusedEnd() => GUI.GetNameOfFocusedControl() == nextControlId;
        public static void BeginHorizontal() => EditorGUILayout.BeginHorizontal();
        public static void EndHorizontal() => EditorGUILayout.EndHorizontal();
        public static void BeginVertical() => EditorGUILayout.BeginVertical();
        public static void EndVertical() => EditorGUILayout.EndVertical();
        public static bool FolderField(string title, ref UnityEngine.Object value, ref bool changed, float width = -1, float labelWidth = -1) {
            UnityEngine.Object newValue = null;
            var _value = value;
            WithLabelWidth(() => {
                if (width < 0)
                    newValue = EditorGUILayout.ObjectField(title, _value, typeof(DefaultAsset), false);
                else
                    newValue = EditorGUILayout.ObjectField(title, _value, typeof(DefaultAsset), false, GUILayout.Width(width));
            }, labelWidth);
            if (newValue != null) {
                var path = AssetDatabase.GetAssetPath(newValue);
                if (!Directory.Exists(path))
                    newValue = null;
            }
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool ColorField<T>(string title, IEnumerable<T> items, Func<T, Color> get, Action<T, Color> set,
            ref bool changed, float width = -1) {
            var col = Color.black;
            var exists = false;
            var same = true;
            foreach (var item in items) {
                var currCol = get(item);
                if (exists && currCol != col) {
                    same = false;
                    break;
                }
                col = currCol;
                exists = true;
            }
            EditorGUI.showMixedValue = !same;
            var edited = ColorField(title, ref col, ref changed, width);
            EditorGUI.showMixedValue = false;
            if (edited)
                items.ForEach(i => set(i, col));
            return edited;
        }
        public static bool ColorField(string title, ref Color value, ref bool changed, float width = -1) {
            Color newValue;
            if (width < 0)
                newValue = EditorGUILayout.ColorField(title, value);
            else
                newValue = EditorGUILayout.ColorField(title, value, GUILayout.Width(width));
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool FloatField(string title, ref float value, ref bool changed, float width = -1, float labelWidth = -1) {
            float newValue = default;
            var _value = value;
            WithLabelWidth(() => {
                if (width < 0)
                    newValue = EditorGUILayout.FloatField(title, _value);
                else
                    newValue = EditorGUILayout.FloatField(title, _value, GUILayout.Width(width));
            }, labelWidth);
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool ProbabilityField(string title, ref float value, ref bool changed, float labelWidth = -1) {
            BeginHorizontal();
            const float fieldWidth = 200;
            var percents = value * 100;
            var currChanged = FloatField(title, ref percents, ref changed, fieldWidth, labelWidth);
            Label("%");
            EndHorizontal();
            if (currChanged)
                value = Mathf.Clamp01(percents * 0.01f);
            return currChanged;
        }
        public static bool PercentField(string title, ref float value, ref bool changed, float labelWidth = -1) {
            BeginHorizontal();
            const float fieldWidth = 200;
            var percents = value * 100;
            var currChanged = FloatField(title, ref percents, ref changed, fieldWidth, labelWidth);
            Label("%");
            EndHorizontal();
            if (currChanged)
                value = percents * 0.01f;
            return currChanged;
        }
        public static bool DoubleField(string title, ref double value, ref bool changed, float width = -1, float labelWidth = -1) {
            double newValue = default;
            var _value = value;
            WithLabelWidth(() => {
                if (width < 0)
                    newValue = EditorGUILayout.DoubleField(title, _value);
                else
                    newValue = EditorGUILayout.DoubleField(title, _value, GUILayout.Width(width));
            }, labelWidth);
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static void TextFieldReadOnly(string title, string value, float width = -1, float height = -1, float labelWidth = -1) {
            PushDisabling();
            var changed = false;
            TextField(title, ref value, ref changed, width, height, labelWidth);
            PopEnabling();
        }
        public static bool TextField(string title, ref string value, ref bool changed, float width = -1, float height = -1, float labelWidth = -1) {
            string newValue = null;
            var val = value;
            WithLabelWidth(() => {
                if (width < 0 && height < 0)
                    newValue = EditorGUILayout.TextField(title, val);
                else if (width >= 0 && height < 0)
                    newValue = EditorGUILayout.TextField(title, val, GUILayout.Width(width));
                else if (width < 0 && height >= 0)
                    newValue = EditorGUILayout.TextField(title, val, GUILayout.Height(height));
                else
                    newValue = EditorGUILayout.TextField(title, val, GUILayout.Width(width), GUILayout.Height(height));
            }, labelWidth);
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool TextAreaSelectable(string title, ref string value, ref bool changed, out TextEditor seleciton,
            float width = -1, float height = -1, float labelWidth = -1) {

            string newValue = null;
            var val = value;
            BeginHorizontal();
            if (title.IsSomething())
                Label(title, labelWidth);
            var focused = CheckUIControlFocused(() => {
                WithLabelWidth(() => {
                    if (width < 0 && height < 0)
                        newValue = GUILayout.TextArea(val);
                    else if (width >= 0 && height < 0)
                        newValue = GUILayout.TextArea(val, GUILayout.Width(width));
                    else if (width < 0 && height >= 0)
                        newValue = GUILayout.TextArea(val, GUILayout.Height(height));
                    else
                        newValue = GUILayout.TextArea(val, GUILayout.Width(width), GUILayout.Height(height));
                }, labelWidth);
            });
            EndHorizontal();
            seleciton = focused ? (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl) : null;
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        static GUIStyle textAreaStyle = new GUIStyle(EditorStyles.textArea);
        public static bool TextArea(string title, ref string value, ref bool changed, float width = -1, float height = -1, float labelWidth = 200, bool wordWrap = true) {
            if (textAreaStyle == null)
                textAreaStyle = new GUIStyle(EditorStyles.textArea);
            textAreaStyle.wordWrap = wordWrap;
            string newValue;
            GUILayout.BeginHorizontal();
            if (!string.IsNullOrEmpty(title))
                Label(title, labelWidth);
            if (width < 0 && height < 0)
                newValue = EditorGUILayout.TextArea(value, textAreaStyle);
            else if (width >= 0 && height < 0)
                newValue = EditorGUILayout.TextArea(value, textAreaStyle, GUILayout.Width(width));
            else if (width < 0 && height >= 0)
                newValue = EditorGUILayout.TextArea(value, textAreaStyle, GUILayout.Height(height));
            else
                newValue = EditorGUILayout.TextArea(value, textAreaStyle, GUILayout.Width(width), GUILayout.Height(height));
            GUILayout.EndHorizontal();
            if (newValue == value)
                return false;
            changed = true;
            value = newValue.ToLf();
            return true;
        }
        public static bool LayerField(string title, ref int value, ref bool changed, float width = -1) {
            int newValue;
            if (width < 0)
                newValue = EditorGUILayout.LayerField(title, value);
            else
                newValue = EditorGUILayout.LayerField(title, value, GUILayout.Width(width));
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool IntField(string title, ref int value, ref bool changed, float width = -1, float labelWidth = -1) {
            var value1 = value;
            int newValue = -1;
            WithLabelWidth(() => {
                if (width < 0)
                    newValue = EditorGUILayout.IntField(title, value1);
                else
                    newValue = EditorGUILayout.IntField(title, value1, GUILayout.Width(width));
            }, labelWidth);
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool LongField(string title, ref long value, ref bool changed, float width = -1, float labelWidth = -1) {
            var value1 = value;
            long newValue = -1;
            WithLabelWidth(() => {
                if (width < 0)
                    newValue = EditorGUILayout.LongField(title, value1);
                else
                    newValue = EditorGUILayout.LongField(title, value1, GUILayout.Width(width));
            }, labelWidth);
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool MultipleItemsInspectorObjectField<TItem, TObject>(string title,
            IEnumerable<TItem> items, Func<TItem, TObject> get, Action<TItem, TObject> set,
            ref bool changed, float width = -1, bool allowSceneObjects = true)
            where TObject : UnityEngine.Object {

            TObject obj = null;
            var exists = false;
            var same = true;
            foreach (var item in items) {
                var currObj = get(item);
                if (exists && currObj != obj) {
                    same = false;
                    obj = null;
                    break;
                }
                obj = currObj;
                exists = true;
            }
            EditorGUI.showMixedValue = !same;
            var edited = TypedField(title, ref obj, ref changed, width, -1, allowSceneObjects);
            EditorGUI.showMixedValue = false;
            if (edited)
                items.ForEach(item => set(item, obj));
            return edited;
        }
        static bool ObjectField(string title, ref UnityEngine.Object value, Type t, bool allowSceneObjects, ref bool changed, float width = -1, float labelWidth = -1) {
            UnityEngine.Object val = value;
            WithLabelWidth(() => {
                if (width < 0)
                    val = EditorGUILayout.ObjectField(title, val, t, allowSceneObjects);
                else
                    val = EditorGUILayout.ObjectField(title, val, t, allowSceneObjects, GUILayout.Width(width));
            }, labelWidth);
            var currChanged = val != value;
            value = val;
            changed |= currChanged;
            return changed;
        }
        static bool EnumField(string title, ref object value, Type t, bool allowSceneObjects, ref bool changed, float width = -1, float labelWidth = -1) {
            BeginHorizontal();
            Indent();
            BeginVertical();
            foreach (var fieldInfo in t.GetFields(BindingFlags.Instance | BindingFlags.Public)) {
                var fieldValue = fieldInfo.GetValue(value);
                if (GenericTypeField(fieldInfo.Name, fieldInfo.FieldType, ref fieldValue, ref changed)) {
                    fieldInfo.SetValue(value, fieldValue);
                }
            }
            EndVertical();
            EndHorizontal();
            return changed;
        }
        public static bool TypedField(string title, ref UnityEngine.Object value, Type T, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true) 
            => ObjectField(title, ref value, T, allowSceneObjects, ref changed, width, labelWidth);
        public static bool TypedField<T>(string title, ref T value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            where T : UnityEngine.Object {
            var objVal = value as UnityEngine.Object;
            var currChanged = ObjectField(title, ref objVal, typeof(T), allowSceneObjects, ref changed, width, labelWidth);
            value = objVal as T;
            return currChanged;
        }
        public static bool GameObjectField(string title, ref GameObject value, ref bool changed,
            float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);
        public static bool ScriptField(string title, ref MonoScript value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);
        public static bool PrefabField<T>(string title, ref T value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = false) where T : UnityEngine.Object
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);
        public static bool SpriteField(string title, ref Sprite value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);
        public static bool FontField(string title, ref TMP_FontAsset value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);
        public static bool TransformField(string title, ref Transform value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);
        public static bool MeshFilterField(string title, ref MeshFilter value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);
        public static bool MaterialField(string title, ref Material value, ref bool changed, float width = -1, float labelWidth = -1, bool allowSceneObjects = true)
            => TypedField(title, ref value, ref changed, width, labelWidth, allowSceneObjects);

        public static class DerivedTypes<TBase> {
            static void InitTypes(Func<Type, string> getTypeName) {
                if (getTypeName == null)
                    getTypeName = t => t.Name;
                var currTypes = new List<Type>();
                ReflectionUtils.ForEachDerivedType<TBase>(type => {
                    if (!type.CanCreateInstance()) return;
                    currTypes.Add(type);
                });
                types = currTypes.ToArray();
                typesStrings = currTypes.ConvertAll(getTypeName).ToArray();
            }
            static string[] typesStrings;
            static Type[] types;
            public static bool TypePopup(string title, ref Type value, ref bool changed, Func<Type, string> getTypeName = null, float width = -1, float labelWidth = -1) {
                if (typesStrings == null)
                    InitTypes(getTypeName);
                var selectedStepTypeInd = value == null ? -1 : types.IndexOf(value);
                if (EditorGUIUtils.Popup(title, ref selectedStepTypeInd, typesStrings, ref changed, width, labelWidth)) {
                    value = types[selectedStepTypeInd];
                    return true;
                }
                return false;
            }
            public static bool InstancePopup(string title, ref TBase instance, ref bool changed, Func<Type, string> getTypeName = null, float width = -1, float labelWidth = -1) {
                var type = instance?.GetType();
                if (TypePopup(title, ref type, ref changed, getTypeName, width, labelWidth)) {
                    if (instance?.GetType() != type)
                        instance = (TBase)Activator.CreateInstance(type);
                    return true;
                }
                return false;
            }
        }
        public static void HorizontalLine(int height = 1) {
            var rect = EditorGUILayout.GetControlRect(false, height);
            rect.height = height;
            rect.center += Vector2.up * 10;
            EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
        }
        [Obsolete]
        public static bool EditList<T>(string title, List<T> items, ref bool changed, ref bool unfolded, 
            Func<T> createNew = null, Action<T> removeOld = null) where T : MonoBehaviour {

            BeginVertical();
            BeginHorizontal();
            ShowOpenClose(ref unfolded);
            var count = items.Count;
            var currChanged = IntField(title, ref count, ref changed);
            items.SetCount(count, createNew, removeOld);
            EndHorizontal();
            if (unfolded) {
                for (int i = 0; i < items.Count; i++) {
                    BeginHorizontal();
                    var item = items[i];
                    var itemChanged = TypedField("", ref item, ref changed);
                    currChanged |= itemChanged;
                    if (itemChanged)
                        items[i] = item;
                    EndHorizontal();
                }
                if (createNew != null && Button("add", ref changed))
                    items.Add(createNew());
            }
            EndVertical();
            return currChanged;
        }
        public static bool EditItemsList<T>(IList items, ref bool changed, Func<object> createNewItem) {
            var unfoldedInd = -1;
            return EditItemsList(items, ref changed, ref unfoldedInd, ("add", createNewItem),
                (int i, ref bool ch) => {
                    object item = items[i];
                    if (GenericTypeField("", typeof(T), ref item, ref ch))
                        items[i] = (T)item;
                },
            null,
            100, false, false, false, null, null);
        }
        public static bool EditItemsList(IList items, ref int unfoldedInd, ref bool changed, (string addItemButton, Func<object> newItem) adding,
            ValRefAction<int, bool> onEditItemTitleGUI, ValRefAction<int, bool> onEditItemGUI,
            int buttonWidth = 100, bool insertInTheMiddle = false, bool horizontalLines = true, bool order = true, Action<object> onItemRemoved = null) {
            return EditItemsList(items, ref changed, ref unfoldedInd, adding,
                onEditItemTitleGUI, onEditItemGUI, buttonWidth, insertInTheMiddle, horizontalLines, order, onItemRemoved);
        }
        public static void LoseFocus() => EditorGUI.FocusTextInControl(null);
        static string searchedText = "";
        public static bool EditItemsList(IList items, ref bool changed, ref int unfoldedInd, (string addItemButton, Func<object> newItem) adding, 
            ValRefAction<int, bool> onEditItemTitleGUI, ValRefAction<int, bool> onEditItemGUI, 
            int buttonWidth = 100, bool insertInTheMiddle = false, bool horizontalLines = true, bool order = true,
            Action<object> onItemRemoved = null, Func<int, bool> filterShown = null, Func<string, object, bool> search = null, Func<int, bool> canRemove = null) {

            if (search != null) {
                BeginHorizontal();
                ShowSearchIco();
                TextField("", ref searchedText, ref changed);
                if (XButton(ref changed)) {
                    searchedText = string.Empty;
                    LoseFocus();
                }
                EndHorizontal();
                if (searchedText.IsSomething()) {
                    var foundCount = items.Count(i => search(searchedText, i));
                    RichMultilineLabel($"Showing search results, found <b>{foundCount}/{items.Count}</b> items");
                }
            }

            var currChanged = false;
            var first = true;
            for (int i = -1; i < items.Count; i++) {
                if (i >= 0) {
                    if (filterShown != null && !filterShown(i))
                        continue;
                    if (search != null && searchedText.IsSomething() && !search(searchedText, items[i]))
                        continue;
                }
                if (!first) {
                    GUILayout.Space(5);
                    if (horizontalLines) {
                        HorizontalLine();
                        GUILayout.Space(10);
                    }
                    GUILayout.Space(5);
                    OnEditItemGUI(i, items, ref currChanged, ref unfoldedInd, onEditItemTitleGUI, onEditItemGUI, 20, order, onItemRemoved, canRemove);
                }
                first = false;
                if (insertInTheMiddle) {
                    if (horizontalLines) {
                        GUILayout.Space(5);
                        HorizontalLine();
                        GUILayout.Space(15);
                    }
                    ShowAddButton(i);
                }
            }
            if (!insertInTheMiddle)
                ShowAddButton(items.LastInd());
            void ShowAddButton(int i) {
                if (adding != default) {
                    GUILayout.BeginHorizontal();
                    if (Button(adding.addItemButton, ref currChanged, buttonWidth))
                        items.Insert(i + 1, adding.newItem());
                    GUILayout.EndHorizontal();
                }
            }
            changed |= currChanged;
            return currChanged;
        }
        private static void OnEditItemGUI(int ind, IList items, ref bool changed, ref int unfoldedInd,
            ValRefAction<int, bool> onEditItemTitleGUI, ValRefAction<int, bool> onEditItemGUI, int buttonWidth, bool order, 
            Action<object> onItemRemoved = null, Func<int, bool> canRemove = null) {

            var currStep = items[ind];

            GUILayout.BeginHorizontal();
            var openingPossible = onEditItemGUI != null;
            var opened = true;
            if (openingPossible) {
                if (unfoldedInd != int.MinValue) {
                    opened = unfoldedInd == ind;
                    if (ShowOpenClose(ref opened)) {
                        if (opened)
                            unfoldedInd = ind;
                        else
                            unfoldedInd = -1;
                    }
                }
            } else
                opened = false;
            onEditItemTitleGUI?.Invoke(ind, ref changed);

            if (order) {
                PushEnabling(ind > 0);
                if (Button("/\\", buttonWidth))
                    items.Swap(ind, ind - 1);
                PopEnabling();

                PushEnabling(ind < items.Count - 1);
                if (Button("\\/", buttonWidth))
                    items.Swap(ind, ind + 1);
                PopEnabling();
            }

            var removed = false;
            PushEnabling(canRemove == null || canRemove(ind));
            if (Button("X", ref removed, buttonWidth)) {
                var oldItem = items[ind];
                items.RemoveAt(ind);
                onItemRemoved?.Invoke(oldItem);
            }
            PopEnabling();

            GUILayout.EndHorizontal();

            if (removed || !opened) return;
            BeginHorizontal();
            Indent();
            BeginVertical();
            onEditItemGUI?.Invoke(ind, ref changed);
            EndVertical();
            EndHorizontal();
        }
        public static object DoWithTargetValue(this SerializedProperty property, bool setValue, object valueToSet) {
            object tgt = property.serializedObject.targetObject;
            var path = property.propertyPath.Split('.');
            var pathInd = 0;
            while (pathInd < path.Length) {
                var currPathName = path[pathInd];
                if (currPathName == "Array") {
                    pathInd++; currPathName = path[pathInd];
                    var arrayInd = int.Parse(currPathName.Replace("data[", "").Replace("]", ""));
                    if (pathInd == path.LastInd() && setValue)
                        ReflectionUtils.SetAtIndex(tgt, arrayInd, valueToSet);
                    tgt = ReflectionUtils.GetAtIndex(tgt, arrayInd);
                } else {
                    if (pathInd == path.LastInd() && setValue)
                        ReflectionUtils.SetField(tgt, currPathName, valueToSet);
                    tgt = ReflectionUtils.GetField(tgt, currPathName);
                }
                pathInd++;
            }
            return tgt;
        }
        public static object GetTargetValue(this SerializedProperty property)
            => DoWithTargetValue(property, false, null);
        public static void SetTargetValue(this SerializedProperty property, object value)
            => DoWithTargetValue(property, true, value);
        public static T GetTargetValue<T>(this SerializedProperty property) => (T)GetTargetValue(property);
        public static void SetTargetValue<T>(this SerializedProperty property, T value) => SetTargetValue(property, (object)value);
        public static void DoIfConfirmed(string confirmationQuestion, Action onConfirmed) {
            if (EditorUtility.DisplayDialog("Are you sure?", confirmationQuestion, "ok", "cancel"))
                onConfirmed.Invoke();
        }
        public static void ShowInfoWindow(string message)
            => EditorUtility.DisplayDialog("Info", message, "ok");
        public static Color red = new Color(1, 0, 0, 1);
        public static Color gray = Color.gray;
        public static Color green = new Color(0, 0.7f, 0, 1);
        public static Color warningColor = new Color(0.3f, 0.3f, 0, 1);
        public static void Error(string error) => WithLabelTextColor(red, () => RichMultilineLabel(error));
        public static void WithLabelTextColor(Color col, Action action) {
            var prevCol = EditorStyles.label.normal.textColor;
            EditorStyles.label.normal.textColor = col;
            action?.Invoke();
            EditorStyles.label.normal.textColor = prevCol;
        }
        public static void WithButtonTextColor(Color col, Action action) {
            var prevCol = EditorStyles.label.normal.textColor;
            GUI.skin.button.normal.textColor = col;
            action?.Invoke();
            GUI.skin.button.normal.textColor = prevCol;
        }
        static Stack<Color> backColorsStack;
        public static void SetBackgroundColor(Color col) {
            if (backColorsStack == null) backColorsStack = new Stack<Color>();
            backColorsStack.Push(GUI.backgroundColor);
            GUI.backgroundColor = col;
        }
        public static void RestoreBackgroundColor() {
            GUI.backgroundColor = backColorsStack.Pop();
        }
        public static void WithBackgoundColor(Color col, Action action) {
            SetBackgroundColor(col);
            action?.Invoke();
            RestoreBackgroundColor();
        }
        public static void InHorizontal(Action action) {
            GUILayout.BeginHorizontal();
            action?.Invoke();
            GUILayout.EndHorizontal();
        }
        public static void Label(string text, float width = -1) {
            if (width == -1)
                GUILayout.Label(text);
            else
                GUILayout.Label(text, GUILayout.Width(width));
        }
        public static void ColoredLabel(string text, Color col, float width = -1)
            => WithLabelTextColor(col, () => RichMultilineLabel(text, width));
        public static void RichLabel(string str, TextAnchor anchor = TextAnchor.MiddleLeft,
            bool eatAllWidth = false, bool eatAllHeight = false, Color col = new Color(),
            FontStyle fontStyle = FontStyle.Normal, bool italics = false, float width = -1, bool richText = false) {
            var style = new GUIStyle(GUI.skin.label) { alignment = anchor };
            style.fontStyle = fontStyle;
            style.richText = richText;
            List<GUILayoutOption> options = new List<GUILayoutOption>();
            if (eatAllWidth)
                options.Add(GUILayout.ExpandWidth(true));
            if (eatAllHeight)
                options.Add(GUILayout.ExpandHeight(true));
            if (width != -1)
                options.Add(GUILayout.Width(width));
            var prevCol = EditorStyles.label.normal.textColor;
            EditorStyles.label.normal.textColor = col;
            EditorGUILayout.LabelField(str, style, options.ToArray());
            EditorStyles.label.normal.textColor = prevCol;
        }
        public static void LabelAtCenter(string str, FontStyle fontStyle = FontStyle.Normal)
            => RichLabel(str, TextAnchor.MiddleCenter, true, false, Color.black, fontStyle);
        private static void WithLabelWidth(Action action, float width) {
            var originalValue = EditorGUIUtility.labelWidth;
            if (width >= 0)
                EditorGUIUtility.labelWidth = width;
            action?.Invoke();
            EditorGUIUtility.labelWidth = originalValue;
        }
        public static bool Toggle(string title, ref bool value, ref bool changed, float width = -1, float labelWidth = -1) {
            BeginHorizontal();
            var val = value;
            const int toggleWidth = 18;
            var newValue = EditorGUILayout.Toggle("", val, GUILayout.Width(toggleWidth));
            if (labelWidth != -1 && width != -1)
                throw new Exception("set width OR label width only");
            if (labelWidth == -1 && width != -1)
                labelWidth = width - toggleWidth;
            Label(title, labelWidth);
            EndHorizontal();
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }
        public static bool ToggleMadeFromButton(string enableDisabled, string disableEnabled, ref bool value, ref bool changed) {
            var title = value ? disableEnabled : enableDisabled;
            if (GUILayout.Button(title)) {
                value = !value;
                changed = true;
                return true;
            }
            return false;
        }
        public static bool ToggleMadeFromToolbar(string enabled, string disabled, ref bool value, ref bool changed, float width = -1) {
            int newValue, oldValue = value ? 1 : 0;
            if (width == -1)
                newValue = GUILayout.Toolbar(oldValue, new string[] { disabled, enabled });
            else
                newValue = GUILayout.Toolbar(oldValue, new string[] { disabled, enabled }, GUILayout.Width(width));
            if (newValue == oldValue)
                return false;
            changed = true;
            value = newValue == 1;
            return true;
        }
        public static bool XButton() => Button("X", 18);
        public static bool XButton(ref bool changed) {
            if (XButton()) {
                changed = true;
                return true;
            } else 
                return false;
        }

        public static bool Toolbar<T>(string title, ref T value, ref bool changed, float width = -1, float labelWidth = -1) where T : Enum {
            GUILayout.BeginHorizontal();
            if (!string.IsNullOrEmpty(title))
                Label(title, labelWidth);
            var newVal = GUIUtils.Toolbar(value, width);
            GUILayout.EndHorizontal();
            if (newVal.CompareTo(value) == 0)
                return false;
            changed = true;
            value = newVal;
            return true;
        }

        public static bool Toolbar(string title, ref string value, string[] options, ref bool changed, float width = -1) {
            GUILayout.BeginHorizontal();
            if (!string.IsNullOrEmpty(title))
                GUILayout.Label(title);
            var newVal = GUIUtils.Toolbar(value, options, width);
            GUILayout.EndHorizontal();
            if (newVal.CompareTo(value) == 0)
                return false;
            changed = true;
            value = newVal;
            return true;
        }

        public static bool Button(string caption, ref bool changed, float width = -1) {
            var pressed = Button(caption, width);
            changed |= pressed;
            return pressed;
        }
        public static bool Button(string caption, float width = -1) {
            if (width == -1) return GUILayout.Button(caption);
            else return GUILayout.Button(caption, GUILayout.Width(width));
        }
        public static bool ButtonPlayMode(string caption, float width = -1) {
            PushEnabling(Application.isPlaying);
            var res = Button(caption, width);
            PopEnabling();
            return res;
        }
        public static Color subtleTextColor => new Color32(100, 100, 100, 255);
        public static Color subtleBackColor => new Color32(150, 150, 150, 255);
        public static bool SubtleButton(string caption, float width = -1f) {
            var pressed = false;
            WithButtonTextColor(subtleTextColor, () => {
                WithBackgoundColor(subtleBackColor, () => {
                    if (Button(caption, width))
                        pressed = true;
                });
            });
            return pressed;
        }

        //public static T PopupMadeFromButton<T>(T val, float width = -1, float labelWidth = -1, bool allowMultiple = false, params T[] except) where T : Enum {
        //    var (vals, names, _) = GUIUtils.GetOptionsData(val);
        //    if (width != -1)
        //        width /= vals.Length - except.Length;
        //    if (labelWidth != -1)
        //        labelWidth /= vals.Length - except.Length;
        //    var _ = false;
        //    for (int i = 0; i < vals.Length; i++) {
        //        if (except.Any(e => vals[i].Equals(e))) continue;
        //        var pressed = (Convert.ToInt32(val) & ((int)vals[i])) != 0;// oldInds.Contains(i);
        //        var buttonName = names[i];
        //        if (Toggle(buttonName, ref pressed, ref _, width, labelWidth)) {
        //            if (!allowMultiple)
        //                val = (T)vals[i];
        //            else
        //                val = (T)(object)(Convert.ToInt32(val) ^ Convert.ToInt32((T)vals[i]));
        //        }
        //    }
        //    return val;
        //}

        //public static bool PopupMadeFromButton<T>(string title, ref T value, ref bool changed,
        //    float width = -1, float labelWidth = -1, bool allowMultiple = false, params T[] except) where T : Enum {

        //    GUILayout.BeginHorizontal();
        //    if (!title.IsNullOrEmpty())
        //        GUILayout.Label(title);
        //    var newVal = PopupMadeFromButton(value, width, labelWidth, allowMultiple, except);
        //    GUILayout.EndHorizontal();
        //    if (newVal.CompareTo(value) == 0)
        //        return false;
        //    changed = true;
        //    value = newVal;
        //    return true;
        //}

        public static T Popup<T>(T val, float width = -1) where T : Enum {
            var changed = false;
            Popup(string.Empty, ref val, ref changed, width);
            return val;
        }

        public static bool Popup<T>(string title, ref T value, ref bool changed, float width = -1, float labelWidth = -1) where T : Enum {
            var untypedValue = (object)value;
            var res = Popup(title, typeof(T), ref untypedValue, ref changed, width, labelWidth);
            value = (T)untypedValue;
            return res;
        }

        public static bool Popup(string title, ref string value, string[] options, ref bool changed, float width = -1, float labelWidth = -1) {
            GUILayout.BeginHorizontal();
            if (!title.IsNullOrEmpty())
                Label(title, labelWidth);

            var oldInd = options.IndexOf(value);
            int newInd;
            if (width < 0)
                newInd = EditorGUILayout.Popup(oldInd, options);
            else
                newInd = EditorGUILayout.Popup(oldInd, options, GUILayout.Width(width - labelWidth));
            var newVal = options.IndIsValid(newInd) ? options[newInd] : "";
            GUILayout.EndHorizontal();

            if (oldInd == newInd)
                return false;
            changed = true;
            value = newVal;
            return true;
        }

        public static bool Popup(string title, Type type, ref object value, ref bool changed, float width = -1, float labelWidth = -1) {
            GUILayout.BeginHorizontal();
            if (!title.IsNullOrEmpty())
                Label(title, labelWidth);

            var (vals, names, oldInd) = GUIUtils.GetOptionsData(type, value);
            int newInd;
            if (width < 0)
                newInd = EditorGUILayout.Popup(oldInd, names);
            else
                newInd = EditorGUILayout.Popup(oldInd, names, GUILayout.Width(width - labelWidth));
            var newVal = vals.IndIsValid(newInd) ? vals[newInd] : type.GetDefault();
            GUILayout.EndHorizontal();

            if (oldInd == newInd)
                return false;
            changed = true;
            value = newVal;
            return true;
        }

        public static bool Popup(string title, ref int value, string[] options, ref bool changed, float width = -1, float labelWidth = -1) {
            GUILayout.BeginHorizontal();
            if (!title.IsNullOrEmpty())
                Label(title, labelWidth);
            int newValue;
            if (width <= 0)
                newValue = EditorGUILayout.Popup(value, options);
            else
                newValue = EditorGUILayout.Popup(value, options, GUILayout.Width(width));
            GUILayout.EndHorizontal();
            if (newValue == value)
                return false;
            changed = true;
            value = newValue;
            return true;
        }

        public static bool EnumValuesList<T>(string title, string addButtonTitle, ref List<T> items, ref bool changed, float width = -1, float labelWidth = -1) where T : Enum {
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            var currChanged = false;
            if (!title.IsNullOrEmpty())
                Label(title, labelWidth);
            T newItem = (T)Enum.ToObject(typeof(T), -1);
            if (Popup(addButtonTitle, ref newItem, ref changed, width)) {
                items.Add(newItem);
                currChanged = true;
            }
            GUILayout.EndHorizontal();
            for (int i = 0; i < items.Count; i++) {
                GUILayout.BeginHorizontal();
                Indent(1);
                PushDisabling();
                Button(items[i].ToString(), width);
                PopEnabling();
                if (XButton()) {
                    items.RemoveAt(i);
                    currChanged = changed = true;
                }
                GUILayout.EndHorizontal();
            }
            
            GUILayout.EndVertical();
            return currChanged;
        }

        const float CharWidth = 5;
        public static void RichMultilineLabel(string str, float width = -1, bool autoWidth = false) {
            EditorStyles.label.richText = true;
            if (autoWidth) {
                width = str.Length * CharWidth;
                EditorStyles.label.wordWrap = false;
            } else
                EditorStyles.label.wordWrap = true;
            if (width < 0)
                EditorGUILayout.LabelField(str);
            else
                EditorGUILayout.LabelField(str, GUILayout.Width(width));
            EditorStyles.label.wordWrap = false;
            EditorStyles.label.richText = false;
        }

        public static void LabelToCopy(string str) {
            GUILayout.BeginHorizontal();
            RichMultilineLabel(str);
            if (Button("copy", 50)) {
                str.CopyToClipboard();
                Debug.Log($"'{str}' copied!");
            }
            GUILayout.EndHorizontal();
        }

        private static Stack<bool> mixedValueShown;
        static void UpdateMixedEnabled() {
            EditorGUI.showMixedValue = mixedValueShown.TryPeek(out var mixedShown) ? mixedShown : false;
        }
        public static void PushShowMixedValue<T, TVal>(IEnumerable<T> tgts, Func<T, TVal> getShownValue) {
            TVal firstValue = default;
            bool first = true;
            var mixed = false;
            foreach (var tgt in tgts) {
                var currValue = getShownValue(tgt);
                if (first) {
                    firstValue = currValue;
                    first = false;
                } else if (!currValue.DefaultEquals(firstValue)) {
                    mixed = true;
                    break;
                }
            }
            PushShowMixedValue(mixed);
        }
        public static void PushShowMixedValue(bool showMixed = true) {
            if (mixedValueShown == null) {
                mixedValueShown = new Stack<bool>();
                mixedValueShown.Push(false);
            }
            mixedValueShown.Push(showMixed);
            UpdateMixedEnabled();
        }
        public static void PopShowMixedValue() {
            mixedValueShown.Pop();
            UpdateMixedEnabled();
        }

        private static Stack<bool> guiEnabled;
        static void UpdateGUIEnabled() {
            var enabled = true;
            foreach (var val in guiEnabled) {
                if (!val) {
                    enabled = false;
                    break;
                }
            }
            GUI.enabled = enabled;
        }
        public static void PushDisabling() => PushEnabling(false);
        public static void PushEnabling(bool enabled = true) {
            if (guiEnabled == null) {
                guiEnabled = new Stack<bool>();
                guiEnabled.Push(true);
            }
            guiEnabled.Push(enabled);
            UpdateGUIEnabled();
        }
        public static void PopEnabling() {
            guiEnabled.Pop();
            UpdateGUIEnabled();
        }
        static GUIStyle noBorderSkin = GUIStyle.none;// new GUIStyle(GUI.skin.box);
        static GUIStyle defaultTextureSkin = new GUIStyle(GUI.skin.box);
        static Texture2D good, bad, open, close, added, modified, deleted, renamed, search, edit;
        static void InitPicsIfNeeded() {
            if (good != null)
                return;
            const int size = 5;
            noBorderSkin.margin = new RectOffset(size, size, size, size);
            var folder = $"{AntonToolsManager.MainPluginFolder}/EditorTools/Editor/EditorUIPics";
            void Load(ref Texture2D pic, string fileName) 
                => pic = AssetDatabase.LoadAssetAtPath<Texture2D>($"{folder}/{fileName}.png");
            Load(ref good, "good");
            Load(ref bad, "bad");
            Load(ref open, "open");
            Load(ref close, "close");
            Load(ref added, "added");
            Load(ref modified, "modified");
            Load(ref deleted, "deleted");
            Load(ref renamed, "renamed");
            Load(ref search, "search");
            Load(ref edit, "edit");
        }
        public static Texture2D GetFileStatusPic(FileStatus status) {
            InitPicsIfNeeded();
            switch (status) {
                case FileStatus.Added: return added;
                case FileStatus.Deleted: return deleted;
                case FileStatus.Modified:return modified;
                case FileStatus.Renamed: return renamed;
                default:  return null;
            }
        }
        public static void Texture(Texture2D pic, float width = ShowValidWidth, float height = ShowValidWidth, GUIStyle style = null) {
            if (style == null)
                style = defaultTextureSkin;
            GUILayout.Box(pic, style, GUILayout.Width(width), GUILayout.Height(height));
        }
        public static void TextureNoBorders(Texture2D pic, float width = ShowValidWidth, float height = ShowValidWidth)
            => Texture(pic, width, height, noBorderSkin);
        public static void ShowSearchIco() {
            InitPicsIfNeeded();
            TextureNoBorders(search);
        }
        public const int ShowValidWidth = 12;
        private static void ShowValidBoxTexture(Texture2D texture) {
            InitPicsIfNeeded();
            TextureNoBorders(texture, ShowValidWidth, ShowValidWidth);
        }
        public static void ShowValid(string title, bool valid) {
            GUILayout.BeginHorizontal();
            ShowValid(valid);
            GUILayout.Label(title);
            GUILayout.EndHorizontal();
        }
        public static void ShowValid(bool valid)
            => ShowValidBoxTexture(valid ? good : bad);
        public static void ShowValidEmpty()
            => ShowValidBoxTexture(null);
        public static bool ShowOpenCloseListItem(ref int selectedInd, int ind) {
            var opened = selectedInd == ind;
            if (!ShowOpenClose(ref opened)) return false;
            if (opened)
                selectedInd = ind;
            else
                selectedInd = -1;
            return true;
        }
        public static bool ShowOpenClose(ref bool opened) {
            InitPicsIfNeeded();
            var changed = false;
            if (!IcoButton(opened ? close : open, ref changed))
                return false;
            opened = !opened;
            LoseFocus();
            return true;
        }
        const float IcoButtonSize = 12;
        public static bool IcoButton(Texture2D ico, ref bool changed) {
            InitPicsIfNeeded();
            if (!GUILayout.Button(ico, noBorderSkin, GUILayout.Width(IcoButtonSize), GUILayout.Height(IcoButtonSize)))
                return false;
            changed = true;
            return true;
        }
        public static bool EditButton(ref bool changed) {
            InitPicsIfNeeded();
            if (!GUILayout.Button(edit, GUILayout.Width(IcoButtonSize+10)))
                return false;
            changed = true;
            return true;
        }
        public static bool ShowOpenClose(ref bool opened, ref bool changed) {
            var currChanged = ShowOpenClose(ref opened);
            changed |= currChanged;
            return currChanged;
        }
        public static bool ShowOpenClose(ref bool opened, string title) {
            if (string.IsNullOrEmpty(title))
                return ShowOpenClose(ref opened);
            else {
                GUILayout.BeginHorizontal();
                var changed = ShowOpenClose(ref opened);
                GUILayout.Label(title);
                GUILayout.EndHorizontal();
                return changed;
            }
        }
        public static void Indent(int indents = 1) {
            const float size = 12;
            GUILayout.Box(default(Texture2D), noBorderSkin, GUILayout.Width(size * indents), GUILayout.Height(size));
        }
        // Obsolete.
        public static void List<T>(string title, string itemTitle, List<T> list, bool allowSceneObjects, ref bool unfolded, ref bool changed,
            float labelWidth = -1, float sizeWidth = -1) //where T : UnityEngine.Object
            => List(title, list, ref unfolded, ref changed, (ref T val, ref bool chngd)
                => {
                    var valObj = val as object;
                    if (GenericTypeField(itemTitle, typeof(T), ref valObj, ref chngd, width: -1, labelWidth, allowSceneObjects))
                        val = valObj == null ? default : (T)valObj;
                    //val = EditorGUILayout.ObjectField(val, typeof(T), allowSceneObjects) as T;
                }, () => default, labelWidth, sizeWidth);
        // Obsolete.
        public static void List<T>(string title, List<T> list, ref bool unfolded, ref bool changed, RefAction<T, bool> editItem, Func<T> getNewItem,
            float labelWidth = -1, float sizeWidth = 80) {

            GUILayout.BeginHorizontal();
            ShowOpenClose(ref unfolded);
            Label(title, labelWidth);
            var count = list.Count;
            if (IntField("size", ref count, ref changed, sizeWidth, sizeWidth-30f))
                count = Mathf.Max(0, count);
            GUILayout.EndHorizontal();

            list.SetCount(count, getNewItem);
            if (!unfolded) return;
            for (int i = 0; i < list.Count; i++) {
                var oldItem = list[i];
                var newItem = oldItem;
                editItem(ref newItem, ref changed);
                if (oldItem.DefaultEquals(newItem))
                    continue;
                list[i] = newItem;
                changed = true;
            }
        }

        public static bool GenericTypeField(string name, Type type, ref object fieldValue, ref bool changed, 
            float width = -1, float labelWidth = -1, bool allowSceneObjects = true) {

            if (type == typeof(bool)) {
                var boolValue = (bool)fieldValue;
                if (Toggle(name, ref boolValue, ref changed, width, labelWidth)) {
                    fieldValue = boolValue;
                    return true;
                }
            } else if (type == typeof(int)) {
                var intValue = (int)fieldValue;
                if (IntField(name, ref intValue, ref changed, width, labelWidth)) {
                    fieldValue = intValue;
                    return true;
                }
            } else if (type == typeof(long)) {
                var longValue = (long)fieldValue;
                if (LongField(name, ref longValue, ref changed, width, labelWidth)) {
                    fieldValue = longValue;
                    return true;
                }
            } else if (type == typeof(float)) {
                var floatValue = (float)fieldValue;
                if (FloatField(name, ref floatValue, ref changed, width, labelWidth)) {
                    fieldValue = floatValue;
                    return true;
                }
            } else if (type == typeof(double)) {
                var doubleValue = (double)fieldValue;
                if (DoubleField(name, ref doubleValue, ref changed, width, labelWidth)) {
                    fieldValue = doubleValue;
                    return true;
                }
            } else if (type == typeof(string)) {
                var stringValue = (string)fieldValue;
                if (TextField(name, ref stringValue, ref changed, width: width, labelWidth: labelWidth)) {
                    fieldValue = stringValue;
                    return true;
                }
            } else if (type == typeof(Sprite)) {
                var spriteValue = fieldValue as Sprite;
                if (SpriteField(name, ref spriteValue, ref changed, width, labelWidth)) {
                    fieldValue = spriteValue;
                    return true;
                }
            } else if (type.IsEnum) {
                var enumValue = fieldValue == null ? Activator.CreateInstance(type) : fieldValue;
                if (Popup(name, type, ref enumValue, ref changed)) {
                    fieldValue = enumValue;
                    return true;
                }
            } else if (type.IsSubclassOf( typeof(UnityEngine.Object))) {
                var objValue = fieldValue as UnityEngine.Object;
                if (ObjectField(name, ref objValue, type, allowSceneObjects, ref changed, width, labelWidth)) {
                    fieldValue = objValue;
                    return true;
                }
            } else if (type.IsStruct()) {
                if (EnumField(name, ref fieldValue, type, allowSceneObjects, ref changed, width, labelWidth)) {
                    return true;
                }
            } else
                GUILayout.Label($"editing {type.Name} not implemented for field {name}");
            return false;
        }
    }
}
#endif