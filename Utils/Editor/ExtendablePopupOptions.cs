﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace AntonTools
{
    public class ExtendablePopupOptions : EditorWindow
    {
        public static bool shown { get; private set; }
        public static ExtendablePopupOptions instance { get; private set; }
        public static int controlId { get; private set; }
        public static ExtendablePopupOptions ShowWindow(int controlId) {
            ExtendablePopupOptions.controlId = controlId;
            var rect = GUILayoutUtility.GetLastRect();
            rect = EditorGUIUtility.GUIToScreenRect(rect);
            rect.y += rect.height;
            var oldInst = GetWindow<ExtendablePopupOptions>();
            if (oldInst != null) {
                oldInst.Close();
                DestroyImmediate(oldInst);
            }
            if (instance == null)
                instance = CreateInstance<ExtendablePopupOptions>();
            instance.position = rect;
            instance.ShowPopup();
            shown = true;
            return instance;
        }
        public static void CloseWindow() {
            instance?.Close();
            shown = false;
        }
        const int buttonHeight = 20;
        const int windowBorders = 10;
        static List<string> optionsToShow = new List<string>();
        const int MaxOptionsToShow = 10;
        private void OnGUI() {
            if (options == null)
                return;
            optionsToShow.Clear();
            for (int i = 0; i < options.Count; i++) {
                if (optionsToShow.Count >= MaxOptionsToShow)
                    break;
                if (value.IsNothing() || options[i].ContainsIgnoreCase(value))
                    optionsToShow.Add(options[i]);
            }
            var addNewInd = -1;
            if (value.IsSomething() && !options.ContainsIgnoreCase(value)) {
                optionsToShow.Add($"Add {value}");
                addNewInd = optionsToShow.LastInd();
            }

            this.minSize = Vector2.zero;
            var rect = this.position;
            rect.height = buttonHeight * optionsToShow.Count + windowBorders * 2;
            this.position = rect;
            GUILayout.BeginVertical();
            GUILayout.Space(windowBorders);
            for (int i = 0; i < optionsToShow.Count; i++) {
                GUILayout.BeginHorizontal();
                GUILayout.Space(windowBorders);
                var pressed = GUILayout.Button(optionsToShow[i], GUILayout.Width(rect.width - windowBorders * 2), GUILayout.Height(buttonHeight));
                if (pressed) {
                    if (addNewInd != i)
                        newValue = optionsToShow[i];
                    else {
                        options.Add(value);
                        newValue = value;
                    }
                }
                GUILayout.Space(windowBorders);
                GUILayout.EndHorizontal();
            }
            GUILayout.Space(windowBorders);
            GUILayout.EndVertical();
            if (newValue.IsSomething()) {
                CloseWindow();
                GUI.FocusControl(null);
            }
        }

        static List<string> options;
        static string value, newValue;
        public static void OnInspectorGUI(ref string value, List<string> options, ref bool changed) {
            ExtendablePopupOptions.options = options;
            ExtendablePopupOptions.value = value;
            if (newValue.IsSomething()) {
                value = newValue;
                newValue = null;
                changed = true;
                ExtendablePopupOptions.CloseWindow();
                GUI.FocusControl(null);
            }
        }
    }
}
#endif