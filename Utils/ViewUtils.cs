﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public static partial class Utils {
        public static Color AlphaTo(this Color col, float a) {
            col.a = a;
            return col;
        }
        public static bool IsVertical() => Screen.height > Screen.width;
        public static bool IsHorizontal() => !IsVertical();

        public static (int years, int monthes, int days, int hours, int minutes, int sec) SplitTime(float seconds) {
            const float SecondsPerMinute = 60;
            const float SecondsPerHour = SecondsPerMinute * 60;
            const float SecondsPerDay = SecondsPerHour * 24;
            const float SecondsPerYear = SecondsPerDay * 365;
            const float SecondsPerMonth = SecondsPerDay * 30;
            var years = (int)(seconds / SecondsPerYear);
            seconds -= years * SecondsPerYear;
            var monthes = (int)(seconds / SecondsPerMonth);
            seconds -= monthes * SecondsPerMonth;
            var days = (int)(seconds / SecondsPerDay);
            seconds -= days * SecondsPerDay;
            var hours = (int)(seconds / SecondsPerHour);
            seconds -= hours * SecondsPerHour;
            var minutes = (int)(seconds / SecondsPerMinute);
            seconds -= minutes * SecondsPerMinute;
            var sec = (int)seconds;
            return (years, monthes, days, hours, minutes, sec);
        }
        public static string ToShownTimeShort(this float seconds) {
            var (years, monthes, days, hours, minutes, sec) = SplitTime(seconds);
            if (years != 0)
                return $"{years.ToString("00")}:{monthes.ToString("00")}:{days.ToString("00")}:{hours.ToString("00")}:{minutes.ToString("00")}:{sec.ToString("00")}";
            else if (monthes != 0)
                return $"{monthes.ToString("00")}:{days.ToString("00")}:{hours.ToString("00")}:{minutes.ToString("00")}:{sec.ToString("00")}";
            else if (days != 0)
                return $"{days.ToString("00")}:{hours.ToString("00")}:{minutes.ToString("00")}:{sec.ToString("00")}";
            else if (hours != 0)
                return $"{hours.ToString("00")}:{minutes.ToString("00")}:{sec.ToString("00")}";
            else
                return $"{minutes.ToString("00")}:{sec.ToString("00")}";
        }
        public static string ToShownTime(this float seconds) {
            var (years, monthes, days, hours, minutes, sec) = SplitTime(seconds);
            if (years != 0) {
                if (monthes != 0) {
                    if (days != 0)
                        return $"{years}y {monthes}m {days}d";
                    else
                        return $"{years}y {monthes}m";
                } else
                    return $"{years}y";
            }
            if (monthes != 0) {
                if (days != 0)
                    return $"{monthes}m {days}d";
                else
                    return $"{monthes}m";
            }
            if (days != 0) {
                if (hours != 0)
                    return $"{days}d {hours}h";
                else
                    return $"{days}d";
            }
            if (hours != 0) {
                if (minutes != 0) {
                    if (sec != 0)
                        return $"{hours}h {minutes}m {sec}s";
                    else
                        return $"{hours}h {minutes}m";
                } else
                    return $"{hours}h";
            } else if (minutes != 0) {
                if (sec != 0)
                    return $"{minutes}m {sec}s";
                else
                    return $"{minutes}m";
            } else
                return $"{sec}s";
        }
        public static string ToShownPercents(this float coef)
            => $"{(coef * 100).RoundToInt()}%";
        public static string ToShownPercents(this double coef)
            => $"{(coef * 100).RoundToInt()}%";

        public static void ShowProgressbar(int curr, int max, TextMeshProUGUI label, Image progressFill) {
            if (label != null)
                label.text = $"{curr}/{max}";
            if (progressFill != null)
                progressFill.fillAmount = curr / (float)max;
        }
#if COM_UNITY_MODULES_ANIMATION
        public static void UpdateShownAnimation(string animation, float progress, Animator animator) {
            if (!animator.gameObject.activeInHierarchy) return;
            animator.Play(animation, 0, progress);
            animator.Update(0f);
        }
        public static Dictionary<string, AnimationClip> GetClipsDict(Animator animator) {
            var clips = new Dictionary<string, AnimationClip>();
            animator.runtimeAnimatorController.animationClips.ForEach(
                clip => clips.Add(clip.name, clip));
            return clips;
        }
#endif
    }
}