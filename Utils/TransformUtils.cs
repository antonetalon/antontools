﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AntonTools {
    public static partial class Utils {

        public static Quaternion InverseTransformRotation(this Transform transform, Quaternion rotationInWorld)
            => Quaternion.Inverse(transform.rotation) * rotationInWorld;
        public static Quaternion TransformRotation(this Transform transform, Quaternion rotationInLocal)
            => transform.rotation * rotationInLocal;
        public static void TransformPositionsConvert(IEnumerable<Transform> parents, Action<Transform> changePosition)
            => TransformPositionsConvert(parents, p => changePosition(p.tr));
        public static void TransformPositionsConvert(IEnumerable<Transform> parents, Action<(Transform tr, Vector3 prevPos, Quaternion prevRot)> changePosition) {
            var originalPositions = new Dictionary<Transform, Matrix4x4>();
            parents.ForEach(transform => transform.GetComponentsInChildren<Transform>(true).Foreach(tr => originalPositions.TryAdd(tr, tr.localToWorldMatrix)));
            var iterated = new HashSet<Transform>();

            foreach (var transform in parents) {
                var transformsToIterate = transform.ToListFromItem();
                while (transformsToIterate.Count > 0) {
                    var children = new List<Transform>();
                    foreach (var tr in transformsToIterate) {
                        if (iterated.Contains(tr))
                            continue;
                        iterated.Add(tr);
                        children.AddRange(tr.IterateChildren());
                        var prevPos = tr.position;
                        var prevRot = tr.rotation;
                        var originalPos = originalPositions[tr];
                        tr.SetPositionAndRotation(originalPos.ExtractPosition(), originalPos.ExtractRotation());
                        changePosition((tr, prevPos, prevRot));
                    }
                    transformsToIterate = children;
                }
            }
        }
        public static void TransformPositionsConvertNormalizeScale(IEnumerable<Transform> parents, Action<(Transform tr, Matrix4x4 originalPos)> changePosition) {
            var originalPositions = new Dictionary<Transform, Matrix4x4>();
            parents.ForEach(transform => transform.GetComponentsInChildren<Transform>(true).Foreach(tr => originalPositions.TryAdd(tr, tr.localToWorldMatrix)));
            originalPositions.Keys.ForEach(tr => tr.localScale = Vector3.one);
            var iterated = new HashSet<Transform>();

            foreach (var transform in parents) {
                var transformsToIterate = transform.ToListFromItem();
                while (transformsToIterate.Count > 0) {
                    var children = new List<Transform>();
                    foreach (var tr in transformsToIterate) {
                        if (iterated.Contains(tr))
                            continue;
                        iterated.Add(tr);
                        children.AddRange(tr.IterateChildren());
                        var prevPos = tr.position;
                        var prevRot = tr.rotation;
                        var originalPos = originalPositions[tr];
                        tr.SetPositionAndRotation(originalPos.ExtractPosition(), originalPos.ExtractRotation());
                        changePosition((tr, originalPos));
                    }
                    transformsToIterate = children;
                }
            }
        }

        public static IEnumerable<Transform> IterateChildren(this Transform tr) {
            for (int i = 0; i < tr.childCount; i++)
                yield return tr.GetChild(i);
        }
        public static Transform Find(this Transform tr, Predicate<Transform> check) {
            if (check(tr))
                return tr;
            for (int i = 0; i < tr.childCount; i++) {
                var found = Find(tr.GetChild(i), check);
                if (found != null)
                    return found;
            }
            return null;
        }
        public static IEnumerable<Transform> FindAll(this Transform tr, Predicate<Transform> check) {
            if (check(tr))
                yield return tr;
            for (int i = 0; i < tr.childCount; i++) {
                foreach (var found in FindAll(tr.GetChild(i), check))
                    yield return found;
            }
        }
        public static string GetFullPath(this GameObject obj, bool withAssetPath = false)
            => obj.transform.GetFullPath(withAssetPath);
        public static string GetFullPath(this Transform obj, bool withAssetPath = false) {
            var builder = new StringBuilder();
            builder.Append(obj.name);
            var p = obj.parent;
            while (p != null) {
                builder.Insert(0, p.name + "/");
#if UNITY_EDITOR
                if (!Application.isPlaying && PrefabUtils.openedPrefabInstanceRoot == p)
                    break;
#endif
                p = p.parent;
            }
#if UNITY_EDITOR
            if (withAssetPath) {
                var assetPath = UnityEditor.AssetDatabase.GetAssetPath(obj);
                builder.Insert(0, $"{assetPath}:");
            }
#endif
            return builder.ToString();
        }
        private static Transform GetChildByFullPath(this Transform root, string[] names) {
            if (root.name != names[0]) return null;
            var ind = 0;
            var transform = root;
            while (ind < names.Length - 1) {
                ind++;
                transform = transform.Find(names[ind]);
                if (transform == null) return null;
            }
            return transform;
        }
        public static Transform GetChildByFullPath(this Transform root, string fullPath, char separator = '.')
            => root.GetChildByFullPath(fullPath.Split(separator));
        public static Transform GetChildByFullPath(this Scene scene, string fullPath) {
            Debug.Assert(scene.isLoaded, "GetChildByFullPath scene should be loaded");
            var names = fullPath.Split('/');
            var roots = scene.GetRootGameObjects();
            foreach (var root in roots) {
                var child = root.transform.GetChildByFullPath(names);
                if (child != null)
                    return child;
            }
            return null;
        }
        public static int GetChildDepth(this Transform tr, Transform relativeToParent = null) {
            var depth = 0;
            while (relativeToParent != tr && tr != null) {
                tr = tr.parent;
                depth++;
            }
            return depth;
        }
        public static void SetChanged(this Component tr) {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(tr);
#endif
        }
        public static Bounds GetBoundsWithChildren(this Transform transform, bool draw = false) {
            var first = true;
            var bounds = new Bounds(transform.position, Vector3.zero);
            transform.GetComponentsInChildren<Renderer>().ForEach(r => {
                if (first) {
                    first = false;
                    bounds = r.bounds;
                } else
                    bounds.Encapsulate(r.bounds);
                if (draw)
                    r.bounds.Draw(Color.green, 5);
            });
            if (draw)
                bounds.Draw(Color.white, 5);
            return bounds;
        }
        public static T GetOrAddComponent<T>(this GameObject go) where T : Component {
            var t = go.GetComponent<T>();
            if (t == null)
                t = go.AddComponent<T>();
            return t;
        }
        public static bool DestroyIfPlaying(this MonoBehaviour mono) {
            if (!Application.isPlaying) return false;
            MonoBehaviour.Destroy(mono);
            return true;
        }
        public static bool HasChild(this Transform parent, Transform child, bool recursive = true) {
            for (int i = 0; i < parent.childCount; i++) {
                var ch = parent.GetChild(i);
                if (ch == child)
                    return true;
                if (recursive && ch.HasChild(child, true))
                    return true;
            }
            return false;
        }
        public static Transform GetChild(this Transform transform, string name) {
            for (int i = 0; i < transform.childCount; i++) {
                if (transform.GetChild(i).name == name)
                    return transform.GetChild(i);
            }
            return null;
        }
        // Destroyed objects can't still are not null when checked with operator '?'.
        public static T Safe<T>(this T val) where T : UnityEngine.Object {
            if (val != null) return val;
            else return null;
        }
        public static GameObject Safe(this GameObject val) {
            if (val != null) return val;
            else return null;
        }
        public static void Safe<T>(this T component, Action action)
            where T : Component {
            if (component != null && action != null)
                action();
        }
        public static T[] Safe<T>(this T[] components) where T : Component {
            if (components != null) return components;
            else return null;
        }
        public static List<T> Safe<T>(this List<T> components) where T : Component {
            if (components != null) return components;
            else return null;
        }
        public static T GetComponentInParentIncludingInactive<T>(this GameObject go) where T : Component
            => go.transform.GetComponentInParentIncludingInactive<T>();
        public static T GetComponentInParentIncludingInactive<T>(this Transform tr) where T : Component {
            if (tr == null) return null;
            var t = tr.GetComponent<T>();
            if (t != null) return t;
            return tr.parent.GetComponentInParentIncludingInactive<T>();
        }
        public static async Task MovingTo(this Transform tr, Vector3 tgtPos, float duration, bool realtime = false, bool local = false) {
            var startPos = local ? tr.localPosition : tr.position;
            await AsyncUtils.SecondsWithProgress(duration, progress => {
                var currPos = Vector3.Lerp(startPos, tgtPos, progress);
                if (local)
                    tr.localPosition = currPos;
                else
                    tr.position = currPos;
            }, realtime);
        }
        public static async Task ScalingTo(this Transform tr, float tgtScale, float duration, bool realTime = false) {
            var startScale = tr.localScale.x;
            await AsyncUtils.SecondsWithProgress(duration, progress => tr.localScale = Vector3.one * Mathf.Lerp(startScale, tgtScale, progress), realTime);
        }
        public static List<Transform> GetAllEnabledRootTransformsOnScene()
            => UnityEngine.Object.FindObjectsOfType<Transform>().Filter(tr => tr.parent == null && tr.gameObject.name != "New Game Object");
        public static List<T> FindSceneObjectsWithInactive<T>()
            where T : Behaviour {
            var objectsInScene = new List<T>();
            IterateSceneObjectsWithInactive<T>(t => objectsInScene.Add(t));
            return objectsInScene;
        }
        public static void IterateSceneObjectsWithInactive<T>(Action<T> action) where T : Behaviour {
            var all = Resources.FindObjectsOfTypeAll(typeof(T)) as T[];
            foreach (T t in all) {
#if UNITY_EDITOR
                if (UnityEditor.EditorUtility.IsPersistent(t.transform.root.gameObject))
                    continue;
#endif
                if (!(t.gameObject.hideFlags == HideFlags.NotEditable || t.gameObject.hideFlags == HideFlags.HideAndDontSave))
                    action?.Invoke(t);
            }
        }
        public static void SetDefaultLocalPosition(this Transform t) {
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
        }
        public static void IterateInterfacesInScene<T>(Action<T> action) where T : class
            => SceneManager.GetActiveScene().GetRootGameObjects().ForEach(root => root.GetInterfacesInChildren<T>().ForEach(action));
        public static List<T> GetInterfacesInChildren<T>(this GameObject go) where T : class
            => go.transform.GetInterfacesInChildren<T>();
        public static List<T> GetInterfacesInChildren<T>(this Transform t, bool includeInactive = false) where T : class
            => t.GetComponentsInChildren<Component>(includeInactive).ConvertAll(c => c as T).Filter(c => c != null);
        public static T GetInterface<T>(this Transform t) where T : class
            => t.GetComponents<Component>().Find(c=>c is T) as T;
        public static T GetInterface<T>(this GameObject t) where T : class
            => t.transform.GetInterface<T>();
        public static void IterateChildren(this Transform tr, Action<Transform> action) {
            action?.Invoke(tr);
            for (int i = 0; i < tr.childCount; i++)
                IterateChildren(tr.GetChild(i), action);
        }
        public static void IterateChildren(this GameObject go, Action<GameObject> action) {
            action?.Invoke(go);
            for (int i = 0; i < go.transform.childCount; i++)
                IterateChildren(go.transform.GetChild(i).gameObject, action);
        }
        public static void SetLayerRecursively(this GameObject go, int layer)
            => go.transform.IterateChildren(tr => tr.gameObject.layer = layer);
        public static int GetSingleLayer(this LayerMask mask)
            => Mathf.Log(mask.value, 2).FloorToInt();
        public static void DestroyAlways(this Transform tr) {
            if (Application.isPlaying)
                MonoBehaviour.Destroy(tr.gameObject);
            else
                MonoBehaviour.DestroyImmediate(tr.gameObject);
        }
        public static void DestroyChildren(this Transform tr) {
            for (int i = tr.childCount - 1; i >= 0; i--)
                tr.GetChild(i).DestroyAlways();
        }
        public static void DestroyChildrenImmediate(this Transform tr) {
            for (int i = tr.childCount - 1; i >= 0; i--)
                MonoBehaviour.DestroyImmediate(tr.GetChild(i).gameObject);
        }
        static StringBuilder sb;
        public static string FullName(this Transform transform) {
            if (sb == null) sb = new StringBuilder();
            sb.Clear();
            do {
                sb.Insert(0, transform.name);
                transform = transform.parent;
                if (transform == null)
                    break;
                sb.Insert(0, ".");
            } while (true);
            return sb.ToString();
        }
        public static PositionRotation GetPositionAndRotation(this Transform tr)
            => new PositionRotation { position = tr.position, rotation = tr.rotation };
        public static void SetPositionAndRotation(this Transform tr, PositionRotation posRot)
            => tr.SetPositionAndRotation(posRot.position, posRot.rotation);
        public static void SetPositionAndRotation(this Transform transform, Matrix4x4 m)
            => transform.SetPositionAndRotation(m.ExtractPosition(), m.ExtractRotation());
        public static PositionRotation GetLocalPositionAndRotation(this Transform tr)
            => new PositionRotation { position = tr.localPosition, rotation = tr.localRotation };
        public static void SetLocalPositionAndRotation(this Transform tr, PositionRotation posRot) {
            tr.localPosition = posRot.position;
            tr.localRotation = posRot.rotation;
        }
        public static void SetLocalPositionAndRotation(this Transform tr, Matrix4x4 m)
            => tr.SetLocalPositionAndRotation(new PositionRotation { position = m.ExtractPosition(), rotation = m.ExtractRotation() });
    }
    public struct PositionRotation {
        public Vector3 position;
        public Quaternion rotation;
    }
}