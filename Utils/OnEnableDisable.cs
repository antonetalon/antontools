﻿using UnityEngine;

namespace AntonTools {
    public class OnEnableDisable : MonoBehaviour {
        private void OnEnable() => Debug.Log($"OnEnable called ({transform.FullName()})");
        private void OnDisable() => Debug.Log($"OnDisable called ({transform.FullName()})");
    }
}