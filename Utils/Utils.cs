﻿using System;
using UnityEngine;

namespace AntonTools {
    public static partial class Utils {
        public static void DrawSphere(Vector3 pos, float radius, Color col) {
            Gizmos.color = col;
            Gizmos.matrix = Matrix4x4.TRS(pos, Quaternion.identity, Vector3.one);
            Gizmos.DrawSphere(Vector3.zero, radius);
        }
        public static void DrawWireCube(Vector3 pos, Vector3 size, Color col)
            => DrawCube(pos, size, Quaternion.identity, col, true);
        public static void DrawCube(Vector3 pos, Vector3 size, Quaternion rotation, Color col,
            bool wire = false) {
            Gizmos.color = col;
            Gizmos.matrix = Matrix4x4.TRS(pos, rotation, Vector3.one);
            if (wire)
                Gizmos.DrawWireCube(Vector3.zero, size);
            else
                Gizmos.DrawCube(Vector3.zero, size);
        }
        public static void DrawCube(Vector3 pos, Vector3 size, Color col, bool wire = false)
            => DrawCube(pos, size, Quaternion.identity, col, wire);

        public static void DrawCircleSector(Vector3 center, float radius, Vector3 forward, Vector3 normal, float minAngle, float maxAngle, Color color, int minSections = 15, float sectionLength = 1, float widthCoef = 0.01f) {
            var toSide = forward.normalized * radius;
            var totalRange = maxAngle - minAngle;
            var ptsCount = Mathf.Max(minSections, Mathf.CeilToInt(radius * totalRange * Mathf.PI * 2f / sectionLength));
            var rotate = Quaternion.AngleAxis(totalRange / ptsCount, normal);
            var prevPt = Quaternion.AngleAxis(minAngle, normal) * toSide;
            var width = widthCoef * radius;
            for (int i = 0; i < ptsCount; i++) {
                var nextPt = rotate * prevPt;
                DrawLine(prevPt + center, nextPt + center, width, color);
                prevPt = nextPt;
            }
        }

        public static void DrawCircle(Vector3 center, float radius, Vector3 normal, Color color, int minSections = 15, float sectionLength = 1, float widthCoef = 0.01f) {
            var toSide = Utils.GetPerpendicular(normal).normalized * radius;
            var ptsCount = Mathf.Max(minSections, Mathf.CeilToInt(radius * Mathf.PI * 2f / sectionLength));
            var rotate = Quaternion.AngleAxis(360f / ptsCount, normal);
            var prevPt = toSide;
            var width = widthCoef * radius;
            for (int i = 0; i < ptsCount; i++) {
                var nextPt = rotate * prevPt;
                DrawLine(prevPt + center, nextPt + center, width, color);
                prevPt = nextPt;
            }
        }

        public static void DrawLine(Vector3 pt1, Vector3 pt2, float width, Color col) {
            var dir = pt2 - pt1;
            var up = Utils.GetPerpendicular(dir);
            DrawCube((pt1 + pt2) * 0.5f,
                new Vector3(width, width, dir.magnitude),
                Matrix4x4.LookAt(Vector3.zero, dir, up).ExtractRotation(), col);
        }

        const float PointerWidthCoef = 3;
        private static void DrawArrowPointer(Vector3 pt1, Vector3 pt2, float width, Color col) {
            var dir = (pt2 - pt1).normalized;
            var dist = width * PointerWidthCoef;
            var toSide = Utils.GetPerpendicular(dir).normalized * dist;
            var toSide2 = Vector3.Cross(toSide, dir).normalized * dist;
            var c = pt2 - dir * dist;
            DrawLine(c + toSide, pt2, width, col);
            DrawLine(c - toSide, pt2, width, col);
            DrawLine(c + toSide2, pt2, width, col);
            DrawLine(c - toSide2, pt2, width, col);
        }
        public static void DrawArrow(Vector3 pt1, Vector3 pt2, float width, Color col) {
            DrawLine(pt1, pt2, width, col);
            DrawArrowPointer(pt1, pt2, width, col);
        }
        public static void DrawDoubleSidedArrow(Vector3 pt1, Vector3 pt2, float width, Color col) {
            DrawLine(pt1, pt2, width, col);
            DrawArrowPointer(pt1, pt2, width, col);
            DrawArrowPointer(pt2, pt1, width, col);
        }
        public static void DrawSpring(Vector3 pt1, Vector3 pt2, float width, Color col) {
            var radius = width * PointerWidthCoef;
            var dir = pt2 - pt1;
            var dist = dir.magnitude;
            var circles = Mathf.Max(3, (dist / radius * 0.3f).RoundToInt());
            const int linesInCircle = 5;
            var linesTotal = circles * linesInCircle;
            var m = Matrix4x4.LookAt(pt1, pt2, GetPerpendicular(dir));
            var prevPt = Vector3.zero;
            Vector3 nextPt;
            for (int i = 0; i < linesTotal; i++) {
                var angle = i * Mathf.PI * 2 / linesInCircle;
                nextPt = new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius, dist * i / (linesTotal - 1));
                nextPt = m.MultiplyPoint3x4(nextPt);
                if (i > 0)
                    DrawLine(prevPt, nextPt, width, col);
                prevPt = nextPt;
            }
        }
    }
}