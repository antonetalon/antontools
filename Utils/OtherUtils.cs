﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace AntonTools {
    public static partial class Utils {
        public static Texture2D ToReadWriteTexture(this Texture2D source) {
            Texture2D readable = null;
            source.DoWithTemporaryRenderTexture(rt => {
                readable = ToReadWriteTexture(rt);
            });
            return readable;
        }
        public static void DoWithTemporaryRenderTexture(this Texture2D source, Action<RenderTexture> action) {
            RenderTexture renderTex = RenderTexture.GetTemporary(
                        source.width,
                        source.height,
                        0,
                        RenderTextureFormat.Default,
                        RenderTextureReadWrite.Linear);
            Graphics.Blit(source, renderTex);
            action(renderTex);
            RenderTexture.ReleaseTemporary(renderTex);
        }
        public static Texture2D ToReadWriteTexture(this RenderTexture renderTexture) {
            var tex = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false, false);
            var oldRt = RenderTexture.active;
            RenderTexture.active = renderTexture;
            tex.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            tex.Apply();
            RenderTexture.active = oldRt;
            return tex;
        }
        public static void SavePNG(this RenderTexture renderTexture, string filePath) {
            var tex = renderTexture.ToReadWriteTexture();
            tex.SavePNG(filePath);
            UnityEngine.Object.Destroy(tex);
        }
        public static void SavePNG(this Texture2D tex, string filePath) {
            Utils.EnsureFolderExists(filePath);
            File.WriteAllBytes(filePath, tex.EncodeToPNG());
        }
        public static Texture2D LoadPNG(string icoPath, bool markNonReadable = true) {
            if (!File.Exists(icoPath)) return null;
            var fileData = File.ReadAllBytes(icoPath);
            var tex = new Texture2D(2, 2, TextureFormat.ARGB32, false, false);
            tex.LoadImage(fileData, markNonReadable);
            return tex;
        }
        public static void EnsureFolderExists(string path) {
            if (path.IsNothing()) return;
            var isDirectory = Path.GetExtension(path).IsNothing();
            var dir = isDirectory ? path : GetDirectoryName(path);
            if (Directory.Exists(dir))
                return;
            EnsureFolderExists(GetDirectoryName(dir));
            Directory.CreateDirectory(dir);
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
        public static string TryReadAllText(string path) {
            if (!File.Exists(path))
                return null;
            return File.ReadAllText(path);
        }
        public static byte[] TryReadAllBytes(string path) {
            if (!File.Exists(path))
                return null;
            return File.ReadAllBytes(path);
        }
#if !NO_COM_UNITY_MODULES_PHYSICS
        public static void Stop(this Rigidbody body) {
            body.velocity = Vector3.zero;
            body.angularVelocity = Vector3.zero;
        }
#endif
        public static bool IsVertical(this ScreenOrientation orient)
            => orient == ScreenOrientation.Portrait || orient == ScreenOrientation.PortraitUpsideDown;
        public static bool IsHorizontal(this ScreenOrientation orient)
            => orient == ScreenOrientation.LandscapeLeft || orient == ScreenOrientation.LandscapeRight;
        public static bool EnumEquals<TEnum>(this TEnum a, TEnum b) where TEnum : Enum
            => EqualityComparer<TEnum>.Default.Equals(a, b);
        public static bool DefaultEquals<T>(this T a, T b)
            => EqualityComparer<T>.Default.Equals(a, b);
        public static bool DefaultEqualsWithCollections<T>(this T a, T b) {
            if (a is IEnumerable aList && b is IEnumerable bList)
                return aList.ItemsEquals(bList);
            else
                return a.DefaultEquals(b);
        }
        public static TEnum EnumParse<TEnum>(string str, TEnum defaultValue = default) where TEnum : struct
            => Enum.TryParse<TEnum>(str, out var val) ? val : defaultValue;
        public static bool TryParseEnum(string str, Type type, out object value) {
            if (!str.IsNullOrEmpty() && Enum.IsDefined(type, str)) {
                value = Enum.Parse(type, str);
                return true;
            } else {
                value = null;
                return false;
            }
        }
        
        public static IEnumerable<TEnum> EnumValues<TEnum>() where TEnum : Enum {
            foreach (TEnum val in Enum.GetValues(typeof(TEnum)))
                yield return val;
        }
#if UNITY_EDITOR
        public static void ClearConsole() {
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.SceneView));
            var type = assembly.GetType("UnityEditor.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
        }
#endif
        public static void Swap<T>(ref T t1, ref T t2) {
            var temp = t1;
            t1 = t2;
            t2 = temp;
        }
        public static void Swap(this IList list, int ind1, int ind2) {
            var temp = list[ind1];
            list[ind1] = list[ind2];
            list[ind2] = temp;
        }
    }
    public delegate void ValRefAction<T1, T2>(T1 arg1, ref T2 arg2);
    public delegate void ValRefRefAction<T1, T2, T3>(T1 arg1, ref T2 arg2, ref T3 arg3);
    public delegate void RefAction<T1>(ref T1 arg1);
    public delegate void RefAction<T1, T2>(ref T1 arg1, ref T2 arg2);
    public delegate void RefAction<T1, T2, T3>(ref T1 arg1, ref T2 arg2, ref T3 arg3);
    public delegate void RefAction<T1, T2, T3, T4>(ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4);
}