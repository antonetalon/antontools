﻿using System;
namespace AntonTools {
    public class Singleton<T> where T : Singleton<T> {//, new() { private ctor should be ok.
        static T _instance;
        public static T instance {
            get {
                EnsureInstance();
                return _instance;
            }
        }
        public static void EnsureInstance() {
            if (_instance == null)
                _instance = (T)Activator.CreateInstance(typeof(T), true);
        }
        protected static void SaveInstanceInCtor(T instance) => _instance = instance;
    }
}