﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AntonTools {
    public static partial class Utils {
        public static void Quit() {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        public static bool IsUnityNull<T>(T value) {
            // So Unity's overriden null check does not handle this case.
            if (value is Object valueObj) return valueObj == null; 
            return value == null;
        }
        public static Sprite ToSprite(this Texture2D tex) 
            => Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        public static void Bind(this Toggle toggle, System.Func<bool> get, System.Action<bool> set) {
            toggle.isOn = get();
            toggle.onValueChanged.AddListener(value => set(value));
        }
        public static Texture2D ToTexture2D(this RenderTexture renderTexture) {
            var tex = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
            tex.filterMode = FilterMode.Point;
            RenderTexture.active = renderTexture;
            tex.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            tex.Apply();
            RenderTexture.active = null;
            return tex;
        }
        static int UILayer = LayerMask.NameToLayer("UI");
        public static bool IsCursorOverUI() {
            var eventData = new PointerEventData(EventSystem.current);
            eventData.position = Input.mousePosition;
            var raysastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, raysastResults);
            for (int index = 0; index < raysastResults.Count; index++) {
                var curRaysastResult = raysastResults[index];
                if (curRaysastResult.gameObject.layer == UILayer)
                    return true;
            }
            return false;
        }
        public static void CopySerializedFrom<T>(this T dest, T source) where T : Component {
            var fields = GetAllSerializedFields(typeof(T));
            foreach (var field in fields)
                field.SetValue(dest, field.GetValue(source));
            var props = GetAllSerializedProperties(typeof(T));
            foreach (var prop in props)
                prop.SetValue(dest, prop.GetValue(source));
        }
        private static IEnumerable<FieldInfo> GetAllSerializedFields(System.Type t) {
            if (t == null || !t.HasParent(typeof(Component)))
                yield break;
            var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            foreach (var f in t.GetFields(flags))
                yield return f;
            foreach (var f in GetAllSerializedFields(t.BaseType))
                yield return f;
        }
        private static IEnumerable<PropertyInfo> GetAllSerializedProperties(System.Type t) {
            if (t == null || !t.HasParent(typeof(Component)))
                yield break;
            var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            foreach (var f in t.GetProperties(flags)) {
                if (f.CanRead && f.CanWrite && t.GetProperty($"shared{f.Name.FromUpper()}") == null)
                    yield return f;
            }
            foreach (var f in GetAllSerializedProperties(t.BaseType))
                yield return f;
        }
        public static void FillParentRect(this RectTransform rect) {
            rect.pivot = Vector2.one * 0.5f;
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.one;
            rect.localRotation = Quaternion.identity;
            rect.localScale = Vector3.one;
            rect.offsetMin = Vector2.zero;
            rect.offsetMax = Vector2.zero;
        }
        public static void SetChanged(this Object c, bool andSaveOnDisk = false) {
#if UNITY_EDITOR
            if (c == null)
                return;
            UnityEditor.EditorUtility.SetDirty(c);
            if (andSaveOnDisk)
                UnityEditor.AssetDatabase.SaveAssets();
#endif
        }
        public static string RenamePath(string path, string newName) {
            var dir = Path.GetDirectoryName(path);
            var ext = Path.GetExtension(path);
            return $"{(dir.IsNothing() ? "" : dir)}/{newName}{ext}";
        }
        public static string GetDirectoryName(string path) {
            if (path.IsNothing()) return path;
            var dir = ToUnitySeparator(Path.GetDirectoryName(path));
            return dir;
        }
        public static string ToUnitySeparator(string path) {
            if (path.IsNothing()) return path;
            path = path.Replace('\\', '/');
            return path;
        }
#if UNITY_EDITOR
        public static UnityEditor.EditorBuildSettingsScene GetEditorBuildSettingsScene(string path)
            => UnityEditor.EditorBuildSettings.scenes.Find(s => s.path == path);
#endif
#if !NO_COM_UNITY_MODULES_PHYSICS
        public static bool Contains(this Collider collider, Vector3 pt, float eps = 0.001f)
            => Vector3.SqrMagnitude(pt - collider.ClosestPoint(pt)) < eps * eps;
#endif
        public static bool IsEdited(this TMP_InputField input) => EventSystem.current.currentSelectedGameObject == input.gameObject && input.isFocused;
        public static void SetTextSafe(this Text text, string str) {
            if (text != null)
                text.text = str;
        }
        public static void SetTextSafe(this TextMeshProUGUI text, string str) {
            if (text != null)
                text.text = str;
        }
        private static float CalcEnsureVisibilityScrollDelta(this ScrollRect scrollRect, RectTransform child, float padding) {
            var bounds = RectTransformUtility.CalculateRelativeRectTransformBounds(scrollRect.content, child.transform);

            var viewportHeight = scrollRect.viewport.rect.height;
            var scrollPosition = scrollRect.content.anchoredPosition;

            var elementTop = bounds.max.y;//child.anchoredPosition.y;
            var elementBottom = bounds.min.y;//elementTop - child.rect.height;

            var visibleContentTop = -scrollPosition.y - padding;
            var visibleContentBottom = -scrollPosition.y - viewportHeight + padding;

            var scrollDelta =
                elementTop > visibleContentTop ? visibleContentTop - elementTop :
                elementBottom < visibleContentBottom ? visibleContentBottom - elementBottom :
                0f;
            return scrollDelta;
        }

        public static void EnsureVisibility(this ScrollRect scrollRect, RectTransform child, float padding = 0) {
            var scrollDelta = CalcEnsureVisibilityScrollDelta(scrollRect, child, padding);
            var scrollPosition = scrollRect.content.anchoredPosition;
            scrollPosition.y += scrollDelta;
            scrollRect.content.anchoredPosition = scrollPosition;
        }
        public static async Task EnsureVisibilityAsync(this ScrollRect scrollRect, RectTransform child, float padding, float duration) {
            var scrollDelta = CalcEnsureVisibilityScrollDelta(scrollRect, child, padding);
            if (scrollDelta == 0f) return;
            var startPos = scrollRect.content.anchoredPosition;
            var endPos = startPos + Vector2.up * scrollDelta;
            await Awaiters.Seconds(duration, progress => scrollRect.content.anchoredPosition = Vector2.Lerp(startPos, endPos, progress));
        }
        public static void SetActiveSafe(this GameObject go, bool val) {
            if (go != null)
                go.SetActive(val);
        }
        public static void SetActiveSafe(this Component c, bool val) {
            if (c != null)
                c.gameObject.SetActive(val);
        }
        public static void SetSpriteSafe(this Image pic, Sprite sprite) {
            if (pic != null)
                pic.sprite = sprite;
        }
        public static void SetColorSafe(this Graphic uiElem, Color col) {
            if (uiElem != null)
                uiElem.color = col;
        }
        public static void SetWidthFromHeight(this Image pic) {
            if (pic == null || pic.sprite == null) return;
            var width = pic.rectTransform.rect.height * pic.sprite.rect.width / pic.sprite.rect.height;
            pic.rectTransform.sizeDelta = new Vector2(width, pic.rectTransform.sizeDelta.y);
        }
        public static void SetHeightFromWidth(this Image pic) {
            if (pic == null || pic.sprite == null) return;
            var height = pic.rectTransform.rect.width * pic.sprite.rect.height / pic.sprite.rect.width;
            pic.rectTransform.sizeDelta = new Vector2(pic.rectTransform.sizeDelta.x, height);
        }
        public static bool IsInsideScreen(Vector3 pos, Camera cam = null) {
            if (cam == null)
                cam = Camera.main;
            var viewportPos = cam.WorldToViewportPoint(pos);
            return viewportPos.x.InRange(0, 1) && viewportPos.y.InRange(0, 1);
        }
    }
}