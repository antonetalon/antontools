﻿using System;
using UnityEngine;

namespace AntonTools {
    public abstract class InPrefs<T> {
        protected readonly string key;
        public readonly T defaultValue;
        public InPrefs(string key, T defaultValue = default) {
            this.key = key;
            this.defaultValue = defaultValue;
        }
        public T value {
            get => PlayerPrefs.HasKey(key) ? Read() : defaultValue;
            set => Write(value);
        }
        public static implicit operator T(InPrefs<T> item) => item.value;

        protected virtual T Read() => PlayerPrefsUtils.Get(key, defaultValue);
        protected virtual void Write(T value) => PlayerPrefsUtils.Set(key, value);
    }
    public class BoolInPrefs : InPrefs<bool> {
        public BoolInPrefs(string key, bool defaultValue = default) : base(key, defaultValue) { }
    }
    public class IntInPrefs : InPrefs<int> {
        public IntInPrefs(string key, int defaultValue = default) : base(key, defaultValue) { }
    }
    public class FloatInPrefs : InPrefs<float> {
        public FloatInPrefs(string key, float defaultValue = default) : base(key, defaultValue) { }
    }
    public class StringInPrefs : InPrefs<string> {
        public StringInPrefs(string key, string defaultValue = default) : base(key, defaultValue) { }
    }
    public class EnumInPrefs<T> : InPrefs<T> where T : Enum {
        public EnumInPrefs(string key, T defaultValue = default) : base(key, defaultValue) { }
    }
}