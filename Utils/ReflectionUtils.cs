﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AntonTools {
    public static class ReflectionUtils {
        public static T Clone<T>(T instance) {
            var clone = Clone((object)instance);
            if (clone == default) return default;
            return (T)clone;
        }
        public static object Clone(object origin) {
            var alreadyCloned = new Dictionary<object, object>();
            return Clone(origin, alreadyCloned);
        }
        private static object Clone(object origin, Dictionary<object, object> alreadyCloned) {
            if (origin == null)
                return null;
            var t = origin.GetType();
            if (t.IsPrimitive || t == typeof(string) || t.IsEnum)
                return origin;
            if (alreadyCloned.TryGetValue(origin, out var clone))
                return clone;
            clone = CreateInstanceDefaultArgs(t);
            alreadyCloned[origin] = clone;
            if (t.HasParent(typeof(List<>)))
                return CloneList((IList)origin, (IList)clone); // Does not work with indexers, just list.
            while (t != null) {
                CloneMembers(origin, clone, t, alreadyCloned);
                t = t.BaseType;
            }
            return clone;

            IList CloneList(IList origin, IList clone) {
                foreach (var item in origin) {
                    var itemClone = Clone(item, alreadyCloned);
                    clone.Add(itemClone);
                }
                return clone;
            }
        }

        private static void CloneMembers(object origin, object clone, Type t, Dictionary<object, object> alreadyCloned) {
            foreach (var field in t.GetFields()) {
                if (field.IsStatic) continue;
                var fieldValue = field.GetValue(origin);
                var fieldValueClone = Clone(fieldValue, alreadyCloned);
                field.SetValue(clone, fieldValueClone);
            }
            foreach (var prop in t.GetProperties()) {
                if (!prop.CanWrite || (prop.Name == "Capacity" && t.HasParent(typeof(List<>))) || prop.GetIndexParameters().CountSafe() > 0)
                    continue;
                UnityEngine.Debug.Log($"{t.Name}.{prop.Name}");
                var propValue = prop.GetValue(origin);
                var propValueClone = Clone(propValue, alreadyCloned);
                prop.SetValue(clone, propValueClone);
            }
        }
        public static IEnumerable<FieldInfo> GetFields(this Type type, bool includeObjBase) {
            do {
                var f = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
                foreach (var fieldInfo in type.GetFields(f))
                    yield return fieldInfo;
                type = type.BaseType;
            } while (type != null && includeObjBase);
        }
        public static IEnumerable<FieldType> GetFields<FieldType>(object obj, 
            bool includeObjBase, bool includeFieldDerivedTypes) where FieldType : class {
            var type = obj.GetType();
            do {
                var f = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
                foreach (var fieldInfo in type.GetMembers(f)) {
                    var memberType = fieldInfo.GetFieldPropertyType();
                    if (memberType == typeof(FieldType)
                        || (includeFieldDerivedTypes && typeof(FieldType).IsAssignableFromAllowingGenerics(memberType)))
                        yield return GetValue(fieldInfo, obj) as FieldType;
                }
                type = type.BaseType;
            } while (type != null && includeObjBase);
        }
        public static object GetDefault(this Type t) => t.IsValueType ? Activator.CreateInstance(t) : null;
        private static string RemoveGenericName(string name) {
            if (name.Contains('`'))
                return name.Substring(0, name.IndexOf('`'));
            if (name.Contains('<'))
                return name.Substring(0, name.IndexOf('<'));
            return name;
        }
        public static string NameNoGeneric(this Type t, bool fullName = true) => RemoveGenericName(t.GetName(fullName));
        static List<(string name, string fullName, string shortName)> typeNameToShortName = new List<(string name, string fullName, string shortName)> {
            ( "Int16","System.Int16", "short" ),
            ( "Int32","System.Int32", "int" ),
            ( "Int64","System.Int64", "long" ),
            ( "String","System.String", "string" ),
            ( "Boolean","System.Boolean", "bool" ),
            ( "Single", "System.Single", "float"),
        };
        public static string NameInCode(this Type type, bool fullName = true) {
            var genericArgs = type.GetGenericArguments().PrintCollection(toString: t1 => t1.NameInCode(fullName));
            var name = $"{type.NameNoGeneric(fullName)}" +
                $"{(!type.IsGenericType ? "" : $"<{genericArgs}>")}";
            name = name.Replace("+", ".");
            foreach (var (currShortName, currFullName, currSimplifiedName) in typeNameToShortName) {
                if (fullName && currFullName == name)
                    name = currSimplifiedName;
                else if (!fullName && currShortName == name)
                    name = currSimplifiedName;
            }
            return name;
        }
        //public static Type GetTypeBy
        public static bool CanBeIdentifier(string str) {
            if (string.IsNullOrEmpty(str))
                return false;
            if (!char.IsLetter(str[0]) && str[0] != '_')
                return false;
            for (int i = 1; i < str.Length; ++i)
                if (!char.IsLetterOrDigit(str[i]) && str[i] != '_')
                    return false;
            return true;
        }
        #region Field-Property
        public static PropertyInfo GetPropertyInfo(Type type, string name)
            => type.GetProperty(name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly);
        public static FieldInfo GetFieldInfo(Type type, string name)
            => type.GetField(name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly);

        public static FieldInfo GetFieldInfo(Type type, string name, bool searchHierarchy) {
            var f = GetFieldInfo(type, name);
            if (f != null || !searchHierarchy) return f;
            foreach (var i in type.GetInterfaces()) {
                f = GetFieldInfo(i, name);
                if (f != null)
                    return f;
            }
            var baseClass = type.BaseType;
            if (baseClass == null) return null;
            return GetFieldInfo(baseClass, name, true);
        }
        public static PropertyInfo GetPropertyInfo(Type type, string name, bool searchHierarchy = false) {
            var f = GetPropertyInfo(type, name);
            if (f != null || !searchHierarchy) return f;
            foreach (var i in type.GetInterfaces()) {
                f = GetPropertyInfo(i, name);
                if (f != null)
                    return f;
            }
            var baseClass = type.BaseType;
            if (baseClass == null) return null;
            return GetPropertyInfo(baseClass, name, true);
        }
        public static MemberInfo GetFieldPropertyInfo(Type type, string name, bool searchHierarchy = true)
            => GetFieldInfo(type, name, searchHierarchy) ?? (MemberInfo)GetPropertyInfo(type, name, searchHierarchy);
        public static IEnumerable<PropertyInfo> GetPropertyInfos(Type type, bool searchHierarchy = false) {
            foreach (var prop in type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly))
                yield return prop;
            if (!searchHierarchy) yield break;
            foreach (var i in type.GetInterfaces()) {
                foreach (var interfaceProp in GetPropertyInfos(i))
                    yield return interfaceProp;
            }
            var baseClass = type.BaseType;
            if (baseClass == null) yield break;
            foreach (var baseProp in GetPropertyInfos(baseClass, searchHierarchy))
                yield return baseProp;
        }

        public static T GetProperty<T>(object obj, string name) {
            var value = GetProperty(obj, name);
            return value == null ? default : (T)value;
        }
        public static T GetField<T>(object obj, string name, bool searchHierarchy = true)
            => (T)GetField(obj, name, searchHierarchy);
        public static T GetFieldProperty<T>(object obj, string name, bool searchHierarchy = true)
            => (T)GetFieldPropertyInfo(obj.GetType(), name, searchHierarchy).GetValue(obj);
        public static PropertyInfo GetPropertyInfo(object obj, string name, bool searchHierarchy = false)
            => GetPropertyInfo(obj.GetType(), name, searchHierarchy);
        public static object GetProperty(object obj, string name, bool searchHierarchy = false)
            => GetPropertyInfo(obj, name, searchHierarchy).GetValue(obj);
        public static FieldInfo GetFieldInfo(object obj, string name)
            => GetFieldInfo(obj.GetType(), name);
        public static object GetField(object obj, string name, bool searchHierarchy = false) {
            var value = GetFieldInfo(obj.GetType(), name, searchHierarchy).GetValue(obj);
            return value;
        }
        public static object GetFieldProperty(object obj, string name, bool searchHierarchy = false) {
            var value = GetFieldPropertyInfo(obj.GetType(), name, searchHierarchy).GetValue(obj);
            return value;
        }
        public static void SetField<T>(object obj, string name, T value, bool baseClassFields = false)
            => SetField(obj, obj.GetType(), name, value, baseClassFields);
        public static void SetField<T>(object obj, Type type, string name, T value, bool baseClassFields = false) {
            var field = GetFieldInfo(type, name, baseClassFields);
            field.SetValue(obj, value);
        }
        #endregion

        public static PropertyInfo GetIndexerInfo(this Type type, Type itemType)
            => type.GetProperty("Item", itemType, new Type[] { typeof(int) });
        public static object GetAtIndex(object obj, int ind) => obj.GetType().GetProperty("Item").GetValue(obj, new object[] { ind });
        public static void SetAtIndex(object obj, int ind, object value) => obj.GetType().GetProperty("Item").SetValue(obj, value, new object[] { ind });
        
        
        public static PropertyInfo GetPropertyInfoMostParent(Type type, string name) {
            var baseClass = type.BaseType;
            if (baseClass == null)
                return GetPropertyInfo(type, name, false);
            var baseProp = GetPropertyInfoMostParent(baseClass, name);
            if (baseProp != null)
                return baseProp;
            return GetPropertyInfo(type, name, false);
        }
        
        
        
        public static PropertyInfo GetPropertyInfoWithDerived(this Type type, string name, bool searchHierarchy = false) {
            var prop = GetPropertyInfo(type, name, searchHierarchy);
            if (prop != null) return prop;
            foreach (var derived in GetAllDerivedTypes(type, false)) {
                prop = GetPropertyInfo(derived, name, false);
                if (prop != null)
                    return prop;
            }
            return null;
        }
        public static void SetProperty<T>(object obj, string name, T value, bool searchHierarchy) {
            var prop = GetPropertyInfo(obj, name, searchHierarchy);
            prop.SetValue(obj, value);
        }
        public static FieldInfo GetStaticFieldInfo(Type type, string name)
            => (type == null || type == typeof(object)) ? null :
            (type.GetField(name, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy)
            ?? GetStaticFieldInfo(type.BaseType, name));
        public static PropertyInfo GetStaticPropertyInfo(Type type, string name)
            => type == null ? null :
            (type.GetProperty(name, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy)
            ?? GetStaticPropertyInfo(type.BaseType, name));
        public static T GetStaticField<T>(this Type type, string name) {
            var value = GetStaticField(type, name);
            return value == null ? default : (T)value;
        }
        public static object GetStaticField(this Type type, string name, bool failIfNoField = true) {
            var field = GetStaticFieldInfo(type, name);
            if (field != null)
                return field.GetValue(null);
            var prop = GetStaticPropertyInfo(type, name);
            if (prop != null)
                return prop.GetValue(null);
            if (failIfNoField)
                throw new Exception($"{type.Name} has no static field or property with name {name}");
            return default;
        }
        public static void SetStaticField<T>(this Type type, string name, T value) {
            var field = GetStaticFieldInfo(type, name);
            field.SetValue(null, value);
        }

        public static bool CanCreateInstance(this Type type) => !type.IsAbstract && !type.ContainsGenericParameters && !type.IsInterface;
        public static object CreateInstance(Type type, params object[] args) => Activator.CreateInstance(type, args);
        public static T CreateInstance<T>(Type type, params object[] args) => (T)CreateInstance(type, args);
        static Dictionary<Type, object[]> ctorArgs;
        public static object CreateInstanceDefaultArgs(Type type) {
            //System.Runtime.Serialization.FormatterServices.GetUninitializedObject(type);
            if (ctorArgs == null)
                ctorArgs = new Dictionary<Type, object[]>();
            if (ctorArgs.TryGetValue(type, out var argsArray))
                return Activator.CreateInstance(type, argsArray);
            var args = new List<object>();
            while (true) {
                if (args.Count > 100) {
                    UnityEngine.Debug.LogError($"CreateInstanceDefaultArgs cant find ctor for {type.Name}");
                    return null;
                }
                try {
                    argsArray = args.ToArray();
                    var inst = Activator.CreateInstance(type, argsArray);
                    ctorArgs[type] = argsArray;
                    return inst;
                } catch (Exception _) {
                    args.Add(null);
                }
            }
        }
        static Dictionary<(string typeName, bool allAsemblies, bool fullName), Type> getTypeByNameCache 
            = new Dictionary<(string typeName, bool allAsemblies, bool fullName), Type>();
        public static Type GetTypeByName(string typeName, bool allAsemblies = false, bool fullName = false) {
            getTypeByNameCache.GetOrAddValue((typeName, allAsemblies , fullName), out var t, ()=> {
                Type type = null;
                var typeNameParts = typeName.Split(new char[] { '<', ',', '>' }, StringSplitOptions.RemoveEmptyEntries).ConvertAll(s => s.Trim());
                var searchGeneric = typeNameParts.Count > 1;//typeName.Contains('<');
                string notGenericName = null;
                Type[] genericArgs = null;
                if (searchGeneric) {
                    notGenericName = typeNameParts[0];//RemoveGenericName(typeName);
                    genericArgs = typeNameParts.ToList().Without(typeNameParts[0]).ConvertAll(argName => GetTypeByName(argName, allAsemblies, fullName)).ToArray();
                }
                IterateTypes(currType => {
                    //UnityEngine.Debug.Log($"TYPE = {currType.FullName} in {currType.Assembly.FullName}"); 
                    if (!searchGeneric) {
                        if (currType.GetName(fullName) == typeName)
                            type = currType;
                    } else {
                        if (currType.IsGenericType && currType.MemberType == MemberTypes.TypeInfo && currType.NameNoGeneric(fullName) == notGenericName) {//NameInCode(currType, fullName) == typeName)
                            var argsCount = typeNameParts.Count - 1;
                            if (argsCount != currType.GetGenericArguments().Length)
                                return;
                            try {
                                type = currType.MakeGenericType(genericArgs);
                            } catch { }
                        }
                    }
                }, allAsemblies);
                return type;
            });
            return t;
        }

        public static Type GetTypeByName(string typeName, Assembly assembly, bool fullName = false) {
            Type type = null;
            IterateTypes(currType => {
                if (currType.GetName(fullName) == typeName)
                    type = currType;
            }, assembly);
            return type;
        }
        public static string GetName(this Type type, bool fullName) => fullName ? type.FullName : type.Name;
        public static void IterateTypes(Action<Type> action, bool allAsemblies = false) {
            if (allAsemblies)
                AppDomain.CurrentDomain.GetAssemblies().ForEach(assembly => IterateTypes(action, assembly));
            else
                ForEachUnityAssemblies(assembly=> IterateTypes(action, assembly));
        }
        public static void IterateTypes(Action<Type> action, Assembly assembly) {
            var types = assembly.GetTypes();
            foreach (var currType in types)
                action(currType);
            //foreach (var currTypeNoNesting in types)
            //IterateNestedTypesRecursively(currTypeNoNesting, action);
        }
        private static void IterateNestedTypesRecursively(Type type, Action<Type> action) {
            action(type);
            var nestedTypes = type.GetNestedTypes();
            nestedTypes.ForEach(nestedType => IterateNestedTypesRecursively(nestedType, action));
        }
        public static void ForEachDerivedType<T>(Action<Type> action)
            => ForEachDerivedType(typeof(T), action);
        public static void ForEachDerivedType(Type t, Action<Type> action) {
            var types = GetAllDerivedTypes(t);
            types.ForEach(action);
        }
        public static Assembly GetExecutingAssembly() => GetAssemblyByName("Assembly-CSharp");
        public static Assembly GetEditorAssembly() => GetAssemblyByName("Assembly-CSharp-Editor");
        public static Assembly GetFirstPassAssembly() => GetAssemblyByName("Assembly-CSharp-firstpass");
        public static Assembly GetEditorFirstPassAssembly() => GetAssemblyByName("Assembly-CSharp-Editor-firstpass");
        public static Assembly GetAntonToolsAssembly() => GetAssemblyByName(AntonToolsManager.MainPluginName);
        public static Assembly GetAssemblyByName(string name) {
            return AppDomain.CurrentDomain.GetAssemblies().
                   SingleOrDefault(assembly => assembly.GetName().Name == name);
        }
        public static void ForEachUnityAssemblies(Action<Assembly> action) {
            void ActionIfAssemblyExists(Assembly assembly) {
                if (assembly != null)
                    action(assembly);
            }
#if UNITY_EDITOR
            ActionIfAssemblyExists(GetExecutingAssembly());
            ActionIfAssemblyExists(GetEditorAssembly());
            ActionIfAssemblyExists(GetFirstPassAssembly());
            ActionIfAssemblyExists(GetEditorFirstPassAssembly());
            ActionIfAssemblyExists(GetAntonToolsAssembly());
#else
            ActionIfAssemblyExists(GetExecutingAssembly());
            ActionIfAssemblyExists(GetAntonToolsAssembly());
#endif
        }

        public static bool DoesClassExistInUnityAssembly(string classNameWithNamespace) {
            if (string.IsNullOrEmpty(classNameWithNamespace))
                return false;
            bool exists = false;
            ForEachUnityAssemblies(assembly => {
                if (assembly?.GetType(classNameWithNamespace) != null)
                    exists = true;
            });
            return exists;
        }
        public static string GetMembersDescription(Type t) {
            bool MemberIgnored(MemberInfo m) => m.DeclaringType == typeof(Object);
            var sb = StringUtils.InitStringBuilder();
            sb.AppendLine($"{t.FullName} members:");
            var properties = t.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var m in t.GetMembers(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)) {
                if (MemberIgnored(m))
                    continue;
                if (m.MemberType == MemberTypes.Method || m.MemberType == MemberTypes.Property || m.MemberType == MemberTypes.Field)
                    continue;
                sb.AppendLine($"member {m.Name} {m.MemberType} {m.GetType().Name}");
            }
            foreach (var m in t.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)) {
                if (MemberIgnored(m))
                    continue;
                sb.AppendLine(GetFieldDescription(m));
            }
            foreach (var m in properties) {
                if (MemberIgnored(m))
                    continue;
                sb.AppendLine(GetPropertyDescription(m));
            }
            foreach (var m in t.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)) {
                if (MemberIgnored(m))
                    continue;
                var isPropertyMethod = properties.Any(p => p.GetMethod == m || p.SetMethod == m);
                if (isPropertyMethod)
                    continue;
                sb.AppendLine(GetMethodDescription(m));
            }
            if (t.IsEnum) {
                if (t.IsGenericType)
                    sb.AppendLine("enum is nested in generic type, getting its values not implemented");
                else {
                    foreach (var val in Enum.GetValues(t))
                        sb.AppendLine($"enum value {val}");
                }
            }
            return sb.ToString();
        }
        private static string Desc(bool propertyPresence, string propertyName) => propertyPresence ? propertyName : "";
        public static bool IsPrimitiveOrDecimal(this Type t) => t.IsPrimitive || t == typeof(decimal);
        public static bool IsStruct(this Type t) => t.IsValueType && !t.IsPrimitive && !t.IsEnum && t != typeof(decimal);
        public static string GetTypeDescription(Type t)
            => $"{Desc(t.IsPublic, "public ")}{Desc(t.IsNestedPublic, "nested public ")}{Desc(t.IsNotPublic, "private ")}" +
            $"{Desc(t.IsClass, "class ")}{Desc(t.IsInterface, "interface ")}{Desc(t.IsEnum, "enum ")}{Desc(t.IsStruct(), "struct ")}{GetNameWithBaseTypes(t)}" +
            $"\n{GetMembersDescription(t)}";

        private static string GetNameWithBaseTypes(Type t) {
            if (t == null || t == typeof(object))
                return "";
            return $"{t.FullName} : {GetNameWithBaseTypes(t.BaseType)}";
        }

        public static string GetAssemblyDescription(Assembly assembly, string searchInTypeNames = null, string searchInTypeDescriptions = null) {
            if (searchInTypeNames != null)
                searchInTypeNames = searchInTypeNames.ToLower();
            if (searchInTypeDescriptions != null)
                searchInTypeDescriptions = searchInTypeDescriptions.ToLower();
            var types = assembly.GetTypes();
            var sb = new StringBuilder($"Description of assembly {assembly.FullName} ({types.Length} types total):\n");
            foreach (var currType in types) {
                if (!string.IsNullOrEmpty(searchInTypeNames) && !currType.FullName.ToLower().Contains(searchInTypeNames))
                    continue;
                var typeDescription = GetTypeDescription(currType);
                if (!string.IsNullOrEmpty(searchInTypeDescriptions) && !typeDescription.ToLower().Contains(searchInTypeDescriptions))
                    continue;
                sb.AppendLine(typeDescription);
            }
            return sb.ToString();
        }
        public static string GetAllAssembliesDescription(string searchInTypeNames = null, string searchInTypeDescriptions = null) {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var sb = new StringBuilder($"All assemblies({assemblies.Length}) description:\n");
            assemblies.ForEach(a => sb.AppendLine(GetAssemblyDescription(a, searchInTypeNames, searchInTypeDescriptions)));
            return sb.ToString();
        }
        static string GetVisibilityString(bool isPublic, bool isPrivate, bool isProtected, bool isInternal)
            => $"{Desc(isPublic, "public ")}{Desc(isPrivate, "private ")}{Desc(isProtected, "protected ")}{Desc(isInternal, "internal ")}";
        static string GetVisibilityString(MethodInfo m) => GetVisibilityString(m.IsPublic, m.IsPrivate, m.IsFamily, m.IsAssembly);
        static string GetVisibilityString(FieldInfo m) => GetVisibilityString(m.IsPublic, m.IsPrivate, m.IsFamily, m.IsAssembly);
        public static string GetMethodDescription(MethodInfo m)
            => $"method {GetVisibilityString(m)} {Desc(m.IsAbstract, "abstract ")}{Desc(m.IsVirtual, "virtual ")} {Desc(m.IsStatic, "static ")} {m.ReturnType.Name} " +
                $"{m.Name}({m.GetParameters().ConvertAll(p => $"{p.ParameterType.Name.ToLower()} {p.Name}").PrintCollection(",", "")}) " +
            $"{(m.ContainsGenericParameters ? ("has generic params") : "")}";
        public static string GetFieldDescription(FieldInfo m)
            => $"field {GetVisibilityString(m)}{Desc(m.IsStatic, "static ")}{m.FieldType.Name} {m.Name}";
        public static string GetPropertyDescription(PropertyInfo m)
            => $"property " +
            $"{(m.GetAccessors(true).Any(a => a.IsStatic) ? "static" : "")}" +
            $" {m.PropertyType} {m.Name} {{ {(m.CanRead ? $"{GetVisibilityString(m.GetMethod)}get;" : "")} {(m.CanWrite ? $"{GetVisibilityString(m.SetMethod)}set;" : "")} }}";

        public static bool DoesAssemblyExist(string name) => GetAssemblyByName(name) != null;
        public static List<Type> GetAllDerivedTypes<T>() => GetAllDerivedTypes(typeof(T));
        static Dictionary<Type, List<Type>> derivedTypesCache;
        public static List<Type> GetAllDerivedTypes(Type baseClass, bool includingSelf = false)
            => Utils.CachedGet(baseClass, () => {
                var result = new List<Type>();
                ForEachUnityAssemblies(assembly => result.AddRange(GetAllDerivedTypes(baseClass, assembly, includingSelf)));
                return result;
            }, ref derivedTypesCache);
        public static List<Type> GetAllUnityAssemblyTypes() {
            var result = new List<Type>();
            ForEachUnityAssemblies(assembly => result.AddRange(assembly.GetTypes()));
            return result;
        }
        public static List<Type> GetAllDerivedTypes(Type baseClass, Assembly checkedAssembly, bool includingSelf = false) {
            var result = new List<Type>();
            IterateTypes(type => {
                if (!includingSelf && baseClass == type)
                    return;
                if (baseClass.IsAssignableFromAllowingGenerics(type))
                    result.Add(type);
            }, checkedAssembly);
            return result;
        }
        public static bool IsAssignableFromAllowingGenerics(this Type baseCanBeGenericType, Type givenType) {
            if (!baseCanBeGenericType.IsGenericType)
                return baseCanBeGenericType.IsAssignableFrom(givenType);
            var interfaceTypes = givenType.GetInterfaces();
            foreach (var it in interfaceTypes) {
                if (it.IsGenericType && it.GetGenericTypeDefinition() == baseCanBeGenericType)
                    return true;
            }
            if (givenType == baseCanBeGenericType)
                return true;
            if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == baseCanBeGenericType)
                return true;
            Type baseType = givenType.BaseType;
            if (baseType == null)
                return false;
            return baseCanBeGenericType.IsAssignableFromAllowingGenerics(baseType);
        }
        public static bool HasParent(this Type derivedType, Type parentType, bool allowingGenerics = true, bool allowSame = true) {
            if (!allowSame && derivedType == parentType)
                return false;
            return allowingGenerics ? parentType.IsAssignableFromAllowingGenerics(derivedType) : parentType.IsAssignableFrom(derivedType);
        }
        public static Type GetGenericArgument(this Type type, Type genericParentType) {
            foreach (var (definedInType, genericArg) in type.GetGenericArgumentsRecursive()) {
                if (definedInType.GetGenericTypeDefinition() == genericParentType)
                    return genericArg;
            }
            return null;
        }
        public static IEnumerable<Type> GetInterfaces(this Type type, bool hierarchy) {
            var interfaces = type.GetInterfaces();
            foreach (var i in  interfaces) {
                if (!hierarchy && interfaces.Any(ii => ii != i && ii.HasParent(i)))
                    continue;
                yield return i;
            }
        }
        public static IEnumerable<(Type definedInType, Type genericArg)> GetGenericArgumentsRecursive(this Type type) {
            while (type!=null) {
                var args = type.GetGenericArguments();
                if (args != null)
                    foreach (var arg in args)
                        yield return (type, arg);
                var interfaces = type.GetInterfaces();
                if (interfaces != null) {
                    foreach (var interf in interfaces) {
                        foreach (var (definedInType, genericArg) in interf.GetGenericArgumentsRecursive())
                            yield return (definedInType, genericArg);
                    }
                }
                type = type.BaseType;
            }
        }
        public static bool DoesClassExist(string classNameWithNamespace) => GetTypeByName(classNameWithNamespace, allAsemblies: true, fullName: true) != null;
        public static bool TryGetAttribute<TAttribute>(this Type type, out TAttribute attribute)
            where TAttribute : Attribute {
            attribute = Attribute.GetCustomAttribute(type, typeof(TAttribute), true) as TAttribute;
            return attribute != null;
        }
        public static List<TAttribute> GetAllAttributeInstances<TAttribute>(this Type type)
            where TAttribute : Attribute
            => Attribute.GetCustomAttributes(type, typeof(TAttribute), true).ConvertAll(a => a as TAttribute);
        public static bool HasAttribute<TAttribute>(this MemberInfo m)
            where TAttribute : Attribute
            => Attribute.IsDefined(m, typeof(TAttribute));
        public static object GetValue(this MemberInfo m, object obj) {
            if (m is FieldInfo f)
                return f.GetValue(obj);
            else
                return (m as PropertyInfo).GetValue(obj);
        }
        public static void SetValue(this MemberInfo m, object obj, object value) {
            if (m is FieldInfo f)
                f.SetValue(obj, value);
            else
                (m as PropertyInfo).SetValue(obj, value);
        }
        public static Type GetFieldPropertyType(this MemberInfo m) {
            if (m is FieldInfo f)
                return f.FieldType;
            else if (m is PropertyInfo p)
                return p.PropertyType;
            else
                return null;
        }
        public static object CallStaticMethod(this Type t, string name, params object[] methodParams) {
            MethodInfo methodInfo;
            var currT = t;
            do {
                if (currT == null) throw new Exception($"static method {name} not found in {t.Name}");
                methodInfo = currT.GetMethod(name, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                currT = currT.BaseType;
            } while (methodInfo == null);
            return methodInfo.Invoke(null, methodParams);
        }
        public static void CallStaticMethodExplicitParamTypes(this Type t, string name, params (Type paramType, object paramValue)[] methodParams) {
            var methodInfo = GetMethodInfo(t, name, methodParams);
            methodInfo.Invoke(null, methodParams.ConvertAll(p => p.paramValue).ToArray());
        }
        public static object CallStaticGenericMethod(this Type t, string name, Type genericParam, Type[] types, params object[] methodParams) {
            var genericMethodInfo = t.GetMethod(name, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public, null, types, null);
            var methodInfo = genericMethodInfo.MakeGenericMethod(genericParam);
            return methodInfo.Invoke(null, methodParams);
        }
        public static object CallMethod(object obj, string name, params object[] methodParams) {
            var type = obj.GetType();
            MethodInfo methodInfo;
            do {
                methodInfo = type.GetMethod(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                if (methodInfo != null)
                    break;
                type = type.BaseType;
            } while (type != null);
            return methodInfo.Invoke(methodInfo.IsStatic ? null : obj, methodParams);
        }
        public static void CallMethodExplicitParamTypes(object obj, string name, params (Type paramType, object paramValue)[] methodParams) {
            var methodInfo = GetMethodInfo(obj.GetType(), name, methodParams);
            methodInfo.Invoke(methodInfo.IsStatic ? null : obj, methodParams.ConvertAll(p => p.paramValue).ToArray());
        }
        private static MethodInfo GetMethodInfo(Type type, string name, params (Type paramType, object paramValue)[] methodParams) 
            => type.GetMethod(name, methodParams.ConvertAll(p => p.paramType).ToArray());
        public static List<MemberInfo> GetMembersWithAttribute<TAttribute>(this Type type)
            where TAttribute : Attribute {
            var members = new List<MemberInfo>();
            foreach (var member in type.GetFields()) {
                if (member.HasAttribute<TAttribute>())
                    members.Add(member);
            }
            foreach (var member in type.GetProperties()) {
                if (member.HasAttribute<TAttribute>())
                    members.Add(member);
            }
            return members;
        }
        public static bool TryParseEnum(string value, Type enumType, out object parsedValue) {
            if (Enum.IsDefined(enumType, value)) {
                parsedValue = Enum.Parse(enumType, value);
                return true;
            } else {
                parsedValue = null;
                return false;
            }
        }
    }
}