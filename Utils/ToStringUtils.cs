﻿using System;
using System.Globalization;

namespace AntonTools {
    public static partial class Utils {
        public static NumberFormatInfo useDot = new NumberFormatInfo { NumberDecimalSeparator = "." };
        public static string ToString(this float amount, int decimalPlaces, bool signed = false) {
            var str = Math.Round(amount, decimalPlaces).ToString(useDot);
            if (signed && amount > 0)
                str = $"+{str}";
            return str;
        }
        public static string ToString(this double amount, int decimalPlaces, bool signed = false) {
            var str = Math.Round(amount, decimalPlaces).ToString(useDot);
            if (signed && amount > 0)
                str = $"+{str}";
            return str;
        }
    }
}