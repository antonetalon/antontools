﻿using AntonTools;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AntonTools {
    /// <summary>
    /// Inherited classes must be [Serializable].
    /// valueOptions can be called in edit mode.
    /// </summary>
    [Serializable] public abstract class ExtendableEnum {
        [SerializeField] string _value;
        public string value => _value;
        public abstract IReadOnlyList<string> valueOptions { get; }
        public override bool Equals(object obj) => (obj is ExtendableEnum enum2) && enum2.value == value;
        public override int GetHashCode() => value.GetHashCode();
        public override string ToString() => value;
        public bool valid => valueOptions.Contains(value);
        public ExtendableEnum(string value) {
            this._value = value;
            Debug.Assert(valid, $"value {value} have to be one of following: {valueOptions.PrintCollection()}");
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ExtendableEnum), true)]
    public class ExtendableEnumPropertyDrawer : TypeInspectorView<ExtendableEnum> {
        string[] optionStrings;
        protected override void OnGUI(string name, ref ExtendableEnum value, ref bool changed) {
            if (optionStrings == null)
                optionStrings = value.valueOptions.ToArray();
            var ind = optionStrings.IndexOf(value.value);
            if (EditorGUIUtils.Popup(name, ref ind, optionStrings, ref changed))
                value = (ExtendableEnum)Activator.CreateInstance(value.GetType(), optionStrings.GetElementSafe(ind));
        }
    }
#endif
}