﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace AntonTools {
    /// <summary>
    /// Inherited classes must be [CustomPropertyDrawer(typeof(T), true/false)].
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class TypeInspectorView<T> : PropertyDrawer {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => 0;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var value = property.GetTargetValue<T>();
            var changed = false;
            OnGUI(property.name, ref value, ref changed);
            if (!changed) return;
            property.SetTargetValue(value);
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
        protected abstract void OnGUI(string propertyName, ref T value, ref bool changed);
    }
}
#endif