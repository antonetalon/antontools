﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools
{
    public abstract class OcTree<TItem> : OcTree<TItem, OcTreeNode<TItem>>
    {
        protected OcTree(float minHalfWidth) : base(minHalfWidth) { }
        protected override OcTreeNode<TItem> CreateNode()
            => new OcTreeNode<TItem>(this);
    }
    public abstract class OcTree<TItem, TNode> : OcTreeNode<TItem>.OcTree
        where TNode : OcTreeNode<TItem>
    {
        protected int startNodeCapacity;
        public OcTree(float minHalfWidth = 0.001f, int nodesPoolItemsCount = 100, int startNodeCapacity = 2) {
            this.minHalfWidth = minHalfWidth;
            this.startNodeCapacity = startNodeCapacity;
            nodesPool = new Pool<TNode>(nodesPoolItemsCount, CreateNode);
        }

        public Pool<TNode> nodesPool;
        protected TNode root;
        protected abstract TNode CreateNode();
        private TNode CreateNode(Vector3 center, float halfWidth) {
            var node = nodesPool.Get();
            node.Init(center, halfWidth);
            return node;
        }
        public void Add(TItem item) {
            var itemPos = GetPos(item);
            if (root == null)
                root = CreateNode(Vector2.zero, minHalfWidth * 4 + 1.97f * Mathf.Max(Mathf.Abs(itemPos.x), Mathf.Abs(itemPos.y), Mathf.Abs(itemPos.z)));
            while (!Contains(root, item))
                root = CreateParent(root, itemPos);
            root.Add(itemPos, item);
        }
        public void Remove(TItem item) {
            if (root == null) return;
            root.Remove(GetPos(item), item);
        }

        public IEnumerable<TItem> GetCloseItems(Vector3 pos, float r) {
            if (root == null) yield break;
            foreach (var item in root.GetCloseItems(pos, r))
                yield return item;
        }

        public TNode CreateParent(TNode currOc, Vector3 itemPos) {
            var parentHalfWidth = currOc.halfWidth * 2;
            var parentPosShift = new Vector3(itemPos.x > currOc.center.x ? 1 : -1, itemPos.y > currOc.center.y ? 1 : -1, itemPos.z > currOc.center.z ? 1 : -1) * currOc.halfWidth;
            var parent = CreateNode(currOc.center + parentPosShift, parentHalfWidth);
            parent.children[(int)parent.GetChildOcPos(currOc.center)] = currOc;
            return parent;
        }
        public override OcTreeNode<TItem> CreateChild(OcTreeNode<TItem> node, Vector3 pos) {
            var center = node.center;
            var childHalfWidth = node.childHalfWidth;
            var ocPos = node.GetChildOcPos(pos);
            OcTreeNode<TItem> newNode = null;
            switch (ocPos) {
                case OcPos.XYZ: node.XYZ = newNode = CreateNode(center + new Vector3(1, 1, 1) * childHalfWidth, childHalfWidth); break;
                case OcPos.XyZ: node.XyZ = newNode = CreateNode(center + new Vector3(1, -1, 1) * childHalfWidth, childHalfWidth); break;
                case OcPos.xYZ: node.xYZ = newNode = CreateNode(center + new Vector3(-1, 1, 1) * childHalfWidth, childHalfWidth); break;
                case OcPos.xyZ: node.xyZ = newNode = CreateNode(center + new Vector3(-1, -1, 1) * childHalfWidth, childHalfWidth); break;
                case OcPos.XYz: node.XYz = newNode = CreateNode(center + new Vector3(1, 1, -1) * childHalfWidth, childHalfWidth); break;
                case OcPos.Xyz: node.Xyz = newNode = CreateNode(center + new Vector3(1, -1, -1) * childHalfWidth, childHalfWidth); break;
                case OcPos.xYz: node.xYz = newNode = CreateNode(center + new Vector3(-1, 1, -1) * childHalfWidth, childHalfWidth); break;
                case OcPos.xyz: node.xyz = newNode = CreateNode(center + new Vector3(-1, -1, -1) * childHalfWidth, childHalfWidth); break;
            }
            return newNode;
        }

        void DestroyNode(TNode node) {
            node.Clear();
            nodesPool.Return(node);
        }
        public void Clear() {
            if (root == null) return;
            DestroyNode(root);
            root = null;
        }
        public override void ClearChildren(OcTreeNode<TItem> node) {
            for (int i = 0; i < node.children.Length; i++) {
                var child = node.children[i];
                if (child == null) continue;
                DestroyNode((TNode)child);
                node.children[i] = null;
            }
        }

        public void IteratePartialOpening(Action<TNode> onClosedNodeFound, Action<TItem> onItemFound, Func<TNode, bool> openingCondition) {
            if (root == null) return;
            IteratePartialOpening(root, onClosedNodeFound, onItemFound, openingCondition);
        }
        private void IteratePartialOpening(TNode node, Action<TNode> onClosedNodeFound, Action<TItem> onItemFound, Func<TNode, bool> openingCondition) {
            if (!openingCondition(node))
                onClosedNodeFound(node);
            else {
                foreach (var item in node.items)
                    onItemFound(item);
                for (int i = 0; i < node.children.Length; i++) {
                    var child = node.children[i] as TNode;
                    if (child != null)
                        IteratePartialOpening(child, onClosedNodeFound, onItemFound, openingCondition);
                }
            }
        }

        public void DebugDraw(bool drawOcs, bool drawItems, float vertexSize, Color ocColor, Color itemColor) {

            void Draw(TNode node) {
                if (node == null) return;
                if (drawOcs) {
                    var X = new Vector3(node.halfWidth, 0, 0);
                    var Y = new Vector3(0, node.halfWidth, 0);
                    var Z = new Vector3(0, 0, node.halfWidth);
                    var XYZ = node.center + X + Y + Z;
                    var XyZ = node.center + X - Y + Z;
                    var xYZ = node.center - X + Y + Z;
                    var xyZ = node.center - X - Y + Z;
                    var XYz = node.center + X + Y - Z;
                    var Xyz = node.center + X - Y - Z;
                    var xYz = node.center - X + Y - Z;
                    var xyz = node.center - X - Y - Z;
                    Debug.DrawLine(XYZ, xYZ, ocColor);
                    Debug.DrawLine(XYZ, XyZ, ocColor);
                    Debug.DrawLine(xyZ, xYZ, ocColor);
                    Debug.DrawLine(xyZ, XyZ, ocColor);
                    Debug.DrawLine(XYz, xYz, ocColor);
                    Debug.DrawLine(XYz, Xyz, ocColor);
                    Debug.DrawLine(xyz, xYz, ocColor);
                    Debug.DrawLine(xyz, Xyz, ocColor);
                    Debug.DrawLine(XYZ, XYz, ocColor);
                    Debug.DrawLine(XyZ, Xyz, ocColor);
                    Debug.DrawLine(xYZ, xYz, ocColor);
                    Debug.DrawLine(xyZ, xyz, ocColor);

                }
                if (drawItems)
                    node.items.ForEach(item => Utils.DrawCross(GetPos(item), vertexSize, itemColor));
                for (int i = 0; i < node.children.Length; i++)
                    Draw(node.children[i] as TNode);
            }
            Draw(root);
        }
    }
    public enum OcPos { XYZ = 0, XyZ = 1, xYZ = 2, xyZ = 3, XYz = 4, Xyz = 5, xYz = 6, xyz = 7 }
}