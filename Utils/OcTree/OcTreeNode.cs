﻿using System.Collections.Generic;
using UnityEngine;

namespace AntonTools
{
    public class OcTreeNode<TItem>
    {
        public List<TItem> items;
        public OcTreeNode<TItem> XYZ { get => children[(int)OcPos.XYZ]; set => children[(int)OcPos.XYZ] = value; }
        public OcTreeNode<TItem> XyZ { get => children[(int)OcPos.XyZ]; set => children[(int)OcPos.XyZ] = value; }
        public OcTreeNode<TItem> xYZ { get => children[(int)OcPos.xYZ]; set => children[(int)OcPos.xYZ] = value; }
        public OcTreeNode<TItem> xyZ { get => children[(int)OcPos.xyZ]; set => children[(int)OcPos.xyZ] = value; }
        public OcTreeNode<TItem> XYz { get => children[(int)OcPos.XYz]; set => children[(int)OcPos.XYz] = value; }
        public OcTreeNode<TItem> Xyz { get => children[(int)OcPos.Xyz]; set => children[(int)OcPos.Xyz] = value; }
        public OcTreeNode<TItem> xYz { get => children[(int)OcPos.xYz]; set => children[(int)OcPos.xYz] = value; }
        public OcTreeNode<TItem> xyz { get => children[(int)OcPos.xyz]; set => children[(int)OcPos.xyz] = value; }
        public readonly OcTreeNode<TItem>[] children = new OcTreeNode<TItem>[8];
        public Vector3 center;
        public float halfWidth;

        bool isMinimum => childHalfWidth < ocTree.minHalfWidth;
        public float childHalfWidth => halfWidth * 0.5f;
        float boundingRadius => halfWidth * 1.74f;

        public abstract class OcTree
        {
            public float minHalfWidth { get; protected set; } // few times smaller than radius of common search.
            public abstract OcTreeNode<TItem> CreateChild(OcTreeNode<TItem> node, Vector3 pos);
            public abstract Vector3 GetPos(TItem item);
            public abstract bool Contains(OcTreeNode<TItem> node, TItem item);
            public abstract void ClearChildren(OcTreeNode<TItem> node);
        }
        OcTree ocTree;
        public OcTreeNode(OcTree ocTree, int itemsStartCapacity = 0) {
            this.ocTree = ocTree;
            items = new List<TItem>(itemsStartCapacity);
        }
        public virtual void Init(Vector3 center, float halfWidth) {
            this.center = center;
            this.halfWidth = halfWidth;
        }
        public void Clear() {
            items.Clear();
            ocTree.ClearChildren(this);
        }
        public bool Contains(Vector3 pos)
            => Mathf.Abs(pos.x - center.x) < halfWidth && Mathf.Abs(pos.y - center.y) < halfWidth && Mathf.Abs(pos.z - center.z) < halfWidth;

        public OcTreeNode<TItem> GetChild(Vector3 pos) => children[(int)GetChildOcPos(pos)];
        public OcPos GetChildOcPos(Vector3 pos) {
            var xPlus = pos.x > center.x;
            var yPlus = pos.y > center.y;
            var zPlus = pos.z > center.z;
            if (zPlus) {
                if (xPlus) {
                    if (yPlus)
                        return OcPos.XYZ;
                    else
                        return OcPos.XyZ;
                } else {
                    if (yPlus)
                        return OcPos.xYZ;
                    else
                        return OcPos.xyZ;
                }
            } else {
                if (xPlus) {
                    if (yPlus)
                        return OcPos.XYz;
                    else
                        return OcPos.Xyz;
                } else {
                    if (yPlus)
                        return OcPos.xYz;
                    else
                        return OcPos.xyz;
                }
            }
        }
        public bool hasChildren => XYZ != null || XyZ != null || xYZ != null || xyZ != null || XYz != null || Xyz != null || xYz != null || xyz != null;
        public void Add(Vector3 itemPos, TItem item) {
            var addToCurr = isMinimum;
            if (!addToCurr) {
                var child = GetChild(itemPos);
                if (child == null)
                    child = ocTree.CreateChild(this, itemPos);
                if (ocTree.Contains(child, item))
                    child.Add(itemPos, item);
                else
                    addToCurr = true;
            }
            if (addToCurr)
                items.Add(item);
        }
        public void Remove(Vector3 itemPos, TItem item) {
            if (items.Remove(item))
                return;
            GetChild(itemPos)?.Remove(itemPos, item);
        }
        public IEnumerable<TItem> GetAllItems() {
            for (int i = 0; i < items.Count; i++) {
                var item = items[i];
                if (item != null)
                    yield return item;
            }
            for (int i = 0; i < children.Length; i++) {
                var child = children[i];
                if (child == null) continue;
                foreach (var item in children[i].GetAllItems())
                    yield return item;
            }
        }
        public IEnumerable<TItem> GetCloseItems(Vector3 pos, float r) {
            // if outside return
            // if inside return all items recursively
            // if on edge return close items

            var distSqr = (center - pos).sqrMagnitude;
            var outsideRSqr = r + boundingRadius;
            outsideRSqr *= outsideRSqr;
            var outside = distSqr > outsideRSqr;
            if (outside)
                yield break;

            var insideRSqr = Mathf.Max(0, r - boundingRadius);
            insideRSqr *= insideRSqr;
            var inside = distSqr < insideRSqr;
            if (inside) {
                foreach (var item in GetAllItems())
                    yield return item;
            } else {
                // On edge.
                var rSqr = r * r;
                if (items.Count > 0) {
                    for (int i = 0; i < items.Count; i++) {
                        if ((ocTree.GetPos(items[i]) - pos).sqrMagnitude < rSqr)
                            yield return items[i];
                    }
                }
                foreach (var child in children) {
                    if (child == null) continue;
                    foreach (var item in child.GetCloseItems(pos, r))
                        yield return item;
                }
            }
        }
    }
}