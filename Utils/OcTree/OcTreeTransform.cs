﻿using UnityEngine;

namespace AntonTools
{
    public class OcTreeTransform : OcTree<Transform> {
        public OcTreeTransform(float minHalfWidth) : base(minHalfWidth) { }
        public override bool Contains(OcTreeNode<Transform> node, Transform item) => node.Contains(item.position);
        public override Vector3 GetPos(Transform item) => item.position;
    }
}