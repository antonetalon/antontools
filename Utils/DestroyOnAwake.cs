﻿using UnityEngine;

namespace AntonTools {
    public class DestroyOnAwake : MonoBehaviour {
        void Awake() => Destroy(gameObject);
    }
}