﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public static class PlayerPrefsUtils {
        public static void SetDouble(string key, double val) {
            var bytes = BitConverter.GetBytes(val);
            var str = Convert.ToBase64String(bytes);
            PlayerPrefs.SetString(key, str);
        }
        public static double GetDouble(string key, double defaultVal = -1) {
            if (!PlayerPrefs.HasKey(key))
                return defaultVal;
            var str = PlayerPrefs.GetString(key);
            var bytes = Convert.FromBase64String(str);
            var res = BitConverter.ToDouble(bytes, 0);
            return res;
        }

        public static bool GetBool(string key, bool defaultValue = false)
            => PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) == 1;
        public static void SetBool(string key, bool value)
            => PlayerPrefs.SetInt(key, value ? 1 : 0);

        public static T GetEnum<T>(string key, T defaultValue = default) where T : Enum
            => GetEnumNoChecks(key, defaultValue);
        private static T GetEnumNoChecks<T>(string key, T defaultValue = default) 
            => Utils.TryParseEnum(PlayerPrefs.GetString(key), typeof(T), out var result) ? (T)result : defaultValue;
        public static void SetEnum<T>(string key, T value) where T : Enum
            => SetEnumNoChecks(key, value);
        private static void SetEnumNoChecks<T>(string key, T value) 
            => PlayerPrefs.SetString(key, value.ToString());

        static void InitGeneric() {
            if (readGenericDict != null) return;
            readGenericDict = new Dictionary<Type, Func<string, object, object>>();
            writeGenericDict = new Dictionary<Type, Action<string, object>>();
            // Generic reading from player prefs - all supported types init here. Also all enums supported specially.
            AddGeneric<bool>((key, defaultValue) => GetBool(key, defaultValue), (key, value) => SetBool(key, value));
            AddGeneric<int>((key, defaultValue) => PlayerPrefs.GetInt(key, defaultValue), (key, value) => PlayerPrefs.SetInt(key, value));
            AddGeneric<long>((key, defaultValue) => {
                var (def1, def2) = defaultValue.To2Ints();
                var val1 = PlayerPrefs.GetInt($"{key}_1", def1);
                var val2 = PlayerPrefs.GetInt($"{key}_2", def2);
                return (val1, val2).ToLong();
            }, (key, value) => {
                var (v1, v2) = value.To2Ints();
                PlayerPrefs.SetInt($"{key}_1", v1);
                PlayerPrefs.SetInt($"{key}_2", v2);
            });
            AddGeneric<float>((key, defaultValue) => PlayerPrefs.GetFloat(key, defaultValue), (key, value) => PlayerPrefs.SetFloat(key, value));
            AddGeneric<string>((key, defaultValue) => PlayerPrefs.GetString(key, defaultValue), (key, value) => PlayerPrefs.SetString(key, value));
            AddGeneric<double>((key, defaultValue) => GetDouble(key, defaultValue), (key, value) => SetDouble(key, value));
        }
        static Dictionary<Type, Func<string, object, object>> readGenericDict;
        static Dictionary<Type, Action<string, object>> writeGenericDict;
        private static void AddGeneric<T>(Func<string, T, T> read, Action<string, T> write) {
            readGenericDict.Add(typeof(T), (key, defaultValue) => read(key, (T)defaultValue));
            writeGenericDict.Add(typeof(T), (key, value) => write(key, (T)value));
        }
        public static T Get<T>(string key, T defaultValue = default) {
            InitGeneric();
            var t = typeof(T);
            if (t.IsEnum)
                return GetEnumNoChecks(key, defaultValue);
            if (readGenericDict.TryGetValue(t, out var read))
                return (T)read(key, defaultValue);
            else
                throw new Exception($"generic reading type {t.Name} from player prefs not supported");
        }
        public static void Set<T>(string key, T value) {
            InitGeneric();
            var t = typeof(T);
            if (t.IsEnum)
                SetEnumNoChecks(key, value);
            else if (writeGenericDict.TryGetValue(t, out var write))
                write(key, value);
            else
                throw new Exception($"generic writing type {t.Name} to player prefs not supported");
        }
    }
}