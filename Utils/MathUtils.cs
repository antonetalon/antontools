﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace AntonTools {
    public static partial class Utils {
        public static (int i1, int i2) To2Ints(this long l) {
            var i1 = (int)(l & uint.MaxValue);
            var i2 = (int)(l >> 32);
            return (i1, i2);
        }
        public static long ToLong(this (int i1, int i2) i) {
            long l = i.i2;
            l = l << 32;
            l = l | (uint)i.i1;
            return l;
        }
        public static Vector3 Mean(this IEnumerable<Vector3> pts) {
            var count = 0;
            Vector3 sum = Vector3.zero;
            foreach (var pt in pts) {
                count++;
                sum += pt;
            }
            sum /= count;
            return sum;
        }
        public static Vector2 Mean(this IEnumerable<Vector2> pts) {
            var count = 0;
            Vector2 sum = Vector2.zero;
            foreach (var pt in pts) {
                count++;
                sum += pt;
            }
            sum /= count;
            return sum;
        }
        public static float StDevSqr(this IEnumerable<Vector3> pts) {
            var mean = pts.Mean();
            var count = 0;
            var sum = 0f;
            foreach (var pt in pts) {
                count++;
                sum += (pt - mean).sqrMagnitude;
            }
            sum /= count;
            return sum;
        }
        public static float StDevSqr(this IEnumerable<Vector2> pts) {
            var mean = pts.Mean();
            var count = 0;
            var sum = 0f;
            foreach (var pt in pts) {
                count++;
                sum += (pt - mean).sqrMagnitude;
            }
            sum /= count;
            return sum;
        }
        public static bool EqualsWithEps(this (float v1, float v2) v, float eps) => (v.v1 - v.v2).Abs() < eps; 
        public static int Indicator(this bool val) => val ? 1 : 0;
        public static Matrix4x4 MatrixFromPosXYZToWorld(Vector3 pos, Vector3 x, Vector3 y, Vector3 z) {
            var m = new Matrix4x4();
            m.m00 = x.x;
            m.m10 = x.y;
            m.m20 = x.z;
            m.m01 = y.x;
            m.m11 = y.y;
            m.m21 = y.z;
            m.m02 = z.x;
            m.m12 = z.y;
            m.m22 = z.z;
            m.m03 = pos.x;
            m.m13 = pos.y;
            m.m23 = pos.z;
            m.m33 = 1f;
            return m;
        }
        public static float Dot(this (Vector3 v1, Vector3 v2) v) => Vector3.Dot(v.v1, v.v2);
        public static float Dot(this (Vector2 v1, Vector2 v2) v) => Vector2.Dot(v.v1, v.v2);
        public static Vector3 Cross(this (Vector3 v1, Vector3 v2) v) => Vector3.Cross(v.v1, v.v2);
        public static float WeightedMean(params (float value, float mass)[] items) {
            var massSum = 0f;
            var valueSum = 0f;
            for (int i = 0; i < items.Length; i++) {
                massSum += Mathf.Abs(items[i].mass);
                valueSum += items[i].mass * items[i].value;
            }
            var mean = valueSum / massSum;
            return mean;
        }
        public static float Sqr(this float f) => f * f;
        public static float Sqrt(this float f) => Mathf.Sqrt(f);
        public static float Abs(this float f) => Mathf.Abs(f);
        public static int Abs(this int v) => Mathf.Abs(v);
        public static double Abs(this double f) => Math.Abs(f);
        public static Vector2 Abs(this Vector2 v) => new Vector2(v.x.Abs(), v.y.Abs());
        public static Vector3 Abs(this Vector3 v) => new Vector3(v.x.Abs(), v.y.Abs(), v.z.Abs());
        public static Vector3 GetPerpendicular(Vector3 dir) {
            var option1 = Vector3.Cross(dir, Vector3.up);
            var option2 = Vector3.Cross(dir, Vector3.forward);
            if (option1.sqrMagnitude > option2.sqrMagnitude)
                return option1;
            else
                return option2;
        }
        public static float Multiply(this IEnumerable<float> items) {
            var mul = 1f;
            foreach (var v in items)
                mul *= v;
            return mul;
        }
        public static float Sigmoid(float x) => 1f / (1f + Mathf.Exp(-x));
        public static float SigmoidInverse(float s) => -Mathf.Log(1f / s - 1);
        public static int Pow(int basE, int power) => Mathf.RoundToInt(Mathf.Pow(basE, power));
        public static float Frac(this float v) => v - Mathf.Floor(v);
        public static double Frac(this double v) => v - Math.Floor(v);
        public static bool Between(this int val, int min, int max, bool including = true) {
            if (min < val && val < max)
                return true;
            if (including && (val == min || val == max))
                return true;
            return false;
        }
        public static int DivSafe(this int val1, int val2, int whatIfDivZero = int.MaxValue)
            => val2 == 0 ? whatIfDivZero : val1 / val2;
        public static float Round(this float val, int decimalPlaces) => (float)((double)val).Round(decimalPlaces);
        public static double Round(this double val, int decimalPlaces) => Math.Round(val, decimalPlaces);
        public static int RoundToInt(this double val) => Mathf.RoundToInt((float)val);
        public static int RoundToInt(this float val) => Mathf.RoundToInt(val);
        public static int FloorToInt(this float val) => Mathf.FloorToInt(val);
        public static int CeilToInt(this float val) => Mathf.CeilToInt(val);
        public static bool InRange(this float v, float min, float max, bool including = true) {
            if (including)
                return min <= v && v <= max;
            else
                return min < v && v < max;
        }
        public static bool InRange(this int v, int min, int max, bool including = true) {
            if (including)
                return min <= v && v <= max;
            else
                return min < v && v < max;
        }
        static float ZeroIfSmaller(float v, float eps) => Mathf.Abs(v) < eps ? 0 : v;
        public static Vector3 RemoveSmallComponents(this Vector3 v, float eps)
            => new Vector3(ZeroIfSmaller(v.x, eps), ZeroIfSmaller(v.y, eps), ZeroIfSmaller(v.z, eps));
        public static int Sign(int val) => val > 0 ? 1 : (val < 0 ? -1 : 0);
        public static int Sign(this bool val) => val ? 1 : -1;
        public static float Sign(this float val) => val > 0 ? 1f : val == 0 ? 0 : -1f;
        public static double Sign(this double val) => val > 0 ? 1f : val == 0 ? 0 : -1f;
        public static Vector3 Rotate(this Vector3 v, Quaternion rotation) => rotation * v;
        public static Vector3 Rotate(this Vector3 v, Vector3 axis, float degrees) => v.Rotate(Quaternion.AngleAxis(degrees, axis));
        public static Vector2 Rotate(this Vector2 v, float degrees) {
            var rad = degrees * Mathf.Deg2Rad;
            var cos = Mathf.Cos(rad);
            var sin = Mathf.Sin(rad);
            return new Vector2(v.x * cos - v.y * sin, v.x * sin + v.y * cos);
        }
        public static Vector2 ToVector2(this Vector2Int v) => new Vector2(v.x, v.y);
        public static Vector2 ToVector2(this Vector3 v) => new Vector2(v.x, v.y);
        public static Vector3 ToVector3(this Vector2 v, float z = 0) => new Vector3(v.x, v.y, z);
        public static Vector2 XZ(this Vector3 v) => new Vector2(v.x, v.z);
        public static Vector2 XY(this Vector3 v) => new Vector2(v.x, v.y);
        public static Vector2 YZ(this Vector3 v) => new Vector2(v.y, v.z);
        public static Vector2 Rotate90(this Vector2 v) => new Vector2(-v.y, v.x);
        public static Vector2 XTo(this Vector2 v, float x) => new Vector2(x, v.y);
        public static Vector2 YTo(this Vector2 v, float y) => new Vector2(v.x, y);
        public static Vector3 ToXZ(this Vector2 v) => new Vector3(v.x, 0, v.y);
        public static Vector3 ZTo(this Vector2 v, float z) => new Vector3(v.x, v.y, z);
        public static Vector3 XTo0(this Vector3 v) => new Vector3(0, v.y, v.z);
        public static Vector3 YTo0(this Vector3 v) => new Vector3(v.x, 0, v.z);
        public static Vector3 ZTo0(this Vector3 v) => new Vector3(v.x, v.y, 0);
        public static Vector3 XTo(this Vector3 v, float x) => new Vector3(x, v.y, v.z);
        public static Vector3 YTo(this Vector3 v, float y) => new Vector3(v.x, y, v.z);
        public static Vector3 ZTo(this Vector3 v, float z) => new Vector3(v.x, v.y, z);
        public static bool IsSane(this Quaternion v) => IsSane(v.x) && IsSane(v.y) && IsSane(v.z) && IsSane(v.w);
        public static bool IsSane(this Vector3 v) => IsSane(v.x) && IsSane(v.y) && IsSane(v.z);
        public static bool IsSane(this Vector2 v) => IsSane(v.x) && IsSane(v.y);
        public static bool IsSane(this int v) => v != int.MaxValue && v != int.MinValue;
        public static bool IsSane(this float v) => !float.IsInfinity(v) && !float.IsNaN(v);
        public static bool IsSane(this double v) => !double.IsInfinity(v) && !double.IsNaN(v);
        public static float EnsureSane(this float v, float ifInsane = 0) => v.IsSane() ? v : ifInsane;
        public static double EnsureSane(this double v, double ifInsane = 0) => v.IsSane() ? v : ifInsane;
        public static int EnsureSane(this int v, int ifInsane = 0) => v.IsSane() ? v : ifInsane;
        public static int Min(this (int v1, int v2) v) => Mathf.Min(v.v1, v.v2);
        public static int Max(this (int v1, int v2) v) => Mathf.Max(v.v1, v.v2);
        public static float Min(this (float v1, float v2) v) => Mathf.Min(v.v1, v.v2);
        public static float Max(this (float v1, float v2) v) => Mathf.Max(v.v1, v.v2);
        public static float Clamp(this float v, float min, float max)
            => Mathf.Clamp(v, min, max);
        public static int Clamp(this int v, int min, int max) {
            if (v < min) return min;
            if (v > max) return max;
            return v;
        }
        public static Vector2 Clamp(this Vector2 v, Vector2 min, Vector2 max)
            => new Vector2(Mathf.Clamp(v.x, min.x, max.x), Mathf.Clamp(v.y, min.y, max.y));
        public static Vector3 Clamp(this Vector3 v, Vector3 min, Vector3 max)
           => new Vector3(Mathf.Clamp(v.x, min.x, max.x), Mathf.Clamp(v.y, min.y, max.y), Mathf.Clamp(v.z, min.z, max.z));
        public static Vector3 Clamp(this Vector3 v, float maxLength) {
            var sqr = v.sqrMagnitude;
            var maxSqr = maxLength * maxLength;
            if (sqr <= maxSqr) return v;
            return v * maxLength / Mathf.Sqrt(sqr);
        }
        public static long ToHash(this long val1, long val2, params long[] vals) {
            var hash = val1.ToHash();
            hash += val2;
            hash += hash << 11; hash ^= hash >> 7;
            if (vals != null) {
                for (int i = 0; i < vals.Length; i++) {
                    hash += vals[i];
                    hash += hash << 11; hash ^= hash >> 7;
                }
            }
            return hash;
        }
        const long startHashSalt = 925549877;
        public static long ToHash(this long val) {
            long hash = startHashSalt;
            hash += val;
            hash += hash << 11; hash ^= hash >> 7;
            return hash;
        }
        public static long ToHash(this float val) {
            var bytes = BitConverter.GetBytes(val);
            long hash = bytes[0] + (bytes[1] << 8) + (bytes[2] << 16) + (bytes[2] << 24);
            return hash;
        }
        public static long ToHash(this int val) => ToHash((long)val);
        public static long ToHash(this bool val) => ToHash(val ? 1 : 0);
        public static long ToHash(this string val) => val?.GetHashCode() ?? startHashSalt;
        public static long ToHash<T>(this IEnumerable<T> items, Func<T, long> itemToHash) {
            long hash = startHashSalt;
            foreach (var item in items)
                hash = hash.ToHash(itemToHash(item));
            return hash;
        }
        public static double MoveTowards(double curr, double tgt, double maxDelta) {
            if (Math.Abs(curr - tgt) <= maxDelta)
                return tgt;
            else if (curr < tgt)
                return curr + maxDelta;
            else
                return curr - maxDelta;
        }
        public static double Clamp(double val, double min, double max) => Math.Min(Math.Max(val, min), max);
        public static double Lerp(double from, double to, double t) {
            t = Clamp(t, 0, 1);
            return from * (1 - t) + to * t;
        }
        public static float PowerInt(float v, int power) {
            var res = 1f;
            for (int i = 0; i < power; i++)
                res *= v;
            return res;
        }
        public static double PowerInt(double v, int power) {
            var res = 1d;
            for (int i = 0; i < power; i++)
                res *= v;
            return res;
        }
    }
}