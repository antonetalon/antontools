﻿using UnityEngine;

namespace AntonTools {
    /// <summary>
    /// Like singleton, but does not create instance, needs it to be already in scene.
    /// f.e.: a specific prefab should exist in scene.
    /// </summary>
    public abstract class MonoBehaviourHasInstance<TSelf> : MonoBehaviour
        where TSelf : MonoBehaviourHasInstance<TSelf> {
        public static TSelf instance { get; private set; }
        protected virtual void OnEnable() => instance = (TSelf)this; // For edit mode instances.
        protected virtual void Awake() {
            if (instance != null) {
                if (Application.isPlaying)
                    Destroy(gameObject);
                return;
            }
            instance = this as TSelf;
        }
    }
}