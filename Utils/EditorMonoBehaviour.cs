﻿using UnityEngine;

namespace AntonTools {
    [ExecuteAlways]
    public class EditorMonoBehaviour : MonoBehaviour {
        protected virtual void Awake() {
            if (Application.isPlaying)
                Destroy(this);
        }
    }
}