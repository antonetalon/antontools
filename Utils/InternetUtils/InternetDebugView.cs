﻿using System;
using TMPro;
using UnityEngine;

namespace AntonTools {
    public class InternetDebugView : OpenedDebugPanelItemView {
        public override (string tab, string name) whereToShow => (ModuleDebugPanel.CommonTab, "internet");

        [SerializeField] TMP_InputField input;
        protected override void OnEnablePlaying() {
            base.OnEnablePlaying();
            input.text = AntonToolsSettings.instance.simulatedInternetDelay.ToString();
            input.onValueChanged.AddListener(e => OnChanged());
        }

        private void OnChanged() {
            if (!float.TryParse(input.text, out var value))
                AntonToolsSettings.instance.simulatedInternetDelay = value;
        }
    }
}