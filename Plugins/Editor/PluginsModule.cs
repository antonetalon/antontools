﻿#if UNITY_EDITOR
namespace AntonTools {
    public class PluginsModule : RootModule {
        public override HowToModule HowTo() => new PluginsModule_HowTo();
    }
}
#endif