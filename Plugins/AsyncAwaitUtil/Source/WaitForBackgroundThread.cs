using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace AntonTools {
    public class WaitForBackgroundThread {
        public ConfiguredTaskAwaitable.ConfiguredTaskAwaiter GetAwaiter()
            => Task.Run(() => { }).ConfigureAwait(false).GetAwaiter();
    }
}