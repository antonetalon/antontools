﻿using System.IO;
using System.IO.Compression;

namespace AntonTools {
    public static class Zip {
#if UNITY_STANDALONE
        public static byte[] Compress(byte[] data) {
            using (var compressedStream = new MemoryStream())
            using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress)) {
                using (var writer = new BinaryWriter(zipStream))
                    writer.Write(data, 0, data.Length);
                return compressedStream.ToArray();
            }
        }

        public static byte[] Decompress(byte[] data) {
            using (var compressedStream = new MemoryStream(data))
            using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
            using (var resultStream = new MemoryStream()) {
                zipStream.CopyTo(resultStream);
                return resultStream.ToArray();
            }
        }
#endif
    }
}