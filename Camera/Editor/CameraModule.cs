﻿#if UNITY_EDITOR
using System.Collections.Generic;

namespace AntonTools {
    public class CameraModule : RootModule {
        public override HowToModule HowTo() => new CameraModule_HowTo();
    }
}
#endif