﻿using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class ParticlesCountView : IntervalMonoBehaviour {
        [SerializeField] Text particlesLabel;
        protected override void UpdateInInterval()
            => particlesLabel.SetTextSafe($"Particles={PerformanceStatsView.GetParticlesDescShort()}");
    }
}