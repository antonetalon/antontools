﻿namespace AntonTools {
    public class PerformanceDebugView : ModuleDebugPanel {
        public override string tab => "Performance";
        public override string module => "DEBUG_PERFORMANCE";
        public override bool wholeTab => true;
        public override void OnDebugPanelAwake() => PerformanceBudgetManager.EnsureExists();
    }
}