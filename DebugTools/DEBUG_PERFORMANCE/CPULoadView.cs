﻿using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class CPULoadView : IntervalMonoBehaviour {
        [SerializeField] Text cpuLabel;
        protected override void UpdateInInterval()
            => cpuLabel.SetTextSafe($"CPU={PerformanceStatsView.GetCPUUsagePercents()}%");
    }
}