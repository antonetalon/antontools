﻿using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public static class ResolutionManager {
        static void InitResolutions() {
            _resolutions = new List<Resolution>();
            if (Application.platform.ToTargetPlatform() != TargetPlatform.Android)
                _resolutions = Screen.resolutions.ToList();
            else {
                var res = currResolution;
                for (int i = 0; i < 8; i++) {
                    _resolutions.Add(res);
                    res.width = res.width * 2 / 3;
                    res.height = res.height * 2 / 3;
                }
            }
        }
        static List<Resolution> _resolutions;
        public static IReadOnlyList<Resolution> resolutions {
            get {
                if (_resolutions == null)
                    InitResolutions();
                return _resolutions;
            }
        }
        public static Resolution currResolution {
            get {
                var res = Screen.currentResolution;
#if UNITY_EDITOR
                res.width = Screen.width;
                res.height = Screen.height;
#endif
                return res;
            }
            set {
                if (currResolution.DefaultEquals(value)) return;
                Debug.Log($"change resolution from {currResolution} to {value}");
                Screen.SetResolution(value.width, value.height, fullScreen, value.refreshRate);
            }
        }
        public static int currResolutionInd {
            get => resolutions.FindIndex(r => r.DefaultEquals(currResolution));
            set {
                if (resolutions.IndIsValid(value))
                    currResolution = resolutions[value];
                else
                    Debug.Log($"invalid resolution ind={value} set, resoslutions count={resolutions.Count}");
            }
        }
        public static bool fullScreen {
            get {
#if UNITY_EDITOR
                if (Application.isEditor)
                    return PlayModeFullscreenManager.isInFullScreen;
#endif
                return Screen.fullScreen;
            }
            set {
#if UNITY_EDITOR
                if (Application.isEditor) { 
                    if (value)
                        PlayModeFullscreenManager.EnterFullScreen(false);
                    else
                        PlayModeFullscreenManager.ExitFullScreen();
                }
#endif
                Screen.fullScreen = value;
            }
        }
    }
}