﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AntonTools {
    public static class StopWatchByName {
        static Stopwatch sw;
        static Dictionary<string, (long ticks, int count)> finished;
        static SortedDictionary<string, long> started;
        static void InitIfNeeded() {
            if (sw != null)
                return;
            sw = new Stopwatch();
            sw.Start();
            finished = new Dictionary<string, (long ticks, int count)>();
            started = new SortedDictionary<string, long>();
        }
        public static bool IsStarted(string name) => started?.ContainsKey(name) ?? false;
        public static void Start(string name) {
            InitIfNeeded();
            Debug.Assert(!IsStarted(name), $"{name} should not be started");
            started.Add(name, sw.ElapsedTicks);
        }
        public static void Stop(string name) {
            Debug.Assert(IsStarted(name), $"{name} should be started");
            var elapsed = sw.ElapsedTicks - started[name];
            started.Remove(name);
            finished.TryGetValue(name, out var item);
            item.count++;
            item.ticks += elapsed;
            finished[name] = item;
        }
        public static void StopStart(string nameToStop, string nameToStart) {
            Stop(nameToStop);
            Start(nameToStart);
        }
        public static void StopLog(string name) {
            Stop(name);
            Log();
        }
        public static long GetMS(long ticks) => ticks / TimeSpan.TicksPerMillisecond;
        public static List<(string name, long ms, int count)> GetData() {
            var res = new List<(string name, long ms, int count)>();
            foreach (var (name, (ticks, count)) in finished)
                res.Add((name, GetMS(ticks), count));
            res.SortBy(item => item.ms, true);
            return res;
        }
        public static long GetMS(string name) => finished.TryGetValue(name, out var val) ? GetMS(val.ticks) : -1;
        public static void Log()
            => UnityEngine.Debug.Log(GetData().ConvertAll(d => $"{d.name} {d.ms * 0.001f}s x{d.count}").PrintCollection("\n"));
        public static void Clear() {
            InitIfNeeded();
            finished.Clear();
            started.Clear();
        }
        public static void Restart(string name) {
            Clear();
            Start(name);
        }
        public static void Execute(Action actionToMeasure, out float seconds) {
            Clear();
            var name = Utils.RandomLong().ToString();
            Start(name);
            actionToMeasure?.Invoke();
            Stop(name);
            seconds = GetMS(name) * 0.001f;
        }

        public static StopWatchSequence StartSequence() => new StopWatchSequence();
    }
}