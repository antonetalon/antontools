﻿#if UNITY_EDITOR

namespace AntonTools {
    public class DebugPerformanceModule : DebugToolsModule {
        public override HowToModule HowTo() => new DEBUG_PERFORMANCE_HowTo();
        protected override string debugViewPath => "DebugTools/DEBUG_PERFORMANCE/PerformanceDebugView";

        // simulated/budget prefabs/cpu items editing and showing. Prefabs automatically found and put into settings here.
        protected override void OnCompiledEnable() {
            base.OnCompiledEnable();
            SettingsInEditor<DebugPerformanceSettings>.EnsureExists();
            SettingsInEditor<DebugPerformanceLocalSettings>.EnsureExists();
        }
    }
}
#endif