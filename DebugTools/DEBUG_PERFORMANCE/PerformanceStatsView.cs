﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace AntonTools {
    public class PerformanceStatsView : MonoBehaviour {
        const float bInMb = 1024 * 1024;
        string ShowBInMB(long b) => $"{(b / bInMb).ToString("0.00")}mb";
        [SerializeField] TextOrTMPro text;
        public float updateInterval = 1;

        float nextUpdateTime;
        const string na = "N/A";
        string GetAllocatedMemoryForGraphicsDriver() {
            // Returns the amount of allocated memory for the graphics driver.
            if (!Debug.isDebugBuild)
                return na;
            return ShowBInMB(Profiler.GetAllocatedMemoryForGraphicsDriver());
        }
        string GetTotalReservedMemoryLong() {
            // The total memory Unity has reserved.
            var value = Profiler.GetTotalReservedMemoryLong();
            if (value == 0)
                return na;
            return ShowBInMB(value);
        }
        string GetTotalAllocatedMemoryLong() {
            //     The total memory allocated by the internal allocators in Unity. Unity reserves
            //     large pools of memory from the system. This function returns the amount of used
            //     memory in those pools.
            var value = Profiler.GetTotalAllocatedMemoryLong();
            if (value == 0)
                return na;
            return ShowBInMB(value);
        }
        string GetMonoHeapSizeLong() {
            //     Returns the size of the reserved space for managed-memory.
            var value = Profiler.GetMonoHeapSizeLong();
            if (value == 0)
                return na;
            return ShowBInMB(value);
        }
        public static int GetCPUUsagePercents() {
            var platform = Application.platform.ToTargetPlatform();
            switch (platform) {
                case TargetPlatform.Android: return CPUAndroid.ReadCPUAndroid(); // Does it work?
                case TargetPlatform.MacOS:
                case TargetPlatform.Windows: return CPUPC.ReadCPUPC();
                default: return -1;
            }
        }

        /// <summary>
        /// Slow func, searches all scene particles.
        /// </summary>
        /// <returns>particle systems / total particles count</returns>
        public static string GetParticlesDescShort() {
            var particleSystems = 0;
            var particles = 0;
            for (int i=0;i<SceneManager.sceneCount;i++) {
                var scene = SceneManager.GetSceneAt(i);
                scene.GetRootGameObjects(currRootGOs);
                foreach (var go in currRootGOs) {
                    go.GetComponentsInChildren(currParticles);
                    particleSystems += currParticles.Count;
                    foreach (var ps in currParticles)
                        particles += ps.particleCount;
                }
            }
            return $"{particleSystems}/{particles}";
        }
        static List<GameObject> currRootGOs = new List<GameObject>();
        static List<ParticleSystem> currParticles = new List<ParticleSystem>();

        [SerializeField] GPULoadView gpuLoad;
        bool profilerEnabled => Profiler.enabled;
        private void Start() {
            Profiler.enabled = true;
        }
        void Update() {
            if (nextUpdateTime > Time.realtimeSinceStartup)
                return;
            nextUpdateTime = Time.realtimeSinceStartup + updateInterval;
            text.text =
                $"graphicsDeviceType={SystemInfo.graphicsDeviceType}, Debug.isDebugBuild = {Debug.isDebugBuild}, profiler {(profilerEnabled ? "enabled" : "disabled")}\n" +
                // Used by managed code/Used by program/Allocated from OS/Total on device.
                $"RAM memory = \t{GetMonoHeapSizeLong()}/{GetTotalAllocatedMemoryLong()}/{GetTotalReservedMemoryLong()}/{SystemInfo.systemMemorySize}mb\n" + 
                $"CPU = {SystemInfo.processorCount} lcores, {(SystemInfo.processorFrequency * 0.001f).ToString(2)}GHz, {SystemInfo.processorType}\n" +
                $"CPU usage = \t{GetCPUUsagePercents()}%\n" +
                $"GPU = {SystemInfo.graphicsDeviceName}\n" + 
                $"GPU usage = \t{Mathf.RoundToInt(gpuLoad.gpuLoadPercents)}%\n" +
                $"GPU memory = \t{GetAllocatedMemoryForGraphicsDriver()}/{SystemInfo.graphicsMemorySize}mb\n" + // Amount of video memory present.
                "";
        }
    }
}