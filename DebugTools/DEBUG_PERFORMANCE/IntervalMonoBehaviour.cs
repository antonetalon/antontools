﻿using UnityEngine;

namespace AntonTools {
    public abstract class IntervalMonoBehaviour : MonoBehaviour {
        public float interval = 1;
        float elapsed;
        private void Update() {
            if (elapsed > interval) {
                UpdateInInterval();
                elapsed = 0;
            }
            elapsed += Time.unscaledDeltaTime;
        }
        protected abstract void UpdateInInterval();
    }
}