﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace AntonTools {
    public class StopWatchSequence {
        List<(string name, long startTime)> items = new List<(string name, long startTime)>();
        Stopwatch sw = new Stopwatch();
        public StopWatchSequence() => sw.Start();
        public void OnStarted(string whatsStarted) => items.Add((whatsStarted, sw.ElapsedTicks));
        public void FinishAndLog(string title) {
            OnStarted(null);
            var sb = new StringBuilder();
            sb.AppendLine(title);
            for (int i=0;i<items.Count-1;i++) {
                var end = items[i + 1].startTime;
                var start = items[i].startTime;
                var ms = StopWatchByName.GetMS(end - start);
                sb.AppendLine($"{items[i].name} {ms}ms");
            }
            UnityEngine.Debug.Log(sb.ToString());
            sw.Stop();
            sw = null;
        }
    }
}