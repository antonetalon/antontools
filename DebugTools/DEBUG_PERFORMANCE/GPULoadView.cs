﻿using UnityEngine;

namespace AntonTools {
    public class GPULoadView : IntervalMonoBehaviour {
        [SerializeField] TextOrTMPro label;
        public float gpuLoadPercents { get; private set; } = -1;
        int currProcessId = -1;
        private void Start() => InitingProcessId();
        async void InitingProcessId() {
            //cmd.logging = true;
            var processName = Application.isEditor ? "Unity" : Application.productName;
            var response = await cmd.Executing($"$p = Get-Process \"{processName}\"; $p.id");
            if (!int.TryParse(response, out currProcessId))
                currProcessId = -1;
            //            Debug.Log($"curr process id = {currProcessId}");
        }
        protected override void UpdateInInterval() {
            UpdateGPULoad();
            if (label != null)
                label.text = $"GPU={gpuLoadPercents.RoundToInt()}%";
        }
        private async void UpdateGPULoad() {
            if (currProcessId == -1 || cmd.isExecuting) return;
            var str = $"$c = Get-Counter \\\"\\GPU Engine(pid_$(${currProcessId})*engtype_3D)\\Utilization Percentage\\\"; ($c.CounterSamples | where CookedValue).CookedValue";
            var response = await cmd.Executing(str);
            var lines = response.Split('\n');
            gpuLoadPercents = -1;
            foreach (var line in lines) {
                if (StringUtils.TryParse(line, out float curr))
                    gpuLoadPercents = Mathf.Max(gpuLoadPercents, curr);
            }
            //Debug.Log($"{Mathf.RoundToInt(gpuLoadPercents)}%");
        }
    }
}