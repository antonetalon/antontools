﻿using UnityEngine;

namespace AntonTools {
    public class CPUTimeMeasuringFirstMonobehaviour : MonoBehaviour {
        private void Update() => PerformanceBudgetManager.instance.OnFirstScriptStartedFrame();
    }
}