﻿using UnityEngine;

namespace AntonTools {
    public class CPUTimeMeasuringLastMonobehaviour : MonoBehaviour {
        private void LateUpdate() => PerformanceBudgetManager.instance.OnLastScriptFinishedFrame();
    }
}