﻿using TMPro;
using UnityEngine;

namespace AntonTools {
    public class ResolutonView : MonoBehaviour {
        [SerializeField] TMP_Dropdown view;
        private void Awake() {
            view.ClearOptions();
            view.AddOptions(ResolutionManager.resolutions.ConvertAll(r => $"{r.width}x{r.height} ({r.refreshRate}Hz)"));
            view.value = ResolutionManager.currResolutionInd;
            view.onValueChanged.AddListener(val => ResolutionManager.currResolutionInd = val);
        }
    }
}
