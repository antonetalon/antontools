﻿using UnityEngine;

namespace AntonTools {
    public class DebugStopTime : MonoBehaviour {
        [SerializeField] KeyCode button = KeyCode.Space;
        void Update() {
            if (Input.GetKeyUp(button))
                GameTime.SetPause(!GameTime.paused);
        }
    }
}