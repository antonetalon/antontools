﻿#if UNITY_EDITOR
namespace AntonTools {
    public class DebugLogsOnDeviceModule : DebugToolsModule {
        public override HowToModule HowTo() => new DEBUG_LOGS_HowTo();
        protected override string debugViewPath => "DebugTools/DEBUG_LOGS/LogsDebugView";
    }
}
#endif