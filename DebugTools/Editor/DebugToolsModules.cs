﻿#if UNITY_EDITOR
using System;

namespace AntonTools {
    public class DebugToolsModuleFolder : ModulesFolder {
        public override HowToModule HowTo() => new DEBUG_TOOLS_HowTo();
    }
    public abstract class DebugToolsModule : ModuleManager {
        public override Type parentModule => typeof(DebugToolsModuleFolder);
    }
}
#endif