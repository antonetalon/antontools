﻿using UnityEngine;

namespace AntonTools {
    public class OverGameDebugPanelByName : DebugPanelItemView {
        [SerializeField] string _id;
        public override string id => _id;
        public override WhereToShow showingType => WhereToShow.OverGame;
    }
}