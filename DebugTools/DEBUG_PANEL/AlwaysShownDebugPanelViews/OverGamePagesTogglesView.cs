﻿using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public class OverGamePagesTogglesView : OpenedDebugPanelItemView {
        public override (string tab, string name) whereToShow => ("common", "Always shown views");
        [SerializeField] OverGameToggleView togglePrefab;
        List<OverGameToggleView> shownToggles = new List<OverGameToggleView>();
        protected override void AwakePlaying() {
            base.AwakePlaying();
            Utils.UpdatePrefabsList(shownToggles, DebugPanel.instance.overGameViews, togglePrefab, (view, viewToggle) => viewToggle.Show(view));
        }
    }
}