﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class OverGameToggleView : MonoBehaviour {
        [SerializeField] TextMeshProUGUI label;
        [SerializeField] Toggle toggle;
        DebugPanelItemView view;

        private void Awake() {
            toggle.onValueChanged.AddListener(OnToggleChanged);
        }

        public void Show(DebugPanelItemView view) {
            this.view = view;
            label.text = view.id;
            toggle.isOn = false;
        }

        private void OnToggleChanged(bool enabled) {
            view.Safe()?.gameObject.SetActiveSafe(enabled);
        }
    }
}