﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class DebugPanelTabView : MonoBehaviour {
        public DebugPanelItemHeader headerPrefab;
        [SerializeField] GameObject multipleItemsParentGameObject;
        [SerializeField] Transform multipleItemsParentTransform;
        [SerializeField] Transform singleItemParent;
        public DebugPanelSettings settings => DebugPanelSettings.instance;
        List<OpenedDebugPanelItemView> prefabs;
        public void SetItems(List<OpenedDebugPanelItemView> prefabs)
            => this.prefabs = prefabs.SortedBy(item => item.sortPriority);
        List<OpenedDebugPanelItemView> items;
        private void OnEnable() {
            if (items != null) return;
            items = new List<OpenedDebugPanelItemView>();
            var showSingleItem = prefabs[0].wholeTab;
            multipleItemsParentGameObject.SetActive(!showSingleItem);
            singleItemParent.gameObject.SetActive(showSingleItem);
            if (showSingleItem) {
                // Single item.
                var item = Instantiate(prefabs[0], singleItemParent);
                if (prefabs.Count != 1)
                    Debug.LogError($"Debug panel tab {prefabs[0].whereToShow.tab} should have only one tab, " +
                        $"but has {prefabs.ConvertAll(p => p.name).PrintCollection()}");
                item.GetComponent<RectTransform>().FillParentRect();
                items.Add(item);
            } else {
                // Multiple items.
                prefabs.ForEach(prefab => {
                    var (_, name) = prefab.whereToShow;
                    var header = Instantiate(headerPrefab, multipleItemsParentTransform);
                    header.header.text = name;
                    var item = Instantiate(prefab, multipleItemsParentTransform);
                    items.Add(item);
                });
            }
            LanscapeDebugPanelView.Init(transform);
        }
    }
}