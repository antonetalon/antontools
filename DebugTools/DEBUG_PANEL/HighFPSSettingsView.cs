﻿using UnityEngine;

namespace AntonTools {
    public class HighFPSSettingsView : ToggleView {
        protected override bool value {
            get => Application.targetFrameRate == FrameRate.HighFPS;
            set => Application.targetFrameRate = value ? FrameRate.HighFPS : FrameRate.LowFPS;
        }
    }
}