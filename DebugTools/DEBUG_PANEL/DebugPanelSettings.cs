﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public class DebugPanelSettings : SettingsScriptable<DebugPanelSettings> {
        public List<string> disabledModules = new List<string>();
        public List<DebugPanelItemView> itemViews = new List<DebugPanelItemView>();
        public bool autoOrientation = true;

        [Serializable] public class BuildModeSettings {
            public BuildModeType buildMode;
            public bool openPanelByClicks = true;
            public int openPanelClicks = 3;
            public float openPanelClicksDuration = 2;
            public bool openPanelByKey = true;
            public KeyCode openPanelKey = KeyCode.Tab;
            public bool passwordEnabled = false;
            public int password = 2020;
            public bool startsShown = false;
        }
        public List<BuildModeSettings> buildModeSettings = new List<BuildModeSettings>() {
            new BuildModeSettings() { buildMode = BuildModeType.Develop },
            new BuildModeSettings() { buildMode = BuildModeType.Test },
            new BuildModeSettings() { buildMode = BuildModeType.Release, openPanelByKey = false, openPanelClicks = 10, passwordEnabled = true },
        };
        public BuildModeSettings currSettings => buildModeSettings.Find(s => s.buildMode == BuildMode.type);
    }
}