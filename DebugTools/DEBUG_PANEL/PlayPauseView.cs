﻿using UnityEngine;

namespace AntonTools {
    public class PlayPauseView : MonoBehaviourHasInstance<PlayPauseView> {
        [SerializeField] GameObject parent, playParent, pauseParent;
        protected override void Awake() {
            base.Awake();
            //parent.SetActive(shouldBeShown);
        }
        void Update() {
            if (!parent.activeSelf) return;
            var paused = GameTime.paused;
            playParent.SetActive(paused);
            pauseParent.SetActive(!paused);
        }
        public void OnPlayPressed() => Time.timeScale = 1;//GameTime.Unpause();
        public void OnPausePressed() => Time.timeScale = 0;//GameTime.Pause();

        //const string key = "ShownInDebug";
        //public static bool shouldBeShown => ShownInDebug && !BuildModeSettings.release;
        //public static bool ShownInDebug {
        //    get =>
        //        false;
        //    set {
        //        PlayerPrefs.SetInt(key, value ? 1 : 0);
        //        instance?.parent.SetActive(shouldBeShown);
        //    }
        //}
    }
}