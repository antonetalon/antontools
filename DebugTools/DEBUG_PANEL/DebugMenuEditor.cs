﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEditor;

namespace AntonTools {
    [ExecuteAlways, Obsolete]
    public class DebugMenuEditor : MonoBehaviour {
        [SerializeField] List<GameObject> tabs;
        [SerializeField] Button tabButtonPrefab;
        [SerializeField] TabView tabsView;
        [SerializeField] Transform tabsParent;

        [SerializeField] LogsDebugView logsViewPrefab;
        [SerializeField] bool logs;

        [SerializeField] ConfigDebugView ConfigPrefab;
        [SerializeField] bool config;

        [SerializeField] PerformanceDebugView ConfigPerformance;
        [SerializeField] bool performance;

#if UNITY_EDITOR
        private void Awake() {
            if (Application.isPlaying)
                Destroy(this);
        }
        long prevHash;
        private void Update() {
            if (!Utils.IsPrefabOpened())
                return;
            var prefabPath = PrefabUtils.currPrefabAssetPath;
            var prefabName = Path.GetFileNameWithoutExtension(prefabPath);
            if (prefabName != gameObject.name)
                return; // Dont manage, another prefab opened.
            if (prefabName == "DebugMenu")
                return; // Manage only variants.
            long currHash = 23412415;
            currHash = currHash.ToHash(logs.ToHash());
            currHash = currHash.ToHash(config.ToHash());
            currHash = currHash.ToHash(performance.ToHash());
            tabs.ForEach(t => currHash = currHash.ToHash(t.GetHashCode()));
            if (prevHash == currHash)
                return;
            prevHash = currHash;
            UpdateDebugMenu();
        }

        void AddTab(GameObject tab) => tabsView.AddTab(tab.name, tab, tabButtonPrefab);
        void FitInParent(GameObject go) {
            var rect = go.GetComponent<RectTransform>();
            rect.pivot = new Vector2(0.5f, 0.5f);
            rect.anchorMin = new Vector2(0, 0);
            rect.anchorMax = Vector2.one;
            rect.localRotation = Quaternion.identity;
            rect.localScale = Vector3.one;
            rect.offsetMin = Vector2.zero;
            rect.offsetMax = Vector2.zero;
        }
        private void UpdateDebugMenu() {
            tabsView.transform.DestroyChildrenImmediate();
            tabsView.tabButtons.Clear();
            tabsView.tabs.Clear();
            tabs.ForEach(tab => AddTab(tab));

            UpdateLogsView();
            UpdateConfigView();
            UpdatePerformanceView();
            tabsView.tabs.ForEachWithInd((tab, ind) => tab.SetActive(ind == tabsView.startTabInd));
            EditorUtility.SetDirty(gameObject);
        }
        void UpdateChildPrefabTab<T>(T prefab, bool tabEnabled, Action<T> onCreated = null, Action onDestroyed = null) where T : MonoBehaviour {
            var existingTab = transform.GetComponentInChildren<T>(true);
            if (tabEnabled) {
                if (existingTab == null)
                    existingTab = (T)PrefabUtility.InstantiatePrefab(prefab, tabsParent);
                existingTab.transform.name = prefab.name.Replace("DebugView", "");
                FitInParent(existingTab.gameObject);
                AddTab(existingTab.gameObject);
                onCreated?.Invoke(existingTab);
            } else {
                if (existingTab != null)
                    DestroyImmediate(existingTab.gameObject);
                onDestroyed?.Invoke();
            }
        }
        void UpdateLogsView() => UpdateChildPrefabTab(logsViewPrefab, logs);
        void UpdateConfigView() => UpdateChildPrefabTab(ConfigPrefab, config);
        void UpdatePerformanceView() => UpdateChildPrefabTab(ConfigPerformance, performance);
#endif
    }
}