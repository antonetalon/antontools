﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public sealed class DebugPanel : MonoBehaviourHasInstance<DebugPanel> {
        #region Open/close
        DebugPanelSettings settings => DebugPanelSettings.instance;
        int clicks => settings.currSettings.openPanelClicks;
        float clicksDuration => settings.currSettings.openPanelClicksDuration;
        List<float> pressTimes = new List<float>();
        public GameObject debugMenuParent;
        public PasswordView passwordView;
        bool passwordPassed;
        public bool shown {
            get => debugMenuParent.activeSelf;
            set {
                debugMenuParent.SetActive(value);
                overGameParent.SetActiveSafe(!value);
                if (value)
                    Pause();
                else
                    UnPause();
                if (!value)
                    lastClosingTime = Time.realtimeSinceStartup;
                else if (!BuildModeSettings.develop && settings.currSettings.passwordEnabled && !passwordPassed)
                    passwordView.Show(OnPasswordEntered);
            }
        }
        void OnPasswordEntered(bool passwordCorrect) {
            if (!passwordCorrect)
                shown = false;
            else
                passwordPassed = true;
        }
        private void OnApplicationQuit() {
            if (shown)
                UnPause();
        }
        #region Time
        bool pausedWhenOpened;
        void Pause() {
            pausedWhenOpened = !GameTime.paused;
            if (pausedWhenOpened)
                GameTime.SetPause(true);
        }
        void UnPause() {
            if (pausedWhenOpened && GameTime.paused)
                GameTime.Unpause();
        }
        #endregion

        void UpdateOpening() {
            UpdateOpeningByClicks();
            UpdateOpeningByKeyPress();
        }
        #region Open by clicks
        float lastClosingTime;
        const float SmallTime = 0.1f;
        public bool shownRecently => shown || Time.realtimeSinceStartup < lastClosingTime + SmallTime;
        void UpdateOpeningByClicks() {
            if (!settings.currSettings.openPanelByClicks) return;
            if (!Input.GetMouseButtonUp(0)) return;
            if (!(Input.mousePosition.x < Screen.width * 0.15f && Input.mousePosition.y > Screen.height * (1 - 0.15f)))
                return;
            pressTimes.Add(Time.realtimeSinceStartup);
            while (pressTimes.Count > clicks)
                pressTimes.RemoveAt(0);
            if (pressTimes.Count >= clicks && pressTimes[0] > Time.realtimeSinceStartup - clicksDuration) {
                pressTimes.Clear();
                shown = !shown;
            }
        }
        public void OnOpenPressed() {
            shown = true;
        }
        public void OnClosePressed() {
            shown = false;
        }
        #endregion
        #region Open by key press
        void UpdateOpeningByKeyPress() {
            if (!settings.currSettings.openPanelByKey) return;
            if (Input.GetKeyUp(settings.currSettings.openPanelKey))
                shown = !shown;
        }
        #endregion
        #endregion

        #region Common
        bool startsShown => settings.currSettings.startsShown;
        [SerializeField] Button closeButton;
        protected override void Awake() {
            base.Awake();
            shown = startsShown;
            if (closeButton != null)
                closeButton.onClick.AddListener(OnClosePressed);
            InitTabs();
        }
        void Update() => UpdateOpening();
        #endregion

        #region Tabs
        [SerializeField] TabView tabView;
        [SerializeField] Button tabButtonPrefab;
        [SerializeField] DebugPanelTabView tabPrefab;
        [SerializeField] Transform tabsParent;
        List<DebugPanelTabView> shownTabs = new List<DebugPanelTabView>();
        [SerializeField] Transform overGameParent;
        public List<DebugPanelItemView> overGameViews = new List<DebugPanelItemView>(); 
        void InitTabs() {
            // Sort items by tabs.
            var itemsByTabs = new Dictionary<string, List<OpenedDebugPanelItemView>>();
            var overGameItems = new List<DebugPanelItemView>();
            settings.itemViews.ForEach(item => {
                if (item == null) return;
                //var (tab, name) = item.whereToShow;
                if (settings.disabledModules.Contains(item.id))
                    return;
                var itemInOpened = item as OpenedDebugPanelItemView;
                if (itemInOpened != null) {
                    if (!itemsByTabs.TryGetValue(itemInOpened.whereToShow.tab, out var itemsInTab)) {
                        itemsInTab = new List<OpenedDebugPanelItemView>();
                        itemsByTabs.Add(itemInOpened.whereToShow.tab, itemsInTab);
                    }
                    itemsInTab.Add(itemInOpened);
                } else if (item.showingType == DebugPanelItemView.WhereToShow.OverGame)
                    overGameItems.Add(item);
                item.OnDebugPanelAwake();
            });
            // Sort tabs.
            var itemsByTabsList = itemsByTabs.ConvertAll(i => i.Value);
            itemsByTabsList.SortBy(list => list.Max(item => -item.sortPriority));
            // Show tabs, but activate only one.
            tabsParent.gameObject.SetActive(false);
            Utils.UpdatePrefabsList(shownTabs, itemsByTabsList, tabPrefab, tabsParent,
                (itemsInTab, tab) => tab.SetItems(itemsInTab));
            shownTabs.ForEach(tab => tab.gameObject.SetActive(false));
            tabsParent.gameObject.SetActive(true);
            for (int i = 0; i < itemsByTabsList.Count; i++) {
                var (tabName, _) = itemsByTabsList[i][0].whereToShow;
                tabView.AddTab(tabName, shownTabs[i].gameObject, tabButtonPrefab);
            }
            tabView.SetShownTab(tabView.startTabInd);
            // Show over game items.
            overGameItems.SortBy(i => i.sortPriority);
            foreach (var prefab in overGameItems) {
                var inst = Instantiate(prefab, overGameParent);
                inst.gameObject.SetActive(false);
                inst.GetComponent<RectTransform>()?.FillParentRect();
                overGameViews.Add(inst);
            }
        }
        #endregion
    }
}