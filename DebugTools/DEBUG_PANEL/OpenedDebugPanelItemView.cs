﻿using UnityEngine;

namespace AntonTools {
    [ExecuteAlways]
    public abstract class OpenedDebugPanelItemView : DebugPanelItemView {
        public override WhereToShow showingType => wholeTab ? WhereToShow.AsWholeDebugPanelTab : WhereToShow.InSharedDebugPanelTab;
        public override string id => wholeTab ? whereToShow.tab : whereToShow.name;
        public override string name => wholeTab ? $"{whereToShow.tab} tab": $"{whereToShow.tab}->{whereToShow.name}";
        public abstract (string tab, string name) whereToShow { get; }
        public virtual bool wholeTab => string.IsNullOrEmpty(whereToShow.name);
    }
}