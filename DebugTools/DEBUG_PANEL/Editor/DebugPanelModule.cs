﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public class DebugPanelModule : DebugToolsModule {
        public override HowToModule HowTo() => new DEBUG_PANEL_HowTo();

        DebugPanelSettings settings => SettingsInEditor<DebugPanelSettings>.instance;
        public override void OnEnable() {
            base.OnEnable();
            SettingsInEditor<DebugPanelSettings>.EnsureExists();
        }

        BuildModeType shownBuildMode = BuildModeType.Develop;
        protected override void OnCompiledGUI() {
            base.OnCompiledGUI();
            var changed = false;
            // Set opening.
            GUILayout.Label("open debug panel:");
            EditorGUIUtils.Toolbar("in build mode", ref shownBuildMode, ref changed);
            var shownModeSettings = settings.buildModeSettings.Find(s => s.buildMode == shownBuildMode);
            GUILayout.BeginHorizontal();
            EditorGUIUtils.Indent();
            EditorGUIUtils.Toggle("by clicking top-left corner", ref shownModeSettings.openPanelByClicks, ref changed, width:180, labelWidth: 160);
            if (shownModeSettings.openPanelByClicks) {
                EditorGUIUtils.IntField(", after ", ref shownModeSettings.openPanelClicks, ref changed, width:80, labelWidth: 40);
                EditorGUIUtils.FloatField("clicks during", ref shownModeSettings.openPanelClicksDuration, ref changed, width: 120, labelWidth: 80);
                EditorGUIUtils.Label("seconds");
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            EditorGUIUtils.Indent();
            EditorGUIUtils.Toggle("by pressing key", ref shownModeSettings.openPanelByKey, ref changed, width: 180, labelWidth: 160);
            if (shownModeSettings.openPanelByKey) 
                EditorGUIUtils.Popup("key", ref shownModeSettings.openPanelKey, ref changed, width: 120, labelWidth: 40);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            EditorGUIUtils.Indent();
            EditorGUIUtils.Toggle("starts shown", ref shownModeSettings.startsShown, ref changed);
            GUILayout.EndHorizontal();
            // Set password protection.
            GUILayout.BeginHorizontal();
            EditorGUIUtils.Indent();
            EditorGUIUtils.Toggle("password on not develop", ref shownModeSettings.passwordEnabled, ref changed, width: 250);
            if (shownModeSettings.passwordEnabled)
                EditorGUIUtils.IntField("password", ref shownModeSettings.password, ref changed, width: 200, labelWidth: 100);
            GUILayout.EndHorizontal();

            // Other params.
            EditorGUIUtils.Toggle("auto orientation", ref settings.autoOrientation, ref changed);
            // Modules shown.
            GUILayout.BeginHorizontal();
            GUILayout.Label("items:");
            GUILayout.EndHorizontal();
            settings.itemViews.ForEach(item => {
                StartHorizontal();
                var enabled = !settings.disabledModules.Contains(item.id);
                if (EditorGUIUtils.Toggle("", ref enabled, ref changed, 20)) {
                    if (enabled)
                        settings.disabledModules.Remove(item.id);
                    else
                        settings.disabledModules.Add(item.id);
                }
                GUILayout.Label(item.name, GUILayout.Width(400));
                EndHorizontal();
            });
            StartHorizontal();
            if (GUILayout.Button("update", GUILayout.Width(200)))
                UpdateItems();
            EndHorizontal();
            void StartHorizontal() {
                GUILayout.BeginHorizontal();
                GUILayout.Space(100);
            }
            void EndHorizontal() {
                GUILayout.Space(200);
                GUILayout.EndHorizontal();
            }
            if (changed)
                settings.SetChanged();

        }
        void UpdateItems() {
            DebugPanelSettings.instance.itemViews.Clear();
            Utils.IterateAllPrefabsInProject((path, prefab) => {
                var item = prefab.GetComponent<DebugPanelItemView>();
                if (item != null)
                    DebugPanelItemView.UpdateAddToSettings(item);
            });
        }
    }
}
#endif