﻿using TMPro;
using UnityEngine;

namespace AntonTools {
    public class DebugPanelItemHeader : MonoBehaviour {
        public TextMeshProUGUI header;
    }
}
