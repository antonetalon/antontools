﻿namespace AntonTools {
    public abstract class ModuleDebugPanel : OpenedDebugPanelItemView {
        public abstract string module { get; }
        public abstract string tab { get; }
        public override (string tab, string name) whereToShow => (tab, module);
        public override float sortPriority => -1;
        public const string CommonTab = "common";
    }
    public abstract class ECSModuleDebugPanel : ModuleDebugPanel {
        public override float sortPriority => -0.5f;
    }
}