﻿using UnityEngine;

namespace AntonTools {
    [ExecuteAlways]
    public class DebugShowDeviceModel : MonoBehaviour {
        public string deviceModel;
        private void Update() {
            deviceModel = SystemInfo.deviceModel;
        }
    }
}