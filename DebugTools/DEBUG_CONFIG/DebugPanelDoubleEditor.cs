﻿using System.Globalization;

namespace AntonTools {
    public class DebugPanelDoubleEditor : DebugPanelNumberEditor<double> {
        protected override (bool success, double value) TryParse(string text)
            => (StringUtils.TryParse(text, out double value), value);
    }
}
