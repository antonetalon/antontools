﻿
namespace AntonTools {
    public class DebugPanelIntEditor : DebugPanelNumberEditor<int> {
        protected override (bool success, int value) TryParse(string text)
            => (int.TryParse(text, out var val), val);
    }
}