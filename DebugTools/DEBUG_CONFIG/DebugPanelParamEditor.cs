﻿using System;
using TMPro;
using UnityEngine;

namespace AntonTools {
    public abstract class DebugPanelParamEditor<T> : MonoBehaviour, IDebugPanelParamEditor {
        [SerializeField] TextMeshProUGUI title;
        public abstract void Show();
        protected Func<T> get { get; private set; }
        protected Action<T> set { get; private set; }
        public virtual IDebugPanelParamEditor Init(string name, int indent, Func<T> get, Action<T> set) {
            DebugPanelParamEditorLabel.Show(name, indent, title);
            this.get = get;
            this.set = set;
            Show();
            return this;
        }
    }
    public interface IDebugPanelParamEditor { void Show(); }
}