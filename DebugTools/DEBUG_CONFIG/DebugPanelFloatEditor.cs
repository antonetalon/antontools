﻿using System.Globalization;

namespace AntonTools {
    public class DebugPanelFloatEditor : DebugPanelNumberEditor<float> {
        protected override (bool success, float value) TryParse(string text)
            => (StringUtils.TryParse(text, out float value), value);
    }
}