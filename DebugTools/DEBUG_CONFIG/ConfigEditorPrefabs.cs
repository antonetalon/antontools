﻿using UnityEngine;

namespace AntonTools {
    public class ConfigEditorPrefabs : MonoBehaviour {
        public DebugPanelBoolEditor boolPrefab;
        public DebugPanelFloatEditor floatPrefab;
        public DebugPanelDoubleEditor doublePrefab;
        public DebugPanelIntEditor intPrefab;
        public DebugPanelParamEditorLabel labelPrefab;
    }
}