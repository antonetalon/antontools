﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class ConfigDebugView : ModuleDebugPanel {
        public static ConfigDebugView instance { get; private set; }
        protected override void Awake() {
            base.Awake();
            instance = this;
            if (!Application.isPlaying) return;
            UpdateView();
        }
        public override string tab => "Config";
        public override string module => "DEBUG_CONFIG";
        public override bool wholeTab => true;
        [SerializeField] Button tabButtonPrefab;
        [SerializeField] ConfigEditorFromMonoBeh tabPrefab;
        [SerializeField] TabView tabsView;
        [SerializeField] Transform tabsParent;
        List<ConfigEditorFromMonoBeh> configEditors = new List<ConfigEditorFromMonoBeh>();
        void UpdateView() {
            tabsParent.DestroyChildren();
            tabsView.Clear();
            configEditors.Clear();
            BalanceSettings.instances.SortBy(i => i.ToString());
            BalanceSettings.instances.ForEach(inst => {
                var tab = Instantiate(tabPrefab, tabsParent);
                tab.transform.GetComponent<RectTransform>().FillParentRect();
                var name = ReflectionUtils.GetProperty<string>(inst, "tabName");
                tabsView.AddTab(name, tab.gameObject, tabButtonPrefab);
                tab.SetConfig(inst);
                configEditors.Add(tab);
            });
            tabsView.SetShownTab(tabsView.startTabInd);
            LanscapeDebugPanelView.Init(transform);
        }
        public void UpdateConfigValuesView() => configEditors.ForEach(c => c.Show());
    }
}