﻿using UnityEngine;

namespace AntonTools {
    public class ConfigEditorFromMonoBeh : DebugPanelConfigEditor {
        [SerializeField] BalanceSettings config;
        public override BalanceSettings configInstance => config;
        public void SetConfig(BalanceSettings config) {
            this.config = config;
            UpdateView();
        }
    }
}