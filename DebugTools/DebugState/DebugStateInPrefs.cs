﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace AntonTools
{
    public abstract class DebugStateInPrefs
    {
#if UNITY_EDITOR
        public abstract void OnEditorGUI();
        private static List<DebugStateInPrefs> _debugStates;
        public static IReadOnlyList<DebugStateInPrefs> debugStates {
            get {
                if (_debugStates == null) {
                    _debugStates = new List<DebugStateInPrefs>();
                    ReflectionUtils.IterateTypes(t => {
                        var staticFields = t.GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                        foreach (var f in staticFields) {
                            if (!f.FieldType.HasParent(typeof(DebugStateInPrefs)))
                                continue;
                            var debugState = f.GetValue(null) as DebugStateInPrefs;
                            _debugStates.Add(debugState);
                        }
                    });
                }
                return _debugStates;
            }
        }
#endif
    }
    public class DebugStateInPrefs<T> : DebugStateInPrefs where T : Enum
    {
        private readonly EnumInPrefs<T> _debugState;
        public readonly string title;
        public DebugStateInPrefs(string title, T valueInRelease) {
            this.title = title;
            _debugState = new EnumInPrefs<T>(title, valueInRelease);
        }
        public T value {
            get => Application.isEditor ? _debugState : _debugState.defaultValue;
            set => _debugState.value = value;
        }
        public static implicit operator T(DebugStateInPrefs<T> inst) => inst.value;
#if UNITY_EDITOR
        public override void OnEditorGUI() {
            var value = this.value;
            var _ = false;
            if (EditorGUIUtils.Popup(title, ref value, ref _))
                this.value = value;
        }
#endif
    }
}