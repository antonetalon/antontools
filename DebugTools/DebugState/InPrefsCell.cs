﻿#if ZERGRUSH
using System;
using ZergRush;
using ZergRush.ReactiveCore;

namespace AntonTools
{
    public class InPrefsCell<T> : InPrefs<T>, ICell<T>
    {
        public InPrefsCell(string key, T defaultValue = default) : base(key, defaultValue) { }
        protected override void Write(T value) {
            var old = Read();
            base.Write(value);
            if (!old.DefaultEquals(value))
                reactions?.Invoke(value);
        }
        Action<T> reactions;
        public IDisposable ListenUpdates(Action<T> reaction) {
            reactions += reaction;
            return new AnonymousDisposable(() => reactions -= reaction);
        }
    }
}
#endif