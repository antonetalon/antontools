﻿namespace AntonTools {
    public class HapticSettingsView : ToggleView {
        protected override bool value { get => Haptic.on; set => Haptic.on = value; }
    }
}