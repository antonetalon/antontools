﻿#if UNITY_EDITOR
using UnityEngine;

namespace AntonTools {
    public class HapticModule : RootModule {
        public override HowToModule HowTo() => new HapticModule_HowTo();

        static HapticSettings settings => SettingsInEditor<HapticSettings>.GetSettingsInstance();
        protected override void OnCompiledGUI() {
            base.OnCompiledGUI();
            bool changed = false;
            EditorGUIUtils.Toggle("availableInEditor", ref settings.availableInEditor, ref changed);
            EditorGUIUtils.Toggle("log", ref settings.log, ref changed);
            EditorGUIUtils.Toolbar("default vibration power", ref settings.defaultType, ref changed);
            if (changed) EditorUtils.SetDirty(settings);
        }
    }
}
#endif