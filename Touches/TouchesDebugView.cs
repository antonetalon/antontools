﻿using UnityEngine.UI;

namespace AntonTools {
    public class TouchesDebugView : ModuleDebugPanel {
        public override string module => "TOUCHES";
        public override string tab => CommonTab;
        public Toggle debugCursorToggle;

        protected override void AwakePlaying() {
            base.AwakePlaying();
            if (TouchesView.instance == null)
                return;
            AddToggle(debugCursorToggle, () => TouchesView.instance.showTouches, val => TouchesView.instance.showTouches = val);
        }
    }
}