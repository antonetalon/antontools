﻿using UnityEngine;

namespace AntonTools {
    public class TouchSettings : SettingsScriptable<TouchSettings> {
        public Sprite picNormal;
        public Sprite picPressed;
        public bool okIfNoPics;
    }
}