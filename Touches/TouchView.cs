﻿using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class TouchView : MonoBehaviour {
#if COM_UNITY_MODULES_ANIMATION
        //[SerializeField] SimpleAnimation anim;
        [SerializeField] AnimationClip appear;
        [SerializeField] AnimationClip disappear;
        [SerializeField] AnimationClip idle;
#endif
        [SerializeField] Image picPressed, picNotPressed;
        Vector2 _screenPos;
        RectTransform parentRect;
        public Vector2 screenPos {
            get => _screenPos;
            set {
                _screenPos = value;
                if (parentRect == null)
                    parentRect = transform.parent.GetComponent<RectTransform>();
                RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, _screenPos,
                    TouchesView.instance.uiCamera, out var localPos);
                transform.localPosition = localPos;
            }
        }
        public Vector3 targetWorldPos {
            set => screenPos = TouchesView.instance.uiCamera.WorldToScreenPoint(value);
        }
        public void Appear() {
            gameObject.SetActive(true);
            // anim.Play(appear);
        }
        public async void Disappear() {
            // anim.Play(disappear);
#if COM_UNITY_MODULES_ANIMATION
            await Awaiters.SecondsRealtime(disappear.length);
#endif
            gameObject.SetActive(false);
            TouchesView.instance.ReturnToPool(this);
        }
        TouchSettings settings => TouchSettings.instance;
        private void OnEnable() {
            picPressed.SetSpriteSafe(settings.picPressed);
            picNotPressed.SetSpriteSafe(settings.picNormal);
        }
        private void Update() => UpdateTapping();
        public bool isTapping;
        void UpdateTapping() {
            // if (!isTapping || anim.isPlaying) return;
            // anim.Stop();
            // anim.Play(idle);
        }
    }
}