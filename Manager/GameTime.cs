﻿namespace AntonTools {
    // Not implemented.
    public static class GameTime {
        public static bool paused { get; private set; }
        public static void SetPause(bool paused) => GameTime.paused = paused;
        public static void Unpause() => SetPause(false);
        public static void Pause() => SetPause(true);
    }
}