﻿namespace AntonTools {
    public class AntonToolsSettings : SettingsScriptable<AntonToolsSettings> {
        public float simulatedInternetDelay = 0.5f;
        public string appleAppId;
    }
}