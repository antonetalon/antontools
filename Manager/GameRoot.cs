﻿using UnityEngine;

namespace AntonTools {
    public class GameRoot : MonoBehaviour {
        public static GameRoot instance { get; private set; }
        protected virtual void Awake() {
            instance = this;
            DontDestroyOnLoad(gameObject);
            FrameRate.Init();
        }

        [RuntimeInitializeOnLoadMethod]
        private static void InitOnStart() {
            if (instance != null) return;
            var go = new GameObject(typeof(GameRoot).Name);
            go.AddComponent<GameRoot>();
        }
    }
}