﻿namespace AntonTools {
    public class AntonToolsManager : MonoBehaviourHasInstance<AntonToolsManager> {
        public const string AssetsFolder = "Assets";
        public const string PluginsFolder = AssetsFolder + "/Plugins";
        public const string MainPluginName = "AntonTools";
        public const string MainPluginFolder = PluginsFolder + "/" + MainPluginName;
        public const string GeneratedFolder = MainPluginFolder + "Generated";
        protected override void Awake() {
            if (instance != null) {
                Destroy(gameObject);
                return;
            }
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }
    }
}