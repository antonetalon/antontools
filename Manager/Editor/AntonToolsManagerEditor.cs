﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace AntonTools
{
    [InitializeOnLoad]
    public class AntonToolsManagerEditor
    {
        public static bool inited { get; private set; }
        static AntonToolsManagerEditor()
        {
            if (!EditorApplication.isUpdating)
                InitializeOnLoadAndAssetsUpdated();
            else
                InitAfterAssetsUpdated();
        }
        static async void InitAfterAssetsUpdated()
        {
            await Awaiters.While(() => EditorApplication.isUpdating);
            InitializeOnLoadAndAssetsUpdated();
        }
        static void InitializeOnLoadAndAssetsUpdated()
        {
            DefinesModifier.InitOnLoad();
            PrefabUtils.InitOnLoad();
            BuildInfoManagerEditor.InitOnLoad();
            ExamplesDefines.InitOnLoad();
            ProjectSettingItemManager.InitOnLoad();
            ScriptOrderSetter.InitOnLoad(); 
            SwitchBuildModeToReleaseOnReleaseBranch.InitOnLoad();
            AntonToolsWindow.instance?.InitIfNeeded();
            //Ads.AdsModule.UpdateAutoSelectedAdsManager();
            inited = true;
        } 
    }
}
#endif