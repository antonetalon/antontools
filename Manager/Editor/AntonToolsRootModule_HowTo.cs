﻿#if UNITY_EDITOR
namespace AntonTools {
    public class AntonToolsRootModule_HowTo : HowToModule {
        public override string forWhat => "reusing code among multiple games";
        protected override void OnHowToGUI() {
        }
        protected override string docsURL => "https://docs.google.com/document/d/1IonykJ2VsAztlcd9NXI_yCROvaW79tEm3weYWMrLhb4/edit?usp=sharing";
    }
}
#endif