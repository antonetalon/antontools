﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using System.Text;

namespace AntonTools {
    public abstract class ModuleManager // call OnGUI Update OnEnable OnFocus OnLostFocus
    {
        #region Showing in editor window
        public enum ShowingState { Collapsed, Detailed, HowTo }
        public virtual bool hasCollapsedView => true;
        public virtual bool hasDetailedView => true;
        public virtual void ShortcutOnGUI() { }
        public abstract Type parentModule { get; }
        public string Name => GetType().Name;
        public virtual void OnGUI(bool collapsed) {
            Debug.Assert(!collapsed || hasCollapsedView, $"Cant show collapsed gui for {GetType().Name} module manager");
            if (collapsed) {
                EditorGUILayout.BeginHorizontal();
                EditorGUIUtils.RichLabel(Name, fontStyle: FontStyle.Bold, width: 200);
                OnCollapsedGUI();
                EditorGUILayout.EndHorizontal();
            } else {
                EditorGUIUtils.RichLabel(Name, TextAnchor.MiddleCenter, eatAllWidth: true, fontStyle: FontStyle.Bold);
                if (hasHowTo)
                    howTo.OnGUI();
                else
                    GUILayout.Label("this module is not yet documented");
                ExamplesOnGUI();
                    OnCompiledGUI();
            }
        }
        protected virtual string collapsedDescription => string.Empty;
        protected virtual void OnCollapsedGUI() {
            if (hasDetailedView) {
                const float ButtonWidth = 150;
                if (GUILayout.Button("Details", GUILayout.Width(ButtonWidth)))
                    AntonToolsWindow.instance.OnShowModulePressed(this);
            }
            var desc = collapsedDescription;
            if (desc != null) {
                const int MaxLength = 45;
                if (desc.Length > MaxLength) {
                    desc = desc.Substring(0, MaxLength);
                    if (desc.EndsWith("<b"))
                        desc = desc.Substring(0, desc.Length - 2);
                    if (desc.EndsWith("</b"))
                        desc = desc.Substring(0, desc.Length - 3);
                    if (desc.EndsWith("</"))
                        desc = desc.Substring(0, desc.Length - 2);
                    if (desc.EndsWith("<"))
                        desc = desc.Substring(0, desc.Length - 1);
                    if (Regex.Matches(desc, "<b>").Count > Regex.Matches(desc, "</b>").Count)
                        desc += "</b>";
                    desc += "...";
                }
                EditorGUIUtils.RichMultilineLabel(desc, width: 350);
            }
        }
        protected virtual void OnCompiledGUI() { }

        public virtual void OnEnable() {
            OnCompiledEnable();
            UpdateDebugPanel();
        }
        protected virtual void OnCompiledEnable() { }
        public virtual void OnDisable() => UpdateDebugPanel();
        public void OnFocus() {
            OnCompiledFocus();
        }
        protected virtual void OnCompiledFocus() { }
        public void OnLostFocus() {
            OnCompiledLostFocus();
        }
        protected virtual void OnCompiledLostFocus() { }
        public virtual void Update() {
            OnCompiledUpdate();
        }
        protected virtual void OnCompiledUpdate() { }
        #endregion

        #region How to
        public virtual HowToModule HowTo() => null;
        private HowToModule _howTo;
        bool howToInited;
        public HowToModule howTo {
            get {
                if (!howToInited) {
                    _howTo = HowTo();
                    howToInited = true;
                }
                return _howTo;
            }
        }
        public bool hasHowTo => howTo != null;
        public virtual string DoReleaseChecks() => null;
        protected bool PlatformValid(TargetPlatform platform, Func<bool> check, StringBuilder sb, string error) {
            if (!BuildModeSettings.instance.PlatformEnabled(platform))
                return true;
            if (!check()) {
                sb?.AppendLine(error);
                return false;
            }
            return true;
        }
        #endregion

        #region Examples
        void ExamplesOnGUI() {
            var examples = SettingsInEditor<ExamplesSettings>.GetSettingsInstance(false);
            if (examples == null)
                return;
            var currModuleExamples = examples.examples.Filter(e => e.forModules.Contains(GetType().FullName));
            if (currModuleExamples.Count == 0)
                return;
            GUILayout.Label("Examples:");
            currModuleExamples.ForEach(e => {
                EditorGUILayout.ObjectField(e.scene.m_SceneAsset, typeof(SceneAsset), false);
            });
        }
        #endregion

        #region Debug panel
        protected virtual string debugViewPath => null;
        private ModuleDebugPanel GetDebugPanelPrefab() {
            var path = debugViewPath;
            if (string.IsNullOrEmpty(path)) return null;
            path = $"{AntonToolsManager.MainPluginFolder}/{path}.prefab";
            var prefab = AssetDatabase.LoadAssetAtPath<ModuleDebugPanel>(path);
            Debug.Assert(prefab != null, $"Debug panel for module {GetType().Name} not found at {path}");
            return prefab;
        }
        public void UpdateDebugPanel() => DebugPanelItemView.UpdateAddToSettings(GetDebugPanelPrefab());
        #endregion
    }
    public abstract class RootModule : ModuleManager {
        public override Type parentModule => typeof(AntonToolsRootModule);
    }
}
#endif