﻿#if UNITY_EDITOR
using System.Collections.Generic;

namespace AntonTools {
    public class UpdatesSettings : SettingsScriptable<UpdatesSettings> {
        public List<string> completedChanges = new List<string>();
    }
}
#endif