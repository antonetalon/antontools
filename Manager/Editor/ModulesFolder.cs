﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;

namespace AntonTools {
    public abstract class ModulesFolder : ModuleManager {
        public override Type parentModule => typeof(AntonToolsRootModule);
        protected List<ModuleManager> compiledItems;
        void InitItemsIfNeeded() {
            if (compiledItems != null) return;
            compiledItems = AntonToolsWindow.allModules.Filter(m => m.parentModule == GetType());
        }
        protected override string collapsedDescription {
            get {
                string str = string.Empty;
                if (compiledItems?.Count > 0)
                    str = $"{compiledItems.ConvertAll(i => $"<b>{i.Name}</b>").PrintCollection(", ")} ";
                str += base.collapsedDescription;
                return str;
            }
        }
        protected override void OnCollapsedGUI() {
            base.OnCollapsedGUI();
            InitItemsIfNeeded();
        }
    }
}
#endif