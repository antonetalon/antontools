﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

namespace AntonTools {
    public class AntonToolsWindow : EditorWindow {
        public static AntonToolsWindow instance { get; private set; }
        [MenuItem(AntonToolsManager.MainPluginName + "/Menu")]
        public static AntonToolsWindow Open() {
            // Get existing open window or if none, make a new one:
            instance = (AntonToolsWindow)GetWindow(typeof(AntonToolsWindow), false, AntonToolsManager.MainPluginName);
            instance.Show();
            return instance;
        }

        #region Modules navigation
        static List<ModuleManager> modules = new List<ModuleManager>() {
            new AntonToolsRootModule(),

            new LocalizationModule(),

            new DebugToolsModuleFolder(),
            new DebugPanelModule(),
            new DebugPerformanceModule(),
            new DebugLogsOnDeviceModule(),
            new DebugPanelConfigModule(),

            new EditorToolsModule(),
            new BuildModesModule(),
            new AutoBalanceModule(),

            new CameraModule(),
            new TouchesModule(),
            new UIModule(),

            new OtherModule(),
            new PluginsModule(),
            new UtilsModule(),
            new HapticModule(),
        };
        public static IReadOnlyList<ModuleManager> allModules => modules;
        public static T GetModule<T>() where T : ModuleManager
            => allModules.Find(m => m.GetType() == typeof(T)) as T;
        public static ModuleManager GetModule(string define) => allModules.Find(m => m.Name == define);
        AntonToolsLocalSettings settings => AntonToolsRootModule.settings;
        public ModuleManager shownParenModule => shownModules[0];
        bool rootShown => shownParenModule.GetType() == typeof(AntonToolsRootModule);
        private List<ModuleManager> shownModules = new List<ModuleManager>();
        void InitModulesNavigation() {
            if (string.IsNullOrEmpty(settings.shownParentModuleDefine))
                settings.shownParentModuleDefine = typeof(AntonToolsRootModule).FullName;
            ShowModule(ReflectionUtils.GetTypeByName( settings.shownParentModuleDefine, false, true));
        }
        public void ShowModule(Type define) {
            settings.shownParentModuleDefine = define.FullName;
            bool ModuleShouldBeShown(ModuleManager module) {
                if (settings.showAllModules && define == typeof(AntonToolsRootModule))
                    return true;
                else
                    return (module.parentModule?.FullName == settings.shownParentModuleDefine) // Is child.
                    || (module.GetType() == typeof(AntonToolsRootModule) && settings.shownParentModuleDefine == typeof(AntonToolsRootModule).FullName) // Is root and root shown.
                    || (module.GetType().FullName == settings.shownParentModuleDefine); // Is shown parent module.
            }
            var remainingModules = shownModules.Filter(m => ModuleShouldBeShown(m));
            var hiddenModules = shownModules.Filter(m => !remainingModules.Contains(m));
            hiddenModules.ForEach(m => {
                if (focused)
                    m.OnLostFocus();
                m.OnDisable();
            });
            shownModules = modules.Filter(m => ModuleShouldBeShown(m));
            if (settings.alphabetOrder) {
                shownModules.SortBy(m => m.Name);
                var shownRoot = shownModules.Find(m => m.GetType() == typeof(AntonToolsRootModule));
                if (shownRoot != null) {
                    shownModules.Remove(shownRoot);
                    shownModules.Insert(0, shownRoot);
                }
            }
            shownModules.ForEach(m => {
                if (remainingModules.Contains(m))
                    return;
                m.OnEnable();
            });
            shownModules.ForEach(m => {
                if (remainingModules.Contains(m))
                    return;
                if (focused)
                    m.OnFocus();
            });
        }
        public void OnShowModulePressed(ModuleManager module) {
            ShowModule(module.GetType());
        }
        #endregion

        #region Transfer unity events (enabling, focusing) to modules
        private void OnEnable() {
            instance = this;
            if (AntonToolsManagerEditor.inited)
                InitIfNeeded();
        }
        public bool inited => shownModules.CountSafe() > 0;
        public void InitIfNeeded() {
            if (inited) return;
            InitModulesNavigation();
            shownModules.ForEach(m => m.OnEnable());
            ChangesEditorUI.OnEnable();
        }
        private void OnDisable() {
            shownModules.ForEach(m => m.OnDisable());
        }
        bool focused;
        private void OnFocus() {
            focused = true;
            EditorAsync.ExecuteAfter(() => inited, () => shownModules.ForEach(m => m.OnFocus()));
        }
        private void OnLostFocus() {
            focused = false;
            shownModules.ForEach(m => m.OnLostFocus());
        }
        #endregion

        Vector2 scrollPos;
        void OnGUI() {
            if (!inited) return;
            EditorGUIUtils.PushEnabling(!EditorApplication.isCompiling);
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            if (ChangesEditorUI.shown)
                ChangesEditorUI.OnGUI();
            else
                CommonOnGUI();
            EditorGUILayout.EndScrollView();
            EditorGUIUtils.PopEnabling();
        }
        void CommonOnGUI() {
            EditorGUIUtils.PushEnabling(!rootShown);
            if (EditorApplication.isCompiling)
                EditorGUIUtils.RichLabel("waiting compilation...", TextAnchor.MiddleCenter, true, false, EditorGUIUtils.warningColor, FontStyle.Bold, true);
            else {
                if (GUILayout.Button("back"))
                    ShowModule(shownParenModule.parentModule);
            }
            EditorGUIUtils.PopEnabling();
            shownModules.ForEach(m => {
                bool collapsed;
                if (shownParenModule == m)
                    collapsed = false;
                else
                    collapsed = true;
                if (collapsed && !m.hasCollapsedView)
                    collapsed = false;
                if (!collapsed && !m.hasDetailedView)
                    collapsed = true;

                var showChildTitles = shownParenModule == m && shownModules.Count > 1;
                //if (showChildTitles)
                //    EditorGUIUtils.RichLabel("Shown module:", TextAnchor.MiddleCenter, eatAllWidth: true, fontStyle: FontStyle.BoldAndItalic);
                m.OnGUI(collapsed);
                if (m.GetType() == typeof(AntonToolsRootModule))
                    ShortcutsOnGUI();

                if (showChildTitles)
                    EditorGUIUtils.RichLabel("Submodules:", TextAnchor.MiddleCenter,
                        eatAllWidth: true, fontStyle: FontStyle.BoldAndItalic);
            });
        }
        void ShortcutsOnGUI() {
            allModules.ForEach(m => {
                m.ShortcutOnGUI();
            });
        }
        private void Update() {
            if (EditorApplication.isCompiling)
                return;
            shownModules.ForEach(m => m.Update());
        }
    }
}
#endif