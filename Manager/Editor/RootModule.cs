﻿#if UNITY_EDITOR
using System;
using UnityEngine;

namespace AntonTools {
    public class AntonToolsRootModule : ModuleManager {
        public override Type parentModule => null;
        public override bool hasCollapsedView => false;
        public override HowToModule HowTo() => new AntonToolsRootModule_HowTo();

        public static AntonToolsLocalSettings settings => SettingsInEditor<AntonToolsLocalSettings>.instance;
        //protected override void EnablingOnGUI(string toggleTitle) {
        //    GUILayout.BeginHorizontal();
        //    var changed = false;
        //    EditorGUIUtils.Toggle("Show all modules", ref settings.showAllModules, ref changed);
        //    ChangesEditorUI.ShowChangesEnabling();
        //    EditorGUIUtils.Toggle("alphabet order", ref settings.alphabetOrder, ref changed);
        //    GUILayout.EndHorizontal();
        //    if (changed) {
        //        EditorUtils.SetDirty(settings);
        //        AntonToolsWindow.instance.OnShowModulePressed(AntonToolsWindow.instance.shownParenModule);
        //    }
        //}
    }
}
#endif