﻿namespace AntonTools {
    public interface IHasScreenSizeChangeCallback {
        void OnScreenSizeChanged();
    }
}