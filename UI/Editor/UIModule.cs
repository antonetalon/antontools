﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace AntonTools {
    public class UIModule : RootModule {
        public override HowToModule HowTo() => new UIModule_HowTo();
        public const string DefaultFontName = "Roboto-Regular-AntonTools SDF";

        public static TMPro.TMP_FontAsset defaultFont => new ExampleFontAsset(DefaultFontName).asset;
        protected override void OnCompiledGUI() {
            base.OnCompiledGUI();

            if (SettingsInEditor<ScreenSettings>.GetSettingsInstance(false) == null
                && GUILayout.Button("emulate screen safe area"))
                Selection.activeObject = SettingsInEditor<ScreenSettings>.GetSettingsInstance();
        }
    }
}
#endif