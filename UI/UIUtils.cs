﻿using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public static class UIUtils {
        public static Vector2 GetUISize(this RectTransform tr) 
            => tr.GetRootCanvas().GetComponent<RectTransform>().rect.size;
        public static Canvas GetRootCanvas(this Transform tr) 
            => tr.GetComponentInParent<Canvas>().rootCanvas;
        public static Camera GetUICamera(this Transform tr)
            => tr.GetRootCanvas().GetActualWorldCamera() ?? Camera.main;
        public static Camera GetActualWorldCamera(this Canvas canvas) 
            => canvas == null ? null : (canvas.renderMode == RenderMode.ScreenSpaceOverlay?null:canvas.worldCamera);
        public static (Camera uiCamera, bool overlay) GetUICameraOrOverlay(this Transform tr) {
            var rootCanvas = tr.GetRootCanvas();
            if (rootCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
                return (null, true);
            else
                return (rootCanvas.worldCamera ?? Camera.main, false);
        }
        public static float GetAlpha(this MaskableGraphic item) => item.color.a;
        public static void SetAlpha(this MaskableGraphic item, float alpha) {
            var col = item.color;
            col.a = alpha;
            item.color = col;
        }
    }
}