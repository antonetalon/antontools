﻿using System.Collections.Generic;
using UnityEngine;
#if ZERGRUSH
using ZergRush;
using ZergRush.ReactiveCore;
#endif

namespace AntonTools
{
    public abstract class ItemsView<TModel, TView> : MonoBehaviour where TView : Component
    {
        [SerializeField] TView prefab;
        protected List<TView> views = new List<TView>();
        [SerializeField] GameObject noItemsParent;
        protected abstract void ShowItem(TModel model, TView view);
        public void Show(IReadOnlyList<TModel> models) => UpdateView(models);
        protected virtual void UpdateView(IReadOnlyList<TModel> models)
            => Utils.UpdatePrefabsList(views, models, prefab, ShowItem, noItemsParent: noItemsParent);
#if ZERGRUSH
        readonly Connections showing = new Connections();
        public void Show(IReactiveCollection<TModel> models) {
            showing.DisconnectAll();
            showing.AddConnection(models.ShowReactiveCollection(views, prefab, (model, view) => ShowItem(model, view)));
        }
#endif
    }
}