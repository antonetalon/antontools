﻿using UnityEngine;

namespace AntonTools
{
    [ExecuteAlways]
    public class ScaleWithScreenSizeSaveAspect : ScaleFitInParentSaveAspect
    {
        int prevWidth, prevHeight;
        private void Update() {
            if (prevWidth != Screen.width || prevHeight != Screen.height)
                Scale();
        }
        public override bool Scale() {
            var success = base.Scale();
            prevWidth = Screen.width;
            prevHeight = Screen.height;
            return success;
        }
    }
}