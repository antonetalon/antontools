﻿using System.Collections.Generic;
using UnityEngine;
#if ZERGRUSH
#endif

namespace AntonTools
{
    public abstract class ItemsViewMultiplePrefabs<TModel, TView> : MonoBehaviour where TView : MonoBehaviour {
        protected abstract IEnumerable<TView> prefabs { get; }
        protected abstract TView GetPrefab(TModel model);
        Dictionary<TView, List<TView>> viewsByPrefab;
        HashSet<TView> usedViews = new HashSet<TView>();
        [SerializeField] GameObject noItemsParent;
        protected abstract void Show(TModel model, TView view);
        public void Show(IReadOnlyList<TModel> models) => UpdateView(models);
        protected virtual void Awake() => Init();
        private void Init() {
            if (viewsByPrefab != null) return;
            viewsByPrefab = new Dictionary<TView, List<TView>>();
            foreach (var prefab in prefabs)
                viewsByPrefab.GetOrAddValue(prefab, out var views, () => new List<TView> { prefab });
        }
        protected void UpdateView(IReadOnlyList<TModel> models) {
            Init();
            usedViews.Clear();
            for (int i = 0; i < models.Count; i++) {
                var model = models[i];
                var prefab = GetPrefab(model);
                var views = viewsByPrefab[prefab];
                var view = views.Find(v => !usedViews.Contains(v));
                if (view == null) {
                    view = Instantiate(prefab, prefab.transform.parent);
                    views.Add(view);
                }
                usedViews.Add(view);
                view.gameObject.SetActive(true);
                Show(model, view);
            }
            // Hide whats not used.
            foreach (var views in viewsByPrefab.Values) {
                foreach (var view in views) {
                    if (!usedViews.Contains(view))
                        view.gameObject.SetActive(false);
                }
            }
            NestedLayoutsUpdater.UpdateNearestParent(transform);
        }
    }
}