﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class TabView : MonoBehaviour {
        public List<HorizontalLayoutGroup> tabButtonLines;
        public List<Button> tabButtons;
        public List<GameObject> tabs;
        public int startTabInd;
        int tabInd;
        bool inited;
        void InitIfNeeded() {
            if (inited) return;
            inited = true;
            for (int i = 0; i < tabButtons.Count; i++) {
                var ind = i;
                tabButtons[i].onClick.AddListener(() => SetShownTab(ind));
            }
            SetShownTab(startTabInd);
        }
        private void Awake() => InitIfNeeded();

        public void Clear() {
            InitIfNeeded();
            if (tabButtonLines.Count == 0)
                transform.DestroyChildren();
            else
                tabButtonLines.ForEach(t => t.transform.DestroyChildren());
            tabButtons.Clear();
            tabs.Clear();
        }

        public void SetShownTab(int tabInd) {
            InitIfNeeded();
            this.tabInd = tabInd;
            for (int i = 0; i < tabButtons.Count; i++) {
                var selected = i == tabInd;
                tabs[i].SetActive(selected);
                tabButtons[i].interactable = !selected;
            }
        }

        public void AddTab(string name, GameObject tab, Button tabButtonPrefab) {
            InitIfNeeded();
            tab.name = name;
            var newTabInd = tabs.Count;
            var tabButton = Instantiate(tabButtonPrefab);
            tabButton.onClick.AddListener(() => SetShownTab(newTabInd));
            tabButton.name = $"{name}TabButton";
            tabButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = name;
            tabButtons.Add(tabButton);
            tabs.Add(tab);
            UpdateButtonsParenting();
        }
        void UpdateButtonsParenting() {
            if (tabButtonLines.Count == 0) {
                tabButtons.ForEach(t => {
                    t.transform.SetParent(transform);
                    t.transform.localScale = Vector3.one;
                    t.transform.localRotation = Quaternion.identity;
                    ZTo0(t.transform);
                });
                return;
            }

            var buttonsPerLine = Mathf.CeilToInt(tabButtons.Count / (float)tabButtonLines.Count);
            for (int i = 0; i < tabButtons.Count; i++) {
                var lineInd = i / buttonsPerLine;
                tabButtons[i].transform.SetParent(tabButtonLines[lineInd].transform);
                ZTo0(tabButtons[i].transform);
                tabButtons[i].transform.localScale = Vector3.one;
                tabButtons[i].transform.localRotation = Quaternion.identity;
            }
        }
        void ZTo0(Transform tr) => tr.localPosition = tr.localPosition.ZTo0();

        // Active means tab shown and tab button shown.
        public void SetTabActive(Button tabButton, bool active) {
            InitIfNeeded();
            var ind = tabButtons.IndexOf(tabButton);
            if (ind == -1) 
                throw new Exception($"Cant find tab {tabButton.name}");
            tabButton.SetActiveSafe(active);
            if ((tabInd == ind && !active) || (tabInd == -1)) {
                ind = tabButtons.FindIndex(b => b.gameObject.activeSelf);
                SetShownTab(ind);
            }
        }
    }
}