﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AntonTools {
    public class UIUnderPos : MonoBehaviourHasInstance<UIUnderPos> {
        [SerializeField] Canvas rootOfWorld; // 2D games are all from UI, so I need to separate game world from game UI somehow.
        [SerializeField] Transform rootTransformOfWorld;

        public bool InputFieldFocused() {
            var focused = EventSystem.current.currentSelectedGameObject?.GetComponent<TMP_InputField>();
            return focused;
        }

        public bool FingersAboveUI() {
            int count = 0;
            foreach (var _ in IterateUIUnderFingers())
                count++;
            return count > 0;
        }
        List<GameObject> uiUnderFingers = new List<GameObject>();
        float underFingersLastTime = -1;
        public IEnumerable<GameObject> IterateUIUnderFingers() {
            if (underFingersLastTime < Time.realtimeSinceStartup) {
                uiUnderFingers.Clear();
                underFingersLastTime = Time.realtimeSinceStartup;
                if (!Input.touchSupported) {
                    foreach (var go in IterateUIUnderPos(Input.mousePosition))
                        uiUnderFingers.Add(go);
                } else {
                    var touches = Input.touches;
                    foreach (var t in touches) {
                        foreach (var go in IterateUIUnderPos(t.position))
                            uiUnderFingers.Add(go);
                    }
                }
            }
            foreach (var go in uiUnderFingers)
                yield return go;
        }
        List<RaycastResult> hits = new List<RaycastResult>();
        public IEnumerable<GameObject> IterateUIUnderPos(Vector3 screenPos) {
            if (EventSystem.current == null)
                yield break;
            var pe = new PointerEventData(EventSystem.current);
            pe.position = screenPos;
            hits.Clear();
            EventSystem.current.RaycastAll(pe, hits);
            foreach (RaycastResult h in hits) {
                var root = h.gameObject.transform.root;
                if (root == rootTransformOfWorld) continue;
                if (rootOfWorld != null && rootOfWorld.transform == root) continue;
                yield return h.gameObject;
            }
        }
    }
}