﻿using UnityEngine;

namespace AntonTools {
    [RequireComponent(typeof(RectTransform)), ExecuteAlways]
    public class FillParentRect : MonoBehaviour {
        private void Awake() => GetComponent<RectTransform>().FillParentRect();
    }
}