﻿using UnityEngine;

namespace AntonTools
{
    [RequireComponent(typeof(RectTransform))]
    public class ScaleFitInParentSaveAspect : MonoBehaviour
    {
        private void OnEnable() {
            Scale();
        }
        private void Start() {
            Scale();
        }
        float prevParentWidth, prevParentHeight;
        private void Update() {
            if (parentRect == null)
                return;
            if (prevParentWidth != parentRect.rect.width || prevParentHeight != parentRect.rect.height)
                Scale();
        }
        RectTransform parentRect;
        public virtual bool Scale() {
            if (Utils.IsPrefabOpened()) return false;
            var rect = GetComponent<RectTransform>();
            if (parentRect == null)
                parentRect = rect.parent.GetComponent<RectTransform>();
            if (parentRect.rect.height == 0) return false;
            var topScale = (parentRect.rect.height * 0.5f + rect.anchoredPosition.y) / (rect.rect.height * 0.5f);
            var bottomScale = (parentRect.rect.height * 0.5f - rect.anchoredPosition.y) / (rect.rect.height * 0.5f);
            var rightScale = (parentRect.rect.width * 0.5f + rect.anchoredPosition.x) / (rect.rect.width * 0.5f);
            var leftScale = (parentRect.rect.width * 0.5f - rect.anchoredPosition.x) / (rect.rect.width * 0.5f);
            var minScale = Mathf.Min(topScale, bottomScale, rightScale, leftScale);
            rect.localScale = Vector3.one * minScale;
            prevParentWidth = parentRect.rect.width;
            prevParentHeight = parentRect.rect.height;
            return true;
        }
    }
}