﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class NestedLayoutsUpdater : MonoBehaviour {
        [SerializeField] bool once;
        private void OnEnable() {
            Do(gameObject);
            if (once)
                Destroy(this);
        }
        public void Do() => Do(gameObject);
        static bool doing;
        public static void Do(GameObject gameObject) {
            if (gameObject == null) return;
            if (!gameObject.activeInHierarchy) {
                // Layouts cant be updated correctly when go is not active.
                var doLater = gameObject.AddComponent<NestedLayoutsUpdater>();
                doLater.once = true;
                return;
            }
            if (doing) return;
            doing = true;
            var layoutGroups = gameObject.GetComponentsInChildren<HorizontalOrVerticalLayoutGroup>();
            RebuildLayoutGroupsUIFromMostNested(layoutGroups, gameObject);
            doing = false;
        }
        private static void RebuildLayoutGroupsUIFromMostNested(HorizontalOrVerticalLayoutGroup[] layoutGroups, GameObject gameObject) {
            var mostNestedLevel = layoutGroups.Max(l => l?.transform.GetChildDepth(gameObject.transform) ?? 0);
            var anythingLeft = false;
            for (int i = 0; i < layoutGroups.Length; i++) {
                if (layoutGroups[i] == null) continue;
                if (mostNestedLevel == layoutGroups[i].transform.GetChildDepth(gameObject.transform)) {
                    RebuildLayoutGroup(layoutGroups[i]);
                    layoutGroups[i] = null;
                } else
                    anythingLeft = true;
            }
            if (anythingLeft)
                RebuildLayoutGroupsUIFromMostNested(layoutGroups, gameObject);
        }
        private static void RebuildLayoutGroup(HorizontalOrVerticalLayoutGroup layoutGroup) {
            if (!layoutGroup.gameObject.activeSelf) return;
            layoutGroup.gameObject.SetActive(false);
            layoutGroup.gameObject.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate(layoutGroup.gameObject.GetComponent<RectTransform>());
        }
        public static void UpdateNearestParent(Transform transform, int parentsCount = 1) {
            while (parentsCount > 0 && transform != null) {
                parentsCount--;
                var updater = transform.GetComponentInParent<NestedLayoutsUpdater>();
                updater?.Do();
                transform = updater?.transform?.parent;
            }
        }
    }
}