﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace AntonTools {
    public class TextStyleSettings : SettingsScriptable<TextStyleSettings> {
        [ListItemName("type")]
        public List<TypeSettings> items = new List<TypeSettings>();
        [Serializable] public class TypeSettings {
            public TextStyleType type;
            public float size = 30;
            public bool sizeApplied = true;
            public float sizeMax = 30;
            public bool sizeMaxApplied = true;
            public float sizeMin = 10;
            public bool sizeMinApplied = true;
            public bool autoSize = false;
            public bool autoSizeApplied = true;
            public bool richText = true;
            public bool richTextApplied = true;
            public Color color;
            public bool colorApplied = true;
            public TMP_FontAsset font;
            public bool fontApplied = true;
            public FontStyles style = FontStyles.Normal;
            public bool styleApplied = true;
        }
        public static TypeSettings Get(TextStyleType type) {
#if UNITY_EDITOR
            SettingsInEditor<TextStyleSettings>.EnsureExists();
#endif
            var s = instance.items.Find(c => c.type == type);
            if (s == null) {
                s = new TypeSettings();
                s.type = type;
                instance.items.Add(s);
            }
            return s;
        }
    }
}