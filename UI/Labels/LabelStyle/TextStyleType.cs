﻿namespace AntonTools {
    public enum TextStyleType {
        None = 0, BigTitleText = 1, MediumButtonText = 2, SmallDescriptionText = 3, MediumYellow = 4
    }
}