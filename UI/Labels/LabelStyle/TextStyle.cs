using TMPro;
using UnityEngine;

namespace AntonTools {
    public class TextStyle : MonoBehaviour {
        public TextStyleType type = TextStyleType.None;
        TextStyleSettings.TypeSettings GetSettings() => TextStyleSettings.Get(type);
        public void Apply() {
            var s = GetSettings();
            var label = gameObject.GetComponent<TextMeshProUGUI>();
            if (s.sizeApplied)
                label.fontSize = s.size;
            if (s.sizeMaxApplied)
                label.fontSizeMax = s.sizeMax;
            if (s.sizeMinApplied)
                label.fontSizeMin = s.sizeMin;
            if (s.autoSizeApplied)
                label.enableAutoSizing = s.autoSize;
            if (s.richTextApplied)
                label.richText = s.richText;
            if (s.colorApplied)
                label.color = s.color;
            if (s.fontApplied)
                label.font = s.font;
            if (s.styleApplied)
                label.fontStyle = s.style;
        }

        public void Save() {
            var s = GetSettings();
            var label = gameObject.GetComponent<TextMeshProUGUI>();
            s.size = label.fontSize; s.sizeApplied = true;
            s.sizeMax = label.fontSizeMax; s.sizeMaxApplied = true;
            s.sizeMin = label.fontSizeMin; s.sizeMinApplied = true;
            s.autoSize = label.enableAutoSizing; s.autoSizeApplied = true;
            s.richText = label.richText; s.richTextApplied = true;
            s.color = label.color; s.colorApplied = true;
            s.font = label.font; s.fontApplied = true;
            s.style = label.fontStyle; s.styleApplied = true;
            TextStyleSettings.instance.SetChanged();
            label.SetChanged();
        }
    }
}