﻿#if UNITY_EDITOR
#if UNITY_2020_1_OR_NEWER
using TMPro;

namespace AntonTools {
    public class TextStyleEditor : LabelEditor {
        public override void OnInspectorGUI(TextMeshProUGUI label, ref bool changed) {
            var style = label.GetComponent<TextStyle>();
            if (style == null) {
                if (EditorGUIUtils.Button("set style", ref changed)) {
                    style = label.gameObject.AddComponent<TextStyle>();
                    style.type = TextStyleType.None;
                }
            } else {
                EditorGUIUtils.BeginHorizontal();
                if (EditorGUIUtils.Popup("type", ref style.type, ref changed))
                    style.Apply();
                if (EditorGUIUtils.Button("save style", ref changed))
                    style.Save();
                EditorGUIUtils.EndHorizontal();
            }
        }
    }
}
#endif
#endif