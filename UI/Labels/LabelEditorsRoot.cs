﻿#if UNITY_EDITOR
#if UNITY_2020_1_OR_NEWER
#if !NO_LOCALIZATION
using System;
using System.Collections.Generic;
using TMPro;
using TMPro.EditorUtilities;
using UnityEditor;
using UnityEngine;

namespace AntonTools {
    [CustomEditor(typeof(TextMeshProUGUI), true)]
    public class LabelEditorsRoot : TMP_EditorPanelUI {
        TextMeshProUGUI tgt => (TextMeshProUGUI)target;
        static List<LabelEditor> editors;
        public override void OnInspectorGUI() {
            if (ReplaceClassIfNeeded())
                return;
            if (editors == null)
                editors = ReflectionUtils.GetAllDerivedTypes<LabelEditor>().Filter(t
                    => t.CanCreateInstance()).ConvertAll(t => (LabelEditor)Activator.CreateInstance(t));
            var changed = false;
            foreach (var editor in editors)
                editor.OnInspectorGUI(tgt, ref changed);
            base.OnInspectorGUI();
            if (changed)
                tgt.SetChanged();
        }
        private bool ReplaceClassIfNeeded() {
            var go = ((Component)target).gameObject;
            return ReplaceClass<TextMeshProUGUI>(go, t => t.text) || ReplaceClass<TMP_Text>(go, t => t.text);
        }
        public static bool ReplaceClass<T>(GameObject go, Func<T, string> getText) where T : Component {
            if (!LocalizationSettings.editorSettings.allTextsToLocalizedLabel)
                return false;
            var oldText = go.GetComponent<T>();
            if (oldText != null && oldText.GetType() == typeof(T)) {
                var text = getText(oldText);
                DestroyImmediate(oldText);
                var newText = go.GetOrAddComponent<LocalizedLabel>();
                newText.text = Localization.GetGlobal(text);
                go.name = go.name.Replace(" (TMP)", "");
                go.SetChanged();
                return true;
            } else
                return false;
        }
    }
}
#endif
#endif
#endif