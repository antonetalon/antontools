﻿#if UNITY_EDITOR
#if UNITY_2020_1_OR_NEWER
using TMPro;

namespace AntonTools {
    public abstract class LabelEditor {
        public abstract void OnInspectorGUI(TextMeshProUGUI label, ref bool changed);
    }
}
#endif
#endif