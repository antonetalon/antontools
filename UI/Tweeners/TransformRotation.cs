﻿using UnityEngine;

namespace AntonTools {
    public class TransformRotation : TweenInTime {
        [SerializeField] protected Transform tgt;
        [SerializeField] protected Vector3 angleSpeed;
        protected override void OnProgress(float progress) {
            tgt.localEulerAngles += angleSpeed * Time.deltaTime;
        }
    }
}