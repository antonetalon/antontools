﻿#if !NO_LOCALIZATION
using System;
using System.Linq;
using UnityEngine;

namespace AntonTools {
    public class LocalizationManager : MonoBehaviourSingleton<LocalizationManager> {

        static LocalizationSettings settings => LocalizationSettings.instance;
        void Start() {
            var (saved, lang) = LoadSavedLanguage();
            if (!saved) {
                lang = Application.systemLanguage.ToLanguage();
                if (!settings.activeLangs.Contains(lang))
                    lang = settings.defaultPlayerLanguage;
            }
            settings.playerLanguage = lang;
        }

        const string PrefsKey = "playerLanguage";
        public static (bool exists, ShownLocalization lang) LoadSavedLanguage() {
            var success = Enum.TryParse<ShownLocalization>(PlayerPrefs.GetString(PrefsKey), out var lang);
            return (success, lang);
        }
        public static void SaveLanguage()
            => PlayerPrefs.SetString(PrefsKey, settings.playerLanguage.ToString());
    }
}
#endif