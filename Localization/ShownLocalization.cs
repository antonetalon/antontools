﻿using UnityEngine;

namespace AntonTools {
    public enum ShownLocalization {
        // CHs = Chinese simplified, CHt = traditional
        Keys = -1, Longest = -2, EN = 0, UA = 1, RU = 2, CHs = 3, CHt = 4
    }
#if !NO_LOCALIZATION
    public static class LanguageUtils {
        public static ShownLocalization ToLanguage(this SystemLanguage systemLang) {
            switch (systemLang) {
                case SystemLanguage.English: return ShownLocalization.EN;
                case SystemLanguage.Chinese:
                case SystemLanguage.ChineseSimplified: return ShownLocalization.CHs;
                case SystemLanguage.ChineseTraditional: return ShownLocalization.CHt;
                case SystemLanguage.Ukrainian: return ShownLocalization.UA;
                case SystemLanguage.Russian: return ShownLocalization.RU;
                default: return LocalizationSettings.instance.defaultPlayerLanguage;
            }
        }
        public static bool IsChinese(this ShownLocalization lang) => lang == ShownLocalization.CHs || lang == ShownLocalization.CHt;
        public static bool IsLanguage(this ShownLocalization lang) => lang != ShownLocalization.Keys && lang != ShownLocalization.Longest;
    }
#endif
}
