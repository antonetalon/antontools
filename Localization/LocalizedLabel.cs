﻿using TMPro;
using UnityEngine;

namespace AntonTools {
    public class LocalizedLabel : TextMeshProUGUI, IMonoBehaviourWithLocalizationKey {
        [SerializeField] LocalizedString _text;
#if !NO_LOCALIZATION
        public new LocalizedString text {
            get {
                if (_text == null) {
                    _text = LocalizedString.Create("");
                    this.SetChanged();
                }
                return _text;
            }
            set {
                _text = value;
                if (Application.isPlaying)
                    UpdateShownText();
            }
        }
        public string key => text.key;
        TMP_InputField currInput;
        protected override void Awake() {
            base.Awake();
            currInput = transform.GetComponentInParent<TMP_InputField>();
            if (currInput != null && currInput.textComponent != this)
                currInput = null;
            richText = true;
            if (_text.key.IsNothing() && _text.isGlobal)
                _text = shownText.ToGlobal();
            Localization.onLanguageChanged += UpdateShownText;
            if (Application.isPlaying)
                UpdateShownText();
        }
        protected override void OnDestroy() {
            Localization.onLanguageChanged -= UpdateShownText;
            base.OnDestroy();
        }
        public void UpdateShownText() {
            var newShownText = (string)text;
            if (text.isGlobal && !Application.isPlaying)
                newShownText = shownText; // Do not update text.
            if (currInput != null)
                currInput.text = newShownText;
            base.text = newShownText;
        }
        public string shownText => base.text;
#else
        string IMonoBehaviourWithLocalizationKey.key => null;
#endif
    }
    public interface IMonoBehaviourWithLocalizationKey
    {
        string key { get; }
    }
}
