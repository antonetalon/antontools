﻿#if UNITY_EDITOR
#if UNITY_2020_1_OR_NEWER
#if !NO_LOCALIZATION
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    [CustomEditor(typeof(Text), true)]
    public class UnityEngineUITextEditor : Editor {
        public override void OnInspectorGUI()
            => ReplaceClassIfNeeded();
        private void ReplaceClassIfNeeded() {
            var go = ((Component)target).gameObject;
            LabelEditorsRoot.ReplaceClass<Text>(go, t => t.text);
        }
    }
}
#endif
#endif
#endif