﻿using System.Collections.Generic;
using UnityEngine;

namespace AntonTools {
    public class SwitchLanguagesView : MonoBehaviour {
        [SerializeField] LanguageButton buttonPrefab;

#if !NO_LOCALIZATION
        List<LanguageButton> shownButtons = new List<LanguageButton>();
        LocalizationSettings settings => LocalizationSettings.instance;
        private void Awake() {
            shownButtons.Add(buttonPrefab);
            Utils.UpdatePrefabsList(shownButtons, settings.activeLangs.ConvertAll(i => i), buttonPrefab, transform, (lang, button) => button.Show(lang));
            AsyncUtils.ExecuteAfterFrames(1, () => gameObject.SetActive(false));
            AsyncUtils.ExecuteAfterFrames(2, () => gameObject.SetActive(true));
        }
#endif
    }
}