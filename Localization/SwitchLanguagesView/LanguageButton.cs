﻿using UnityEngine;
using UnityEngine.UI;

namespace AntonTools {
    public class LanguageButton : MonoBehaviour {
        [SerializeField] Image ico;
        [SerializeField] Button button;
        [SerializeField] GameObject selectedParent;
#if !NO_LOCALIZATION
        ShownLocalization lang;
        LocalizationSettings settings => LocalizationSettings.instance;
        protected void Awake() {
            Localization.onLanguageChanged += UpdateView;
            button.onClick.AddListener(OnLanguagePressed);
        }
        private void OnDestroy() 
            => Localization.onLanguageChanged -= UpdateView;
        public void Show(ShownLocalization lang) {
            this.lang = lang;
            ico.sprite = LanguageIcons.Get(lang);
            UpdateView();
        }
        private void OnLanguagePressed() {
            settings.playerLanguage = lang;
        }
        void UpdateView() {
            selectedParent.SetActive(lang == settings.playerLanguage);
        }
#endif
    }
}