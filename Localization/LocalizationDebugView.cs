﻿
namespace AntonTools {
    public class LocalizationDebugView : ModuleDebugPanel {
        public override string module => "LOCALIZATION";
        public override string tab => CommonTab;
    }
}