﻿#if !NO_LOCALIZATION
using System;

namespace AntonTools {
    public static class Localization {
        public static RefAction<string> onBeforeLocalization;
        static LocalizationSettings settings => LocalizationSettings.instance;
        public static LocalizedString Get(string localizationKey, params (string paramName, LocalizedString paramValue)[] parameters)
            => LocalizedString.Create(localizationKey, parameters);
        public static LocalizedString Get(string localizationKey, LocalizedString param, params LocalizedString[] moreParams)
            => LocalizedString.Create(localizationKey, param, moreParams);
        public static LocalizedString GetGlobal(string text)
            => LocalizedString.CreateGlobal(text);
        public static bool Exists(string key) => settings.Get(key) != null;

        public static event Action onLanguageChanged;
        public static void OnLanguageChanged() => onLanguageChanged?.Invoke();
    }
}
#endif