﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AntonTools {
    [Serializable]
    public sealed class LocalizedString {
        public bool isGlobal;
        [SerializeField] string _key; // Global localized strings are just simple strings.
#if !NO_LOCALIZATION
        public string key => isGlobal ? string.Empty : _key;
        private struct Param { public string paramName; public LocalizedString localizedText; }
        List<Param> parameters = new List<Param>();
        public static LocalizedString Create(string localizationKey, params (string paramName, LocalizedString paramValue)[] parameters)
            => CreatePrivate(localizationKey, parameters.CountSafe() > 0 ? parameters.ConvertAll(p
                => new Param { paramName = p.paramName, localizedText = p.paramValue }) : null, false);
        public static LocalizedString CreateGlobal(string text)
            => CreatePrivate(text, null, true);

        public static LocalizedString Create(string localizationKey, LocalizedString param, params LocalizedString[] moreParams) {
            var parameters = new List<Param>();
            parameters.Add(new Param { localizedText = param });
            moreParams.ForEach(p => parameters.Add(new Param { localizedText = param }));
            return CreatePrivate(localizationKey, parameters, false);
        }
        private static LocalizedString CreatePrivate(string localizationKey, List<Param> parameters, bool global) {
            var localized = new LocalizedString();
            localized.isGlobal = global;
            localized._key = localizationKey;
            if (parameters != null)
                localized.parameters = parameters;
            return localized;
        }
        private LocalizedString() { }

        public static bool GivenAndRequestedParamsMatch(string localizedText, IEnumerable<string> paramNames) {
            foreach (var name in paramNames) {
                var formatToFind = $"{{{name}}}";
                if (!localizedText.Contains(formatToFind))
                    return false;
            }
            return true;
        }
        public static bool localizationLogs = false;
        private static string Get(LocalizedString toLocalize, LocalizationSettings settings) {
            if (toLocalize.isGlobal || settings.playerLanguage == ShownLocalization.Keys)
                return toLocalize._key;
            // Try find localization key.
            var key = settings.Get(toLocalize.key);
            if (key == null) {
                if (localizationLogs)
                    Debug.LogError($"Localization key {toLocalize.key} not found");
                return toLocalize.key;
            }

            // Find longest localization if needed.
            var shownLanguage = settings.playerLanguage;
            if (settings.playerLanguage == ShownLocalization.Longest)
                shownLanguage = key.translations.MaxItem(l => l.localizedText.StripHTML().LengthSafe())?.language ?? shownLanguage;

            // Try find localization.
            var translation = key.GetTranslation(shownLanguage);
            if (translation == null || translation.localizedText.IsNullOrEmpty()) {
                if (localizationLogs)
                    Debug.LogError($"Localization key {toLocalize.key} not translated to {settings.playerLanguage}");
                translation = key.GetTranslation(settings.defaultPlayerLanguage);
                //if (translation == null || translation.localizedText.IsNullOrEmpty())
                return toLocalize.key;
            }

            // Try localize text with parameters.
            var localizedText = translation.localizedText;
            if (Localization.onBeforeLocalization != null)
                Localization.onBeforeLocalization(ref localizedText);
            var showLocalizationError = !BuildModeSettings.release && !GivenAndRequestedParamsMatch(localizedText, toLocalize.parameters.Select(p => p.paramName));
            if (showLocalizationError) {
                if (localizationLogs && !localizedText.IsNullOrEmpty())
                    Debug.LogError($"'{localizedText}' localization for key {toLocalize.key} should have {toLocalize.parameters.Count} params");
                return $"{(toLocalize.parameters != null ? toLocalize.parameters.Select(p => p.localizedText).PrintCollection() : "")}{localizedText}";
            }
            for (int i = 0; i < toLocalize.parameters.Count; i++) {
                var p = toLocalize.parameters[i];
                localizedText = localizedText.Replace($"{{{p.paramName}}}", p.localizedText.value.InReplacementColor());
            }

            return localizedText;
        }
        public override string ToString() => $"localized text: {value}";
        public string value => Get(this, LocalizationSettings.instance);
        public static implicit operator string(LocalizedString text) => text.value;
        public LocalizedString Clone() => Localization.Get(key);
#endif
    }
#if !NO_LOCALIZATION
    public static class LocalizedStringUtils {
        public static LocalizedString ToGlobal(this string str) => LocalizedString.CreateGlobal(str);
        public static LocalizedString ToGlobalString(this object obj) => obj.ToString().ToGlobal();
        public static void SetTextSafe(this LocalizedLabel label, LocalizedString text) {
            if (label == null)
                return;
            label.text = text;
        }
        public static Color ReplacementColor = new Color(1, 0.65f, 0, 1);
        public static string InReplacementColor(this string str) => StringUtils.ColoredText(ReplacementColor, str);
    }
#endif
}