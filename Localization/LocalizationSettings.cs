﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AntonTools {
    public class LocalizationSettings : SettingsScriptable<LocalizationSettings> {
        #if !NO_LOCALIZATION
#if UNITY_EDITOR
        public static LocalizationSettings editorSettings => SettingsInEditor<LocalizationSettings>.instance;
#endif
        public ShownLocalization defaultPlayerLanguage = ShownLocalization.EN;
        public ShownLocalization _playerLanguage = ShownLocalization.EN;
        public List<ShownLocalization> activeLocalizations = new List<ShownLocalization> { ShownLocalization.EN };
        public IEnumerable<ShownLocalization> activeLangs => activeLocalizations.Filter(lang => lang.IsLanguage());
        public List<ShownLocalization> shownTranslations = new List<ShownLocalization>();
        public List<LocalizationKeyData> keys = new List<LocalizationKeyData>();
        public ExportFormat exportFormat = ExportFormat.PlainText;
        public enum ExportFormat { ChinaCSV, PlainText }
        public List<TextReplacement> replaceOnImport
            = new List<TextReplacement> { new TextReplacement { replaceWhat = "？", replaceToWhat = " ? " } };
        [Serializable]
        public struct TextReplacement { public string replaceWhat, replaceToWhat; }
        public bool allTextsToLocalizedLabel = false;
        public bool showContext = true;

        public bool chineseActive => activeLangs.Any(l => l.IsChinese());
        public ShownLocalization playerLanguage {
            get => _playerLanguage;
            set {
                _playerLanguage = value;
                if (Application.isPlaying) {
                    LocalizationManager.SaveLanguage();
                    Localization.OnLanguageChanged();
                }
            }
        }
        public LocalizationKeyData Get(string key) {
            // TODO: make fast for runtime.
            return keys.Find(k => k.localizationKey == key);
        }
#if UNITY_EDITOR
        public void CreateLocalization(string localizationKey, string shownText)
            => CreateLocalization(localizationKey, playerLanguage, shownText);
        public void CreateLocalization(string localizationKey, ShownLocalization lang, string shownText) {
            var keyData = Get(localizationKey);
            if (keyData == null) {
                keyData = new LocalizationKeyData();
                keys.Add(keyData);
            }
            keyData.localizationKey = localizationKey;
            //keyData.paramsCount = 0;
            var translation = keyData.GetTranslation(lang);
            if (translation == null) {
                translation = new TranslationData();
                keyData.translations.Add(translation);
            }
            translation.language = lang;
            translation.localizedText = PostProcessOnImport(shownText);
            if (!activeLangs.Contains(lang))
                activeLocalizations.Add(lang);
            this.SetChanged();
        }
        private string PostProcessOnImport(string stringToImport) {
            if (stringToImport == null) return stringToImport;
            replaceOnImport.ForEach(r => stringToImport = stringToImport.Replace(r.replaceWhat, r.replaceToWhat));
            return stringToImport;
        }
#endif
#endif
    }
    [Serializable]
    public class LocalizationKeyData {
        public string localizationKey;
        public List<string> paramNames = new List<string>();
        public bool paramNamesUnfolded = false;
        public string context; // Optional note for translator - describing context of this translation.
        public enum Status { Normal, NotUsed }
        public Status status;
        public List<TranslationData> translations = new List<TranslationData>();

#if !NO_LOCALIZATION
        public string Get(ShownLocalization language) => GetTranslation(language)?.localizedText;
        public TranslationData GetTranslation(ShownLocalization language) {
            // TODO: make fast for runtime.
            return translations.Find(tr => tr.language == language);
        }
        public TranslationData GetOrAddTranslation(ShownLocalization language, ref bool changed) {
            var tr = GetTranslation(language);
            if (tr == null) {
                tr = new TranslationData { language = language };
                translations.Add(tr);
            }
            return tr;
        }
        public (bool complete, bool partial) GetState() {
            var hasTranslated = false;
            var hasNotTranslated = false;
            foreach (var lang in LocalizationSettings.instance.activeLangs) {
                var translated = !Get(lang).IsNothing();
                hasTranslated |= translated;
                hasNotTranslated |= !translated;
            }
            return (!hasNotTranslated, hasTranslated && hasNotTranslated);
        }
#endif
    }
    [Serializable]
    public class TranslationData {
        public ShownLocalization language;
        public string localizedText;
    }
}