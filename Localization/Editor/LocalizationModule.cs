﻿
#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace AntonTools {
    public class LocalizationModule : RootModule {
#if !NO_LOCALIZATION
        public const string define = "LOCALIZATION";
        public override HowToModule HowTo() => new LocalizationModule_HowTo();

        static LocalizationSettings settings => SettingsInEditor<LocalizationSettings>.instance;
        protected override void OnCompiledEnable() {
            SettingsInEditor<LocalizationSettings>.EnsureExists();
            settings.shownTranslations = settings.activeLangs.Clone();
            settings.SetChanged();
            langToExport = settings.activeLangs.Find(lang => lang != ShownLocalization.EN);
            InitFontExample();
        }
        ShownLocalization langToExport;
        protected override void OnCompiledGUI() {
            var changed = false;
            //OnPlayerLanguageGUI(ref changed);
            if (shownFontGUI)
                OnFontCreationGUI(ref changed);
            else {
                EditorGUIUtils.Popup("default player language", ref settings.defaultPlayerLanguage, ref changed);
                OnExportGUI(ref changed);
                ShowLanguages("Active languages", settings.activeLocalizations, lang => lang.IsLanguage(), ref changed);
                EditorGUIUtils.BeginHorizontal();
                ShowReplaceTextMeshPros(ref settings.allTextsToLocalizedLabel, ref changed);
                if (EditorGUIUtils.Button("create font", ref changed))
                    shownFontGUI = true;
                EditorGUIUtils.EndHorizontal();
                GUILayout.Space(10);
                ShowUpdateKeys(ref changed);
                if (settings.chineseActive)
                    ShowUpdateChineseAtlases(ref changed);
                ShowTranslations(ref changed);
            }
            if (changed)
                settings.SetChanged();
        }

        static bool shownFontGUI;
        static Font sourceFont;
        static Object outputFolder;
        static void OnFontCreationGUI(ref bool changed) {
            if (EditorGUIUtils.Button("back", ref changed))
                shownFontGUI = false;
            EditorGUIUtils.TypedField("input font", ref sourceFont, ref changed);
            EditorGUIUtils.FolderField("output folder", ref outputFolder, ref changed);
            EditorGUIUtils.PushEnabling(sourceFont != null && outputFolder != null);
            if (EditorGUIUtils.Button("create", ref changed)) {
                var inputPath = AssetDatabase.GetAssetPath(sourceFont);
                var fontName = Path.GetFileNameWithoutExtension(inputPath);
                var outputPath = $"{AssetDatabase.GetAssetPath(outputFolder)}/{fontName}.asset";
                var chars = new List<char>();
                void AddRange(char s, char e) {
                    for (int i = s; i < e; i++)
                        chars.Add((char)i);
                }
                AddRange(' ', '~'); // Latin.
                AddRange('А', 'я'); // Cyrrilic.
                chars.Add('і');
                chars.Add('ї');
                FontAssetCreator.Create(inputPath, outputPath, chars);
            }
        }

        public static void OnPlayerLanguageGUI(ref bool changed) {
            GUILayout.BeginHorizontal();
            var lang = settings.playerLanguage;
            var localizationsToSelect = settings.activeLocalizations.ConvertAll(l => l.ToString())
                .Adding(ShownLocalization.Keys.ToString()).Adding(ShownLocalization.Longest.ToString()).ToArray();
            var langStr = lang.ToString();
            if (EditorGUIUtils.Popup("Player language", ref langStr, localizationsToSelect, ref changed, 250, labelWidth)) {
                lang = Utils.EnumParse(langStr, settings.defaultPlayerLanguage);
                settings.playerLanguage = lang;
                if (!Application.isPlaying)
                    SetLanguageInOpenedScene(settings.playerLanguage);
                LocalizationManager.SaveLanguage();
            }
            GUILayout.EndHorizontal();
        }

        public static void ShowReplaceTextMeshPros(ref bool allTextsToLocalizedLabel, ref bool changed) {
            if (!EditorGUIUtils.Toggle("replace all Texts with LocalizedLabel", ref allTextsToLocalizedLabel, ref changed, labelWidth: 250))
                return;
            if (!allTextsToLocalizedLabel)
                return;
        }

        const string fontWithChineseNameOriginal = "RobotoWithChineseAntonTools";
        string fontWithChineseName => FontAssetCreator.GetGeneratedFontName(fontWithChineseNameOriginal);
        ExampleFontAsset fontWithChinese;
        void InitFontExample() => fontWithChinese = new ExampleFontAsset(fontWithChineseName, "font not yet generated");
        private void ShowUpdateChineseAtlases(ref bool changed) {
            fontWithChinese.ShowOnGUI("use this font, it has Chinese");
            if (GUILayout.Button("Update Chinese font atlases")) {
                UpdateChineseFontAtlases();
                changed = true;
            }
        }
        void UpdateChineseFontAtlases() {
            var font = UpdateAtlases(ShownLocalization.CHs, ShownLocalization.CHt);
            var aggregatedFont = FontAssetCreator.CreateAggregate(fontWithChineseNameOriginal, UIModule.defaultFont, font);
            Selection.activeObject = aggregatedFont;
            EditorGUIUtility.PingObject(aggregatedFont);
            InitFontExample();
        }

        private TMP_FontAsset UpdateAtlases(params ShownLocalization[] langs) {
            // Symbols.
            var symbols = new List<char>();
            settings.keys.ForEach(key => langs.ForEach(lang => key.Get(lang)?.ForEach(ch => symbols.AddIfNotExists(ch))));

            // Source font.
            var sourceFontName = "HanyiSentyChalk";
            var font = new ExampleFontTTF(sourceFontName).asset;

            return FontAssetCreator.Create(font, sourceFontName, symbols.PrintCollection("", ""));
        }

        public static void ShowUpdateKeys(ref bool changed) {
            if (GUILayout.Button("Update localization keys"))
                UpdateLocalizationKeys();
        }

        private static void UpdateLocalizationKeys() {
            // Remove duplicate keys.
            for (int i = settings.keys.Count - 1; i >= 0; i--) {
                var key = settings.keys[i].localizationKey;
                if (settings.keys.FindIndex(k => k.localizationKey == key) != i) {
                    Debug.Log($"removing duplicate key {key}");
                    settings.keys.RemoveAt(i);
                }
            }

            // Find keys and params count in project.
            var keysUsedInProject = new List<(string localizationKey, List<string> paramNames)>();

            // Get keys from prefabs.
            Action<GameObject> iterateGO = (GameObject go) => {
                var text = go.GetInterface<IMonoBehaviourWithLocalizationKey>();
                if (text != null && text.key.IsSomething())
                    OnKeyFound(text.key, go.GetFullPath(true));
                var monobehs = go.GetComponents<Component>();
                monobehs.ForEach(c => {
                    if (c != null)
                        GetKeysFrom(c);
                });
            };
            // Get keys from scene.

            // Get keys from scriptable objects.
            void GetKeysFrom(Object instance) {
                var t = instance.GetType();
                var members = t.GetFields().ToList();
                members = members.Filter(member => member.MemberType == MemberTypes.Field && member.GetFieldPropertyType() == typeof(LocalizedString));
                if (members.Count == 0) return;
                members.ForEach(member => {
                    var localizedText = ReflectionUtils.GetField<LocalizedString>(instance, member.Name);
                    var key = ReflectionUtils.GetField<string>(localizedText, "_key");
                    OnKeyFound(key, AssetDatabase.GetAssetPath(instance));
                });
            }
            void OnKeyFound(string key, string whereFound) {
                if (key.IsNothing() || key.Contains("{{") || key.Contains("}}")) return;
                if (keysUsedInProject.Any(k => k.localizationKey == key)) return; // Already exists.
                keysUsedInProject.Add((key, new List<string>()));
            }
            // Get keys from code.
            AssetsIterator.IterateAllAssetsRecursively(path => {
                if (path.EndsWith(".prefab")) {
                    var prefabRoot = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    prefabRoot.transform.IterateChildren(tr => iterateGO(tr.gameObject));
                } else if (path.EndsWith(".cs"))
                    IterateKeysFromCode(path, keysUsedInProject);
                else if (path.EndsWith(".asset")) {
                    var scriptable = AssetDatabase.LoadAssetAtPath<ScriptableObject>(path);
                    if (scriptable != null)
                        GetKeysFrom(scriptable);
                }
            });
            AssetsIterator.IterateAllSceneGameObjectsInProject(item => iterateGO(item.go));

            // Find old keys to remove.
            settings.keys.ForEach(key => {
                var usedKeyInd = keysUsedInProject.FindIndex(k => k.localizationKey == key.localizationKey);
                var usedInProject = usedKeyInd != -1;
                key.status = usedInProject ? LocalizationKeyData.Status.Normal : LocalizationKeyData.Status.NotUsed;
                if (usedInProject) {
                    var oldParamNames = key.paramNames;
                    key.paramNames = keysUsedInProject[usedKeyInd].paramNames; // Also update params count.
                    foreach (var paramName in oldParamNames) {
                        if (key.translations.Any(t => t.localizedText.Contains($"{{{paramName}}}")))
                            key.paramNames.AddIfNotExists(paramName);
                    }
                    keysUsedInProject.RemoveAt(usedKeyInd);
                }
            });
            // Add new keys.
            keysUsedInProject.ForEach(k => {
                settings.keys.Add(new LocalizationKeyData {
                    localizationKey = k.localizationKey,
                    paramNames = k.paramNames,
                    status = LocalizationKeyData.Status.Normal
                });
            });


            settings.SetChanged();
        }

        private static int IndexOf(string substr, int startInd, string str) {
            if (startInd == int.MaxValue) return int.MaxValue;
            var ind = str.IndexOf(substr, startInd);
            if (ind == -1) ind = int.MaxValue;
            return ind;
        }
        private static void IterateKeysFromCode(string csPath, List<(string key, List<string> paramNames)> foundedKeys) {
            if (csPath.Contains("/Editor/")) return;
            var code = File.ReadAllText(csPath);
            var localizationGet = "Localization.Get";
            int i = 0;
            while (i != int.MaxValue) {
                Read(localizationGet);
                Read("(");
                if (IndexOf("\"", i, code) < IndexOf(")", i, code)) {
                    Read("\"");
                    var startString = i;
                    Read("\"");
                    var endString = i;
                    if (startString != int.MaxValue && endString != int.MaxValue) {
                        var localizationKey = code.Substring(startString, endString - startString - 1);
                        if (localizationKey.Contains("{") || localizationKey.Contains("}"))
                            continue;
                        //Debug.Log($"{localizationKey} from {csPath}");
                        var paramsNames = new List<string>();
                        while (IndexOf(",", i, code) < IndexOf(")", i, code)) {
                            Read(",");
                            Read("(");
                            Read("\"");
                            var paramNameStartInd = i;
                            Read("\"");
                            var paramNameEndInd = i;
                            while (IndexOf("(", i, code) < IndexOf(")", i, code)) {
                                Read("(");
                                Read(")");
                            }
                            Read(")");
                            var paramName = code.Substring(paramNameStartInd, paramNameEndInd - paramNameStartInd - 1);
                            paramsNames.Add(paramName);
                        }
                        if (!foundedKeys.Any(k => k.Item1 == localizationKey))
                            foundedKeys.Add((localizationKey, paramsNames));
                    }
                }
                Read(")");
            }
            void Read(string str) {
                if (i == -1 || i >= code.Length) return;
                i = IndexOf(str, i, code);
                if (i == int.MaxValue) return;
                i += str.Length;
                if (i >= code.Length) i = int.MaxValue;
            }
        }

        [Flags] enum KeysToShow { 
            All = Localized | NotLocalized | PartiallyLocalized | NotUsed, 
            Localized = 8, NotLocalized = 4, PartiallyLocalized = 2, NotUsed = 1 
        }
        static Dictionary<KeysToShow, int> counts = new Dictionary<KeysToShow, int>();
        static KeysToShow whatToShow = KeysToShow.NotLocalized | KeysToShow.NotUsed | KeysToShow.PartiallyLocalized;
        static KeysToShow GetStatus(LocalizationKeyData key) {
            if (key.status == LocalizationKeyData.Status.NotUsed)
                return KeysToShow.NotUsed;
            var (isComplete, isPartial) = key.GetState();
            if (isPartial)
                return KeysToShow.PartiallyLocalized;
            if (isComplete)
                return KeysToShow.Localized;
            return KeysToShow.NotLocalized;
        }
        static int focusedKeyInd = -1;
        public static void ShowTranslations(ref bool changed) {

            EditorGUIUtils.BeginHorizontal();
            OnPlayerLanguageGUI(ref changed);
            EditorGUIUtils.Toggle("Show context", ref settings.showContext, ref changed, 150);
            EditorGUIUtils.EndHorizontal();
            ShowLanguages("Show translations", settings.shownTranslations, lang
                => settings.activeLangs.Contains(lang) && lang != ShownLocalization.Keys && lang != ShownLocalization.Longest, ref changed);

            EditorGUIUtils.BeginHorizontal();
            var j = 0;
            foreach (var val in Utils.EnumValues<KeysToShow>()) {
                var currShown = whatToShow.HasFlag(val);
                var count = counts.GetValueWithDefault(val, 0);
                if (val == KeysToShow.All)
                    count = counts.Values.Sum();
                if (EditorGUIUtils.Toggle($"{val} ({count})", ref currShown, ref changed, 150)) {
                    if (currShown)
                        whatToShow |= val;
                    else
                        whatToShow &= ~val;
                }
                j++;
            }
            EditorGUIUtils.EndHorizontal();
            OnSearchGUI(ref changed);

            EditorGUIUtils.HorizontalLine();
            EditorGUIUtils.Indent();

            counts.Clear();
            for (int i = 0; i < settings.keys.Count; i++) {
                // Filter.
                var key = settings.keys[i];
                var status = GetStatus(key);
                counts[status] = counts.GetValueWithDefault(status, 0) + 1;
                if (i != focusedKeyInd) {
                    if (!whatToShow.HasFlag(status))
                        continue;
                    if (!SearchFilter(key))
                        continue;
                }
                // Show localization key.
                GUILayout.Space(10);
                GUILayout.BeginHorizontal();
                EditorGUIUtils.SetBackgroundColor(GetStatusColor(key.status));
                EditorGUIUtils.TextField("localization key", ref key.localizationKey, ref changed, width: 400, labelWidth: 100);
                EditorGUIUtils.RestoreBackgroundColor();
                GUILayout.BeginVertical();
                EditorGUIUtils.List("parameters", "param name", key.paramNames, false,  ref key.paramNamesUnfolded, ref changed, labelWidth: 100, sizeWidth: 100);
                GUILayout.EndVertical();
                if (EditorGUIUtils.XButton()) {
                    settings.keys.RemoveAt(i);
                    i--;
                    GUILayout.EndHorizontal();
                    continue;
                }
                GUILayout.EndHorizontal();
                if (settings.showContext)
                    EditorGUIUtils.TextField("context", ref key.context, ref changed);
                foreach (ShownLocalization lang in Utils.EnumValues<ShownLocalization>()) {
                    if (!settings.shownTranslations.Contains(lang)) continue;
                    GUILayout.BeginHorizontal();
                    var tr = key.GetTranslation(lang);
                    GUILayout.Label(lang.ToString(), GUILayout.Width(30));
                    var localizedText = tr?.localizedText;
                    EditorGUIUtils.SetBackgroundColor(TranslationDataValid(tr, key.localizationKey, key.paramNames) ? Color.white : Color.red);

                    EditorGUIUtils.CheckUIControlFocusedStart();
                    if (EditorGUIUtils.TextAreaSelectable(string.Empty, ref localizedText, ref changed, out var currSeleciton))
                        UpdateLocalizedText(tr, key, localizedText, lang);
                    var focused = EditorGUIUtils.CheckUIControlFocusedEnd();
                    if (focused)
                        focusedKeyInd = i;

                    EditorInput.Update();
                    if (currSeleciton != null && currSeleciton.selectIndex != -1 
                        && EditorInput.GetKey(KeyCode.LeftControl) && EditorInput.GetKeyUp(KeyCode.L)) {
                        seleciton = currSeleciton;
                        Action<Color> onColorChanged = col => {
                            OnColorSelected(col, key, tr, lang);
                        };
                        ReflectionUtils.CallStaticMethodExplicitParamTypes(GetPickerType(), "Show", 
                            (typeof(Action<Color>), onColorChanged), (typeof(Color), Color.red), (typeof(bool), true), (typeof(bool), false));
                    }

                    EditorGUIUtils.RestoreBackgroundColor();
                    GUILayout.EndHorizontal();
                }
            }
        }
        static TextEditor seleciton;
        static void OnColorSelected(Color col, LocalizationKeyData key, TranslationData translation, ShownLocalization lang) {
            if (seleciton == null)
                return;
            var localizedText = translation?.localizedText;
            var selectionStartInd = seleciton.selectIndex;
            var startInd = localizedText.LastIndexOf(StringUtils.ColoredTextStart(), seleciton.cursorIndex + StringUtils.ColoredTextStart().Length);
            if (startInd == -1) {
                localizedText = localizedText.Insert(selectionStartInd + seleciton.SelectedText.Length, StringUtils.ColoredTextEnd());
                localizedText = localizedText.Insert(selectionStartInd, StringUtils.ColoredTextStart(col));
            } else {
                var endInd = localizedText.IndexOf(StringUtils.ColoredTextEnd(), startInd) + StringUtils.ColoredTextEnd().Length;
                var insideTextStartInd = localizedText.IndexOf('>', startInd) + 1;
                var insideTextEndInd = localizedText.IndexOf('<', insideTextStartInd) - 1;
                var textInsideColor = localizedText.Substring(insideTextStartInd, insideTextEndInd - insideTextStartInd + 1);
                localizedText = localizedText.Remove(startInd, endInd - startInd);
                localizedText = localizedText.Insert(startInd, textInsideColor.WithColor(col));
            }
            seleciton.SelectNone();
            UpdateLocalizedText(translation, key, localizedText, lang);
        }
        static void UpdateLocalizedText(TranslationData translation, LocalizationKeyData key, string localizedText, ShownLocalization lang) {
            if (translation == null) {
                translation = new TranslationData { language = lang };
                key.translations.Add(translation);
            }
            translation.localizedText = localizedText;
        }
        static Type GetPickerType() => ReflectionUtils.GetTypeByName("UnityEditor.ColorPicker", true, true);

        static string searchedString = "";
        static bool searchKeys = true;
        static bool searchTexts = true;
        private static void OnSearchGUI(ref bool changed) {
            EditorGUIUtils.BeginHorizontal();
            EditorGUIUtils.TextField("search", ref searchedString, ref changed, 300, labelWidth: labelWidth);
            EditorGUIUtils.Toggle("search keys", ref searchKeys, ref changed, 100);
            EditorGUIUtils.Toggle("search texts", ref searchTexts, ref changed, 100);
            EditorGUIUtils.EndHorizontal();
        }
        private static bool SearchFilter(LocalizationKeyData data) {
            //data.localizationKey.IndexOf("", StringComparison.OrdinalIgnoreCase);
            if (searchedString.IsNothing())
                return true;
            if (searchKeys && (data.localizationKey?.ContainsIgnoreCase(searchedString) ?? false))
                return true;
            if (searchTexts && data.translations.Any(t => t.localizedText.ContainsIgnoreCase(searchedString)))
                return true;
            return false;
        }
        private static void ShowKey(string key) {
            whatToShow = KeysToShow.All;
            searchKeys = true;
            searchTexts = false;
            searchedString = key;
        }

        private static Color GetStatusColor(LocalizationKeyData.Status status) {
            switch (status) {
                case LocalizationKeyData.Status.Normal: return Color.white;
                default:
                case LocalizationKeyData.Status.NotUsed: return Color.red;
            }
        }

        public const int labelWidth = 150;
        private static void ShowLanguages(string title, List<ShownLocalization> languages, Func<ShownLocalization, bool> filter, ref bool changed) {
            var changed1 = changed;
            GUILayout.BeginHorizontal();
            EditorGUIUtils.Label(title, labelWidth);
            Utils.ForEach<ShownLocalization>(lang => {
                if (filter != null && !filter(lang))
                    return;
                var active = languages.Contains(lang);
                if (EditorGUIUtils.Toggle(lang.ToString(), ref active, ref changed1, 50)) {
                    if (active)
                        languages.Add(lang);
                    else
                        languages.Remove(lang);
                }
                GUILayout.Space(20);
            });
            GUILayout.EndHorizontal();
            changed = changed1;
        }

        public static void SetLanguageInOpenedScene(ShownLocalization lang) {
            settings.playerLanguage = lang;
            if (PrefabUtils.prefabOpened)
                PrefabUtils.openedPrefabInstanceRoot.transform.IterateChildren(tr => SetLanguage(tr.gameObject));
            else
                AssetsIterator.IterateOpenedScene(SetLanguage);
        }
        private static void SetLanguage(GameObject go) {
            var localizable = go.GetComponent<LocalizedLabel>();
            if (localizable == null) return;
            localizable.UpdateShownText();
            go.transform.SetChanged();
        }

        StringBuilder sb = new StringBuilder();
        public override string DoReleaseChecks() {
            sb.Clear();
            if (settings.activeLangs.CountExpensive() == 0)
                sb.AppendLine("any language should be active in localizations");
            settings.keys.ForEach(key => LocalizationKeyDataValid(key, sb));
            return sb.ToString();
        }
        bool LocalizationKeyDataValid(LocalizationKeyData localizationKeyData, StringBuilder sb = null) {
            var valid = true;
            if (localizationKeyData.localizationKey.IsNullOrEmpty()) {
                sb?.AppendLine("localization key is empty");
                valid = false;
            }
            if (localizationKeyData.paramNames.Count > 100) {
                sb?.AppendLine($"localization key {localizationKeyData.localizationKey} " +
                    $"params count({localizationKeyData.paramNames.Count}) should be sane");
                valid = false;
            }
            if (localizationKeyData.paramNames.Any(p => p.IsNothing())) {
                sb?.AppendLine($"localization key {localizationKeyData.localizationKey} " +
                    $"param name should not be emptys");
                valid = false;
            }
            settings.activeLangs.ForEach(lang => {
                var translation = localizationKeyData.translations.Find(tr => tr.language == lang);
                if (translation == null) {
                    sb?.AppendLine($"{localizationKeyData.localizationKey} is not localized to {lang}");
                    valid = false;
                } else
                    TranslationDataValid(translation, localizationKeyData.localizationKey, localizationKeyData.paramNames, sb);
            });
            return valid;
        }
        static bool TranslationDataValid(TranslationData translation, string localizationKey, List<string> paramNames, StringBuilder sb = null) {
            if (translation == null) return false;
            if (translation.localizedText.IsNullOrEmpty()) {
                sb?.AppendLine($"{translation.language} translation for {localizationKey} should not be empty");
                return false;
            }
            if (!LocalizedString.GivenAndRequestedParamsMatch(translation.localizedText, paramNames)) {
                sb?.AppendLine($"{translation.language} translation '{translation.localizedText}' " +
                    $"for {localizationKey} should have {paramNames.Count} parameters with names: {paramNames.PrintCollection()}");
                return false;
            }
            return true;
        }

        public static void Show() => AntonToolsWindow.Open().ShowModule(typeof(LocalizationModule));
        public static void ShowTranslation(string key) {
            Show();
            ShowKey(key);
        }

        #region Export/import

        void OnExportGUI(ref bool changed) {
            GUILayout.BeginHorizontal();
            EditorGUIUtils.Popup("export", ref langToExport, ref changed, 100);
            if (GUILayout.Button($"export {langToExport}"))
                Export();
            if (GUILayout.Button($"import")) {
                Import();
                changed = true;
            }
            EditorGUIUtils.Popup("", ref settings.exportFormat, ref changed, 100);
            GUILayout.EndHorizontal();
        }

        private void Export() {
            string text, fileName;
            switch (settings.exportFormat) {
                default:
                case LocalizationSettings.ExportFormat.PlainText: (text, fileName) = ExportPlain(); break;
                case LocalizationSettings.ExportFormat.ChinaCSV: (text, fileName) = ExportChinaCSV(); break;
            }
            StringUtils.CopyToClipboard(text);
            File.WriteAllText(fileName, text, Encoding.UTF8);
            Debug.Log($"{langToExport} localization copied to clipboard and saved to {fileName}");
        }
        private void Import() {
            var text = StringUtils.PasteFromClipboard().ToLf();
            var importFormat = settings.exportFormat;
            if (text.IndexOf("\n") != -1 && text.Substring(0, text.IndexOf("\n")).Count(",") > 2) // first line has >2 comma.
                importFormat = LocalizationSettings.ExportFormat.ChinaCSV;
            else
                importFormat = LocalizationSettings.ExportFormat.PlainText;
            bool success;
            switch (importFormat) {
                default:
                case LocalizationSettings.ExportFormat.PlainText: success = ImportPlain(text); break;
                case LocalizationSettings.ExportFormat.ChinaCSV: success = ImportChinaCSV(text); break;
            }
            if (success && settings.chineseActive)
                UpdateChineseFontAtlases();
            if (success)
                Debug.Log("Localization updated successfully");
            else
                Debug.LogError($"Import {importFormat} failed");
        }

        #region China CSV
        List<string> columnNames = new List<string> {
            "n","Key","Eng","China Sim 简体中文","China Tr 繁体中文"
        };
        string columnsLine => columnNames.PrintCollection(",");
        (string text, string fileName) ExportChinaCSV() {
            // langToExport
            var sb = new StringBuilder();
            sb.AppendLine(columnsLine);

            var lineNumber = 1;
            settings.keys.ForEach(key => {
                var cells = new List<string> {
                    lineNumber.ToString(),
                    GetKeyWithContext(key.localizationKey, key.context),
                    key.Get(ShownLocalization.EN),
                    key.Get(ShownLocalization.CHs),
                    key.Get(ShownLocalization.CHt)
                };
                var line = UnityCSVLine(cells);
                sb.AppendLine(line);
                lineNumber++;
            });

            var file = $"Chinese translation.csv";
            return (sb.ToString(), file);
        }
        bool ImportChinaCSV(string text) {
            var lines = text.Split('\n').ToList();
            if (lines.GetElementSafe(0) != columnsLine)
                return false;
            lines.RemoveAt(0);
            foreach (var line in lines) {
                if (line.IsNullOrEmpty()) continue;
                var cells = SplitCSVLine(line);
                if (cells.Count != columnNames.Count) return false;
                var key = ParseKeyWithContext(cells[1]);
                settings.CreateLocalization(key, ShownLocalization.EN, cells[2]);
                settings.CreateLocalization(key, ShownLocalization.CHs, cells[3]);
                settings.CreateLocalization(key, ShownLocalization.CHt, cells[4]);
            }

            return true;
        }
        // Screening "," characters.
        string UnityCSVLine(List<string> cells) {
            for (int i = 0; i < cells.Count; i++) {
                if (cells[i]?.Contains(",") ?? false)
                    cells[i] = $"\"{cells[i]}\"";
            }
            return cells.PrintCollection(",");
        }
        List<string> SplitCSVLine(string line) {
            var cells = line.Split(',').ToList();
            for (int i = cells.Count - 1; i >= 0; i--) {
                if (!cells[i].StartsWith("\"")) continue;
                while (i + 1 < cells.Count && !cells[i].EndsWith("\"")) {
                    cells[i] += cells[i + 1];
                    cells.RemoveAt(i + 1);
                }
            }
            return cells;
        }
        #endregion

        #region Plain text export/import
        const string stub = "%REPLACE WITH {0} TRANSLATION%";
        private (string text, string fileName) ExportPlain() {
            var langs = new List<ShownLocalization>();
            langs.Add(ShownLocalization.EN);
            langs.AddIfNotExists(langToExport);
            var sb = new StringBuilder();
            sb.AppendLine(langs.PrintCollection(" "));
            sb.AppendLine();
            settings.keys.ForEach(key => {
                sb.AppendLine(GetKeyWithContext(key.localizationKey, key.context));
                // Write texts.
                langs.ForEach(currLang => {
                    var currText = key.Get(currLang);
                    if (currText.IsNullOrEmpty())
                        currText = string.Format(stub, langToExport);
                    sb.AppendLine(currText);
                });
                sb.AppendLine();
            });
            var text = sb.ToString();
            var file = $"{langToExport} translation.txt";
            return (text, file);
        }
        // Write localization key with context.
        private string GetKeyWithContext(string key, string context)
            => context.IsNullOrEmpty() ? key : $"{key} ({context})";
        private string ParseKeyWithContext(string keyWithContext) {
            void RemoveAfter(char ch) {
                if (keyWithContext.IndexOf(ch) != -1)
                    keyWithContext = keyWithContext.Remove(keyWithContext.IndexOf(ch));
            }
            RemoveAfter(' ');
            RemoveAfter('(');
            return keyWithContext;
        }

        private bool ImportPlain(string text) {
            var lines = text.Split('\n').ToList();
            var langsStr = lines[0].Split(' '); lines.RemoveAt(0);
            lines.RemoveAt(0);
            var langs = new List<ShownLocalization>();
            langsStr.ForEach(langStr => {
                if (!Enum.TryParse<ShownLocalization>(langStr, out var lang))
                    Debug.LogError($"lang {langStr} not found");
                else
                    langs.Add(lang);
            });
            if (langs.Count == 0)
                return false;

            var requiredLinesPerKey = 2 + langs.Count;
            while (lines.Count >= requiredLinesPerKey) {
                // Read localization key.
                var key = ParseKeyWithContext(lines[0]); lines.RemoveAt(0);

                langs.ForEach(lang => {
                    var localizedText = lines[0]; lines.RemoveAt(0);
                    if (localizedText.Contains("%REPLACE WITH "))
                        localizedText = string.Empty;
                    settings.CreateLocalization(key, lang, localizedText);
                });
                lines.RemoveAt(0);
            }
            return true;
        }
        #endregion

        #endregion
#endif
    }
}
#endif
