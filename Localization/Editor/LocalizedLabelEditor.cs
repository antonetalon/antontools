#if UNITY_EDITOR
#if UNITY_2020_1_OR_NEWER
#if !NO_LOCALIZATION
using System;
using TMPro;
using UnityEngine;

namespace AntonTools {
    //[CustomEditor(typeof(TextMeshProUGUI), true)]
    public class LocalizedLabelEditor : LabelEditor {//TMP_EditorPanelUI {
        #region Ensure every label is localized.
        public override void OnInspectorGUI(TextMeshProUGUI label, ref bool changed) {
            OnLocalizationGUI(label, ref changed);
            //base.OnInspectorGUI();
        }
        #endregion

        //LocalizedLabel tgt => (LocalizedLabel)target;
        static LocalizationSettings settings => LocalizationSettings.instance;
        private enum Status { unknown, //localizedFromCode, 
            isGlobal, noKey, saved, notSaved, partial }
        Status status => GetStatus(tgt.text.key, tgt.text.value, tgt.shownText, tgt.text.isGlobal);
        static Status GetStatus(string key, string value, string shownText, bool isGlobal) {
            //if (text.isGlobal)
            //    return Status.localizedFromCode;
            if (isGlobal)
                return Status.isGlobal;
            if (!Localization.Exists(key))
                return Status.noKey;
            if (value == shownText) {
                if (!settings.Get(key).GetState().complete)
                    return Status.partial;
                return Status.saved;
            }
            return Status.notSaved;
        }

        string GetStatusText(Status status) => status switch {
            //Status.localizedFromCode => "localized from code".WithColor(EditorGUIUtils.subtleTextColor),
            Status.isGlobal => "global text, no localization".WithColor(EditorGUIUtils.subtleTextColor),
            Status.noKey => "localization key not added".WithColor(Color.red),
            Status.saved => "localization ok".WithColor(EditorGUIUtils.subtleTextColor),
            Status.partial => PartialStatusText(),
            Status.notSaved => "localization changed".WithColor(Color.yellow),
            _ => "hii"
        };

        private string PartialStatusText() {
            var data = settings.Get(tgt.key);
            return settings.activeLangs.ConvertAll(lang
                => {
                    var translated = !data.Get(lang).IsNothing();
                    return lang.ToString().WithColor(translated ? EditorGUIUtils.subtleTextColor : Color.red);
                }).PrintCollection(" ");
        }

        public static string ToKey(string str) {
            if (str.IsNullOrEmpty())
                return string.Empty;
            str = str.FromUpper();
            for (int i = 0; i < str.Length; i++) {
                if (!char.IsUpper(str[i]) || char.IsUpper(str.GetCharSafe(i - 1)))
                    continue;
                if (str.GetCharSafe(i - 1) != '_' && i > 0)
                    str = str.Insert(i, "_");
            }
            str = str.ToUpper();
            return str;
        }
        public static string KeyToLocalization(string key) {
            var words = key.Split('_', ' ', StringSplitOptions.RemoveEmptyEntries);
            var text = words.ConvertAll(w => w.ToLower().FromUpper()).Join(" ");
            return text;
        }
        public static bool OnEditKeyGUI(string name, ref string key, ref bool changed) {
            EditorGUIUtils.BeginHorizontal();
            var keyChanged = EditorGUIUtils.TextField(name, ref key, ref changed, width: 350, labelWidth: 100);
            var keyCorrected = ToKey(key);
            if (keyCorrected != key) {
                key = keyCorrected;
                keyChanged = true;
                changed = true;
            }
            var settings = LocalizationSettings.instance;
            var config = settings.Get(key);
            var localizationChanged = false;
            var translationData = config?.GetOrAddTranslation(settings.playerLanguage, ref changed);
            var localizedText = translationData?.localizedText;
            var status = GetStatus(key, localizedText, localizedText, false);
            OnAddKeyGUI(status, key, localizedText, ref localizationChanged);
            if (status == Status.saved || status == Status.partial) {
                if (EditorGUIUtils.TextArea($"{settings.playerLanguage}", ref localizedText, ref localizationChanged, labelWidth: 30))
                    translationData.localizedText = localizedText;
            }
            EditorGUIUtils.EndHorizontal();
            if (localizationChanged)
                settings.SetChanged();
            return keyChanged;
        }

        static void OnAddKeyGUI(Status status, string key, string localizedText, ref bool changed) {
            if (status == Status.noKey && !key.IsNothing()) {
                if (EditorGUIUtils.Button("Add key", ref changed, buttonWidth))
                    settings.CreateLocalization(key, localizedText.IsSomething() ? localizedText : KeyToLocalization(key));
            }
        }

        const int buttonWidth = 120;
        const int labelWidth = 90;
        const int width = 250;
        static bool unfolded;
        LocalizedLabel tgt;
        private void OnLocalizationGUI(TextMeshProUGUI label, ref bool changed) {
            tgt = label as LocalizedLabel;
            var status = this.status;
            //var changed = false;

            // Title.
            EditorGUIUtils.BeginHorizontal();
            EditorGUIUtils.ShowOpenClose(ref unfolded);
            EditorGUIUtils.RichMultilineLabel(GetStatusText(status), 170);
            if (status == Status.isGlobal) { //.localizedFromCode) {
                if (EditorGUIUtils.SubtleButton("Translate", buttonWidth)) {
                    tgt.text.isGlobal = false;
                    changed = true;
                }
            }
            OnAddKeyGUI(status, tgt.key, tgt.shownText, ref changed);
            if (status == Status.notSaved) {
                if (EditorGUIUtils.Button("Save", ref changed, buttonWidth / 2))
                    settings.CreateLocalization(tgt.key, tgt.shownText);
                if (EditorGUIUtils.Button("Restore", ref changed, buttonWidth / 2))
                    tgt.UpdateShownText();
            }
            if (status == Status.partial) {
                if (EditorGUIUtils.Button("translate", ref changed, buttonWidth))
                    LocalizationModule.ShowTranslation(tgt.key);
            }
            EditorGUIUtils.EndHorizontal();

            // Unfolded.
            var keyFocused = false;
            if (unfolded) {
                ///if (tgt.key.IsNothing() || tgt.text.isGlobal)
                if (EditorGUIUtils.Toggle("is global", ref tgt.text.isGlobal, ref changed)) {
                    if (tgt.text.isGlobal)
                        tgt.text = "".ToGlobal();
                }
                if (!tgt.text.isGlobal) {
                    EditorGUIUtils.CheckUIControlFocusedStart();
                    var key = tgt.key;
                    if (EditorGUIUtils.TextField("key", ref key, ref changed, width, labelWidth: labelWidth))
                        tgt.text = Localization.Get(key.ToUpper());
                    keyFocused = EditorGUIUtils.CheckUIControlFocusedEnd();
                    if (keyFocused)
                        ShowKeyOptions(ref changed);
                }
                if (status == Status.notSaved) {
                    EditorGUIUtils.TextFieldReadOnly("old value:", tgt.text);
                    EditorGUIUtils.TextFieldReadOnly("new value:", tgt.shownText);
                }
                LocalizationModule.OnPlayerLanguageGUI(ref changed);
            }

            // Auto-setting key from text.
            if (!tgt.shownText.IsNothing() && status == Status.noKey && !keyFocused && tgt.key.IsNothing() && !tgt.text.isGlobal) {
                EditorAsync.ExecuteAfter(0.1f, () => {
                    tgt.text = Localization.Get(GetKeyFromText(tgt.shownText));
                });
            }
            // Auto-removing key from global text.
            if (status == Status.isGlobal && tgt.text.isGlobal && tgt.key.IsSomething())
                tgt.text = Localization.GetGlobal(tgt.shownText);

            // Auto-add language to localization.
            var currSettings = settings.Get(tgt.key);
            if (currSettings != null) {
                var translation = currSettings.GetTranslation(settings.playerLanguage);
                if (translation == null) {
                    translation = new TranslationData { language = settings.playerLanguage, localizedText = tgt.shownText };
                    currSettings.translations.Add(translation);
                    changed = true;
                }
            }

            //if (changed)
            //    tgt.SetChanged();
        }

        private void ShowKeyOptions(ref bool changed) {
            var key = tgt.key;
            int maxCount = 20;
            int currCount = 0;
            foreach (var data in settings.keys) {
                if (!data.localizationKey.Contains(key))
                    continue;
                EditorGUIUtils.BeginHorizontal();
                GUILayout.Space(labelWidth);
                if (EditorGUIUtils.Button(data.localizationKey, ref changed, width - labelWidth))
                    tgt.text = Localization.Get(data.localizationKey);
                EditorGUIUtils.EndHorizontal();
                currCount++;
                if (currCount >= maxCount)
                    break;
            }
        }

        private string GetKeyFromText(string text) {
            text = text.ToUpper().Replace(" ", "_").Replace("__", "_").Replace(".", "");
            const int maxChars = 20;
            var subIndToClampTo = -1;
            if (text.Length > maxChars) {
                var maxClampedInd = Mathf.Min(maxChars, text.Length);
                for (int i = maxClampedInd - 1; i > 0; i--) {
                    if (text[i] != '_') continue;
                    subIndToClampTo = i;
                    break;
                }
                var indToClampTo = subIndToClampTo != -1 ? subIndToClampTo : maxClampedInd;
                text = text.Substring(0, indToClampTo);
            }
            return text;
        }
    }
}
#endif
#endif
#endif